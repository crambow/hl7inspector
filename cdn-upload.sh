#!/bin/bash
# Deploy files using SFTP http://manpages.ubuntu.com/manpages/trusty/en/man1/sftp.1.html
#
# Required globals:
#   CDN_HOSTNAME
#   CDN_USER
#   CDN_REMOTE_PATH
#   CDN_PASSWORD
#
# Optional globals:
#   CDN_LOCAL_PATH
#   CDN_DEBUG
#   CDN_EXTRA_ARGS
#   CDN_DELETE_FLAG

#source "$(dirname "$0")/common.sh"

LFTP_DEBUG_ARGS=
## Enable debug mode.
enable_debug() {
  if [[ "${CDN_DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    LFTP_DEBUG_ARGS="-vvv"
  fi
}

validate() {
  # mandatory parameters
  : CDN_USER=${CDN_USER:?'CDN_USER variable missing.'}
  : CDN_PASSWORD=${CDN_PASSWORD:?'CDN_PASSWORD variable missing'}
  : CDN_HOSTNAME=${CDN_HOSTNAME:?'CDN_HOSTNAME variable missing'}
  : CDN_REMOTE_PATH=${CDN_REMOTE_PATH:?'REMOTE_PATH variable missing.'}
  : CDN_LOCAL_PATH=${CDN_LOCAL_PATH:="${BITBUCKET_CLONE_DIR}"}
  : CDN_DELETE_FLAG=${CDN_DELETE_FLAG:="false"}

  ARGS_STRING=""

  if [ -n "${CDN_DELETE_FLAG}" ]; then
    if [ "${CDN_DELETE_FLAG}" == "true" ]; then
      ARGS_STRING="${ARGS_STRING} --delete-first"
    elif [ "${CDN_DELETE_FLAG}" != "false" ]; then
      echo "Value of the DELETE_FLAG has to be either true or false."
    fi
  fi
}

run_pipe() {
    echo "Starting FTPS deployment to ${CDN_HOSTNAME}:${CDN_REMOTE_PATH}..."
    set +e
    lftp -u $CDN_USER,$CDN_PASSWORD -e "set ftp:ssl-force yes; mirror ${ARGS_STRING} ${LFTP_DEBUG_ARGS} ${CDN_EXTRA_ARGS} -R ${CDN_LOCAL_PATH} ${CDN_REMOTE_PATH};quit" $CDN_HOSTNAME
    set -e

    STATUS=$?

    if [[ "${STATUS}" == "0" ]]; then
      echo "Deployment finished."
    else
      echo "Deployment failed."
    fi

    exit $STATUS
}

validate
enable_debug
run_pipe
