#!/bin/bash

#
# This script makes some basic checks to ensure that we're able to run the mvn release:perform command safely.
#
# For those who have worked with maven in the past, you'll remember that a failed release:perform command can cause issues
# so we attempt to avoid them with this script.
#

echo "Checking for presence of GIT_USER_NAME environment variable...."
if [[ $GIT_USER_NAME ]];
then
  echo "Success - Found Git User Name in environment variables...";
else
  >&2 echo "Failure - GIT_USER_NAME environment variable not set.  Please set this environment variable and try again."
  exit 1
fi;

echo "Checking for presence of GIT_USER_EMAIL environment variable...."
if [[ $GIT_USER_EMAIL ]];
then
  echo "Success - Found Git User Email in environment variables...";
else
  >&2 echo "Failure - GIT_USER_EMAIL environment variable not set.  Please set this environment variable and try again."
  exit 1
fi;

if grep -q "IdentityFile" ~/.ssh/config; then
 echo "Success - IdentityFile entry found in SSH Config.  This will allow the maven release plugin to work properly."
 else
 echo "Warning - IdentityFile entry NOT found in SSH Config.  Go to your BitBucket Pipelines settings, enable an SSH Key, and then add the public key to your account's list of valid SSH Keys.  You need this in order for the Maven Release plugin to work properly."
fi
