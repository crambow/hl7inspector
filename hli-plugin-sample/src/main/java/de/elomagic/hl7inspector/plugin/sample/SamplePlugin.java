/*
 * HL7 Inspector - Plugin Sample
 *
 * Copyright © 2021-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Plugin Sample.
 *
 * HL7 Inspector - Plugin Sample is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Plugin Sample is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Plugin Sample. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.plugin.sample;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.pf4j.PluginWrapper;
import org.pf4j.spring.SpringPlugin;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SamplePlugin extends SpringPlugin {

    private static final Logger LOGGER = LogManager.getLogger(SamplePlugin.class);

    /**
     * Constructor to be used by plugin manager for plugin instantiation.
     * Your plugins have to provide constructor with this exact signature to
     * be successfully loaded by manager.
     *
     * @param wrapper
     */
    public SamplePlugin(@NotNull PluginWrapper wrapper) {
        super(wrapper);
    }

    @Override
    public void start() {
        LOGGER.debug("Staring plugin {}. Runtime mode={} ", SamplePlugin.class.getSimpleName(), getWrapper().getRuntimeMode());
    }

    @Override
    public void stop() {
        LOGGER.debug("Stopping plugin {}", SamplePlugin.class.getName());
        super.stop();
    }

    @Override
    protected ApplicationContext createApplicationContext() {
        LOGGER.debug("Creating plugin application context");
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.setClassLoader(getWrapper().getPluginClassLoader());
        applicationContext.register(SpringConfiguration.class);
        applicationContext.refresh();

        return applicationContext;
    }

}
