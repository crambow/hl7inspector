/*
 * HL7 Inspector - Plugin Sample
 *
 * Copyright © 2021-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Plugin Sample.
 *
 * HL7 Inspector - Plugin Sample is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Plugin Sample is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Plugin Sample. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.plugin.sample;

import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.shared.desktop.SettingsUIDescriptor;

import org.pf4j.Extension;

import javax.swing.Icon;

@Extension
public class SamplePluginSettingsUI extends SettingsUIDescriptor.AbstractSettingsPanel {

    @Override
    public void commit() throws FormBinderException {

    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("sample-32.png");
    }

    @Override
    public String getTitle() {
        return "Sample Plugin Setting";
    }

    @Override
    public String getDescription() {
        return "Description of the sample plugin setting";
    }

    @Override
    protected void load() {

    }

}
