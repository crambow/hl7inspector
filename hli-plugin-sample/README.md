# HL7 Inspector - Plugin Sample

**THIS DOCUMENTS IS STILL IN PROGRESS AND ONLY A DRAFT !!!**

**THE PLUGIN SUPPORT IS STILL EXPERIMENTAL AND THE API CAN CHANGE OR BE REMOVED AT ANY TIME !!!**

## Description

Plugin concept of the HL7 inspector based on the [PF4J framework](hhttps://github.com/pf4j/pf4j).

## HL7 Inspector supported API's

### Menu item API

...

### And so on...

## Constraints 

* Inhibit package name...
* Full JAR archive with unpacked JAR dependencies

## FAQ

### How to test plugin in IDE?

Set following JVM environment variables:
* Property "spring.profiles.active=dev" - To enable developer mode 
* Property "pf4j.pluginsDir" - Set folder where your plugin JAR or ZIP located

### Anything else?