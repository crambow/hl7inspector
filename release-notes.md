# Release Notes #

## 2.8.0 (Upcoming)

* 💣 BUGFIX Log4j injection fixed (CVE-2021-44228)
* 💡 FEATURE Delimiters chars in editor now colorized
* 💡 FEATURE Experimental plugin manager added
* 🔀 CHANGE License changed from GPLv2 to GPLv3
* 🔀 CHANGE Amazon Corretto JDK 11.0.13.8.1
* 🔨 TASK Project separated into multiple Maven modules. Finished
* 🔨 TASK OWASP plugin added

## 2.7.4 (2021-12-14)

* 💣 BUGFIX Log4j hardened (CVE-2021-44228)

## 2.7.3 (2021-12-13)

* 💣 BUGFIX Log4j injection fixed (CVE-2021-44228)

## 2.7.2 (2021-04-21)

* 💣 BUGFIX Choco installer fixed (Invalid CDN link)

## 2.7.1 (2021-03-06)

* 💣 BUGFIX Message Generator: Send option throws exception on save under certain conditions. Fixed

## 2.7.0 (2021-01-07)

* 💡 FEATURE Sending of bulk messages support added
* 💡 FEATURE TLS client authentication added
* 💡 FEATURE Import messages from clipboard support added
* 💣 BUGFIX When "Keep/Reuse socket" feature is disabled then socket will still open. Fixed
* 💣 BUGFIX Some bugs in the message generator fixed
* 💣 BUGFIX The target host in the send options is lost. Fixed
* 💣 BUGFIX Other minor bugfixes
* 🔨 TASK Split into Maven modules

## 2.6.0 (2021-01-03)

* 💣 BUGFIX Missing error messages translation. Fixed
* 💡 FEATURE Multi open HL7 message files support added
* 💡 FEATURE TLS support added
* 💡 FEATURE Bulk message generator added (By using Apache Freemarker)
* 💡 FEATURE Undo/Redo feature to editor added
* 💡 FEATURE UX improvements
* 🔀 CHANGE Amazon Corretto JDK 11.0.9.11.1
* 🔀 CHANGE Tree and editor merged view into one tab. User can switch between tree and editor view.
* 🔀 CHANGE Value editor replaced by standard editor
* 🔀 CHANGE Second external message application removed
* 🔀 CHANGE Message validator improved
* 🔀 CHANGE Search support improved
* 🔀 CHANGE Sending/Receiving feature refactored

## 2.5.5 (2020-11-11)

* 💣 BUGFIX Application sometimes not visible anymore after changing the display environment. Fixed

## 2.5.4 (2020-10-03)

* 💣 BUGFIX Import dialog close freeze on Linux fixed
* 💣 BUGFIX Unable to start application on Linux fixed
* 💣 BUGFIX File modes in Linux distribution package fixed

## 2.5.3 (2020-08-19)

* 💣 BUGFIX Unable to setup sender: Fixed

## 2.5.2 (2020-08-17)

* 💣 BUGFIX Error occur when no profile is active. Fixed
* 💣 BUGFIX Starting App with a file parameter doesn't work. Fixed

## 2.5.0 (2020-08-16)

* 💣 BUGFIX As well, many bugfixes 
* 💡 FEATURE Simple message compare feature added
* 💡 FEATURE Content display feature for Base64, PDF, images and CDA added
* 💡 FEATURE Dark mode skin added
* 🔀 CHANGE UI improvements
* 🔨 TASK Migrated to Spring Boot

## 2.4.2 (2020-07-24)

* 💣 BUGFIX Detail panel on the right side cannot be increased for large segments. Fixed  

## 2.4.1 (2020-07-23)

* 💣 BUGFIX Too many to list them all
* 💡 FEATURE Embedded message editor added
* 🆙 IMPROVEMENT Log files are stored in the specific OS directories
* 🔀 CHANGE List of messages separated from tree view. Tree displays only one message now.  
* 🔀 CHANGE Samples will be stored into windows user documents folder
* 🔨 TASK Distribution without any Java runtime
* 🔨 TASK Upgraded to Open JDK Corretto 11 (32bit > 64bit)
* 🔨 TASK JGoodies-Looks replaced by WebLaF due to license changes

## 2.3.0 (2020-06-06)

* 💡 FEATURE Theming support added
* 💡 FEATURE German translation added
* 🆙 IMPROVEMENT Profile format changed to JSON
* 🔀 CHANGE Downward compatibility to version 2.0 removed
* 🔨 TASK Project moved to Bitbucket.
* 🔨 TASK Java Runtime (Open JDK Corretto 8) embedded. Now external Java Runtime required anymore.
* 🔨 TASK MacOS binary bundle added (Including Java)
* 🔨 TASK Update checker lookup refactored and changed to Bitbucket URL

## 2.2.0.554 (2010-12-29)

* 💡 FEATURE DnD of text and clipboard support added
* 🆙 IMPROVEMENT (Internal) Project files changed to Maven

## 2.1.2.510 (2010-07-25)

* 💣 BUGFIX Unable to use Nimbus L&F. Fixed
* 💣 BUGFIX Unable to drop files into HL7 Inspector on MacOS X. Fixed
* 💡 FEATURE Delimiters support improved.

## 2.1.0.500 (2006-08-02 17:01)

* 💣 BUGFIX Invalid XML profile. Fixed

## 2.1.0.497 RC1 (2006-05-26 10:05)

* 💣 BUGFIX Message receiver: Required fields in acknowledge message are empty. Fixed.
* 💣 BUGFIX Null pointer exception occur when appending segment in compact view mode. Fixed.
* 💣 BUGFIX Profile format use invalid naming rules. Fixed.
* 💣 BUGFIX Some little bug fixes to prevent against application crash.
* 💡 FEATURE Message receiver still listening when end of message stream reached.
* 🆙 IMPROVEMENT Tree node search improved.

## 2.1.0.491 Beta4 (2006-05-14 13:13)

* 💣 BUGFIX Character encoding ignored when receiving message on IP socket. Fixed
* 💣 BUGFIX Character encoding ignored when writing message to file. Fixed
* 💣 BUGFIX Encoding of backslash character in tree nodes failed. Fixed
* 💣 BUGFIX Comboboxes of frames are disabled when open import dialog with parameter "FRAME" format. Fixed
* 💣 BUGFIX NullPointerException occur when using class method JComboBox.getSelectedItem with JDK6. Fixed
* 💣 BUGFIX Some little UI bugs fixed.
* 💣 BUGFIX Some little internal changes.
* 🆙 IMPROVEMENT Libraries dependencies of JGoodies and L2FProd upgraded to latest version
* 💡 FEATURE About dialog: Tabbed panel with product information properties added.

## 2.1.0.488 Beta3 (2006-03-30 01:09)