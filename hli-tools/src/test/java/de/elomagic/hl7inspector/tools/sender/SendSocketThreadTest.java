package de.elomagic.hl7inspector.tools.sender;

import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.Segment;
import de.elomagic.hl7inspector.shared.io.MLLPMessageInputStream;
import de.elomagic.hl7inspector.shared.configuration.SendOptions;
import de.elomagic.hl7inspector.tools.TestTool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

class SendSocketThreadTest {

    Logger LOGGER = LogManager.getLogger(SendSocketThreadTest.class);

    @Test
    void testSendMessages() throws Exception {
        LOGGER.debug("Loading messages...");
        List<Message> messages = List.of(
                TestTool.readMessage("/A01-UTF8-CR.hl7"),
                TestTool.readMessage("/A02-UTF8-CR.hl7"),
                TestTool.readMessage("/A08-UTF8-CR.hl7")
        );

        SendOptions options = new SendOptions();
        options.setHost("localhost");
        options.setEncoding(StandardCharsets.UTF_8);

        try (ServerSocket server = createRandomServerSocket()) {
            options.setPort(server.getLocalPort());

            LOGGER.debug("Creating future...");
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<List<Message>> future = executor.submit(() -> {
                try {
                    LOGGER.debug("Waiting for connection...");
                    Socket socket = server.accept();
                    LOGGER.debug("Connection established.");

                    MLLPMessageInputStream in = new MLLPMessageInputStream(socket.getInputStream(), options.getFrame(), options.getEncoding());
                    OutputStream out = socket.getOutputStream();

                    List<Message> receivedMessages = new ArrayList<>();
                    Message message;
                    while ((message = in.readMessage()) != null) {
                        receivedMessages.add(message);
                        LOGGER.debug("Sending ack message...");
                        sendAck(message, out, options);
                        LOGGER.debug("Ack message send...");
                    }

                    LOGGER.debug("Finishing future...");

                    return receivedMessages;
                } catch (Exception ex) {
                    ex.printStackTrace(System.err);
                    return null;
                }
            });

            LOGGER.debug("Send messages...");
            SendSocketThread thread = new SendSocketThread();
            thread.setOptions(options);
            thread.setMessageSource(messages.listIterator());
            thread.start();

            LOGGER.debug("Waiting for messages...");
            List<Message> receivedMessages = future.get(10, TimeUnit.SECONDS);

            thread.requestTermination();

            LOGGER.debug("Evaluating results...");
            Assertions.assertEquals("A01", receivedMessages.get(0).getFirstSegment("EVN").get(1).asText());
            Assertions.assertEquals("A02", receivedMessages.get(1).getFirstSegment("EVN").get(1).asText());
            Assertions.assertEquals("A08", receivedMessages.get(2).getFirstSegment("EVN").get(1).asText());
        }
    }

    @Test
    void testSendAndNoAck() throws Exception {
        LOGGER.debug("Loading messages...");
        List<Message> messages = List.of(
                TestTool.readMessage("/A01-UTF8-CR.hl7")
        );

        SendOptions options = new SendOptions();
        options.setHost("localhost");
        options.setEncoding(StandardCharsets.UTF_8);

        try (ServerSocket server = createRandomServerSocket()) {
            options.setPort(server.getLocalPort());

            LOGGER.debug("Creating future...");
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<Boolean> future = executor.submit(() -> {
                try {
                    LOGGER.debug("Waiting for connection...");
                    Socket socket = server.accept();
                    LOGGER.debug("Connection established. Reading message and then do nothing");

                    InputStream in = socket.getInputStream();

                    int bytesRead = in.read(new byte[10]);
                    Assertions.assertEquals(10, bytesRead);

                    //MLLPMessageInputStream in = new MLLPMessageInputStream(socket.getInputStream(), options.getFrame(), options.getEncoding());
                    //Message message = in.readMessage();
                    //Assertions.assertEquals("ADT^A01", message.getFirstSegment("MSH").get(9).asText());

                    socket.getOutputStream();

                    LOGGER.debug("Finishing future...");

                    return true;
                } catch (Exception ex) {
                    ex.printStackTrace(System.err);
                    return false;
                }
            });

            LOGGER.debug("Send messages...");
            SendSocketThread thread = new SendSocketThread();
            thread.setOptions(options);
            thread.setMessageSource(List.of(TestTool.readMessage("/A01-UTF8-CR.hl7")).listIterator());
            thread.start();

            boolean serverAccepted = future.get(10, TimeUnit.SECONDS);
            Assertions.assertTrue(serverAccepted);
            LOGGER.debug("Server accepted connection.");
            thread.requestTermination();

            Assertions.assertTrue(thread.isAlive());
        }
    }

    private void sendAck(Message message, OutputStream out, SendOptions options) throws IOException {
        try {
            Segment msh = message.get(0);

            out.write(options.getFrame().getStartByte());

            List<String> seg = new ArrayList<>();
            seg.add("MSH");
            seg.add("^~\\&");
            seg.add(getField(msh, 5)); // Field 3
            seg.add(getField(msh, 6)); // Field 4
            seg.add(getField(msh, 3)); // Field 5
            seg.add(getField(msh, 4)); // Field 6
            seg.add(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())); // Field 7
            seg.add(""); // Field 8
            seg.add("ACK");//^^" + msh.get(9).toString()); // Field 9
            seg.add(getField(msh, 10)); // Field 10
            seg.add(getField(msh, 11)); // Field 11
            seg.add(getField(msh, 12)); // Field 12
            seg.add(""); // Field 13
            seg.add(""); // Field 14
            seg.add(""); // Field 15
            seg.add(""); // Field 16
            seg.add(""); // Field 17
            seg.add(getField(msh, 18)); // Field 18
            out.write(String.join("|", seg).getBytes(options.getEncoding()));
            out.write(Delimiters.DEFAULT_SEGMENT_SEPARATOR);

            seg = new ArrayList<>();
            seg.add("MSA");
            seg.add("AA");
            seg.add(getField(msh, 10));

            out.write(String.join("|", seg).getBytes(options.getEncoding()));
            out.write(Delimiters.DEFAULT_SEGMENT_SEPARATOR);

            out.write(options.getFrame().getStopBytes());
        } finally {
            out.flush();
        }
    }

    private String getField(Segment seg, int index) {
        String result = "";

        if(seg.size() >= index) {
            result = seg.get(index).asText();
        }
        return result;
    }

    private ServerSocket createRandomServerSocket() throws IOException {
        LOGGER.debug("Bind port random for listening...");
        ServerSocket server = new ServerSocket(0, 50, InetAddress.getLoopbackAddress());
        LOGGER.debug("Listening on port " + server.getLocalPort());

        return server;
    }

}
