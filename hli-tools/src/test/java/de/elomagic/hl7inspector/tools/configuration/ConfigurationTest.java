/*
 * Copyright 2020-present Carsten Rambow
 *
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/gpl.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector.tools.configuration;

import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.shared.configuration.SendOptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ConfigurationTest {

    @Test
    void testSetSendOptions() throws Exception {
        SendOptions bean = new SendOptions();

        ToolsConfiguration.getInstance().setSendOptions(bean);

        bean = ToolsConfiguration.getInstance().getSendOptions();

        Assertions.assertEquals(Frame.DEFAULT_START, bean.getFrame().getStartByte());
        Assertions.assertEquals(Frame.DEFAULT_STOP1, bean.getFrame().getStopBytes()[0]);
        Assertions.assertEquals(Frame.DEFAULT_STOP2, bean.getFrame().getStopBytes()[1]);

        bean.setFrame(new Frame(Frame.DEFAULT_START, Frame.DEFAULT_STOP2, null));

        ToolsConfiguration.getInstance().setSendOptions(bean);

        bean = ToolsConfiguration.getInstance().getSendOptions();
        Assertions.assertEquals(1, bean.getFrame().getStopBytesLength());
        Assertions.assertEquals(Frame.DEFAULT_START, bean.getFrame().getStartByte());
        Assertions.assertEquals(Frame.DEFAULT_STOP2, bean.getFrame().getStopBytes()[0]);
    }

}
