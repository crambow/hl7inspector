package de.elomagic.hl7inspector.tools.generator;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.tools.TestTool;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomOptions;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomValueGenerator;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsOptions;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsValueGenerator;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Set;

class MessageGeneratorTest {

    private final MessageGenerator gen = new MessageGenerator();

    @Test
    void testNextMessage() throws Exception {

        String template = TestTool.readResource("/A01.hl7gen");

        gen.setCustomSequencer(Set.of(
                new StringItemsValueGenerator(new StringItemsOptions("fn", Arrays.asList("John", "Carl", "Mike", "Steven"), false)),
                new RandomValueGenerator(new RandomOptions("random", 0, 1000)))
        );
        gen.setTemplate(template);

        for (int i = 0; i < 10; i++) {
            Message message = gen.nextMessage();
            System.out.println(message.asText().replaceAll("\r", "\n"));
        }
    }

}