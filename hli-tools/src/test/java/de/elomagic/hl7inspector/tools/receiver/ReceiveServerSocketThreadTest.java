package de.elomagic.hl7inspector.tools.receiver;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.io.MLLPMessageInputStream;
import de.elomagic.hl7inspector.shared.io.MLLPMessageOutputStream;
import de.elomagic.hl7inspector.shared.services.MessageManagerService;
import de.elomagic.hl7inspector.tools.TestTool;
import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootTest(classes = {
        ReceiveServerSocketThread.class,
        ReceiveSocketThread.class,
        MessageManagerServiceMock.class,
        MessageManagerService.class,
        EventMessengerProcessor.class})
class ReceiveServerSocketThreadTest {

    @Autowired
    BeanFactory beanFactory;

    @Test
    @Timeout(30)
    void testReceive() throws Exception {
        ReceiveServerSocketThread thread = createReceiver();

        thread.start();

        TimeUnit.SECONDS.sleep(2);

        List<Message> receivedMessages = new ArrayList<>();
        try {
            List<Message> messages = List.of(
                    TestTool.readMessage("/A01-UTF8-CR.hl7"),
                    TestTool.readMessage("/A02-UTF8-CR.hl7"),
                    TestTool.readMessage("/A08-UTF8-CR.hl7")
            );

            Socket socket = new Socket("localhost", thread.getLocalPort());

            MLLPMessageOutputStream out = new MLLPMessageOutputStream(socket.getOutputStream(), new Frame(), StandardCharsets.UTF_8);
            MLLPMessageInputStream in = new MLLPMessageInputStream(socket.getInputStream(), new Frame(), StandardCharsets.UTF_8);

            for (Message message : messages) {
                out.writeMessage(message);
                receivedMessages.add(in.readMessage());
            }
        } finally {
            thread.requestTermination();
        }

        Assertions.assertEquals(3, receivedMessages.size());
        Assertions.assertEquals("ACK", receivedMessages.get(0).getFirstSegment("MSH").get(9).asText());
        Assertions.assertEquals("AA", receivedMessages.get(0).getFirstSegment("MSA").get(1).asText());
    }

    @Test
    void testMultipleReceive() throws Exception {
        ReceiveServerSocketThread thread = createReceiver();

        thread.start();

        TimeUnit.SECONDS.sleep(2);

        List<Message> receivedMessages = new ArrayList<>();
        try {
            List<Message> messages = List.of(
                    TestTool.readMessage("/A01-UTF8-CR.hl7"),
                    TestTool.readMessage("/A02-UTF8-CR.hl7"),
                    TestTool.readMessage("/A08-UTF8-CR.hl7")
            );

            for (int i=0; i<10; i++) {
                Socket socket1 = new Socket("localhost", thread.getLocalPort());
                socket1.getOutputStream().write(thread.getFrame().getStartByte());
                socket1.getOutputStream().write(new byte[] { 1, 3, 4 });
                socket1.getOutputStream().flush();

                if (i % 2 == 0) {
                    socket1.close();
                }
            }

            Socket socket2 = new Socket("localhost", thread.getLocalPort());

            MLLPMessageOutputStream out = new MLLPMessageOutputStream(socket2.getOutputStream(), new Frame(), StandardCharsets.UTF_8);
            MLLPMessageInputStream in = new MLLPMessageInputStream(socket2.getInputStream(), new Frame(), StandardCharsets.UTF_8);

            for (Message message : messages) {
                out.writeMessage(message);
                receivedMessages.add(in.readMessage());
            }

            socket2.close();
        } finally {
            thread.requestTermination();
        }

        Assertions.assertEquals(3, receivedMessages.size());
        Assertions.assertEquals("ACK", receivedMessages.get(0).getFirstSegment("MSH").get(9).asText());
        Assertions.assertEquals("AA", receivedMessages.get(0).getFirstSegment("MSA").get(1).asText());
    }

    private ReceiveServerSocketThread createReceiver() {
        ReceiveServerSocketThread thread = beanFactory.getBean(ReceiveServerSocketThread.class);

        thread.setReceiveOptions(ToolsConfiguration.getInstance().getReceiveOptions());
        thread.getReceiveOptions().setPort(0); // Local system must choose a port.
        thread.getReceiveOptions().getTlsOptions().setEnabled(false);
        thread.setOptions(new ImportOptions());
        thread.getOptions().setEncoding(StandardCharsets.UTF_8);

        return thread;
    }

}
