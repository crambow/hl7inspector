package de.elomagic.hl7inspector.tools.generator.ui;

import com.alee.laf.WebLookAndFeel;

import de.elomagic.hl7inspector.tools.generator.TargetOptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

class TargetDialogTest {

    @BeforeAll
    static void beforeEach() {
        WebLookAndFeel.install();
    }

    //@Test Fun fact....@Ignore seams to be also not work
    @Disabled("Will not work and throw Headless Exception.")
    void testUri() throws Exception {
        TargetDialog ui = new TargetDialog();

        Field field = TargetDialog.class.getDeclaredField("edFolder");
        field.setAccessible(true);

        Object fieldInstance = field.get(ui);

        Method setText = fieldInstance.getClass().getMethod("setText", String.class);
        setText.invoke(fieldInstance, "c:\\temp\\test");

        TargetOptions options = new TargetOptions();

        ui.bindBean(options);

        ui.commit();

        Assertions.assertEquals("file:///c:/temp/test", options.getFolder().toString());
    }

}
