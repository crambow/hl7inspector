package de.elomagic.hl7inspector.tools.sender;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

class SourceFolderIteratorTest {

    @Test
    void testAll() {
        SourceFolderIterator it = new SourceFolderIterator(Paths.get(System.getProperty("user.home")), StandardCharsets.UTF_8);

        Assertions.assertTrue(it.hasNext());
        //Assertions.assertNull(it.next());
    }

}