package de.elomagic.hl7inspector.tools.compare;

import com.alee.laf.WebLookAndFeel;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.tools.TestTool;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class ComparePanelTest {

    private ComparePanel comparePanel;

    @BeforeEach
    void beforeEach() {
        WebLookAndFeel.install();

        comparePanel = new ComparePanel(null, null);
    }

    @Test
    void testCompareEqually() throws Exception {
        Message message1 = TestTool.readMessage("/A08-UTF8-CR.hl7");
        Message message2 = TestTool.readMessage("/A08-UTF8-CR.hl7");

        // Test equally
        StringBuilder diff1 = new StringBuilder();
        StringBuilder diff2 = new StringBuilder();
        comparePanel.compare(diff1, diff2, message1, message2);

        assertEquals(diff1.toString(), diff2.toString());
    }

    @Test
    void testCompareNonEqually() throws Exception {
        Message message1 = TestTool.readMessage("/A08-UTF8-CR.hl7");
        Message message2 = TestTool.readMessage("/A08-UTF8-CR.hl7");
        message2.get(0).get(9).get(0).parse("ABC");

        // Test  non equally
        StringBuilder diff1 = new StringBuilder();
        StringBuilder diff2 = new StringBuilder();
        comparePanel.compare(diff1, diff2, message1, message2);

        assertNotEquals(diff1.toString(), diff2.toString());
    }

}