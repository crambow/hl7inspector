/*
 * Copyright 2011-present Carsten Rambow
 * 
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.gnu.org/licenses/gpl.txt
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector.tools;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.MessageMETA;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestTool {

    public static String readResource(String resourceName, Charset charset) throws IOException {
        InputStream in = TestTool.class.getResourceAsStream(resourceName);

        if (in == null) {
            throw new IOException("Unable to find local resource \"" + resourceName + "\".");
        }

        return IOUtils.toString(in, charset);
    }

    /**
     * Read resource with UTF-8 chartset
     *
     * @param resourceName
     * @return
     * @throws IOException
     */
    public static String readResource(String resourceName) throws IOException {
        return readResource(resourceName, StandardCharsets.UTF_8);
    }

    /**
     * Read resource and normalize carriage return and returns to carriage.
     *
     * @param resourceName
     * @return
     * @throws IOException
     */
    public static String readMessageResource(String resourceName) throws IOException {
        String s = readResource(resourceName);

        s = s.replace("\r\n", "\r");
        s = s.replace("\n", "\r");

        return s;
    }

    public static Message readMessage(String resource) throws IOException {
        Message m = new Message();
        m.parse(readMessageResource(resource));
        m.getMeta().setEncoding(StandardCharsets.UTF_8);
        m.getMeta().setSourceType(MessageMETA.SourceType.IN_MEMORY);

        return m;
    }

    public static Path writeTempFile(String content) throws IOException {
        File file = File.createTempFile("junit_", ".tmp");
        file.deleteOnExit();

        FileUtils.write(file, content, StandardCharsets.UTF_8);

        return file.toPath();
    }

    public static Path getTempTestPath() throws IOException {
        Path path = Paths.get(System.getProperty("java.io.tmpdir")).resolve("junit_hl7inspector");
        Files.createDirectories(path);

        return path;
    }

}
