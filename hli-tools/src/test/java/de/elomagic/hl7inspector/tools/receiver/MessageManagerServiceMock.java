package de.elomagic.hl7inspector.tools.receiver;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.services.MessageManagerService;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class MessageManagerServiceMock implements MessageManagerService {

    @Override
    public @NotNull List<Message> getSelectedMessages() {
        return Collections.emptyList();
    }

    @Override
    public boolean add(@NotNull Message message) {
        return true;
    }

}
