/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.sender;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.MetricBean;
import de.elomagic.hl7inspector.platform.utils.security.CertificateTool;
import de.elomagic.hl7inspector.platform.utils.thread.ControllableThread;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadEvent;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadStatusListener;
import de.elomagic.hl7inspector.shared.configuration.SendOptions;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.Segment;
import de.elomagic.hl7inspector.shared.io.MLLPMessageInputStream;
import de.elomagic.hl7inspector.shared.io.MLLPMessageOutputStream;
import de.elomagic.hl7inspector.shared.io.UnexpectedEndOfStreamException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCSException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Scope("prototype")
public class SendSocketThread extends ControllableThread<ThreadEvent<SendSocketThread>, ThreadEvent<SendSocketThread>> {

    private static final Logger LOGGER = LogManager.getLogger(SendSocketThread.class);

    private final List<ThreadStatusListener> listener = new ArrayList<>();
    private final AtomicBoolean terminating = new AtomicBoolean(false);
    private final MetricBean outboundMetrics = new MetricBean();
    private final MetricBean inboundMetrics = new MetricBean();

    private Iterator<Message> source;
    private SendOptions options = new SendOptions();
    private Socket socket;

    public SendOptions getOptions() {
        return options;
    }

    public void setOptions(SendOptions o) {
        options = o;
    }

    public void setMessageSource(@NotNull Iterator<Message> source) {
        this.source = source;
    }

    @Override
    public void requestTermination() {
        LOGGER.debug("Request send socket thread termination.");

        terminating.set(true);

        if(socket != null) {
            try {
                if(socket.isConnected()) {
                    //socket.setSoTimeout(1);
                    socket.close();
                }
            } catch(Exception e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }
    }

    @Override
    public void run() {
        fireAfterStartListener(new ThreadEvent<>(this));
        try {
            while(!terminating.get() && source.hasNext()) {
                try {
                    fireStatusEvent(LocaleTool.get("connecting_system_2args", options.getHost(), options.getPort()));

                    socket = createSocket();
                    fireStatusEvent(LocaleTool.get("system_connected_2args", options.getHost(), options.getPort()));

                    sendAndReceiveMessages();
                } catch(Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                    fireStatusEvent((ex.getMessage() != null) ? ex.getMessage() : ex.toString());
                    requestTermination();
                }

                if (socket != null && socket.isConnected()) {
                    fireStatusEvent(LocaleTool.get("disconnecting_from_system_2args", options.getHost(), options.getPort()));
                    socket.close();
                    fireStatusEvent(LocaleTool.get("system_disconnected_2args", options.getHost(), options.getPort()));
                }
            }

            // Print metrics
            fireStatusEvent(LocaleTool.get("send_metrics_5args", outboundMetrics.getCount(), outboundMetrics.getCount()*outboundMetrics.getAverage(), outboundMetrics.getAverage(), outboundMetrics.getMin(), outboundMetrics.getMax()));
            fireStatusEvent(LocaleTool.get("receive_metrics_5args", inboundMetrics.getCount(), inboundMetrics.getCount()*inboundMetrics.getAverage(), inboundMetrics.getAverage(), inboundMetrics.getMin(), inboundMetrics.getMax()));
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
            fireStatusEvent((e.getMessage() != null) ? e.getMessage() : e.toString());
        } finally {
            fireStatusEvent(LocaleTool.get("send_messages_server_stopped"));
            fireBeforeEndListener(new ThreadEvent<>(this));
        }
    }

    private void sendAndReceiveMessages() throws IOException {
        MLLPMessageOutputStream out = new MLLPMessageOutputStream(socket.getOutputStream(), options.getFrame(), options.getEncoding());
        out.setMetric(outboundMetrics);
        out.addListener((o, b, e) -> fireBytesSendEvent(b, e));
        MLLPMessageInputStream in = new MLLPMessageInputStream(socket.getInputStream(), options.getFrame(), options.getEncoding());
        in.setMetric(inboundMetrics);
        in.addListener((o, b, c) -> fireBytesReceivedEvent(b, c));
        while (source.hasNext() && !terminating.get()) {
            Message message = source.next();

            fireStatusEvent(LocaleTool.get("send_message_dots"));
            out.writeMessage(message);
            fireStatusEvent(LocaleTool.get("message_send"));

            Message messageAck = readAcknowledge(in);

            if (messageAck == null) {
                fireStatusEvent(LocaleTool.get("no_acknowledgement_message_to_evaluate_because_the_other_side_hasnt_sent_anything"));
            } else {
                evaluateAcknowledge(messageAck);
            }

            if (!options.isKeepSocket()) {
                break;
            }
        }

        if (!source.hasNext()) {
            fireStatusEvent(LocaleTool.get("all_selected_messaged_send"));
        }
    }

    /**
     * Reads a framed HL7 message from a stream.
     *
     * @param in Input stream to read
     * @return Returns acknowledge message or null when end of stream reached.
     * @throws IOException Thrown when unable to read bytes from the stream
     */
    @Nullable
    private Message readAcknowledge(@NotNull MLLPMessageInputStream in) throws IOException {
        fireStatusEvent(LocaleTool.get("waiting_for_acknowledge_message_dots"));

        Message message = null;
        try {
            message = in.readMessage();
            if (message == null) {
                LOGGER.debug("End of stream reached. No acknowledge message received.");
                return null;
            }
            message.getMeta().setInMemory(true);
            fireStatusEvent(LocaleTool.get("acknowledge_message_received"));
        } catch (UnexpectedEndOfStreamException e) {
            fireStatusEvent(LocaleTool.get("an_answer_was_expected_but_the_connection_was_terminated_by_the_other_side"));
        }

        return message;
    }

    private void evaluateAcknowledge(Message message) {
        Segment msa = message.getFirstSegment("MSA");

        if(msa == null) {
            fireStatusEvent(LocaleTool.get("invalid_acknowledge_message_structure_segment_msa_is_missing"));
        } else {
            if(msa.size() < 1) {
                fireStatusEvent(LocaleTool.get("invalid_segment_msa_field_msa1_is_missing"));
            } else {
                String ac = msa.get(1).asText();

                String status;
                switch(ac) {
                    case "CA":
                        status = LocaleTool.get("acknowledgment_code_commit_accept");
                        break;
                    case "CE":
                        status = LocaleTool.get("acknowledgment_code_commit_error");
                        break;
                    case "CR":
                        status = LocaleTool.get("acknowledgment_code_commit_reject");
                        break;
                    case "AA":
                        status = LocaleTool.get("acknowledgment_code_application_accept");
                        break;
                    case "AE":
                        status = LocaleTool.get("acknowledgment_code_application_error");
                        break;
                    case "AR":
                        status = LocaleTool.get("acknowledgment_code_application_reject");
                        break;
                    default:
                        status = LocaleTool.get("evaluate_acknowledge_unknown_or_missing_acknowledge_code_in_field_msa1");
                        break;
                }

                fireStatusEvent(status);
            }
        }
    }

    private Socket createSocket() throws IOException, GeneralSecurityException, PKCSException, OperatorCreationException {
        if (options.getTlsOptions().isEnabled()) {
            TrustManager[] tm = CertificateTool.getTrustManagers(options.getTlsOptions().getCertificates());
            KeyManager[] km = options.getTlsOptions().isClientAuthenticationEnabled() ?
                    CertificateTool.getKeyManagers(
                            options.getTlsOptions().getClientAuthenticationPrivateKey(),
                            options.getTlsOptions().getClientAuthenticationPassword()) : null;

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(km, tm, null);

            return context.getSocketFactory().createSocket(options.getHost(), options.getPort());
        } else {
            socket = new Socket();
            socket.connect(new InetSocketAddress(options.getHost(), options.getPort()));
            return socket;
        }
    }

    @NotNull
    public MetricBean getOutboundMetrics() {
        return outboundMetrics;
    }

    @NotNull
    public MetricBean getInboundMetrics() {
        return inboundMetrics;
    }

    public void addListener(ThreadStatusListener value) {
        listener.add(value);
    }

    public void removeListener(ThreadStatusListener value) {
        listener.remove(value);
    }

    protected void fireBytesReceivedEvent(@NotNull byte[] data, @Nullable Charset encoding) {
        listener.forEach(l -> l.bytesRead(this, data, encoding));
    }

    private void fireBytesSendEvent(@NotNull byte[] data, @Nullable Charset encoding) {
        listener.forEach(l -> l.bytesSend(this, data, encoding));
    }

    protected void fireStatusEvent(String text) {
        listener.forEach(l -> l.status(this, text));
    }

}
