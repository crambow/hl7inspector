/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.http;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

@Component
public class HttpService {

    private static final Logger LOGGER = LogManager.getLogger(HttpService.class);
    private final ToolsConfiguration configuration = ToolsConfiguration.getInstance();

    private final Map<String, HttpHandler> pathHandlers = new HashMap<>();
    private boolean running;
    private HttpServer server;

    public boolean isRunning() {
        return running;
    }

    public void restart() throws IOException {
        try {
            if (running) {
                stop();
            }

            LOGGER.debug("Starting HTTP server on port {}...", configuration.getToolsHttpServerPort());
            server = HttpServer.create(new InetSocketAddress(InetAddress.getLoopbackAddress(), configuration.getToolsHttpServerPort()), 0);

            for (Map.Entry<String, HttpHandler> entry : pathHandlers.entrySet()) {
                server.createContext(entry.getKey(), entry.getValue());
            }

            server.start();
            running = true;
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    public void stop() {
        if (!isRunning()) {
            return;
        }

        LOGGER.debug("Stopping HTTP server...");
        server.stop(1);
        running = false;
    }

    public void addContext(@NotNull String path, @NotNull HttpHandler handler) {
        pathHandlers.put(path, handler);
    }

    public void removeContext(@NotNull String path) {
        pathHandlers.remove(path);
    }

}
