/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import java.net.URI;
import java.nio.file.Path;

public class TargetOptions {

    private boolean useMllp;
    private URI mllpUri;
    private boolean keepOpen;
    private Path folder;
    private String filenamePattern = "hl7_${counter}.hl7";
    private int minDigits = 3;

    public boolean isUseMllp() {
        return useMllp;
    }

    public void setUseMllp(boolean useMllp) {
        this.useMllp = useMllp;
    }

    public URI getMllpUri() {
        return mllpUri;
    }

    public void setMllpUri(URI mllpUri) {
        this.mllpUri = mllpUri;
    }

    public boolean isKeepOpen() {
        return keepOpen;
    }

    public void setKeepOpen(boolean keepOpen) {
        this.keepOpen = keepOpen;
    }

    public Path getFolder() {
        return folder;
    }

    public void setFolder(Path folder) {
        this.folder = folder;
    }

    public String getFilenamePattern() {
        return filenamePattern;
    }

    public void setFilenamePattern(String filenamePattern) {
        this.filenamePattern = filenamePattern;
    }

    public int getMinDigits() {
        return minDigits;
    }

    public void setMinDigits(int minDigits) {
        this.minDigits = minDigits;
    }

    public String toSummarizeString() {
        String result;

        if (isUseMllp()) {
            result = getMllpUri().toASCIIString() + ", " + LocaleTool.get("reuse_label") + ": " + LocaleTool.get(isKeepOpen() ? "yes" : "no");
        } else if (getFolder() == null) {
            result = LocaleTool.get("unknown");
        } else {
            result = getFolder().resolve(getFilenamePattern()).toString();
        }
        return result;
    }

}
