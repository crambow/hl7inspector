/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.compare;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.desktop.MessageListToolDescriptor;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.shared.services.MessageManagerService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.util.List;

@Component
public class CompareMessageListDescriptor implements MessageListToolDescriptor {

    private final BeanFactory beanFactory;

    public @Autowired CompareMessageListDescriptor(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public float getPriority() {
        return 1000;
    }

    @Override
    public @Nullable JComponent getComponent() {
        return null;
    }

    @Override
    public @NotNull List<JMenuItem> getContextMenuItems() {
        return List.of(new JMenuItem(new CompareMessageAction(this::handleCompare)));
    }

    private void handleCompare(ActionEvent e) {
        List<Message> selectedMessages = beanFactory.getBean(MessageManagerService.class).getSelectedMessages();
        if(selectedMessages.size() == 2) {
            beanFactory.getBean(ComparePanel.class).compare(selectedMessages.get(0), selectedMessages.get(1));
        } else {
            SimpleDialog.info(LocaleTool.get("two_messages_must_selected"));
        }
    }

}
