/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.valuepresenter.plain;

import com.alee.managers.style.StyleId;
import com.alee.managers.style.StyleManager;

import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;
import de.elomagic.hl7inspector.tools.valuepresenter.ValueException;
import de.elomagic.hl7inspector.tools.valuepresenter.AbstractValueTypePresenterUI;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class StringPresenterUI extends AbstractValueTypePresenterUI {

    private final ToolsConfiguration configuration = ToolsConfiguration.getInstance();
    private final JTextArea textArea = new JTextArea();
    private final JLabel warningLabel = new JLabel();
    private final CardLayout cardLayout = new CardLayout();
    private JPanel cardPanel;
    private String value;

    @PostConstruct
    private void initUI() {
        // InitUI
        setLayout(new BorderLayout());

        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        toolbar.setRollover(true);
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "ok.png", e -> commit()));
        toolbar.addSeparator();
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "fileopen.png", e -> handleLoadFile()));
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "document-save.png", e -> handleSaveFile(GenericFileFilter.TEXT_FILTER)));
        StyleManager.setStyleId(toolbar, StyleId.toolbarUndecorated);

        textArea.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(textArea);

        JPanel warningPanel = FormBuilder.createBuilder()
                .addRowSpacer()
                .addRow(warningLabel, 3, 1)
                .addColumnSpacer()
                .add(new JButton(new GenericAction(e -> showValue(value) ).setName("show_value")), 1, 0)
                .addColumnSpacer()
                .addRow()
                .addRowSpacer()
                .build();

        cardPanel = new JPanel(cardLayout);
        cardPanel.add(scrollPane, "content");
        cardPanel.add(warningPanel, "warning");

        add(toolbar, BorderLayout.NORTH);
        add(cardPanel, BorderLayout.CENTER);
    }

    @Override
    protected void setValue(@Nullable String value) {
        this.value = value;
        if (value != null && value.length() > configuration.getValuePresenterStringMaxSize()) {
            SwingUtilities.invokeLater(() -> {
                warningLabel.setText("<html>" + LocaleTool.get("string_is_bigger_arg", FileUtils.byteCountToDisplaySize(value.length())));
                cardLayout.show(cardPanel, "warning");
            });
        } else {
            showValue(value);
        }
    }

    private void showValue(@Nullable String value)  {
        cardLayout.show(cardPanel, "content");
        textArea.setText(value == null ? "" : value);
    }

    public void commit() {
        commit(textArea.getText());
    }

    @Override
    protected void load(@NotNull Path file) throws ValueException {
        try {
            if (Files.exists(file) && Files.size(file) > configuration.getValuePresenterStringMaxSize()) {
                String text = LocaleTool.get("file_has_bytes_load_anyway_arg", FileUtils.byteCountToDisplaySize(Files.size(file)));
                if (SimpleDialog.confirmYesNo(text) != SimpleDialog.YES_OPTION) {
                    return;
                }
            }

            String content = FileUtils.readFileToString(file.toFile(), StandardCharsets.UTF_8);
            setValue(content);
        } catch (Exception ex) {
            throw new ValueException(ex.getMessage(), ex);
        }
    }

    @Override
    protected void save(Path file) throws IOException {
        Files.writeString(file, textArea.getText());
    }

}
