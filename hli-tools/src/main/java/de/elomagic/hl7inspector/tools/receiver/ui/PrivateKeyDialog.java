/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.receiver.ui;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.security.CertificateTool;
import de.elomagic.hl7inspector.platform.utils.security.PemTool;
import de.elomagic.hl7inspector.shared.configuration.TlsReceiveOptions;

import org.jetbrains.annotations.NotNull;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.security.cert.CertificateException;

public class PrivateKeyDialog extends MyBaseDialog {

    private transient TlsReceiveOptions options;

    private final JCheckBox cbTlsEnabled = new JCheckBox(LocaleTool.get("tls_enabled"));
    private final JPasswordField edPassword = new JPasswordField();
    private final JTextArea edPrivateKey = new JTextArea();

    private final JCheckBox cbClientAuthEnabled = new JCheckBox(LocaleTool.get("client_authentication_enabled"));
    private final JTextArea edClientCertificates = new JTextArea();

    public PrivateKeyDialog() {
        setTitle(LocaleTool.get("configure_private_key_certificate"));
        setModal(true);
        setSize(new Dimension(720,640));
        getBanner().setVisible(false);

        JTabbedPane tabPanel = new JTabbedPane();
        tabPanel.add(LocaleTool.get("server"), createServerKeyPanel());
        tabPanel.add(LocaleTool.get("client_authentication"), createClientAuthenticationPanel());

        getContentPane().add(tabPanel, BorderLayout.CENTER);
    }

    private JPanel createServerKeyPanel() {
        JScrollPane scrollPane = new JScrollPane(edPrivateKey);

        edPrivateKey.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

        return FormBuilder.createBuilder()
                .setBorder(BorderFactory.createEmptyBorder(6, 0, 0,0))
                .addRow(cbTlsEnabled, 2, 1)
                .addRow("password", edPassword, 1, 1)
                .addRow("pem_private_key", 1)
                .addRow(scrollPane, 2, 1, 1, 1)
                .addRow(2,
                        new JButton(new GenericAction(e -> showCertificate(edPrivateKey.getText())).setName("show_certificate")),
                        new JButton(new GenericAction(e -> createServerKey()).setName("create_self_signed_certificate")))
                .build();
    }

    private JPanel createClientAuthenticationPanel() {
        JScrollPane scrollPane = new JScrollPane(edClientCertificates);

        edClientCertificates.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

        return FormBuilder.createBuilder()
                .setBorder(BorderFactory.createEmptyBorder(6, 0, 0,0))
                .addRow(cbClientAuthEnabled, 1, 1)
                .addRow("pem_certificates", 1)
                .addRow(scrollPane, 1, 1, 1, 1)
                .addRow(new JButton(new GenericAction(e -> showCertificate(edClientCertificates.getText())).setName("show_certificate")), 2, 0)
                .build();
    }


    private void showCertificate(String pem) {
        try {
            SimpleDialog.info(LocaleTool.get("certificate_summery"), PemTool.getCertificateSummary(pem));
        } catch (Exception ex) {
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    private void createServerKey() {
        try {
            edPrivateKey.setText(CertificateTool.createSelfSignedPemEncodedCertificate(edPassword.getPassword()));
            showCertificate(edPrivateKey.getText());
        } catch (Exception ex) {
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    @Override
    public void ok() {
        try {
            validateForm();
            commit();

            super.ok();
        } catch (Exception ex) {
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    public void bindBean(@NotNull TlsReceiveOptions options) {
        this.options = options;

        cbTlsEnabled.setSelected(options.isEnabled());
        edPrivateKey.setText(options.getPrivateKey());
        edPassword.setText(options.getPassword() == null ? "" : new String(options.getPassword()));

        cbClientAuthEnabled.setSelected(options.isClientAuthenticationEnabled());
        edClientCertificates.setText(options.getClientAuthenticationCertificates());
    }

    protected void validateForm() throws CertificateException {
        try {
            if (cbTlsEnabled.isSelected()) {
                if (PemTool.readCertificates(edPrivateKey.getText()).length == 0) {
                    throw new CertificateException(LocaleTool.get("certificate_validation_failed"));
                }

                if (PemTool.readPrivateKey(edPrivateKey.getText(), edPassword.getPassword()) == null) {
                    throw new CertificateException(LocaleTool.get("private_key_not_found"));
                }
            }

            if (cbClientAuthEnabled.isSelected() && PemTool.readCertificates(edClientCertificates.getText()).length == 0) {
                throw new CertificateException(LocaleTool.get("client_certificate_validation_failed"));
            }
        } catch (Exception ex) {
            throw new CertificateException(ex.getMessage(), ex);
        }
    }

    public void commit() {
        options.setEnabled(cbTlsEnabled.isSelected());
        options.setPassword(edPassword.getPassword());
        options.setPrivateKey(edPrivateKey.getText());

        options.setClientAuthenticationEnabled(cbClientAuthEnabled.isSelected());
        options.setClientAuthenticationCertificates(edClientCertificates.getText());
    }

}
