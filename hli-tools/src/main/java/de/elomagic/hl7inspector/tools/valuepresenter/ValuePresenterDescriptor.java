/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.valuepresenter;

import com.alee.extended.dock.WebDockableFrame;

import de.elomagic.hl7inspector.shared.desktop.ValueEditorDescriptor;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ValuePresenterDescriptor implements ValueEditorDescriptor {

    private final ValuePresenterFrame valuePresenterFrame;

    public @Autowired ValuePresenterDescriptor(ValuePresenterFrame valuePresenterFrame) {
        this.valuePresenterFrame = valuePresenterFrame;
    }

    @Override
    public @Nullable WebDockableFrame getDockableFrame() {
        return valuePresenterFrame;
    }

}
