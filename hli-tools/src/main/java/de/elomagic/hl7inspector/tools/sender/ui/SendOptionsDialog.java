/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.sender.ui;

import com.alee.laf.combobox.WebComboBox;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.platform.ui.bindings.FormDialog;
import de.elomagic.hl7inspector.platform.ui.components.FolderTextField;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.shared.configuration.SendOptions;
import de.elomagic.hl7inspector.shared.ui.MessageFramePanel;
import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import java.awt.BorderLayout;
import java.nio.charset.Charset;
import java.util.Objects;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SendOptionsDialog extends FormDialog<SendOptions> {

    private static final Logger LOGGER = LogManager.getLogger(SendOptionsDialog.class);
    private final transient Configuration configuration = Configuration.getInstance();

    private final MessageFramePanel framePanel = new MessageFramePanel();

    private SendOptions options;
    private final CertificatesDialog certificatesDialog = new CertificatesDialog();

    private JRadioButton rbSelectedMessages;
    private JRadioButton rbMessagesFromFolder;
    private FolderTextField edSourceFolder;
    private JComboBox<String> cbDest;
    private WebComboBox cbEncoding;
    private JCheckBox cbReuse;
    private final JLabel lblTlsStatus = new JLabel(LocaleTool.get("unknown"));

    @PostConstruct
    private void init() {

        getBanner().setVisible(false);

        setTitle(LocaleTool.get("dialog_title"));
        setModal(true);

        rbSelectedMessages = new JRadioButton(LocaleTool.get("selected_messages"));
        rbMessagesFromFolder = new JRadioButton(LocaleTool.get("messages_from_folder"));
        ButtonGroup bg = new ButtonGroup();
        bg.add(rbSelectedMessages);
        bg.add(rbMessagesFromFolder);
        edSourceFolder = new FolderTextField().setCaption("     " + LocaleTool.get("folder"));

        cbDest = new JComboBox<>(configuration.getRecentDestinationModel());
        cbDest.setEditable(true);
        cbDest.setSelectedItem(cbDest.getModel().getSize() == 0 ? null : cbDest.getModel().getElementAt(0));

        JButton btConfigureTls = new JButton(new GenericAction(e -> configureTls()).setName("configure_tls"));

        cbEncoding = UIFactory.createCharsetComboBox();

        cbReuse = new JCheckBox();
        cbReuse.setToolTipText(LocaleTool.get("reuse_socket"));

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                .addSection("source")
                //.addColumnSpacer(20)
                .addRow(rbSelectedMessages, 2, 1)
                .addRow(rbMessagesFromFolder, 2, 1)
                .addRow(edSourceFolder, 2, 1)
                .addSection("destination")
                .addRow("hostname_and_port_of_destination", 2)
                .addRow(cbDest, 2, 1)

                .add("tls_enabled", 1)
                .addRow(1, lblTlsStatus, btConfigureTls)

                .addSection("options")
                .addRow("encoding_label", cbEncoding, 2, 0)
                .addRow("reuse_label", cbReuse, 2, 1)

                .addSection("message_frame")
                .addRow(framePanel, 2, 1)

                .addRowSpacer()
                .build();

        getContentPane().add(grid, BorderLayout.CENTER);

        setSize(800, getPreferredSize().height);

        setLocationRelativeTo(getOwner());
    }

    @Override
    public void bindBean(@NotNull SendOptions options) {
        this.options = options;

        rbSelectedMessages.setSelected(options.getSourceMode() == 0);
        rbMessagesFromFolder.setSelected(options.getSourceMode() == 1);

        edSourceFolder.setPath(options.getSourceFolder());
        certificatesDialog.bindBean(options.getTlsOptions());

        cbDest.setSelectedItem(options.getHost());

        cbEncoding.setSelectedItem(options.getEncoding());
        cbReuse.setSelected(options.isKeepSocket());
        lblTlsStatus.setText(getTlsModeText());

        framePanel.bindBean(options.getFrame());
    }

    @Override
    public boolean validateForm() {
        try {
            String hp = Objects.toString(cbDest.getSelectedItem(), "");

            if(hp.indexOf(':') == -1) {
                throw new IllegalArgumentException(LocaleTool.get("invalid_destination_format"));
            }

            String host = hp.substring(0, hp.indexOf(':'));
            String port = hp.substring(hp.indexOf(':') + 1);

            int p = NumberUtils.toInt(Objects.toString(port, "-1"), -1);
            if (p < 1 || p > 65535) {
                throw new IllegalArgumentException(LocaleTool.get("invalid_port"));
            }

            if(host.isEmpty()) {
                throw new IllegalArgumentException(LocaleTool.get("host_missing"));
            }

            framePanel.validateForm();
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new FormBinderException(e.getMessage(), e);
        }

        return true;
    }

    @Override
    public void commit() {
        try {
            String hp = Objects.toString(cbDest.getSelectedItem(), "");
            String host = hp.substring(0, hp.indexOf(':'));
            String port = hp.substring(hp.indexOf(':') + 1);

            // Commit to bean
            options.setSourceMode(rbSelectedMessages.isSelected() ? 0 : 1);
            options.setSourceFolder(edSourceFolder.getPath());
            options.setHost(host);
            options.setPort(Integer.parseInt(port));
            options.setEncoding((Charset) cbEncoding.getSelectedItem());
            options.setKeepSocket(cbReuse.isSelected());

            framePanel.commit();

            ToolsConfiguration.getInstance().setSendOptions(options);
            configuration.getRecentDestinationModel().insertElementAt(Objects.toString(cbDest.getSelectedItem(), null), 0);
        } catch (Exception ex) {
            throw new FormBinderException(ex.getMessage(), ex);
        }
    }

    private void configureTls() {
        certificatesDialog.ask();
        lblTlsStatus.setText(getTlsModeText());
    }

    private String getTlsModeText() {
        String tlsModeKey;
        if (options.getTlsOptions().isEnabled() && options.getTlsOptions().isClientAuthenticationEnabled()) {
            tlsModeKey = "client_authentication";
        } else if (options.getTlsOptions().isEnabled()) {
            tlsModeKey = "yes";
        } else {
            tlsModeKey = "no";
        }

        return LocaleTool.get(tlsModeKey);
    }
}