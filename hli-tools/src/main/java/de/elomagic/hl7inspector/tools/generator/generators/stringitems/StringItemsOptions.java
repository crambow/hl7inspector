/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators.stringitems;

import de.elomagic.hl7inspector.tools.generator.generators.AbstractGeneratorOptions;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class StringItemsOptions extends AbstractGeneratorOptions {

    private List<String> items = new ArrayList<>();
    private boolean random;

    public StringItemsOptions() {
    }

    public StringItemsOptions(@NotNull String id, @NotNull List<String> items, boolean random) {
        super(id);

        this.items = items;
        this.random = random;
    }

    @NotNull
    public List<String> getItems() {
        return items;
    }

    public void setItems(@NotNull List<String> items) {
        this.items = items;
    }

    public boolean isRandom() {
        return random;
    }

    public void setRandom(boolean random) {
        this.random = random;
    }

}
