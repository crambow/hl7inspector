/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.receiver;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.thread.ControllableThread;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadEvent;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadStatusListener;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions;
import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.MessageMETA;
import de.elomagic.hl7inspector.shared.hl7.model.Segment;
import de.elomagic.hl7inspector.shared.io.UnexpectedEndOfStreamException;
import de.elomagic.hl7inspector.shared.io.MLLPMessageInputStream;
import de.elomagic.hl7inspector.shared.io.MLLPMessageOutputStream;
import de.elomagic.hl7inspector.shared.services.MessageManagerService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Scope("prototype")
public class ReceiveSocketThread extends ControllableThread {

    private static final Logger LOGGER = LogManager.getLogger(ReceiveSocketThread.class);

    private final List<ThreadStatusListener> listener = new ArrayList<>();
    private final AtomicBoolean terminating = new AtomicBoolean(false);
    private Socket socket;
    private ImportOptions options;

    private final MessageManagerService messageManagerService;

    private static int threadInitNumber;

    public ReceiveSocketThread(@Autowired MessageManagerService messageManagerService) {
        super("RS-" + nextThreadNum());

        this.messageManagerService = messageManagerService;
    }

    private static synchronized int nextThreadNum() {
        return threadInitNumber++;
    }

    @Override
    public void requestTermination() {
        if (socket == null || socket.isClosed()) {
            return;
        }

        terminating.set(true);

        LOGGER.debug("Closing socket...");
        try {
            socket.close();
        } catch (Exception ex) {
            LOGGER.warn(ex.getMessage(), ex);
        }
    }

    public void setSocket(@NotNull Socket socket) throws IOException {
        if (this.socket != null) {
            throw new IOException("Socket already set and can not changed any more for this thread.");
        }

        this.socket = socket;
    }

    public void setOptions(@NotNull ImportOptions options) {
        this.options = options;
    }

    @Override
    public void run() {
        fireAfterStartListener(new ThreadEvent<>(this));
        fireStatusEvent(LocaleTool.get("connecting_from_2args", socket.getInetAddress().getHostName(), socket.getInetAddress().getHostAddress()));
        try (MLLPMessageInputStream in = new MLLPMessageInputStream(socket.getInputStream(), options.getFrame(), options.getEncoding())) {
            in.addListener((o, b, c) -> fireBytesReceivedEvent(b, c));

            while(!terminating.get()) {
                fireStatusEvent(LocaleTool.get("waiting_for_data_dots"));
                Message message = in.readMessage();
                if (message == null) {
                    fireStatusEvent(LocaleTool.get("end_of_stream_detected_still_listening_on_port_1arg", socket.getLocalPort()));
                    break;
                }

                message.getMeta().setInMemory(true);
                message.getMeta().setSourceType(MessageMETA.SourceType.IP_SOCKET);
                handleMessage(message);
            }
        } catch(UnexpectedEndOfStreamException ex) {
            fireStatusEvent(LocaleTool.get("connection_was_disconnected_unexpectedly"));
            requestTermination();
        } catch(SSLException ex) {
            LOGGER.error(ex.getMessage(), ex);
            fireStatusEvent(LocaleTool.get("arg", ex.toString()));
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            fireStatusEvent(ex.getMessage() != null ? ex.getMessage() : ex.toString());
        } finally {
            //fireStatusEvent(LocaleTool.get("receive_server_stopped"));
            fireBeforeEndListener(new ThreadEvent<>(this));
        }
    }

    private void handleMessage(Message message) {
        boolean ignore = false;
        // Now filtering
        if(!options.getPhrase().isEmpty()) {
            String m = options.isCaseSensitive() ? message.asText() : message.asText().toUpperCase();
            String phrase = options.isCaseSensitive() ? options.getPhrase() : options.getPhrase().toUpperCase();

            if(!options.isUseRegExpr()) {
                boolean found = m.contains(phrase);
                ignore = ((!found && !options.isNegReg()) || (found && options.isNegReg()));
            }
        }

        //Desktop.getInstance().addMessages(Collections.singletonList(message), options.getBufferSize(),options.isReadBottom());
        // TODO Set buffer size and read from bottom
        messageManagerService.add(message);

        fireStatusEvent(LocaleTool.get("sending_acknowledge_dots"));

        try {
            MLLPMessageOutputStream out = new MLLPMessageOutputStream(socket.getOutputStream(), options.getFrame(), options.getEncoding());
            out.addListener((o, d, e) -> fireBytesReceivedEvent(d, e));
            try {
                Segment msh = message.get(0);

                List<String> seg = new ArrayList<>();
                seg.add("MSH");
                seg.add("^~\\&");
                seg.add(getField(msh, 5)); // Field 3
                seg.add(getField(msh, 6)); // Field 4
                seg.add(getField(msh, 3)); // Field 5
                seg.add(getField(msh, 4)); // Field 6
                seg.add(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())); // Field 7
                seg.add(""); // Field 8
                seg.add("ACK");//^^" + msh.get(9).toString()); // Field 9
                seg.add(getField(msh, 10)); // Field 10
                seg.add(getField(msh, 11)); // Field 11
                seg.add(getField(msh, 12)); // Field 12
                seg.add(""); // Field 13
                seg.add(""); // Field 14
                seg.add(""); // Field 15
                seg.add(""); // Field 16
                seg.add(""); // Field 17
                seg.add(getField(msh, 18)); // Field 18
                seg.add(Delimiters.DEFAULT_SEGMENT_SEPARATOR + "MSA");
                seg.add("AA");
                seg.add(getField(msh, 10));

                Message ack = new Message();
                ack.parse(String.join("|", seg));

                out.writeMessage(ack);
            } finally {
                out.flush();
            }
        } catch(Exception ex) {
            fireStatusEvent(ex.getMessage());
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private String getField(Segment seg, int index) {
        String result = "";

        if(seg.size() >= index) {
            result = seg.get(index).asText();
        }
        return result;
    }

    public void addListeners(Collection<ThreadStatusListener> values) {
        listener.addAll(values);
    }

    public void removeListeners(Collection<ThreadStatusListener> values) {
        listener.removeAll(values);
    }

    protected void fireBytesReceivedEvent(@NotNull byte[] data, @Nullable Charset encoding) {
        listener.forEach(l -> l.bytesRead(this, data, encoding));
    }

    protected void fireBytesSendEvent(@NotNull byte[] data, @Nullable Charset encoding) {
        listener.forEach(l ->l.bytesSend(this, data, encoding));
    }

    protected void fireStatusEvent(String text) {
        listener.forEach(l -> l.status(this, text));
    }
}
