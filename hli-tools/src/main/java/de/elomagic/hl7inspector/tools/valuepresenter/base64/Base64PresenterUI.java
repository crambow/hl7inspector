/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.valuepresenter.base64;

import com.alee.managers.style.StyleId;
import com.alee.managers.style.StyleManager;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.tools.valuepresenter.ValueException;
import de.elomagic.hl7inspector.tools.valuepresenter.AbstractValueTypePresenterUI;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import java.awt.BorderLayout;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

@Component
public class Base64PresenterUI extends AbstractValueTypePresenterUI {

    private final JScrollPane scrollPane = new JScrollPane();
    private final JTextArea textArea = new JTextArea();

    @PostConstruct
    private void initUI() {
        // InitUI
        setLayout(new BorderLayout());

        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        toolbar.setRollover(true);
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "ok.png", e -> commit()));
        toolbar.addSeparator();
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "fileopen.png", e -> handleLoadFile()));
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "document-save.png", e -> handleSaveFile(GenericFileFilter.PDF_FILTER)));
        StyleManager.setStyleId(toolbar, StyleId.toolbarUndecorated);

        textArea.setLineWrap(true);
        textArea.setEditable(false);
        scrollPane.setViewportView(textArea);

        add(toolbar, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
    }

    @Override
    protected void setValue(@Nullable String value) {
        textArea.setText(value == null ? "" : new String(Base64.getDecoder().decode(value)));
    }

    public void commit() {
        String base64 = Base64.getEncoder().encodeToString(textArea.getText().getBytes());
        commit(base64);
    }

    @Override
    protected void load(@NotNull Path file) throws ValueException {
        try {
            String content = FileUtils.readFileToString(file.toFile(), StandardCharsets.UTF_8);
            setValue(new String(Base64.getDecoder().decode(content)));
        } catch (Exception ex) {
            throw new ValueException(ex.getMessage(), ex);
        }
    }

    @Override
    protected void save(Path file) throws IOException {
        String base64 = Base64.getEncoder().encodeToString(textArea.getText().getBytes());
        Files.writeString(file, base64);
    }

}
