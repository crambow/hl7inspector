/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators;

import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.platform.ui.bindings.FormDialog;
import de.elomagic.hl7inspector.tools.generator.generators.date.DateOptions;
import de.elomagic.hl7inspector.tools.generator.generators.date.DateUI;
import de.elomagic.hl7inspector.tools.generator.generators.fileline.FileLineOptions;
import de.elomagic.hl7inspector.tools.generator.generators.fileline.FileLineUI;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomOptions;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomUI;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsOptions;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsUI;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;

import java.awt.BorderLayout;
import java.awt.Dimension;

public class GeneratorOptionsDialogFactory<T extends AbstractGeneratorOptions> extends FormDialog<T> {

    private AbstractSequencerUI<T> ui;

    public static <U extends AbstractGeneratorOptions> GeneratorOptionsDialogFactory<U> createDialog(@NotNull AbstractSequencerUI<U> ui, @NotNull U options) {
        GeneratorOptionsDialogFactory<U> dialog = new GeneratorOptionsDialogFactory<>();
        dialog.ui = ui;
        dialog.bindBean(options);
        dialog.getContentPane().add(dialog.ui, BorderLayout.CENTER);
        dialog.setSize(new Dimension(640, 480));
        dialog.setModal(true);
        dialog.setTitle(LocaleTool.get(dialog.ui.getTitle()));
        dialog.getBanner().setVisible(false);

        return dialog;
    }

    public static GeneratorOptionsDialogFactory<StringItemsOptions> createStringItemsDialog(@NotNull StringItemsOptions options) {
        return createDialog(new StringItemsUI(), options);
    }

    public static GeneratorOptionsDialogFactory<RandomOptions> createRandomDialog(@NotNull RandomOptions options) {
        return createDialog(new RandomUI(), options);
    }

    public static GeneratorOptionsDialogFactory<FileLineOptions> createFileLineDialog(@NotNull FileLineOptions options) {
        return createDialog(new FileLineUI(), options);
    }

    public static GeneratorOptionsDialogFactory<DateOptions> createDateDialog(@NotNull DateOptions options) {
        return createDialog(new DateUI(), options);
    }

    @Override
    public void bindBean(@NotNull T bean) {
        ui.bindBean(bean);
    }

    @Override
    public boolean validateForm() throws FormBinderException {
        ui.validateForm();

        return true;
    }

    @Override
    public void commit() throws FormBinderException {
        ui.commit();
    }

}
