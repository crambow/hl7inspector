/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.receiver.ui;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.configuration.ReceiveOptions;
import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.security.GeneralSecurityException;
import java.util.Objects;

@Component
public class ReceiveNetworkSetupDialog extends MyBaseDialog {

    private static final Logger LOGGER = LogManager.getLogger(ReceiveNetworkSetupDialog.class);

    private final transient ToolsConfiguration configuration = ToolsConfiguration.getInstance();

    private final PrivateKeyDialog privateKeyDialog = new PrivateKeyDialog();
    private transient ReceiveOptions options;

    private JComboBox<String> cbServerPort;
    private final JLabel lblTlsStatus = new JLabel(LocaleTool.get("unknown"));

    @PostConstruct
    private void init() {

        getBanner().setVisible(false);

        setTitle(LocaleTool.get("receive_network_setup"));
        setModal(true);

        cbServerPort = new JComboBox<>(new String[] {"2100", "2200", "2300", "5555", "5556"});
        cbServerPort.setEditable(true);
        cbServerPort.setSelectedItem("");
        JButton btConfigureTls = new JButton(new GenericAction(e -> configureTls()).setName("configure_tls"));

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                .addSection("network")
                .addRow("server_port", cbServerPort)

                .add("tls_enabled", 1)
                .addRow(1, lblTlsStatus, btConfigureTls)

                .addRowSpacer()
                .build();

        getContentPane().add(grid, BorderLayout.CENTER);

        pack();

        setSize(400, getPreferredSize().height);

        setLocationRelativeTo(getOwner());
    }

    public void bindBean(ReceiveOptions options) {
        this.options = options;

        privateKeyDialog.bindBean(options.getTlsOptions());

        cbServerPort.setSelectedItem(Integer.toString(options.getPort()));
        lblTlsStatus.setText(getTlsModeText());
    }

    @Override
    public void ok() {
        try {
            int port = NumberUtils.toInt(Objects.toString(cbServerPort.getSelectedItem(), "-1"), -1);
            if (port < 1 || port > 65535) {
                SimpleDialog.error(LocaleTool.get("invalid_server_port"));
                return;
            }

            // Commit to bean
            options.setPort(port);

            configuration.setReceiveOptions(options);

            super.ok();
        } catch (Exception ex) {
            LOGGER.fatal(ex.getMessage(), ex);
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    private void configureTls() {
        privateKeyDialog.ask();
        lblTlsStatus.setText(getTlsModeText());
    }

    private String getTlsModeText() {
        String tlsModeKey;
        if (options.getTlsOptions().isEnabled() && options.getTlsOptions().isClientAuthenticationEnabled()) {
            tlsModeKey = "client_authentication";
        } else if (options.getTlsOptions().isEnabled()) {
            tlsModeKey = "yes";
        } else {
            tlsModeKey = "no";
        }

        return LocaleTool.get(tlsModeKey);
    }

}
