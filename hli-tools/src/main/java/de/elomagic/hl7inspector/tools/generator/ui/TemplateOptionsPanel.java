/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.ui;

import com.alee.extended.layout.HorizontalFlowLayout;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.panel.WebPanel;
import com.alee.managers.style.StyleId;
import com.alee.managers.style.StyleManager;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.events.OpenDocumentEvent;
import de.elomagic.hl7inspector.platform.events.OpenToolTabDocumentEvent;
import de.elomagic.hl7inspector.platform.io.gson.GsonTool;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.actions.OpenUrlAction;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.ui.facades.RefreshUIFacade;
import de.elomagic.hl7inspector.shared.ui.facades.SelectPathFacade;
import de.elomagic.hl7inspector.tools.generator.GeneratorOptions;
import de.elomagic.hl7inspector.tools.generator.MessageGenerator;
import de.elomagic.hl7inspector.tools.generator.MessageGeneratorThread;
import de.elomagic.hl7inspector.tools.generator.generators.AbstractGeneratorOptions;
import de.elomagic.hl7inspector.tools.generator.generators.GeneratorDialog;
import de.elomagic.hl7inspector.tools.generator.generators.system.CounterValueGenerator;
import de.elomagic.hl7inspector.tools.generator.generators.system.SystemOptions;
import de.elomagic.hl7inspector.tools.generator.tabs.GeneratorToolTabPanel;
import de.elomagic.hl7inspector.tools.generator.tabs.MessageGeneratorLoggedEvent;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class TemplateOptionsPanel extends WebPanel implements RefreshUIFacade, SelectPathFacade {

    private static final Logger LOGGER = LogManager.getLogger(TemplateOptionsPanel.class);

    private final transient EventMessengerProcessor eventMessengerProcessor;
    private final transient BeanFactory beanFactory;

    private final WebComboBox cbEncoding = UIFactory.createCharsetComboBox();
    private final JLabel lbTarget = UIFactory.createLabel(LocaleTool.get("unknown"));

    private final JSpinner editAmount = new JSpinner(new SpinnerNumberModel(50, 0, 1000000, 50));
    private final JCheckBox cbDryRun = new JCheckBox();

    private final JLabel caption = UIFactory.createH2Label("template");

    private final AbstractButton btStart = new JButton(new GenericAction(e -> start(), "start", "start_service.png"));
    private final AbstractButton btStop = new JButton(new GenericAction(e ->stop(), "cancel", "stop_service.png"));

    private final TemplateMessageEditor editor;
    private final JPopupMenu popupMenu = new JPopupMenu();

    private transient GeneratorOptions options;
    private transient Message message;
    private transient MessageGeneratorThread messageGeneratorThread;

    @Autowired
    public TemplateOptionsPanel(BeanFactory beanFactory, EventMessengerProcessor eventMessengerProcessor, TemplateMessageEditor templateMessageEditor) {
        super();

        this.beanFactory = beanFactory;
        this.eventMessengerProcessor = eventMessengerProcessor;
        this.editor = templateMessageEditor;
    }

    @PostConstruct
    private void initUI() {
        // Form
        editor.setComponentPopupMenu(popupMenu);

        cbDryRun.setSelected(true);

        FormBuilder.createBuilder(this)
                //.setInsets(new Insets(0, 4, 0, 4))
                .noBorder()
                .addRow("amount", editAmount, 1, 0)
                .addRow("target_encoding", cbEncoding, 2, 0)
                .add("target", 1)
                .addRow(2, lbTarget, UIFactory.createFlatIconButton(JButton.class, "edit-12.png", e -> openTargetDialog()))
                .addRow("dry_run", cbDryRun, 2, 1)
                .addRow(createStartStopBar(), 3, 1)
                .addRow(caption, 3, 1)
                .addRow(createTemplateBar(), 3, 1)
                .addRow(editor, 3, 1, 1, 1)
                .build();

        newTemplate();
    }

    private JPanel createStartStopBar() {
        JPanel bar = new JPanel(new HorizontalFlowLayout());

        btStop.setEnabled(false);

        ButtonGroup btGroup = new ButtonGroup();
        btGroup.add(btStart);
        btGroup.add(btStop);

        bar.add(btStart);
        bar.add(btStop);

        return bar;
    }

    private JToolBar createTemplateBar() {
        JToolBar buttonBar = new JToolBar();

        buttonBar.setFloatable(false);
        buttonBar.setRollover(true);
        StyleManager.setStyleId(buttonBar, StyleId.toolbarUndecorated);

        buttonBar.add(UIFactory.createFlatIconButton(JButton.class, "document-new.png", e -> newTemplate()));
        buttonBar.add(UIFactory.createFlatIconButton(JButton.class, "document-open.png", e -> openTemplate()));
        buttonBar.add(UIFactory.createFlatIconButton(JButton.class, "document-save.png", e -> saveTemplateAs()));
        buttonBar.addSeparator();
        buttonBar.add(UIFactory.createFlatIconButton(JButton.class, "options.png", e -> openSequencerDialogs()));

        return buttonBar;
    }

    /**
     * Make me visible
     */
    public void open() {
        eventMessengerProcessor.publishEvent(new OpenDocumentEvent(this, "gen-template", this, LocaleTool.get("message_generator")));
    }

    private void openTargetDialog() {
        TargetDialog dialog = new TargetDialog();
        dialog.bindBean(options.getTarget());
        if (dialog.ask()) {
            lbTarget.setText(options.getTarget().toSummarizeString());
        }
    }

    private void newTemplate() {
        // TODO Check for changes of current

        try (InputStream in = TemplateOptionsPanel.class.getResourceAsStream("/de/elomagic/hl7inspector/tools/generator/template01.hl7gen")){
            GeneratorOptions o = GsonTool.read(in, GeneratorOptions.class);
            bindBean(o);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(LocaleTool.get("unable_to_create_new_template"), ex);
        }
    }

    private void openTemplate() {
        SimpleDialog.chooseFile(
                Configuration.getInstance().getAppFilesLastUsedFolder(),
                LocaleTool.get("load_generator_options_file_dots"),
                e -> {
                    Path file = e.getPath();

                    if (Files.exists(file)) {
                        try {
                            GeneratorOptions o = GsonTool.read(file, GeneratorOptions.class);
                            bindBean(o);
                            caption.setText(LocaleTool.get("template_arg", file.getFileName()));
                        } catch (Exception ex) {
                            LOGGER.error(ex.getMessage(), ex);
                            SimpleDialog.error(LocaleTool.get("unable_to_load_file_1arg", file), ex);
                        }
                    } else {
                        SimpleDialog.error(LocaleTool.get("file_not_found"));
                    }
                },
                GenericFileFilter.HL7GEN_FILTER
        );
    }

    private void saveTemplateAs() {
        try {
            commit();

            SimpleDialog.saveFile(
                    Configuration.getInstance().getAppFilesLastUsedFolder(),
                    LocaleTool.get("save_as_dots"),
                    e -> {
                        Path file = e.getPath();

                        if (Files.exists(file) && (SimpleDialog.confirmYesNo(LocaleTool.get("file_already_exists_overwrite")) == JOptionPane.NO_OPTION)) {
                            SimpleDialog.info(LocaleTool.get("action_canceled"));
                            return;
                        }

                        try {
                            GsonTool.write(options, file);
                            caption.setText(LocaleTool.get("template_arg", file.getFileName()));
                        } catch (Exception ex) {
                            LOGGER.error(ex.getMessage(), ex);
                            SimpleDialog.error(ex);
                        }
                    },
                    GenericFileFilter.HL7GEN_FILTER
            );
        } catch (Exception ex) {
            SimpleDialog.error(ex.getMessage());
        }
    }

    public void openSequencerDialogs() {
        GeneratorDialog dialog = new GeneratorDialog();
        dialog.bindOptions(options.getRandomOptionsSet(), options.getDateOptionsSet(), options.getStringItemsOptionsSet(), options.getFileLineOptionsSet());
        dialog.ask();

        refreshPopMenu();
    }

    public GeneratorOptions getOptions() {
        return options;
    }

    private void start() {
        try {
            commit();

            eventMessengerProcessor.publishEvent(new OpenToolTabDocumentEvent(this, GeneratorToolTabPanel.class, true));
            eventMessengerProcessor.publishEvent(new MessageGeneratorLoggedEvent(this, LocaleTool.get("starting_message_generator")));

            messageGeneratorThread = beanFactory.getBean(MessageGeneratorThread.class);
            messageGeneratorThread.addAfterThreadStartsListener(event -> btStart.setEnabled(false));
            messageGeneratorThread.addBeforeThreadEndListener(event -> btStart.setEnabled(true));
            messageGeneratorThread.setOptions(options);
            messageGeneratorThread.start();
        } catch (Exception ex) {
            SimpleDialog.error(ex.getMessage());
        }
    }

    private void stop() {
        if (messageGeneratorThread != null) {
            eventMessengerProcessor.publishEvent(new MessageGeneratorLoggedEvent(this, LocaleTool.get("initializing_message_generator_termination")));
            messageGeneratorThread.requestTermination();
        }
    }

    public void bindBean(@NotNull GeneratorOptions options) {
        this.options = options;

        message = new Message();
        message.parse(options.getTemplate());
        editor.bindMessage(message);

        lbTarget.setText(options.getTarget().toSummarizeString());
        cbEncoding.setSelectedItem(options.getTargetEncoding());
        editAmount.getModel().setValue(options.getAmount());

        refreshPopMenu();
    }

    public void commit() {
        Validate.notNull(options.getTarget().getFolder(), LocaleTool.get("output_folder_must_be_set"));

        options.setTemplate(message.asText());
        options.setAmount(NumberUtils.toInt(editAmount.getValue().toString()));
        options.setTargetEncoding((Charset) cbEncoding.getSelectedItem());
        options.setDryRun(cbDryRun.isSelected());
    }

    private void refreshPopMenu() {
        popupMenu.removeAll();
        editor.insertCopyPasteToPopupMenu(popupMenu);
        popupMenu.addSeparator();

        popupMenu.add(new GenericAction(e -> insertGenerator(new SystemOptions(CounterValueGenerator.ID))).setName("insert_counter"));

        if (!options.getSequencerSet().isEmpty()) {
            popupMenu.addSeparator();
            options.getSequencerSet()
                    .stream()
                    .map(g -> new GenericAction(e -> insertGenerator(g)).setName("insert_generator_snippet_arg", g.getDescription() == null ? g.getId() : (g.getDescription() + " (" + g.getId() + ")")))
                    .forEach(popupMenu::add);
        }

        popupMenu.addSeparator();
        popupMenu.add(new OpenUrlAction("https://freemarker.apache.org/docs/index.html", "open_apache_freemarker_manual"));
    }

    private void insertGenerator(AbstractGeneratorOptions generatorOptions) {
        try {
            String snippet = MessageGenerator.createGeneratorSnippet(generatorOptions);
            editor.insertText(snippet);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    @Override
    public void refresh(Hl7Object<?> o) {
        editor.refresh(o);
    }

    @Override
    public void selectPath(@Nullable HL7Path path) {
        editor.selectPath(path);
    }
}
