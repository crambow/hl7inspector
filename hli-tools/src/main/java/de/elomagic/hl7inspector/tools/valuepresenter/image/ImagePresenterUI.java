/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.valuepresenter.image;

import com.alee.managers.style.StyleId;
import com.alee.managers.style.StyleManager;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.tools.valuepresenter.ValueException;
import de.elomagic.hl7inspector.tools.valuepresenter.AbstractValueTypePresenterUI;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

@Component
public class ImagePresenterUI extends AbstractValueTypePresenterUI {

    private final JScrollPane scrollPane = new JScrollPane();

    private byte[] value;

    // TODO Show META data like image format in header

    @PostConstruct
    private void initUI() {
        // InitUI
        setLayout(new BorderLayout());

        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        toolbar.setRollover(true);
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "ok.png", e -> commit()));
        toolbar.addSeparator();
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "fileopen.png", e -> handleLoadFile()));
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "document-save.png", e -> handleSaveFile(GenericFileFilter.PDF_FILTER)));
        StyleManager.setStyleId(toolbar, StyleId.toolbarUndecorated);

        add(toolbar, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
    }

    @Override
    public void setValue(@Nullable String value) throws ValueException {
        setValue(value == null ? null : Base64.getDecoder().decode(value));
    }

    private void setValue(@Nullable byte[] data) throws ValueException {
        try {
            this.value = data;

            if (data == null) {
                showImage(null);
            } else {
                BufferedImage image = ImageIO.read(new ByteArrayInputStream(value));
                if (image == null) {
                    throw new IOException("Unsupported image format");
                }

                showImage(image);
            }
        } catch (Exception ex) {
            throw new ValueException(ex.getMessage(), ex);
        }
    }

    private void showImage(@Nullable Image image) {
        if (image == null) {
            scrollPane.setViewportView(new JLabel());
        } else {
            ImageIcon icon = new ImageIcon(image);
            JLabel label = new JLabel(icon);

            scrollPane.setViewportView(label);
        }
    }

    public void commit() {
        String base64 = Base64.getEncoder().encodeToString(value);
        commit(base64);
    }

    @Override
    protected void load(@NotNull Path file) throws ValueException {
        try {
            byte[] data = FileUtils.readFileToByteArray(file.toFile());
            setValue(data);
        } catch (Exception ex) {
            throw new ValueException(ex.getMessage(), ex);
        }
    }

    @Override
    protected void save(@NotNull Path file) throws IOException {
        Files.write(file, value);
    }

}
