/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.tabs;

import com.alee.extended.tab.DocumentData;
import com.alee.extended.tab.WebDocumentPane;

import de.elomagic.hl7inspector.platform.ui.components.AbstractToolTabDocumentPanel;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.ImageIcon;
import java.awt.BorderLayout;

@Component
public class GeneratorToolTabPanel extends AbstractToolTabDocumentPanel {

    private final WebDocumentPane<DocumentData<?>> documentContainer;
    private final LogTabPanel logTabPanel;
    private final PreviewTabPanel previewTabPanel;

    @Autowired
    public GeneratorToolTabPanel(LogTabPanel logTabPanel, PreviewTabPanel previewTabPanel) {
        this.logTabPanel = logTabPanel;
        this.previewTabPanel = previewTabPanel;

        documentContainer =  new WebDocumentPane<>();
    }

    @Override
    public @NotNull String getTitle() {
        return LocaleTool.get("generator");
    }

    @Override
    public @Nullable ImageIcon getIcon() {
       return null;
    }

    @PostConstruct
    private void initUI() {
        setLayout(new BorderLayout());
        add(documentContainer, BorderLayout.CENTER);

        DocumentData<LogTabPanel> treeData = new DocumentData<>(logTabPanel.getId(), logTabPanel.getTitle(), logTabPanel);
        treeData.setClosable(false);
        DocumentData<PreviewTabPanel> editorData = new DocumentData<>(previewTabPanel.getId(), previewTabPanel.getTitle(), previewTabPanel);
        editorData.setClosable(false);

        documentContainer.openDocument(treeData);
        documentContainer.openDocument(editorData);

        documentContainer.setSelected(logTabPanel.getId());
    }

    @EventListener
    public void handleMessageGeneratorLoggedEvent(PreviewGeneratedMessageEvent e) {
        documentContainer.setSelected(previewTabPanel.getId());
    }

}
