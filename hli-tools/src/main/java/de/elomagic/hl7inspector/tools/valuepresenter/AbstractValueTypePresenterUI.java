/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.valuepresenter;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinder;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.shared.events.MessageEditedEvent;
import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.shared.hl7.HL7Decoder;
import de.elomagic.hl7inspector.shared.hl7.HL7Encoder;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class AbstractValueTypePresenterUI extends JPanel implements FormBinder<Hl7Object<?>> {

    private static final Logger LOGGER = LogManager.getLogger(AbstractValueTypePresenterUI.class);

    private final Configuration configuration = Configuration.getInstance();

    @Autowired
    private transient EventMessengerProcessor eventMessengerProcessor;

    private transient Hl7Object<?> o;

    /**
     * Set the decoded value from the HL7 message.
     *
     * @param value Decoded HL7 object as string
     * @throws ValueException Thrown when something went wrong
     */
    protected abstract void setValue(@Nullable @NonNls String value) throws ValueException;

    protected abstract void load(@NotNull Path file) throws ValueException;

    /**
     * Save decoded data to file.
     *
     * If file exist it must be overwritten.
     *
     * @param file The file
     */
    protected abstract void save(Path file) throws IOException;

    /**
     * Set the value from the HL7 message.
     *
     * @param bean HL7 object
     * @throws FormBinderException Thrown when something went wrong
     */
    @Override
    public void bindBean(@NotNull Hl7Object<?> bean) {
        try {
            this.o = bean;

            String v = o.asText();

            if (StringUtils.isNotBlank(v)) {
                v = HL7Decoder.decodeString(v, o.getDelimiters());
            }

            setValue(v);
        } catch (Exception ex) {
            throw new FormBinderException(ex.getMessage(), ex);
        }
    }

    protected void commit(@Nullable String value) {

        String v = HL7Encoder.encodeString(value, o.getDelimiters());

        o.parse(v);

        Message m = o.getRoot();
        HL7Path p = o.toPath();

        eventMessengerProcessor.publishEvent(new MessageEditedEvent(this, m, o));
        eventMessengerProcessor.publishEvent(new MessageNodeSelectionChangedEvent(this, m, p, null));
    }

    protected void activateUI() {
        // noop
    }

    protected void handleSaveFile(@NotNull GenericFileFilter filter) {
        SimpleDialog.saveFile(
                configuration.getAppFilesLastUsedFolder(),
                null,
                e -> {
                    Path file = e.getPath();
                    if (Files.exists(file) && SimpleDialog.confirmYesNo(LocaleTool.get("file_already_exists_overwrite")) != JOptionPane.YES_OPTION) {
                        return;
                    }

                    try {
                        save(file);
                    } catch (Exception ex) {
                        SimpleDialog.error(ex.getMessage(), ex);
                        LOGGER.error(ex.getMessage(), e);
                    }
                },
                filter
        );
    }

    protected void handleLoadFile(@Nullable FileFilter... fileFilters) {
        SimpleDialog.chooseFile(
                null,
                e -> {
                    try {
                        load(e.getPath());
                    } catch (Exception ex) {
                        LOGGER.error(ex.getMessage(), ex);
                        SimpleDialog.error(LocaleTool.get("unable_to_load_file_1arg", ex.getMessage()));
                    }
                },
                fileFilters
        );
    }

}
