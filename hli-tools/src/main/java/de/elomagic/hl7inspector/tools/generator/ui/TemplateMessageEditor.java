/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.ui;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.DragAndDropService;
import de.elomagic.hl7inspector.shared.ui.AbstractMessageEditor;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Component;

@Component("TemplateMessageEditor")
public class TemplateMessageEditor extends AbstractMessageEditor {

    public TemplateMessageEditor(BeanFactory beanFactory, EventMessengerProcessor eventMessengerProcessor, DragAndDropService dragAndDropService) {
        super(beanFactory, eventMessengerProcessor, dragAndDropService);
    }

}
