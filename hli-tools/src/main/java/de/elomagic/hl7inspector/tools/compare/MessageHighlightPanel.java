/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.compare;

import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.hl7.HL7Tool;
import de.elomagic.hl7inspector.shared.hl7.model.Message;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.text.html.HTMLDocument;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;

@Component
@Scope("prototype")
public class MessageHighlightPanel extends JScrollPane {

    private static final Logger LOGGER = LogManager.getLogger(MessageHighlightPanel.class);

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private final transient EventMessengerProcessor eventMessengerProcessor;

    public static final String CLASS_NWB = "nwb";
    public static final String CLASS_DEL = "del";
    public static final String CLASS_DIFF = "diff";
    public static final String CLASS_PRESENT = "present";

    private int suppressCaretChangeEvent = 0;
    private transient Message message;
    private JEditorPane editMessage;

    public @Autowired MessageHighlightPanel(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @PostConstruct
    private void initUI() {
        setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 0));

        Font font = configuration.getAppEditorTextFont();

        editMessage = new JEditorPane();
        editMessage.putClientProperty(StyleId.STYLE_PROPERTY, StyleId.editorpane);
        editMessage.setContentType("text/html");
        editMessage.setEditable(false);

        Color c = editMessage.getForeground();
        String rgb = String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());

        ((HTMLDocument)editMessage.getDocument()).getStyleSheet().addRule("body { font-family: " + font.getFamily() + "; font-size: " + font.getSize() + "pt; color: " + rgb + "; }");
        ((HTMLDocument)editMessage.getDocument()).getStyleSheet().addRule(".del { white-space: nowrap; cursor: default; }");
        ((HTMLDocument)editMessage.getDocument()).getStyleSheet().addRule(".diff { white-space: nowrap; color: white; background-color: #f0ad4e; cursor: pointer; }");
        ((HTMLDocument)editMessage.getDocument()).getStyleSheet().addRule(".present { white-space: nowrap; color: white; background-color: #5cb85c; cursor: pointer; }");
        ((HTMLDocument)editMessage.getDocument()).getStyleSheet().addRule(".nwb { white-space: nowrap; }");
        // Will be called by JDK twice when set text. One for removing old test and one for settings new text. Dam it.
        editMessage.addCaretListener(e -> {
            if (suppressCaretChangeEvent == 0) {
                String m = message == null ? null : message.asText().replace('\r', '\n');

                Point p = m == null ? null : getCursorPosition(m, e.getDot());
                LOGGER.trace("Cursor position: {}", p);

                HL7Path path = p == null ? null : HL7Tool.getHL7Path(m, p);
                LOGGER.trace("HL7 path: {}", path);
                fireCursorChangedEvent(new MessageNodeSelectionChangedEvent(this, message, path, p));
            }
        });
        setViewportView(editMessage);
    }

    public void setText(@Nullable Message message, @NotNull String text) {
        this.message = message;

        suppressCaretChangeEvent++;
        try {
            editMessage.setText(text);
        } finally {
            suppressCaretChangeEvent--;
        }
    }

    private void fireCursorChangedEvent(MessageNodeSelectionChangedEvent event) {
        eventMessengerProcessor.publishEvent(event);
    }

    @NotNull
    private Point getCursorPosition(String text, int totalPosition) {
        String subtext = text.substring(0, Math.min(totalPosition, text.length()));
        int y = StringUtils.countMatches(subtext, '\n');
        int lastIndex = subtext.lastIndexOf('\n');

        return new Point(totalPosition - lastIndex - 1, y);
    }

    JEditorPane getEditorPane() {
        return editMessage;
    }

}
