/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.receiver;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.security.CertificateTool;
import de.elomagic.hl7inspector.platform.utils.thread.ControllableThread;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadEvent;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadStatusListener;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions.StreamFormat;
import de.elomagic.hl7inspector.shared.configuration.ReceiveOptions;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCSException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Scope("prototype")
public class ReceiveServerSocketThread extends ControllableThread<ThreadEvent<ReceiveServerSocketThread>, ThreadEvent<ReceiveServerSocketThread>> {

    private static final Logger LOGGER = LogManager.getLogger(ReceiveServerSocketThread.class);

    private final SharedConfiguration configuration = SharedConfiguration.getInstance();

    private final List<ThreadStatusListener> listener = new ArrayList<>();
    private final AtomicBoolean terminating = new AtomicBoolean(false);
    private final Set<ReceiveSocketThread> socketThreadSet = ConcurrentHashMap.newKeySet();
    private ImportOptions options;
    private ReceiveOptions receiveOptions;
    private ServerSocket server;

    private final BeanFactory beanFactory;

    public @Autowired ReceiveServerSocketThread(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @PostConstruct
    private void init() {
        options = configuration.getImportOptions();
        options.setSourceDescription(LocaleTool.get("ip_socket"));
        options.setImportMode(StreamFormat.FRAMED);
    }

    public ReceiveOptions getReceiveOptions() {
        return receiveOptions;
    }

    public void setReceiveOptions(ReceiveOptions receiveOptions) {
        this.receiveOptions = receiveOptions;
    }

    public Frame getFrame() {
        return options.getFrame();
    }

    public void setFrame(Frame f) {
        options.setFrame(f);
    }

    public ImportOptions getOptions() {
        return options;
    }

    public void setOptions(ImportOptions o) {
        options = o;
    }

    @Override
    public void requestTermination() {
        if(!terminating.get()) {
            fireStatusEvent(LocaleTool.get("shutting_down_receive_server_dots"));
        }

        terminating.set(true);

        // Close threads & sockets
        socketThreadSet.forEach(ReceiveSocketThread::requestTermination);
        socketThreadSet.clear();

        // Shutdown server
        if(server != null && !server.isClosed()) {
            LOGGER.debug("Closing server socket...");
            try {
                server.close();
            } catch (Exception ex) {
                LOGGER.warn(ex.getMessage(), ex);
                fireStatusEvent((ex.getMessage() != null) ? ex.getMessage() : ex.toString());
            }
        }
    }

    @Override
    public void run() {
        fireAfterStartListener(new ThreadEvent<>(this));
        try {
            fireStatusEvent(LocaleTool.get("listening_on_port_1arg", receiveOptions.getPort()));
            server = createServerSocket();

            while(!terminating.get()) {
                try {
                    LOGGER.debug("Listening on port {}", receiveOptions.getPort());
                    Socket socket = server.accept();

                    ReceiveSocketThread receiveSocketThread = createReceiveSocketThread(socket);
                    socketThreadSet.add(receiveSocketThread);
                    receiveSocketThread.addBeforeThreadEndListener(e -> socketThreadSet.remove(e.getSource()));
                    receiveSocketThread.addListeners(listener);
                    receiveSocketThread.start();
                } catch(Exception ex) {
                    LOGGER.error(ex.getMessage(), ex);
                    if(!terminating.get()) {
                        fireStatusEvent(LocaleTool.get("arg", ex.toString()));
                    }
                }
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            fireStatusEvent(ex.getMessage() != null ? ex.getMessage() : ex.toString());
        } finally {
            fireStatusEvent(LocaleTool.get("receive_server_stopped"));
            fireBeforeEndListener(new ThreadEvent<>(this));
        }
    }

    /**
     * Returns local server port.
     *
     * @return Returns the local bound port or -1 when not bound.
     */
    public int getLocalPort() {
        return server.getLocalPort();
    }

    private ServerSocket createServerSocket() throws IOException, GeneralSecurityException, PKCSException, OperatorCreationException {
        if (receiveOptions.getTlsOptions().isEnabled()) {
            SSLContext context = CertificateTool.createSSLContext(
                    receiveOptions.getTlsOptions().getPrivateKey(),
                    receiveOptions.getTlsOptions().getPassword(),
                    receiveOptions.getTlsOptions().isClientAuthenticationEnabled() ? receiveOptions.getTlsOptions().getClientAuthenticationCertificates() : null
            );

            SSLServerSocket serverSocket = (SSLServerSocket)context.getServerSocketFactory().createServerSocket(receiveOptions.getPort());
            serverSocket.setNeedClientAuth(receiveOptions.getTlsOptions().isClientAuthenticationEnabled());

            return serverSocket;
        } else {
            return new ServerSocket(receiveOptions.getPort());
        }
    }

    private ReceiveSocketThread createReceiveSocketThread(@NotNull Socket socket) throws IOException {
        ReceiveSocketThread receiveSocketThread = beanFactory.getBean(ReceiveSocketThread.class);
        receiveSocketThread.setSocket(socket);
        receiveSocketThread.setOptions(options);

        return receiveSocketThread;
    }

    public void addListener(ThreadStatusListener value) {
        listener.add(value);
    }

    public void removeListener(ThreadStatusListener value) {
        listener.remove(value);
    }

    protected void fireStatusEvent(String text) {
        listener.forEach(l -> l.status(this, text));
    }

}
