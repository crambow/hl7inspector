/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators.random;

import de.elomagic.hl7inspector.tools.generator.generators.AbstractValueGenerator;
import de.elomagic.hl7inspector.tools.generator.GeneratorException;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class RandomValueGenerator extends AbstractValueGenerator<RandomOptions> {

    private final Random random = new Random();
    private Integer nextValue;

    public RandomValueGenerator(@NotNull RandomOptions options) {
        super(options);
    }

    @Override
    public String toString() {
        return Integer.toString(nextValue);
    }

    @Override
    public void next() {
        nextValue = random.nextInt(getOptions().getHigh() - getOptions().getLow()) + getOptions().getLow();
    }

    @Override
    public void close() throws GeneratorException {
        // noop
    }

}
