/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.thread.ControllableThread;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadEvent;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.io.MessageFileIO;
import de.elomagic.hl7inspector.tools.generator.generators.AbstractValueGenerator;
import de.elomagic.hl7inspector.tools.generator.generators.GeneratorFactory;
import de.elomagic.hl7inspector.tools.generator.tabs.MessageGeneratorLoggedEvent;
import de.elomagic.hl7inspector.tools.generator.tabs.PreviewGeneratedMessageEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class MessageGeneratorThread extends ControllableThread {

    private static final Logger LOGGER = LogManager.getLogger(MessageGeneratorThread.class);

    private final EventMessengerProcessor eventMessengerProcessor;

    private final AtomicBoolean terminating = new AtomicBoolean(false);
    private GeneratorOptions options;
    private int counter;

    public @Autowired MessageGeneratorThread(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @Override
    public void run() {
        eventMessengerProcessor.publishEvent(new MessageGeneratorLoggedEvent(this, LocaleTool.get("message_generator_started")));
        eventMessengerProcessor.publishEvent(new MessageGeneratorLoggedEvent(this, LocaleTool.get("messages_to_proceed_arg", options.getAmount())));
        fireAfterStartListener(new ThreadEvent<>(this));

        try {
            MessageGenerator generator = new MessageGenerator();
            generator.setCustomSequencer(prepareSequencers());
            generator.setTemplate(options.getTemplate());
            generator.setDefaultEncoding(options.getTargetEncoding());
            generator.setDryRun(options.isDryRun());

            for (counter = 0; counter < options.getAmount() && !terminating.get(); counter++) {
                Message message = generator.nextMessage();

                exportMessage(message);

                if (counter+1 % 100 == 1) {
                    eventMessengerProcessor.publishEvent(new MessageGeneratorLoggedEvent(this, LocaleTool.get("message_generated_arg", counter)));
                }

                if (options.isDryRun()) {
                    break;
                }
            }
            eventMessengerProcessor.publishEvent(new MessageGeneratorLoggedEvent(this, LocaleTool.get("message_generated_arg", counter)));
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            eventMessengerProcessor.publishEvent(new MessageGeneratorLoggedEvent(this, LocaleTool.get("internal_error_occur_arg", ex.getMessage())));
        }

        // TODO publishing thread stopped to switch start/stop buttons
        fireBeforeEndListener(new ThreadEvent<>(this));
        eventMessengerProcessor.publishEvent(new MessageGeneratorLoggedEvent(this, LocaleTool.get("message_generator_stopped")));
    }

    public void requestTermination() {
        terminating.set(true);
    }

    public void setOptions(@NotNull GeneratorOptions options) {
        this.options = options;
    }

    @NotNull
    private Set<AbstractValueGenerator<?>> prepareSequencers() {
        return options.getSequencerSet()
                .stream()
                .map(GeneratorFactory::create)
                .collect(Collectors.toSet());
    }

    private void exportMessage(@NotNull Message message) throws GeneratorException {
        if (options.isDryRun()) {
            eventMessengerProcessor.publishEvent(new PreviewGeneratedMessageEvent(this, message));
        } else  if (options.getTarget().isUseMllp()) {
            sendMessage(message);
        } else {
            writeMessage(message);
        }
    }

    private void writeMessage(@NotNull Message message) throws GeneratorException {
        Path file = MessageFileIO.createFilename(counter, options.getTarget().getMinDigits(), options.getTarget().getFolder(), options.getTarget().getFilenamePattern());

        try {
            Files.writeString(file, message.asText(), options.getTargetEncoding());
        } catch (Exception ex) {
            throw new GeneratorException(ex.getMessage(), ex);
        }
    }

    private void sendMessage(@NotNull Message message) {
        throw new IllegalStateException("Currently not supported yet.");
    }

}
