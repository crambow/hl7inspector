/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators.date;

import com.alee.extended.link.UrlLinkAction;
import com.alee.extended.link.WebLink;
import com.alee.managers.icon.Icons;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyChangeListener;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.tools.generator.generators.AbstractSequencerUI;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class DateUI extends AbstractSequencerUI<DateOptions> {

    private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private final DateFormat formatter = new SimpleDateFormat(DATETIME_FORMAT);

    private transient DateOptions options;

    private final JFormattedTextField editId;
    private final JTextField editDescription;
    private final JFormattedTextField editLow;
    private final JFormattedTextField editHigh;
    private final JTextField editFormat;
    private final JLabel lbExample;

    public DateUI() {
        editId = new JFormattedTextField();
        editDescription = new JTextField();
        editLow = new JFormattedTextField();
        editLow.setColumns(19);
        editLow.getDocument().addDocumentListener((SimplifyChangeListener) e -> refreshExample());
        editLow.setToolTipText(LocaleTool.get("required_pattern_arg", DATETIME_FORMAT));

        editHigh = new JFormattedTextField();
        editHigh.setColumns(19);
        editHigh.getDocument().addDocumentListener((SimplifyChangeListener) e -> refreshExample());
        editHigh.setToolTipText(LocaleTool.get("required_pattern_arg", DATETIME_FORMAT));

        editFormat = new JTextField();
        editFormat.getDocument().addDocumentListener((SimplifyChangeListener) e -> refreshExample());

        lbExample = UIFactory.createLabel("");
        lbExample.setFont(lbExample.getFont().deriveFont(Font.BOLD));

        FormBuilder.createBuilder(this)
                .noBorder()
                .addRow("id", UIFactory.wrapFormattedTextField(editId, REGEX_ID), 1, 1)
                .addRow("description", editDescription, 1, 1)
                .addRow("start", UIFactory.wrapFormattedTextField(editLow, formatter), 1, 0)
                .addRow("end", UIFactory.wrapFormattedTextField(editHigh, formatter), 1, 0)
                .addRow("date_pattern", editFormat, 1, 1)
                .addRowSpacer(10)
                .addRow(new WebLink(Icons.globe, "Open date format description externally", new UrlLinkAction("https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/text/SimpleDateFormat.html")), 2, 0)
                .addRowSpacer(10)
                .addRow("example", lbExample, 1, 0)
                .addRowSpacer();
    }

    @Override
    public @Nls @NotNull String getTitle() {
        return "date_generator";
    }

    public void commit(DateOptions o) throws ParseException {
        o.setId(editId.getText());
        o.setDescription(editDescription.getText());
        o.setDateBegin(formatter.parse(editLow.getText()));
        o.setDateEnd(formatter.parse(editHigh.getText()));
        o.setFormat(editFormat.getText());
    }

    private void refreshExample() {
        try {
            DateOptions o = new DateOptions();
            commit(o);

            DateValueGenerator dg = new DateValueGenerator(o);
            dg.next();

            lbExample.setText(dg.toString());
        } catch (Exception ex) {
            lbExample.setText(LocaleTool.get("invalid_input_please_correct_them"));
        }
    }

    @Override
    public void bindBean(@NotNull DateOptions options) {
        this.options = options;

        editId.setText(options.getId());
        editLow.setText(options.getDateBegin() == null ? formatter.format(Timestamp.valueOf(LocalDateTime.now().minus(80, ChronoUnit.YEARS))) : formatter.format(options.getDateBegin()));
        editHigh.setText(options.getDateEnd() == null ? formatter.format(Timestamp.valueOf(LocalDateTime.now())) : formatter.format(options.getDateEnd()));
        editFormat.setText(options.getFormat());
    }

    @Override
    public boolean validateForm() throws FormBinderException {
        try {
            Validate.notBlank(editId.getText());

            DateOptions o = new DateOptions();
            commit(o);

            DateValueGenerator dg = new DateValueGenerator(o);
            dg.next();
        } catch (Exception ex) {
            throw new FormBinderException(LocaleTool.get("invalid_input_please_correct_them"), ex);
        }

        return true;
    }

    @Override
    public void commit() throws FormBinderException {
        try {
            commit(options);
        } catch (Exception ex) {
            throw new FormBinderException(ex.getMessage(), ex);
        }
    }

}
