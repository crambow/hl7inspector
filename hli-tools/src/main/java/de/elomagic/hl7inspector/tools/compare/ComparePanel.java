/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.compare;

import com.alee.extended.panel.WebComponentPane;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.events.OpenDocumentEvent;
import de.elomagic.hl7inspector.platform.ui.DragAndDropListener;
import de.elomagic.hl7inspector.platform.ui.FilesDroppedEvent;
import de.elomagic.hl7inspector.platform.ui.TextDroppedEvent;
import de.elomagic.hl7inspector.platform.utils.Html;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.hl7.model.Component;
import de.elomagic.hl7inspector.shared.hl7.model.*;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.dnd.DropTarget;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.List;

@org.springframework.stereotype.Component
public class ComparePanel extends WebComponentPane implements DragAndDropListener {

    private static final String SPAN = "span";

    private MessageHighlightPanel messagePanel1;
    private MessageHighlightPanel messagePanel2;

    private final transient BeanFactory beanFactory;
    private final transient EventMessengerProcessor eventMessengerProcessor;

    public @Autowired ComparePanel(BeanFactory beanFactory, EventMessengerProcessor eventMessengerProcessor) {
        this.beanFactory = beanFactory;
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @PostConstruct
    private void initUI() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createEmptyBorder());

        messagePanel1 = beanFactory.getBean(MessageHighlightPanel.class);
        messagePanel1.getHorizontalScrollBar().addAdjustmentListener(e -> messagePanel2.getHorizontalScrollBar().setValue(messagePanel1.getHorizontalScrollBar().getValue()));

        messagePanel2 = beanFactory.getBean(MessageHighlightPanel.class);
        messagePanel2.getHorizontalScrollBar().addAdjustmentListener(e -> messagePanel1.getHorizontalScrollBar().setValue(messagePanel2.getHorizontalScrollBar().getValue()));

        add(messagePanel1, createGridBagConstraint(0));
        add(messagePanel2, createGridBagConstraint(1));

        DropTarget dropTarget1 = new DropTarget();
        dropTarget1.setComponent(messagePanel1.getEditorPane());

        DropTarget dropTarget2 = new DropTarget();
        dropTarget2.setComponent(messagePanel2.getEditorPane());
    }

    private static GridBagConstraints createGridBagConstraint(int row) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = row;
        c.fill = 1;
        c.weightx = 1;
        c.weighty = 0.5;

        return c;
    }

    public void compare(@NotNull Message message1, @NotNull Message message2) {
        String id = MessageFormat.format("compare-{0}-{1}", message1.getId(), message2.getId());

        eventMessengerProcessor.publishEvent(new OpenDocumentEvent(this, id, this, LocaleTool.get("compare")));

        StringBuilder diff1 = new StringBuilder();
        StringBuilder diff2 = new StringBuilder();

        diff1.append("<html>");
        diff2.append("<html>");

        compare(diff1, diff2, message1, message2);

        diff1.append("</html>");
        diff2.append("</html>");

        messagePanel1.setText(message1, diff1.toString());
        messagePanel2.setText(message2, diff2.toString());
    }

    void compare(@NotNull StringBuilder diffResults1, @NotNull StringBuilder diffResult2, @NotNull Message message1, @NotNull Message message2) {
        compareSegments(diffResults1, diffResult2, message1, message2);
    }

    private void compareSegments(@NotNull StringBuilder diffResults1, @NotNull StringBuilder diffResults2, @NotNull Message message1, @NotNull Message message2) {
        int max = maxItems(message1, message2);

        for (int i = 0; i < max; i++) {
            Segment seg1 = message1.size() > i ? message1.get(i) : null;
            Segment seg2 = message2.size() > i ? message2.get(i) : null;
            // TODO https://de.wikipedia.org/wiki/Levenshtein-Distanz http://www.let.rug.nl/kleiweg/lev/

            // Surround segments always
            if (!isItemEqual(diffResults1, diffResults2, seg1, seg2)) {
                compareRepetitionFields(diffResults1, diffResults2, seg1, seg2);
            }
            diffResults1.append("<br/>\n");
            diffResults2.append("<br/>\n");
        }
    }

    private void compareRepetitionFields(@NotNull StringBuilder diffResults1, @NotNull StringBuilder diffResults2, @Nullable Segment segment1, @Nullable Segment segment2) {
        int max = maxItems(segment1, segment2);

        // Build field nodes/rows
        for (int i = 0; i < max; i++) {
            RepetitionField repetitionField1 = segment1 != null && segment1.size() > i ? segment1.get(i) : null;
            RepetitionField repetitionField2 = segment2 != null && segment2.size() > i ? segment2.get(i) : null;

            if (!isItemEqual(diffResults1, diffResults2, repetitionField1, repetitionField2)) {
                compareFields(diffResults1, diffResults2, repetitionField1, repetitionField2);
            }

            int index = segment1 == null ? segment2.getIndex() : segment1.getIndex();

            if ((index != 0) || (i > 1)) {
                delimiter(diffResults1, diffResults2, segment1, segment2, i);
            }

        }

    }

    private void compareFields(@NotNull StringBuilder diffResults1, @NotNull StringBuilder diffResults2, @Nullable RepetitionField repetitionField1, @Nullable RepetitionField repetitionField2) {
        int max = maxItems(repetitionField1, repetitionField2);

        // Build field nodes/rows
        for (int i = 0; i < max; i++) {
            Field field1 = repetitionField1 != null && repetitionField1.size() > i ? repetitionField1.get(i) : null;
            Field field2 = repetitionField2 != null && repetitionField2.size() > i ? repetitionField2.get(i) : null;
            if (!isItemEqual(diffResults1, diffResults2, field1, field2)) {
                compareComponents(diffResults1, diffResults2, field1, field2);
            }

            delimiter(diffResults1, diffResults2, repetitionField1, repetitionField2, i);
        }
    }

    private void compareComponents(@NotNull StringBuilder diffResults1, @NotNull StringBuilder diffResults2, @Nullable Field field1, @Nullable Field field2) {
        int max = maxItems(field1, field2);

                // Build field nodes/rows
        for (int i = 0; i < max; i++) {
            Component component1 = field1 != null && field1.size() > i ? field1.get(i) : null;
            Component component2 = field2 != null && field2.size() > i ? field2.get(i) : null;

            if (!isItemEqual(diffResults1, diffResults2, component1, component2)) {
                compareSubComponents(diffResults1, diffResults2, component1, component2);
            }

            delimiter(diffResults1, diffResults2, field1, field2, i);
        }
    }

    private void compareSubComponents(@NotNull StringBuilder diffResults1, @NotNull StringBuilder diffResults2, @Nullable Component component1, @Nullable Component component2) {
        int max = maxItems(component1, component2);

        // Build field nodes/rows
        for (int i = 0; i < max; i++) {
            Subcomponent subcomponent1 = component1 != null && component1.size() > i ? component1.get(i) : null;
            Subcomponent subcomponent2 = component2 != null && component2.size() > i ? component2.get(i) : null;
            if (!isItemEqual(diffResults1, diffResults2, subcomponent1, subcomponent2)) {
                diffResults1.append(Html.surroundWithElement(asText(subcomponent1), SPAN, "diff"));
                diffResults2.append(Html.surroundWithElement(asText(subcomponent2), SPAN, "diff"));
            }

            delimiter(diffResults1, diffResults2, component1, component2, i);
        }
    }

    private boolean isItemEqual(@NotNull StringBuilder diffResults1, @NotNull StringBuilder diffResults2, @Nullable Hl7Object<?> item1, @Nullable Hl7Object<?> item2) {
        String c1 = asText(item1);
        String c2 = asText(item2);

        if (c1.equals(c2)) {
            diffResults1.append(Html.surroundWithElement(c1, SPAN, "nwb"));
            diffResults2.append(Html.surroundWithElement(c2, SPAN, "nwb"));
            return true;
        }

        // Syntax like SEG-Field(Repetition Field).Component.Subcomponent
        if (StringUtils.isBlank(c1)) {
            diffResults1.append("<span/>");
            diffResults2.append(Html.surroundWithElement(c2, SPAN, "present"));
            return true;
        }
        if (StringUtils.isBlank(c2)) {
            diffResults1.append(Html.surroundWithElement(c1, SPAN, "present"));
            diffResults2.append("<span/>");
            return true;
        }

        return false;
    }

    private int maxItems(@Nullable Hl7Object<?> o1, @Nullable Hl7Object<?> o2) {
        int size1 = o1 == null ? 0 : o1.size();
        int size2 = o2 == null ? 0 : o2.size();

        return Math.max(size1, size2);
    }

    private void delimiter(@NotNull StringBuilder diffResults1, @NotNull StringBuilder diffResults2, @Nullable Hl7Object<?> o1, @Nullable Hl7Object<?> o2, int index) {
        if ((o1 != null) && (index+1 < o1.size())) {
            diffResults1.append(Html.surroundWithElement(o1.getSubDelimiter(), SPAN, "del"));
        }
        if ((o2 != null) && (index+1 < o2.size())) {
            diffResults2.append(Html.surroundWithElement(o2.getSubDelimiter(), SPAN, "del"));
        }
     }

    private String asText(Hl7Object<?> o) {
        return o == null ? "" : o.asText();
    }

    @Override
    public void handleStringDropped(java.awt.Component target, String text) {
        eventMessengerProcessor.publishEvent(new TextDroppedEvent(this, text));
    }

    @Override
    public void handleFilesDropped(java.awt.Component target, List<Path> files) {
        eventMessengerProcessor.publishEvent(new FilesDroppedEvent(this, files));
    }

}
