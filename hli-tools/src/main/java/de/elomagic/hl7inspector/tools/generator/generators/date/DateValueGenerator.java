/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators.date;

import de.elomagic.hl7inspector.tools.generator.generators.AbstractValueGenerator;
import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class DateValueGenerator extends AbstractValueGenerator<DateOptions> {

    private DateFormat formatter;
    private String nextValue;

    public DateValueGenerator(@NotNull DateOptions options) {
       super(options);
    }

    @Override
    public void next() {
        long startMillis = getOptions().getDateBegin().getTime();
        long endMillis = getOptions().getDateEnd().getTime();
        long randomMillisSinceEpoch = ThreadLocalRandom
                .current()
                .nextLong(startMillis, endMillis);

        Date date = new Date(randomMillisSinceEpoch);

        if (formatter == null) {
            formatter = new SimpleDateFormat(getOptions().getFormat());
        }

        nextValue = formatter.format(date);
    }

    @Override
    public void close() {
        // noop
    }

    @Override
    public String toString() {
        return nextValue;
    }

}
