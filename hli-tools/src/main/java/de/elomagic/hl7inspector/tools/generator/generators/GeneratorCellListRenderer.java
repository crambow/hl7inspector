/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators;

import com.alee.laf.list.ListCellParameters;
import com.alee.laf.list.WebListCellRenderer;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.tools.generator.generators.date.DateOptions;
import de.elomagic.hl7inspector.tools.generator.generators.fileline.FileLineOptions;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomOptions;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsOptions;

import javax.swing.BorderFactory;
import javax.swing.JList;
import java.awt.Component;
import java.text.MessageFormat;

public class GeneratorCellListRenderer extends WebListCellRenderer<AbstractGeneratorOptions, JList<AbstractGeneratorOptions>, ListCellParameters<AbstractGeneratorOptions, JList<AbstractGeneratorOptions>>> {

    private static final long serialVersionUID = 3772768483753533378L;

    private static final String PATTERN = "<html><b>{0}</b><br>Id: {1}</<html>";

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        setBorder(BorderFactory.createEmptyBorder(6,6,6,6));
        AbstractGeneratorOptions sequencer = (AbstractGeneratorOptions)value;

        setText(MessageFormat.format(PATTERN, getGeneratorTypeName(sequencer), sequencer.getId()));

        return this;
    }

    private String getGeneratorTypeName(AbstractGeneratorOptions sequencer) {
        if (RandomOptions.class.isAssignableFrom(sequencer.getClass())) {
            return LocaleTool.get("random_generator");
        } else if (StringItemsOptions.class.isAssignableFrom(sequencer.getClass())) {
            return LocaleTool.get("string_items_generator");
        } else if (FileLineOptions.class.isAssignableFrom(sequencer.getClass())) {
            return LocaleTool.get("file_line_generator");
        } else if (DateOptions.class.isAssignableFrom(sequencer.getClass())) {
            return LocaleTool.get("date_generator");
        } else {
            return sequencer.getClass().getSimpleName();
        }
    }
}
