/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.ui;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.actions.ChooseDirectoryAction;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.platform.ui.bindings.FormDialog;
import de.elomagic.hl7inspector.platform.ui.listener.PathSelectedListener;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyDocumentListener;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.io.MessageFileIO;
import de.elomagic.hl7inspector.tools.generator.TargetOptions;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import java.awt.BorderLayout;
import java.awt.Font;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TargetDialog extends FormDialog<TargetOptions> {

    private static final String REGEX_VAR_COUNTER = ".*(\\$\\{counter}).*";

    private transient TargetOptions options;

    private final JTextField edFolder = new JTextField("");
    private final JFormattedTextField edPattern = new JFormattedTextField("");
    private final JSpinner edMinDigits = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
    private final JLabel lbExample = UIFactory.createLabel("");

    public TargetDialog() {
        super();

        getBanner().setVisible(false);

        setTitle(LocaleTool.get("output_options"));
        setModal(true);

        edFolder.getDocument().addDocumentListener((SimplifyDocumentListener) e -> refreshExample());
        edPattern.getDocument().addDocumentListener((SimplifyDocumentListener) e -> refreshExample());

        edMinDigits.addChangeListener(e -> refreshExample());

        lbExample.setFont(lbExample.getFont().deriveFont(Font.BOLD));

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                //.addRow(btFolder, 3, 1)
                .addColumnSpacer(20)
                .add("folder", edFolder, 1, 1)
                .setColumnOffset(1)
                .addRow(new JButton(new ChooseDirectoryAction(Paths.get(edFolder.getText()), this::handlePathSelectedEvent)), 1, 0)
                .addRow("filename", UIFactory.wrapFormattedTextField(edPattern, REGEX_VAR_COUNTER), 3, 1)
                .addRow("digits", edMinDigits, 1, 0)
                .addRow("example", lbExample, 2,0)

                .addRowSpacer(10)
                .setColumnOffset(0)

                //.addRow(btSocket, 3, 1)
                //.setColumnOffset(1)
                //.addRow("hostname_and_port_of_destination", 2)
                //.addRow(cbDest, 3, 1)
                //.addRow(cbReuse, 2, 1)

                .addRowSpacer()
                .build();

        getContentPane().add(grid, BorderLayout.CENTER);

        setSize(700, getPreferredSize().height);
    }

    private void handlePathSelectedEvent(PathSelectedListener.PathSelectedEvent e) {
        edFolder.setText(e.getPath().toString());
        refreshExample();
    }

    @Override
    public void bindBean(@NotNull TargetOptions options) {
        this.options = options;

        edFolder.setText(options.getFolder() == null ? "" : options.getFolder().toString());
        edPattern.setText(options.getFilenamePattern());
        edMinDigits.getModel().setValue(options.getMinDigits());
    }

    private void refreshExample() {
        lbExample.setText(MessageFileIO.createFilename(
                42,
                NumberUtils.toInt(edMinDigits.getValue().toString()),
                Paths.get(edFolder.getText()),
                edPattern.getText()).toString());
    }

    @Override
    public boolean validateForm() {
        try {
            Validate.notBlank(edFolder.getText(), LocaleTool.get("destination_folder_not_set"));
            Path.of(edFolder.getText()).toUri();

            Validate.notBlank(edPattern.getText(), LocaleTool.get("invalid_filename"));

            if (!edPattern.getText().contains(MessageFileIO.VAR_COUNTER)) {
                throw new IllegalArgumentException(LocaleTool.get("counter_variable_in_filename_not_found"));
            }
        } catch(Exception e) {
            throw new FormBinderException(e.getMessage(), e);
        }

        return true;
    }

    @Override
    public void commit() {
        options.setFolder(Paths.get(edFolder.getText()));
        options.setFilenamePattern(edPattern.getText());
        options.setMinDigits(NumberUtils.toInt(edMinDigits.getValue().toString()));
    }

}