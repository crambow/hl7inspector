/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.sender.ui;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.security.CertificateTool;
import de.elomagic.hl7inspector.platform.utils.security.PemTool;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.shared.configuration.TlsSendOptions;

import org.jetbrains.annotations.NotNull;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.security.cert.CertificateException;

public class CertificatesDialog extends MyBaseDialog {

    private transient TlsSendOptions options;

    private final JCheckBox cbTlsEnabled = new JCheckBox(LocaleTool.get("tls_enabled"));
    private final JTextArea edCertificates = new JTextArea();

    private final JCheckBox cbClientAuthEnabled = new JCheckBox(LocaleTool.get("tls_enabled"));
    private final JPasswordField edClientAuthPassword = new JPasswordField();
    private final JTextArea edClientAuthPrivateKey = new JTextArea();

    public CertificatesDialog() {
        setTitle(LocaleTool.get("configure_trusted_certificates"));
        setModal(true);
        setSize(new Dimension(720,640));
        getBanner().setVisible(false);

        JTabbedPane tabPanel = new JTabbedPane();
        tabPanel.add(LocaleTool.get("server_certificates"), createServerCertificatesPanel());
        tabPanel.add(LocaleTool.get("client_authentication"), createClientAuthenticationPanel());

        getContentPane().add(tabPanel, BorderLayout.CENTER);
    }

    private JPanel createServerCertificatesPanel() {
        JScrollPane scrollPane = new JScrollPane(edCertificates);

        edCertificates.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

        return FormBuilder.createBuilder()
                .setBorder(BorderFactory.createEmptyBorder(6, 0, 0,0))
                .addRow(cbTlsEnabled, 1, 1)
                .addRow("pem_certificates", 1)
                .addRow(scrollPane, 1, 1, 1, 1)
                .addRow(new JButton(new GenericAction(e -> showCertificate(edCertificates.getText())).setName("show_certificate")), 2, 0)
                .build();
    }

    private JPanel createClientAuthenticationPanel() {
        JScrollPane scrollPane = new JScrollPane(edClientAuthPrivateKey);

        edClientAuthPrivateKey.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

        return FormBuilder.createBuilder()
                .setBorder(BorderFactory.createEmptyBorder(6, 0, 0,0))
                .addRow(cbClientAuthEnabled, 2, 1)
                .addRow("password", edClientAuthPassword, 1, 1)
                .addRow("pem_private_key", 1)
                .addRow(scrollPane, 2, 1, 1, 1)
                .addRow(2,
                        new JButton(new GenericAction(e -> showCertificate(edClientAuthPrivateKey.getText())).setName("show_certificate")),
                        new JButton(new GenericAction(e -> createClientAuthKey()).setName("create_self_signed_certificate")))
                .build();
    }

    private void createClientAuthKey() {
        try {
            edClientAuthPrivateKey.setText(CertificateTool.createSelfSignedPemEncodedCertificate(edClientAuthPassword.getPassword()));
            showCertificate(edClientAuthPrivateKey.getText());
        } catch (Exception ex) {
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    private void showCertificate(String pem) {
        try {
            SimpleDialog.info(LocaleTool.get("certificate_summery"), PemTool.getCertificateSummary(pem));
        } catch (Exception ex) {
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    @Override
    public void ok() {
        try {
            validateForm();
            commit();

            super.ok();
        } catch (Exception ex) {
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    public void bindBean(@NotNull TlsSendOptions options) {
        this.options = options;

        cbTlsEnabled.setSelected(options.isEnabled());
        edCertificates.setText(options.getCertificates());

        cbClientAuthEnabled.setSelected(options.isClientAuthenticationEnabled());
        edClientAuthPrivateKey.setText(options.getClientAuthenticationPrivateKey());
        edClientAuthPassword.setText(options.getClientAuthenticationPassword() == null ? "" : new String(options.getClientAuthenticationPassword()));
    }

    protected void validateForm() throws CertificateException {
        try {
            if (cbTlsEnabled.isSelected() && PemTool.readCertificates(edCertificates.getText()).length == 0) {
                throw new CertificateException(LocaleTool.get("certificate_validation_failed"));
            }

            if (cbClientAuthEnabled.isSelected()) {
                if (PemTool.readCertificates(edClientAuthPrivateKey.getText()).length == 0) {
                    throw new CertificateException(LocaleTool.get("certificate_validation_failed"));
                }

                if (PemTool.readPrivateKey(edClientAuthPrivateKey.getText(), edClientAuthPassword.getPassword()) == null) {
                    throw new CertificateException(LocaleTool.get("private_key_not_found"));
                }
            }
        } catch (Exception ex) {
            throw new CertificateException(ex.getMessage(), ex);
        }
    }

    public void commit() {
        options.setEnabled(cbTlsEnabled.isSelected());
        options.setCertificates(edCertificates.getText());

        options.setClientAuthenticationEnabled(cbClientAuthEnabled.isSelected());
        options.setClientAuthenticationPrivateKey(edClientAuthPrivateKey.getText());
        options.setClientAuthenticationPassword(edClientAuthPassword.getPassword());
    }

}
