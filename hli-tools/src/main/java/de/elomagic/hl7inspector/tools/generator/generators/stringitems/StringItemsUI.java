/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators.stringitems;

import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.tools.generator.generators.AbstractSequencerUI;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.util.Arrays;

public class StringItemsUI extends AbstractSequencerUI<StringItemsOptions> {

    private transient StringItemsOptions options;

    private final JFormattedTextField editId;
    private final JTextField editDescription;
    private final JCheckBox btRandom;
    private final JTextArea lstItems;

    public StringItemsUI() {
        editId = new JFormattedTextField();
        editDescription = new JTextField();
        btRandom = new JCheckBox(LocaleTool.get("random"));

        lstItems = new JTextArea();
        lstItems.putClientProperty (StyleId.STYLE_PROPERTY, StyleId.textarea);

        FormBuilder.createBuilder(this)
                .noBorder()
                .addRow("id", UIFactory.wrapFormattedTextField(editId, REGEX_ID), 1, 1)
                .addRow("description", editDescription, 1, 1)
                .addRow(btRandom, 2, 1)
                .addRow("items", 2)
                .addRow(lstItems, 2, 1, 1, 1)

                .build();
    }

    @Override
    public @Nls @NotNull String getTitle() {
        return "string_items_generator";
    }

    @Override
    public void bindBean(@NotNull StringItemsOptions options) {
        this.options = options;

        editId.setText(options.getId());
        editDescription.setText(options.getDescription());
        btRandom.setSelected(options.isRandom());
        lstItems.setText(StringUtils.join(options.getItems(), "\n"));
    }

    @Override
    public boolean validateForm() throws FormBinderException {
        try {
            Validate.notBlank(editId.getText());
        } catch (Exception ex) {
            throw new FormBinderException(LocaleTool.get("invalid_input_please_correct_them"), ex);
        }

        return true;
    }

    @Override
    public void commit() {
        options.setId(editId.getText());
        options.setDescription(editDescription.getText());
        options.setRandom(btRandom.isSelected());
        options.setItems(Arrays.asList(lstItems.getText().split("\\r?\\n")));
    }

}
