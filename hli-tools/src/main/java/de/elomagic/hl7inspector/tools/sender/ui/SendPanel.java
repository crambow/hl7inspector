/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.sender.ui;

import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.components.HighlighterPanel;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadEvent;
import de.elomagic.hl7inspector.shared.configuration.SendOptions;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.ui.CharacterMonitorTabPanel;
import de.elomagic.hl7inspector.shared.services.MessageManagerService;
import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;
import de.elomagic.hl7inspector.tools.sender.SendSocketThread;
import de.elomagic.hl7inspector.tools.sender.SourceFolderIterator;
import de.elomagic.hl7inspector.tools.sender.ui.actions.SetupSenderAction;
import de.elomagic.hl7inspector.tools.sender.ui.actions.StartSendMessageAction;
import de.elomagic.hl7inspector.tools.sender.ui.actions.StopSendMessageAction;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.List;

@Component(SendPanel.NAME)
public class SendPanel extends CharacterMonitorTabPanel {

    public static final String NAME = "sendPanel";

    private final ToolsConfiguration configuration = ToolsConfiguration.getInstance();

    private final transient BeanFactory beanFactory;

    private final AbstractButton btStart = UIFactory.createFlatIconButton(JToggleButton.class, new StartSendMessageAction(this::startSender));
    private final AbstractButton btStop = UIFactory.createFlatIconButton(JToggleButton.class, new StopSendMessageAction(this::stopSender));
    private final AbstractButton btOptions = UIFactory.createFlatIconButton(JButton.class, new SetupSenderAction(this::setupSendOptions));

    private final SendOptions sendOptions = configuration.getSendOptions();
    private transient SendSocketThread thread;

    public @Autowired SendPanel(BeanFactory beanFactory, HighlighterPanel highlighterPane) {
        super(highlighterPane);

        this.beanFactory = beanFactory;
    }

    @PostConstruct
    private void initUI() {
        btStop.setSelected(true);
        ButtonGroup btGroup = new ButtonGroup();
        btGroup.add(btStart);
        btGroup.add(btStop);

        getToolBar().add(new JSeparator(), 0);
        getToolBar().add(btOptions, 0);
        getToolBar().add(new JSeparator(), 0);
        getToolBar().add(btStop, 0);
        getToolBar().add(btStart, 0);

        printCurrentSendOptions();
    }

    private void startSender(ActionEvent e) {
        int mode = sendOptions.getSourceMode();
        List<Message> messageList = beanFactory.getBean(MessageManagerService.class).getSelectedMessages();

        Iterator<Message> iterator = mode == 0 ? messageList.listIterator() : new SourceFolderIterator(sendOptions.getSourceFolder(), sendOptions.getEncoding());

        if(mode == 0 && messageList.isEmpty()) {
            SimpleDialog.error(LocaleTool.get("no_messages_selected"));
            btStop.setSelected(true);
        } else {
            thread = beanFactory.getBean(SendSocketThread.class);
            thread.setOptions(sendOptions);
            thread.addAfterThreadStartsListener(this::threadStarted);
            thread.addBeforeThreadEndListener(this::threadStopped);
            thread.addListener(this);
            thread.setMessageSource(iterator);
            thread.start();
        }
    }

    private void stopSender(ActionEvent e) {
        if(thread != null) {
            thread.requestTermination();
        }
    }

    private void setupSendOptions(ActionEvent e) {
        SendOptionsDialog sendOptionsDialog = beanFactory.getBean(SendOptionsDialog.class);
        sendOptionsDialog.bindBean(sendOptions);
        if(sendOptionsDialog.ask()) {
            printCurrentSendOptions();
        }
    }

    public void threadStarted(ThreadEvent<SendSocketThread> event) {
        btStart.setEnabled(false);
        btStop.setEnabled(true);
        btOptions.setEnabled(false);
    }

    public void threadStopped(ThreadEvent<SendSocketThread> event) {
        btStart.setEnabled(true);
        btStop.setEnabled(false);
        btStop.setSelected(true);
        btOptions.setEnabled(true);

        thread.removeListener(this);
    }

    private void printCurrentSendOptions() {
        String tlsModeKey;
        if (sendOptions.getTlsOptions().isEnabled() && sendOptions.getTlsOptions().isClientAuthenticationEnabled()) {
            tlsModeKey = "client_authentication";
        } else if (sendOptions.getTlsOptions().isEnabled()) {
            tlsModeKey = "yes";
        } else {
            tlsModeKey = "no";
        }

        String text = LocaleTool.get(
                "current_send_options_5args",
                sendOptions.getHost(),
                sendOptions.getPort(),
                LocaleTool.get(tlsModeKey),
                LocaleTool.get(sendOptions.isKeepSocket() ? "yes": "no"),
                sendOptions.getEncoding()
                );
        addLine(text);
    }

    @Override
    public @NotNull String getId() {
        return NAME;
    }

    @NotNull
    @Override
    public String getTitle() {
        return LocaleTool.get("panel_title");
    }

    @Override
    public ImageIcon getIcon() {
        return IconThemeManager.getImageIcon("send.png");
    }

}
