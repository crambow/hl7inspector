/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.tools.generator.generators.AbstractGeneratorOptions;
import de.elomagic.hl7inspector.tools.generator.generators.AbstractValueGenerator;
import de.elomagic.hl7inspector.tools.generator.generators.system.CounterValueGenerator;
import de.elomagic.hl7inspector.tools.generator.generators.system.SystemOptions;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MessageGenerator {

    private static final String GENERATOR_FUNCTION_GROUP = "g";
    private static final String SYSTEM_FUNCTION_GROUP = "s";

    private boolean dryRun;
    private final Configuration cfg;
    private final Map<String, Object> root;
    private final Map<String, Object> systemHashMap;
    private final Map<String, Object> sequencerHashMap;

    private Template template;

    public MessageGenerator() {
        cfg = new Configuration(Configuration.VERSION_2_3_29);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setOutputFormat(HL7OutputFormat.INSTANCE);

        systemHashMap = new HashMap<>();
        systemHashMap.put(CounterValueGenerator.ID, new CounterValueGenerator());

        sequencerHashMap = new HashMap<>();

        root = new HashMap<>();
        root.put(SYSTEM_FUNCTION_GROUP, systemHashMap);
        root.put(GENERATOR_FUNCTION_GROUP, sequencerHashMap);
    }

    public void setTemplate(@NotNull String templateContent) throws IOException {
        StringTemplateLoader stringLoader = new StringTemplateLoader();
        stringLoader.putTemplate("message", templateContent);
        cfg.setTemplateLoader(stringLoader);
        this.template = cfg.getTemplate("message");
    }

    public void setCustomSequencer(@NotNull Set<AbstractValueGenerator<?>> sequencerSet) {
        sequencerSet.forEach(s -> sequencerHashMap.put(s.getId(), s));
    }

    public void setDefaultEncoding(@NotNull Charset charset) {
        cfg.setDefaultEncoding(charset.displayName());
    }

    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    private void nextSequencer() throws GeneratorException {
        for(Object o : sequencerHashMap.values()) {
            AbstractValueGenerator<?> sequencer = (AbstractValueGenerator<?>)o;
            sequencer.next();
        }
    }

    public Message nextMessage() throws GeneratorException {
        try {
            nextSequencer();

            if (dryRun) {
                // Highlight functions
                root.put(GENERATOR_FUNCTION_GROUP, wrapHighlighter(sequencerHashMap, "hl1"));
                root.put(SYSTEM_FUNCTION_GROUP, wrapHighlighter(systemHashMap, "hl2"));
            }

            StringWriter sw = new StringWriter();
            template.process(root, sw);

            Message message = new Message();
            message.parse(sw.toString());

            return message;
        } catch (Exception ex) {
            throw new GeneratorException(ex.getMessage(), ex);
        }
    }

    @NotNull
    public static String createGeneratorSnippet(@NotNull AbstractGeneratorOptions generatorOptions) {
        String prefix = generatorOptions instanceof SystemOptions ? "s" : "g";
        return "${" + prefix + "." + generatorOptions.getId() + "}";
    }

    @NotNull
    private Map<String, Object> wrapHighlighter(@NotNull Map<String, Object> toWrap, @NotNull String className) {
        return new HashMap<>(toWrap) {
            @Override
            public Object get(Object key) {
                Object g = super.get(key);
                String v = g.toString();
                return "<span class=\"" + className + "\">" + v + "</span>";
            }
        };
    }

}
