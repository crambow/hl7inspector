/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.valuepresenter;

import com.alee.api.data.CompassDirection;
import com.alee.extended.dock.WebDockableFrame;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.events.EditValuePresenterEvent;
import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.shared.hl7.HL7Tool;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.tools.valuepresenter.plain.StringTypeDescriptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.tree.TreePath;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.List;

@Component
public class ValuePresenterFrame extends WebDockableFrame {

    private static final Logger LOGGER = LogManager.getLogger(ValuePresenterFrame.class);

    private final transient BeanFactory beanFactory;
    private final transient List<? extends ValueTypeDescription> supportedValueTypeList;
    private final JLabel statusLabel = new JLabel();
    private JComboBox<? extends ValueTypeDescription> cbDataType;

    private transient AbstractValueTypePresenterUI valuePresenter;

    @Autowired
    public ValuePresenterFrame(BeanFactory beanFactory, List<? extends ValueTypeDescription> supportedValueTypeList) {
        super("value-presenter-frame", LocaleTool.get("value_viewer"));

        this.beanFactory = beanFactory;
        this.supportedValueTypeList = supportedValueTypeList;
    }

    @PostConstruct
    private void initUI() {
        setPosition(CompassDirection.east);

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder());
        setPreferredSize(new Dimension(400, 600));

        JCheckBox cbAutoDetection = new JCheckBox("Automatic presenter detection");
        cbAutoDetection.addActionListener(e -> {

        });

        cbDataType = new JComboBox<>(supportedValueTypeList.toArray(new ValueTypeDescription[0]));
        cbDataType.putClientProperty(StyleId.STYLE_PROPERTY, StyleId.combobox);
        cbDataType.setRenderer(new ValueTypeDescriptionRenderer());
        cbDataType.addActionListener(this::handlePresenterChanged);

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                //.addRow(cbAutoDetection, 2,1)
                .addRow("source_data_type", cbDataType, 1, 1)
                .build();

        statusLabel.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));

        add(grid, BorderLayout.NORTH);
        add(statusLabel, BorderLayout.SOUTH);

        cbDataType.setSelectedItem(beanFactory.getBean(StringTypeDescriptor.class));
    }

    private void handlePresenterChanged(@NotNull ActionEvent e) {
        try {
            ValueTypeDescription value = (ValueTypeDescription) cbDataType.getSelectedItem();

            if (value == null) {
                return;
            }

            if (valuePresenter != null) {
                remove(valuePresenter);
            }

            valuePresenter = beanFactory.getBean(value.getUIClass());
            valuePresenter.activateUI();

            add(BorderLayout.CENTER, valuePresenter);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @EventListener
    public void handleValueSelected(@NotNull MessageNodeSelectionChangedEvent e) {
        TreePath path = HL7Tool.toTreePath(e.getMessage(), e.getPath());
        Hl7Object<?> o = path == null ? null : (Hl7Object<?>)path.getLastPathComponent();
        bindValue(o);
    }

    @EventListener
    public void handleActionShowValuePresenterEvent(@NotNull EditValuePresenterEvent e) {
        boolean b = e.getVisible() == null ? isClosed() : e.getVisible();

        if (b) {
            dock();
        } else {
            close();
        }
    }

    private void bindValue(@Nullable Hl7Object<?> value) {
        clearStatus();
        try {
            if (valuePresenter != null) {
                valuePresenter.setValue(null);

                // Getting leaf from value
                Hl7Object<?> o = value;

                while (o != null && o.size() != 0) {
                    o = o.get(0);
                }

                if (o != null) {
                    valuePresenter.bindBean(o);
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            setErrorStatus(ex);
        }
    }

    private void clearStatus() {
        statusLabel.setText("");
        statusLabel.setIcon(null);
    }

    private void setErrorStatus(@NotNull Exception ex) {
        String text = LocaleTool.get("unable_to_display_value") + ": " + ex.getMessage();

        statusLabel.setText(text);
        statusLabel.setIcon(IconThemeManager.getImageIcon("error.png"));
    }



}
