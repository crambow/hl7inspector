/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators.fileline;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.tools.generator.generators.AbstractSequencerUI;

import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import java.nio.file.Paths;
import java.util.Objects;

public class FileLineUI extends AbstractSequencerUI<FileLineOptions> {

    private transient FileLineOptions options;

    private final JFormattedTextField editId;
    private final JTextField editDescription;
    private final JTextField editFile;

    public FileLineUI() {
        editId = new JFormattedTextField();
        editDescription = new JTextField();
        editFile = new JTextField();

        JButton btChooseTemplate = new JButton(new GenericAction(e -> SimpleDialog.chooseFile(Paths.get(editFile.getText()), p->editFile.setText(p.getPath().toString()))).setName("dots"));

        FormBuilder.createBuilder(this)
                .noBorder()
                .addRow("id", UIFactory.wrapFormattedTextField(editId, REGEX_ID), 2, 1)
                .addRow("description", editDescription, 1, 1)

                .add("file", editFile, 2, 1)
                .addRow(btChooseTemplate, 1, 0)

                .addRowSpacer();
    }

    @Override
    public @Nls @NotNull String getTitle() {
        return "file_line_generator";
    }

    @Override
    public void bindBean(@NotNull FileLineOptions options) {
        this.options = options;

        editId.setText(options.getId());
        editDescription.setText(options.getDescription());
        editFile.setText(Objects.toString(options.getFile(), ""));
    }

    @Override
    public boolean validateForm() throws FormBinderException {
        try {
            Validate.notBlank(editId.getText());
        } catch (Exception ex) {
            throw new FormBinderException(LocaleTool.get("invalid_input_please_correct_them"), ex);
        }

        return true;
    }

    @Override
    public void commit() {
        options.setId(editId.getText());
        options.setDescription(editDescription.getText());
        options.setFile(Paths.get(editFile.getText()));
    }

}
