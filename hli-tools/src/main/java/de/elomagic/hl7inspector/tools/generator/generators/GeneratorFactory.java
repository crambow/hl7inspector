/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators;

import de.elomagic.hl7inspector.tools.generator.generators.date.DateOptions;
import de.elomagic.hl7inspector.tools.generator.generators.date.DateValueGenerator;
import de.elomagic.hl7inspector.tools.generator.generators.fileline.FileLineOptions;
import de.elomagic.hl7inspector.tools.generator.generators.fileline.FileLineValueGenerator;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomOptions;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomValueGenerator;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsOptions;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsValueGenerator;

import org.jetbrains.annotations.NotNull;

public class GeneratorFactory {

    private GeneratorFactory() {
    }

    @NotNull
    public static AbstractValueGenerator<? extends AbstractGeneratorOptions> create(@NotNull AbstractGeneratorOptions options) {
        if (RandomOptions.class.isAssignableFrom(options.getClass())) {
            return new RandomValueGenerator((RandomOptions)options);
        } else if (StringItemsOptions.class.isAssignableFrom(options.getClass())) {
            return new StringItemsValueGenerator(((StringItemsOptions)options));
        } else if (FileLineOptions.class.isAssignableFrom(options.getClass())) {
            return new FileLineValueGenerator((FileLineOptions)options);
        } else if (DateOptions.class.isAssignableFrom(options.getClass())) {
            return new DateValueGenerator((DateOptions)options);
        } else {
            throw new IllegalArgumentException("Unsupported generator for option \"" + options + "\".");
        }
    }

}
