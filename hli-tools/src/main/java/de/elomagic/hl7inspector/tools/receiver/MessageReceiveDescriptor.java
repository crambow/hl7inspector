/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.receiver;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.shared.desktop.MainToolBarDescriptor;
import de.elomagic.hl7inspector.shared.desktop.MainMenuDescriptor;
import de.elomagic.hl7inspector.shared.desktop.MessageEditorDescriptor;
import de.elomagic.hl7inspector.tools.receiver.ui.actions.ShowReceiveWindowAction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.util.List;

@Component
public class MessageReceiveDescriptor implements MainToolBarDescriptor, MainMenuDescriptor, MessageEditorDescriptor {

    private final EventMessengerProcessor eventMessengerProcessor;

    public @Autowired MessageReceiveDescriptor(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @Override
    public float getPriority() {
        return 1000;
    }

    @Override
    public @Nullable List<JMenuItem> getMessageEditorContextMenuItems() {
        return List.of(new JMenuItem(new ShowReceiveWindowAction(e -> eventMessengerProcessor.publishEvent(new ActionShowReceiverEvent(this)))));
    }

    @Override
    public @Nullable List<JMenuItem> getMenuItems(@NotNull Category category) {
        return category == Category.TOOLS ? List.of(new JMenuItem(new ShowReceiveWindowAction(e -> eventMessengerProcessor.publishEvent(new ActionShowReceiverEvent(this))))) : null;
    }

    @Override
    public @Nullable List<JComponent> getMenuBarItems() {
        JButton button = new JButton(new ShowReceiveWindowAction(e -> eventMessengerProcessor.publishEvent(new ActionShowReceiverEvent(this))));

        return List.of(button);
    }

}
