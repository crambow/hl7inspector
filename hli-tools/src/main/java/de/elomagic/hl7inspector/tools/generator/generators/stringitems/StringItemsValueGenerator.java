/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators.stringitems;

import de.elomagic.hl7inspector.tools.generator.generators.AbstractValueGenerator;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class StringItemsValueGenerator extends AbstractValueGenerator<StringItemsOptions> {

    private final Random r = new Random();
    private int i = -1;

    private Object nextValue;

    public StringItemsValueGenerator(@NotNull StringItemsOptions options) {
        super(options);
    }

    @Override
    public void next() {
        if (getOptions().isRandom()) {
            i = r.nextInt(getOptions().getItems().size());
        } else {
            i++;
            if (i >= getOptions().getItems().size()) {
                i = 0;
            }
        }

        nextValue = getOptions().getItems().get(i);
    }

    @Override
    public void close() {
        // noop
    }

    @Override
    public String toString() {
        return nextValue == null ? null : nextValue.toString();

    }
}
