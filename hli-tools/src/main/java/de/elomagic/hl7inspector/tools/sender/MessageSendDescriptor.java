/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.sender;

import de.elomagic.hl7inspector.shared.desktop.MainToolBarDescriptor;
import de.elomagic.hl7inspector.shared.desktop.MessageEditorDescriptor;
import de.elomagic.hl7inspector.shared.desktop.MainMenuDescriptor;
import de.elomagic.hl7inspector.tools.sender.ui.actions.ShowSendWindowAction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import java.util.List;

@Component
public class MessageSendDescriptor implements MainToolBarDescriptor, MainMenuDescriptor, MessageEditorDescriptor {

    private final BeanFactory beanFactory;

    public @Autowired MessageSendDescriptor(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public float getPriority() {
        return 1100;
    }

    @Override
    public @Nullable List<JMenuItem> getMessageEditorContextMenuItems() {
        return List.of(new JMenuItem(beanFactory.getBean(ShowSendWindowAction.class)));
    }

    @Override
    public @Nullable List<JMenuItem> getMenuItems(@NotNull Category category) {
        return category == Category.TOOLS ? List.of(new JMenuItem(beanFactory.getBean(ShowSendWindowAction.class))) : null;
    }

    @Override
    public @Nullable List<JComponent> getMenuBarItems() {
        JButton button = new JButton(beanFactory.getBean(ShowSendWindowAction.class));

        return List.of(button);
    }

}
