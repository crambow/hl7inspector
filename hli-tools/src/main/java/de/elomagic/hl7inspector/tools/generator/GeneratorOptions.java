/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator;

import de.elomagic.hl7inspector.tools.generator.generators.AbstractGeneratorOptions;
import de.elomagic.hl7inspector.tools.generator.generators.date.DateOptions;
import de.elomagic.hl7inspector.tools.generator.generators.fileline.FileLineOptions;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomOptions;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsOptions;

import org.jetbrains.annotations.NotNull;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class GeneratorOptions {

    private String template;
    private final Set<RandomOptions> randomOptionsSet = new HashSet<>();
    private final Set<DateOptions> dateOptionsSet = new HashSet<>();
    private final Set<StringItemsOptions> stringItemsOptionsSet = new HashSet<>();
    private final Set<FileLineOptions> fileLineOptionsSet = new HashSet<>();

    private TargetOptions targetOptions = new TargetOptions();

    private Charset targetEncoding = StandardCharsets.UTF_8;
    // This property should be moved outside or not ?
    private int amount;
    private boolean dryRun;

    public String getTemplate() {
        return template;
    }

    public void setTemplate(@NotNull String template) {
        this.template = template;
    }

    public Set<RandomOptions> getRandomOptionsSet() {
        return randomOptionsSet;
    }

    public Set<DateOptions> getDateOptionsSet() {
        return dateOptionsSet;
    }

    public Set<FileLineOptions> getFileLineOptionsSet() {
        return fileLineOptionsSet;
    }

    public Set<StringItemsOptions> getStringItemsOptionsSet() {
        return stringItemsOptionsSet;
    }

    /**
     * Returns target.
     */
    public TargetOptions getTarget() {
        return targetOptions;
    }

    /**
     * Set target.
     */
    public void setTarget(@NotNull TargetOptions targetOptions) {
        this.targetOptions = targetOptions;
    }

    public Charset getTargetEncoding() {
        return targetEncoding;
    }

    public void setTargetEncoding(@NotNull Charset targetEncoding) {
        this.targetEncoding = targetEncoding;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isDryRun() {
        return dryRun;
    }

    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    public Set<AbstractGeneratorOptions> getSequencerSet() {
        Set<AbstractGeneratorOptions> set = new HashSet<>();
        set.addAll(randomOptionsSet);
        set.addAll(dateOptionsSet);
        set.addAll(stringItemsOptionsSet);
        set.addAll(fileLineOptionsSet);

        return Collections.unmodifiableSet(set);
    }

}
