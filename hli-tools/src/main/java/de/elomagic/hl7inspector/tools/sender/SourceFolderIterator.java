/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.sender;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.io.MessageFileIO;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SourceFolderIterator implements Iterator<Message> {

    private final Charset encoding;
    private Iterator<String> iterator;

    public SourceFolderIterator(@Nullable Path folder, @Nullable Charset encoding) {
        this.encoding = encoding;

        if (folder == null) {
            return;
        }

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(folder, GenericFileFilter.ALL_FILES_FILTER)) {
            List<String> files = new ArrayList<>();
            stream.forEach(p -> files.add(p.toString()));
            iterator = files.listIterator();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public Message next() {
        try {
            Path file = Paths.get(iterator.next());
            return MessageFileIO.readMessage(file, encoding == null ? StandardCharsets.UTF_8 : encoding);
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

}
