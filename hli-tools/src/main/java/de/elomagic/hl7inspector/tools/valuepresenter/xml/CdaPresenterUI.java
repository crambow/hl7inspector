/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.valuepresenter.xml;

import com.alee.extended.link.LinkAction;
import com.alee.extended.link.WebLink;
import com.alee.managers.style.StyleId;
import com.alee.managers.style.StyleManager;
import com.sun.net.httpserver.HttpExchange;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;
import de.elomagic.hl7inspector.shared.events.OpenSettingsEvent;
import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;
import de.elomagic.hl7inspector.tools.http.HttpService;
import de.elomagic.hl7inspector.tools.http.MimeTypes;
import de.elomagic.hl7inspector.tools.valuepresenter.AbstractValueTypePresenterUI;
import de.elomagic.hl7inspector.tools.valuepresenter.ValueException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Date;

@Component
public class CdaPresenterUI extends AbstractValueTypePresenterUI {

    private static final Logger LOGGER = LogManager.getLogger(CdaPresenterUI.class);
    private static final String HTTP_PATH = "/tools/valuepresenter/xml/";

    private final transient BeanFactory beanFactory;
    private final EventMessengerProcessor eventMessengerProcessor;
    private final ToolsConfiguration configuration = ToolsConfiguration.getInstance();
    private final JScrollPane scrollPane = new JScrollPane();

    private byte[] htmlResponse;
    private byte[] xmlResponse;

    public @Autowired CdaPresenterUI(BeanFactory beanFactory, EventMessengerProcessor eventMessengerProcessor) {
        this.beanFactory = beanFactory;
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @PostConstruct
    private void initUI() {
        // InitUI
        setLayout(new BorderLayout());

        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        toolbar.setRollover(true);
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "ok.png", e -> commit()));
        toolbar.addSeparator();
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "fileopen.png", e -> handleLoadFile(GenericFileFilter.CDA_FILTER)));
        toolbar.add(UIFactory.createFlatIconButton(JButton.class, "document-save.png", e -> handleSaveFile(GenericFileFilter.CDA_FILTER)));
        StyleManager.setStyleId(toolbar, StyleId.toolbarUndecorated);

        add(toolbar, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);

        initServer();
    }

    @Override
    protected void activateUI() {
        if (configuration.getPresenterCdaXsltFile() == null) {
            showNoXsltConfiguredWarning();
        }
    }

    private void initServer() {
        try {
            HttpService http = beanFactory.getBean(HttpService.class);
            http.addContext(HTTP_PATH + "cda.xml", this::responseXml);
            http.addContext(HTTP_PATH + "cda.html", this::responseHtml);
            http.restart();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public void setValue(@Nullable String value) throws ValueException {
        setValue(value == null ? null : Base64.getDecoder().decode(value));
    }

    private void setValue(@Nullable byte[] xmlResponse) throws ValueException {
        JEditorPane editor = new JEditorPane();
        editor.setText("Transformation...");
        scrollPane.setViewportView(editor);

        try {
            this.xmlResponse = xmlResponse;

            if (xmlResponse == null) {
                return;
            }

            if (configuration.getPresenterCdaXsltFile() == null) {
                throw new FileNotFoundException(LocaleTool.get("xslt_file_not_configured"));
            }

            if (Files.notExists(configuration.getPresenterCdaXsltFile())) {
                throw new FileNotFoundException(LocaleTool.get("xslt_file_not_found_1arg", configuration.getPresenterCdaXsltFile()));
            }

            try (InputStream xslt = Files.newInputStream(configuration.getPresenterCdaXsltFile());
                 InputStream xml = new ByteArrayInputStream(xmlResponse)) {

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer(new StreamSource(xslt));
                transformer.transform(new StreamSource(xml), new StreamResult(output));

                htmlResponse = output.toByteArray();

                LOGGER.debug("CDA transformation successful.");
                editor.setText("Transformation successful. " + new Date());

                PlatformTool.browse("http://localhost:" + configuration.getToolsHttpServerPort() + HTTP_PATH + "cda.html");
            }
        } catch (Exception ex) {
            throw new ValueException(ex.getMessage(), ex);
        }
    }

    public void commit() {
        String base64 = Base64.getEncoder().encodeToString(xmlResponse);
        commit(base64);
    }

    @Override
    protected void load(@NotNull Path file) throws ValueException {
        try {
            String content = FileUtils.readFileToString(file.toFile(), StandardCharsets.UTF_8);
            setValue(content.getBytes(StandardCharsets.UTF_8));
        } catch (Exception ex) {
            throw new ValueException(ex.getMessage(), ex);
        }
    }

    @Override
    protected void save(Path file) throws IOException {
        Files.write(file, xmlResponse);
    }

    private void responseXml(HttpExchange exchange) throws IOException {
        exchange.getResponseHeaders().add("Content-Type", MimeTypes.APPLICATION_XML + "; charset=UTF-8");
        exchange.sendResponseHeaders(200, xmlResponse.length);

        try (OutputStream out = exchange.getResponseBody()) {
            out.write(xmlResponse);
            out.flush();
        }
    }

    private void responseHtml(HttpExchange exchange) throws IOException {
        exchange.getResponseHeaders().add("Content-Type", MimeTypes.TEXT_HTML + "; charset=UTF-8");
        exchange.sendResponseHeaders(200, htmlResponse.length);

        try (OutputStream out = exchange.getResponseBody()) {
            out.write(htmlResponse);
            out.flush();
        }
    }

    private void showNoXsltConfiguredWarning() {

        JEditorPane editor = new JEditorPane();
        editor.setText("Transformation...");

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                //.addRow(cbAutoDetection, 2,1)
                .add(new JLabel(IconThemeManager.getImageIcon("warning-32.png")), 1, 0, 3, 0)
                .setColumnOffset(1)
                .addRow("no_xslt_warning", 1)
                .addRow(new WebLink(StyleId.link,  new LinkAction() {
                    @Override
                    public Icon getIcon() {
                        return null;
                    }

                    @Override
                    public String getText() {
                        return LocaleTool.get("click_here_to_configure_xslt");
                    }

                    @Override
                    public void linkExecuted(ActionEvent event) {
                        eventMessengerProcessor.publishEvent(new OpenSettingsEvent(this));
                    }
                }), 2, 1)
                .build();

        scrollPane.setViewportView(grid);
    }

}
