/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators.random;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinderException;
import de.elomagic.hl7inspector.tools.generator.generators.AbstractSequencerUI;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import javax.swing.JFormattedTextField;
import javax.swing.JTextField;

public class RandomUI extends AbstractSequencerUI<RandomOptions> {

    private transient RandomOptions options;

    private final JFormattedTextField editId;
    private final JTextField editDescription;
    private final JFormattedTextField editLow;
    private final JFormattedTextField editHigh;

    public RandomUI() {
        editId = new JFormattedTextField();
        editDescription = new JTextField();
        editLow = new JFormattedTextField();
        editLow.setColumns(6);
        editHigh = new JFormattedTextField();
        editHigh.setColumns(6);

        FormBuilder.createBuilder(this)
                .noBorder()
                .addRow("id", UIFactory.wrapFormattedTextField(editId, REGEX_ID), 1, 1)
                .addRow("description", editDescription, 1, 1)
                .addRow("start", UIFactory.wrapFormattedTextField(editLow, REGEX_INTEGER), 1, 0)
                .addRow("end", UIFactory.wrapFormattedTextField(editHigh, REGEX_INTEGER), 1, 0)
                .addRowSpacer();
    }

    @Override
    public @Nls @NotNull String getTitle() {
        return "random_generator";
    }

    public void commit(RandomOptions o) {
        int low = Integer.parseInt(editLow.getText());
        int high = Integer.parseInt(editHigh.getText());

        o.setId(editId.getText());
        o.setDescription(editDescription.getText());
        o.setLow(low);
        o.setHigh(high);
    }

    @Override
    public void bindBean(@NotNull RandomOptions options) {
        this.options = options;

        editId.setText(options.getId());
        editDescription.setText(options.getDescription());
        editLow.setText(Integer.toString(options.getLow()));
        editHigh.setText(Integer.toString(options.getHigh()));
    }

    @Override
    public boolean validateForm() throws FormBinderException {
        try {
            Validate.notBlank(editId.getText());

            RandomOptions o = new RandomOptions();
            commit(o);

            Validate.isTrue(o.getLow() < o.getHigh(), "Low must not higher then the higher value.");

            RandomValueGenerator dg = new RandomValueGenerator(o);
            dg.next();
        } catch (Exception ex) {
            throw new FormBinderException(LocaleTool.get("invalid_input_please_correct_them"), ex);
        }

        return true;
    }

    @Override
    public void commit() {
        commit(options);
    }

}
