/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.configuration.ui;

import com.alee.extended.link.UrlLinkAction;
import com.alee.extended.link.WebLink;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.components.FileTextField;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.desktop.SettingsUIDescriptor;
import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;

@Component
public class ValueViewerSettingsPanel extends SettingsUIDescriptor.AbstractSettingsPanel {

    private final transient ToolsConfiguration configuration = ToolsConfiguration.getInstance();

    private JTextField editHttpServerPort;
    private FileTextField editXslt;

    @PostConstruct
    protected void initUI() {
        editHttpServerPort = new JTextField();
        editXslt = new FileTextField(GenericFileFilter.XSLT_FILTER);

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                .addSection("common", 3)
                .addRow("server_port", editHttpServerPort, 1, 1)
                .addSection("cda_viewer", 3)
                .addRow("xslt_file", editXslt, 2, 1)
                .addRow("cda_stylesheets_examples", 3)
                .addRow(new WebLink(new UrlLinkAction("https://www.elga.gv.at/technischer-hintergrund/technische-elga-leitfaeden/")), 3, 0)
                .addRow(new WebLink(new UrlLinkAction("https://hl7de.art-decor.org/index.php?prefix=abde-")), 3, 0)
                .addRowSpacer()
                .build();

        add(grid, BorderLayout.CENTER);
    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("picture-32.png");
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("value_viewer");
    }

    @Override
    public String getDescription() {
        return LocaleTool.get("value_viewer");
    }

    @Override
    protected void load() {
        editHttpServerPort.setText(Integer.toString(configuration.getToolsHttpServerPort()));
        editXslt.setFile(configuration.getPresenterCdaXsltFile());
    }

    @Override
    public void commit() {
        configuration.setToolsHttpServerPort(NumberUtils.toInt(editHttpServerPort.getText(), 58081));
        configuration.setPresenterCdaXsltFile(editXslt.getFile());
    }

}
