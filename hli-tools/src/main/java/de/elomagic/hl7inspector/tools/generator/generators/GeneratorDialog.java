/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.generator.generators;

import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.ListModel;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.ui.actions.DefaultCloseWindowAction;
import de.elomagic.hl7inspector.platform.ui.actions.EditItemAction;
import de.elomagic.hl7inspector.platform.ui.actions.RemoveItemAction;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyClickMouseListener;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.tools.generator.generators.date.DateOptions;
import de.elomagic.hl7inspector.tools.generator.generators.fileline.FileLineOptions;
import de.elomagic.hl7inspector.tools.generator.generators.random.RandomOptions;
import de.elomagic.hl7inspector.tools.generator.generators.stringitems.StringItemsOptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.Set;

public class GeneratorDialog extends MyBaseDialog {

    private static final Logger LOGGER = LogManager.getLogger(GeneratorDialog.class);

    private final JList<AbstractGeneratorOptions> listSequenceOptions = new JList<>();
    private final JComboBox<String> btAdd = new JComboBox<>(new String[] {
            LocaleTool.get("add_generator"),
            LocaleTool.get("random_generator"),
            LocaleTool.get("string_items_generator"),
            LocaleTool.get("date_generator"),
            LocaleTool.get("file_line_generator")});
    private final JButton btEdit = new JButton(new EditItemAction(this::editSequence));
    private final JButton btClose = new JButton(new DefaultCloseWindowAction(e -> setVisible(false)));

    private transient Set<RandomOptions> randomOptionsSet;
    private transient Set<DateOptions> dateOptionsSet;
    private transient Set<StringItemsOptions> stringItemsOptionsSet;
    private transient Set<FileLineOptions> fileLineOptionsSet;

    private final transient ListModel<AbstractGeneratorOptions> abstractOptionsModel = new ListModel<>();

    public GeneratorDialog() {
        getBanner().setVisible(false);
        getButtonPane().setVisible(false);
        setTitle(LocaleTool.get("generators"));

        JScrollPane scroll = new JScrollPane(listSequenceOptions);

        JPanel main = new JPanel(new BorderLayout());
        main.add(scroll, BorderLayout.CENTER);
        main.add(createButtonPanel(), BorderLayout.EAST);

        getContentPane().add(main);

        listSequenceOptions.setModel(abstractOptionsModel);
        listSequenceOptions.setCellRenderer(new GeneratorCellListRenderer());
        listSequenceOptions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listSequenceOptions.addMouseListener((SimplifyClickMouseListener) e -> {
            if(e.getClickCount() == 2) {
                btEdit.doClick();
            }
        });

        setSize(600, 480);
        setModal(true);
    }

    private JPanel createButtonPanel() {
        btAdd.addActionListener(this::addSequence);

        JButton btRemove = new JButton(new RemoveItemAction(this::removeSequence));

        return FormBuilder.createBuilder()
                .noBorder()
                .addRow(btAdd, 0, 1)
                .addRow(btEdit, 0, 1)
                .addRow(btRemove, 0, 1)
                .addRowSpacer()
                .addRow(btClose, 0, 1)
                .build();
    }

    private GeneratorOptionsDialogFactory<?> createOptionsDialog(AbstractGeneratorOptions options) {
        GeneratorOptionsDialogFactory<?> dialog;
        if (RandomOptions.class.isAssignableFrom(options.getClass())) {
            dialog = GeneratorOptionsDialogFactory.createRandomDialog((RandomOptions)options);
        } else if (StringItemsOptions.class.isAssignableFrom(options.getClass())) {
            dialog = GeneratorOptionsDialogFactory.createStringItemsDialog((StringItemsOptions)options);
        } else if (FileLineOptions.class.isAssignableFrom(options.getClass())) {
            dialog = GeneratorOptionsDialogFactory.createFileLineDialog((FileLineOptions)options);
        } else if (DateOptions.class.isAssignableFrom(options.getClass())) {
            dialog = GeneratorOptionsDialogFactory.createDateDialog((DateOptions)options);
        } else {
            throw new IllegalArgumentException("Unsupported generator \"" + options +"\"");
        }

        return dialog;
    }

    private void addSequence(ActionEvent e) {
        AbstractGeneratorOptions options;
        Set set;
        switch (btAdd.getSelectedIndex()) {
            case 1:
                options = new RandomOptions();
                set = randomOptionsSet;
                break;
            case 2:
                options = new StringItemsOptions();
                set = stringItemsOptionsSet;
                break;
            case 3:
                options = new DateOptions();
                set = dateOptionsSet;
                break;
            case 4:
                options = new FileLineOptions();
                set = fileLineOptionsSet;
                break;
            default:
                return;
        }

        GeneratorOptionsDialogFactory<?> dialog = createOptionsDialog(options);
        if (dialog.ask()) {
            abstractOptionsModel.add(options);
            set.add(options);
        }
        toFront();

        btAdd.setSelectedIndex(0);
    }

    private void editSequence(ActionEvent e) {
        try {
            AbstractGeneratorOptions options = listSequenceOptions.getSelectedValue();
            if(options == null) {
                SimpleDialog.error(LocaleTool.get("no_entry_selected"));
            } else {
                GeneratorOptionsDialogFactory<?> dialog = createOptionsDialog(options);
                dialog.ask();
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(ex);
        }
    }

    private void removeSequence(ActionEvent e) {
        AbstractGeneratorOptions options = listSequenceOptions.getSelectedValue();
        if(options == null) {
            SimpleDialog.error(LocaleTool.get("no_entry_selected"));
        } else if(SimpleDialog.confirmYesNo(LocaleTool.get("are_you_sure")) == 0) {
            abstractOptionsModel.remove(options);
            randomOptionsSet.remove(options);
            dateOptionsSet.remove(options);
            stringItemsOptionsSet.remove(options);
            fileLineOptionsSet.remove(options);
        }
    }

    @NotNull
    public GeneratorDialog bindOptions(
            @NotNull Set<RandomOptions> randomOptionsSet,
            @NotNull Set<DateOptions> dateOptionsSet,
            @NotNull Set<StringItemsOptions> stringItemsOptionsSet,
            @NotNull Set<FileLineOptions> fileLineOptionsSet) {
        this.randomOptionsSet = randomOptionsSet;
        this.dateOptionsSet = dateOptionsSet;
        this.stringItemsOptionsSet = stringItemsOptionsSet;
        this.fileLineOptionsSet = fileLineOptionsSet;

        this.abstractOptionsModel.clear();
        this.abstractOptionsModel.addAll(randomOptionsSet);
        this.abstractOptionsModel.addAll(dateOptionsSet);
        this.abstractOptionsModel.addAll(stringItemsOptionsSet);
        this.abstractOptionsModel.addAll(fileLineOptionsSet);

        return this;
    }

}
