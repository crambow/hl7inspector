/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.valuepresenter.pdf;

import com.alee.managers.style.StyleId;
import com.alee.managers.style.StyleManager;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.tools.valuepresenter.ValueException;
import de.elomagic.hl7inspector.tools.valuepresenter.AbstractValueTypePresenterUI;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

@Component
public class PdfDocumentPresenterUI extends AbstractValueTypePresenterUI {

    private static final Logger LOGGER = LogManager.getLogger(PdfDocumentPresenterUI.class);

    private final JLabel lbPageIndex = new JLabel();
    private final JScrollPane scrollPane = new JScrollPane();
    private transient MyPDFRenderer renderer;

    private byte[] value;
    private int pageIndex = 0;

    @PostConstruct
    private void initUI() {
        // InitUI
        setLayout(new BorderLayout());

        JToolBar buttonBar = new JToolBar();
        buttonBar.setRollover(true);
        buttonBar.setFloatable(false);
        buttonBar.add(UIFactory.createFlatIconButton(JButton.class, "ok.png", e -> commit()));
        buttonBar.addSeparator();
        buttonBar.add(UIFactory.createFlatIconButton(JButton.class, "fileopen.png", e -> handleLoadFile(GenericFileFilter.PDF_FILTER)));
        buttonBar.add(UIFactory.createFlatIconButton(JButton.class, "document-save.png", e -> handleSaveFile(GenericFileFilter.PDF_FILTER)));
        buttonBar.addSeparator();
        buttonBar.add(UIFactory.createFlatIconButton(JButton.class,"previous.png", e -> previousPage()));
        buttonBar.add(UIFactory.createFlatIconButton(JButton.class,"next.png", e -> nextPage()));
        buttonBar.add(lbPageIndex);
        StyleManager.setStyleId(buttonBar, StyleId.toolbarUndecorated);

        add(buttonBar, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
    }

    @Override
    public void setValue(@Nullable String value ) throws ValueException {
        setValue(value == null ? null : Base64.getDecoder().decode(value));
    }

    private void setValue(@Nullable byte[] data) throws ValueException {
        this.value = data;

        if (data == null) {
            showImage(null);
        } else {
            loadPdfRenderer(data);
        }
    }

    public void commit() {
        String base64 = Base64.getEncoder().encodeToString(value);
        commit(base64);
    }

    private void loadPdfRenderer(@NotNull byte[] data) throws ValueException {
        try {
            if (renderer != null) {
                renderer.getDocument().close();
            }

            pageIndex = 0;

            PDDocument doc = PDDocument.load(data);
            renderer = new MyPDFRenderer(doc);

            showPage(pageIndex);
        } catch (Exception ex) {
            throw new ValueException(ex.getMessage(), ex);
        }
    }

    private void nextPage() {
        if (renderer == null) {
            return;
        }

        try {
            if (pageIndex < renderer.getDocument().getNumberOfPages()-1) {
                pageIndex++;
                showPage(pageIndex);
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private void previousPage() {
        if (renderer == null) {
            return;
        }

        try {
            renderer.getDocument().getNumberOfPages();
            if (pageIndex > 0) {
                pageIndex--;
                showPage(pageIndex);
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private void showImage(@Nullable Image image) {
        if (image == null) {
            scrollPane.setViewportView(new JLabel());
        } else {
            ImageIcon icon = new ImageIcon(image);
            JLabel label = new JLabel(icon);

            scrollPane.setViewportView(label);
        }
    }

    private void showPage(int pageIndex) throws IOException {
        lbPageIndex.setText(LocaleTool.get("page_2arg", pageIndex + 1, renderer.getDocument().getNumberOfPages()));

        BufferedImage image = renderer.renderImage(pageIndex);

        showImage(image);
    }

    @Override
    protected void load(@NotNull Path file) throws ValueException {
        try {
            byte[] data = FileUtils.readFileToByteArray(file.toFile());
            setValue(data);
        } catch (Exception ex) {
            throw new ValueException(ex.getMessage(), ex);
        }
    }

    @Override
    protected void save(@NotNull Path file) throws IOException {
        Files.write(file, value);
    }


}
