/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.configuration;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.configuration.ToolsConfigurationKey;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.shared.configuration.ReceiveOptions;
import de.elomagic.hl7inspector.shared.configuration.SendOptions;
import de.elomagic.spps.bc.SimpleCrypt;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.GeneralSecurityException;

public final class ToolsConfiguration {

    private static final Logger LOGGER = LogManager.getLogger(ToolsConfiguration.class);
    private static final ToolsConfiguration INSTANCE = new ToolsConfiguration();

    private final Configuration configuration = Configuration.getInstance();

    private ToolsConfiguration() {
    }

    @NotNull
    public static ToolsConfiguration getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Frame deserializeMessageFrame(@Nullable String value) {
        Frame result;
        if (value == null) {
            result = new Frame();
        } else {
            try {
                String[] items = value.split(",");

                byte startByte = NumberUtils.toByte(items[0], Frame.DEFAULT_START);
                byte stopByte1 = NumberUtils.toByte(items[1], Frame.DEFAULT_STOP1);

                Byte stopByte2 = items.length > 2 ? NumberUtils.toByte(items[2], Frame.DEFAULT_STOP2) : null;

                return new Frame(startByte, stopByte1, stopByte2);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                result = new Frame();
            }
        }

        return result;
    }

    private String serializeMessageFrame(Frame frame) {
        String s = frame.getStopBytesLength() > 1 ? "," + frame.getStopBytes()[1] : "";
        return frame.getStartByte() + "," + frame.getStopBytes()[0] + s;
    }

    /**
     * Returns the internal HTTP server port.
     *
     * @return Returns the port. By default it will 0
     */
    public int getToolsHttpServerPort() {
        return configuration.getProperty(ToolsConfigurationKey.TOOLS_HTTP_SERVER_PORT, Integer.class, 0);
    }

    public void setToolsHttpServerPort(int port) {
        configuration.setProperty(ToolsConfigurationKey.TOOLS_HTTP_SERVER_PORT, port);
    }

    @Nullable
    public Path getPresenterCdaXsltFile() {
        return configuration.getProperty(ToolsConfigurationKey.TOOLS_VALUE_PRESENTER_XML_XSLT_FILE, Path.class,null);
    }

    public void setPresenterCdaXsltFile(@Nullable Path file) {
        configuration.setProperty(ToolsConfigurationKey.TOOLS_VALUE_PRESENTER_XML_XSLT_FILE, file == null ? null : file.toAbsolutePath().toString());
    }

    /**
     * Returns a new instance of configured send options.
     *
     * @return New instance
     */
    @NotNull
    public SendOptions getSendOptions() {
        SendOptions bean = new SendOptions();

        bean.setSourceMode(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_SOURCE_MODE, Integer.class, 0));
        bean.setSourceFolder(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_SOURCE_FOLDER, Path.class, configuration.getAppFilesLastUsedFolder()));
        bean.setEncoding(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_CHARSET, Charset.class, StandardCharsets.ISO_8859_1));
        bean.setFrame(deserializeMessageFrame(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_FRAME, String.class, null)));
        bean.setHost(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_HOSTNAME, String.class, "localhost"));
        bean.setPort(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_PORT, Integer.class, 2100));
        bean.setKeepSocket(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_KEEP_SOCKET, Boolean.class, true));
        bean.getTlsOptions().setEnabled(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_ENABLED, Boolean.class, false));
        bean.getTlsOptions().setCertificates(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_SERVER_CERTIFICATES, String.class, null));
        bean.getTlsOptions().setClientAuthenticationEnabled(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_ENABLED, Boolean.class, false));

        try {
            bean.getTlsOptions().setClientAuthenticationPrivateKey(SimpleCrypt.decryptToString(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_CLIENT_AUTH_PRIVATE_KEY, String.class, null)));
            bean.getTlsOptions().setClientAuthenticationPassword(SimpleCrypt.decryptToChars(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_CLIENT_AUTH_PASSWORD, String.class, null)));
        } catch(Exception ex) {
            // Simple log and don't raise exception?
            LOGGER.error(ex.getMessage(), ex);
        }

        return bean;
    }

    public void setSendOptions(@NotNull SendOptions bean) {
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_SOURCE_MODE, bean.getSourceMode());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_SOURCE_FOLDER, bean.getSourceFolder());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_CHARSET, bean.getEncoding().name());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_FRAME, serializeMessageFrame(bean.getFrame()));
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_HOSTNAME, bean.getHost());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_PORT, bean.getPort());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_KEEP_SOCKET, bean.isKeepSocket());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_ENABLED, bean.getTlsOptions().isEnabled());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_SERVER_CERTIFICATES, bean.getTlsOptions().getCertificates());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_CLIENT_AUTH_ENABLED, bean.getTlsOptions().isClientAuthenticationEnabled());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_CLIENT_AUTH_PRIVATE_KEY, SimpleCrypt.encrypt(bean.getTlsOptions().getClientAuthenticationPrivateKey()));
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_CLIENT_AUTH_PASSWORD, SimpleCrypt.encrypt(bean.getTlsOptions().getClientAuthenticationPassword()));
    }

    @NotNull
    public ReceiveOptions getReceiveOptions() {
        ReceiveOptions bean = new ReceiveOptions();

        bean.setPort(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_PORT, Integer.class, 2100));
        bean.getTlsOptions().setEnabled(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_ENABLED, Boolean.class, false));
        bean.getTlsOptions().setClientAuthenticationEnabled(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_CLIENT_AUTH_ENABLED, Boolean.class, false));
        bean.getTlsOptions().setClientAuthenticationCertificates(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_CLIENT_AUTH_CERTIFICATES, String.class, null));

        try {
            bean.getTlsOptions().setPassword(SimpleCrypt.decryptToChars(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_PASSWORD, String.class, null)));
            bean.getTlsOptions().setPrivateKey(SimpleCrypt.decryptToString(configuration.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_PRIVATE_KEY, String.class, null)));
        } catch(Exception ex) {
            // Simple log and don't raise exception?
            LOGGER.error(ex.getMessage(), ex);
        }

        return bean;
    }

    public void setReceiveOptions(@NotNull ReceiveOptions bean) {
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_PORT, bean.getPort());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_ENABLED, bean.getTlsOptions().isEnabled());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_PRIVATE_KEY, SimpleCrypt.encrypt(bean.getTlsOptions().getPrivateKey()));
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_PASSWORD, SimpleCrypt.encrypt(bean.getTlsOptions().getPassword()));
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_CLIENT_AUTH_ENABLED, bean.getTlsOptions().isClientAuthenticationEnabled());
        configuration.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_CLIENT_AUTH_CERTIFICATES, bean.getTlsOptions().getClientAuthenticationCertificates());
    }

    public int getValuePresenterStringMaxSize() {
        return configuration.getProperty(ToolsConfigurationKey.TOOLS_VALUE_PRESENTER_PLAIN_AUTO_VIEW_MAX_SIZE, Integer.class, 4096);
    }

    public void setValuePresenterStringMaxSize(int size) {
        configuration.setProperty(ToolsConfigurationKey.TOOLS_VALUE_PRESENTER_PLAIN_AUTO_VIEW_MAX_SIZE, size);
    }

}
