/*
 * HL7 Inspector - Tools
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Tools.
 *
 * HL7 Inspector - Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Tools. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.tools.receiver.ui;

import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.components.HighlighterPanel;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.thread.ThreadEvent;
import de.elomagic.hl7inspector.shared.configuration.ReceiveOptions;
import de.elomagic.hl7inspector.shared.ui.CharacterMonitorTabPanel;
import de.elomagic.hl7inspector.shared.ui.ImportOptionsDialog;
import de.elomagic.hl7inspector.tools.configuration.ToolsConfiguration;
import de.elomagic.hl7inspector.tools.receiver.ReceiveServerSocketThread;
import de.elomagic.hl7inspector.tools.receiver.ui.actions.SetupReceiverAction;
import de.elomagic.hl7inspector.tools.receiver.ui.actions.SetupServerAction;
import de.elomagic.hl7inspector.tools.receiver.ui.actions.StartReceiveMessageAction;
import de.elomagic.hl7inspector.tools.receiver.ui.actions.StopReceiveMessageAction;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import java.awt.event.ActionEvent;

@Component(ReceivePanel.NAME)
public class ReceivePanel extends CharacterMonitorTabPanel {

    public static final String NAME = "receivePanel";

    private transient ReceiveServerSocketThread thread = null;

    private final AbstractButton btStart = UIFactory.createFlatIconButton(JToggleButton.class, new StartReceiveMessageAction(this::startReceiver));
    private final AbstractButton btStop = UIFactory.createFlatIconButton(JToggleButton.class, new StopReceiveMessageAction(this::stopReceiver));
    private final AbstractButton btPort = UIFactory.createFlatIconButton(JButton.class, new SetupServerAction(this::setupReceiverNetwork));
    private final AbstractButton btOptions = UIFactory.createFlatIconButton(JButton.class, new SetupReceiverAction(this::setupImportOptions));

    private final transient BeanFactory beanFactory;
    private final ReceiveNetworkSetupDialog receiveNetworkSetupDialog;
    private final ImportOptionsDialog importOptionsDialog;

    @Autowired
    public ReceivePanel(ReceiveNetworkSetupDialog receiveNetworkSetupDialog, ImportOptionsDialog importOptionsDialog, BeanFactory beanFactory, HighlighterPanel highlighterPane) {
        super(highlighterPane);

        this.receiveNetworkSetupDialog = receiveNetworkSetupDialog;
        this.importOptionsDialog = importOptionsDialog;
        this.beanFactory = beanFactory;
    }

    @PostConstruct
    private void init() {
        initThread();

        btStop.setSelected(true);
        ButtonGroup btGroup = new ButtonGroup();
        btGroup.add(btStart);
        btGroup.add(btStop);

        getToolBar().add(new JSeparator(), 0);
        getToolBar().add(btOptions, 0);
        getToolBar().add(btPort, 0);
        getToolBar().add(new JSeparator(), 0);
        getToolBar().add(btStop, 0);
        getToolBar().add(btStart, 0);

        printCurrentReceiveOptions();
    }

    private void initThread() {
        ReceiveServerSocketThread t = beanFactory.getBean(ReceiveServerSocketThread.class);
        t.addAfterThreadStartsListener(this::threadStarted);
        t.addBeforeThreadEndListener(this::threadStopped);
        t.addListener(this);
        t.setReceiveOptions(ToolsConfiguration.getInstance().getReceiveOptions());

        if(thread != null) {
            thread.requestTermination();
            thread.removeListener(this);

            t.setFrame(thread.getFrame());
            t.setOptions(thread.getOptions());
        }

        thread = t;
    }

    private void startReceiver(ActionEvent e) {
        thread.start();
    }

    private void stopReceiver(ActionEvent e) {
        if(thread != null) {
            thread.requestTermination();
        }
    }

    private void setupReceiverNetwork(ActionEvent e) {
        ReceiveOptions receiveOptions = ToolsConfiguration.getInstance().getReceiveOptions();
        receiveNetworkSetupDialog.bindBean(receiveOptions);
        if(receiveNetworkSetupDialog.ask()) {
            thread.setReceiveOptions(receiveOptions);
            printCurrentReceiveOptions();
        }
    }

    private void setupImportOptions(ActionEvent e) {
        importOptionsDialog.bindBean(thread.getOptions());
        importOptionsDialog.ask();
    }

    public void threadStarted(ThreadEvent<ReceiveServerSocketThread> source) {
        btStart.setEnabled(false);
        btStop.setEnabled(true);
        btPort.setEnabled(false);
        btOptions.setEnabled(false);
    }

    public void threadStopped(ThreadEvent<ReceiveServerSocketThread> source) {
        btStart.setEnabled(true);
        btStop.setEnabled(false);
        btStop.setSelected(true);
        btPort.setEnabled(true);
        btOptions.setEnabled(true);

        initThread();
    }

    private void printCurrentReceiveOptions() {
        ReceiveOptions receiveOptions = ToolsConfiguration.getInstance().getReceiveOptions();

        String tls_key;
        if (receiveOptions.getTlsOptions().isEnabled() && receiveOptions.getTlsOptions().isClientAuthenticationEnabled()) {
            tls_key = "client_authentication";
        } else if (receiveOptions.getTlsOptions().isEnabled()) {
            tls_key = "yes";
        } else {
            tls_key = "no";
        }

        String text = LocaleTool.get(
                "current_receive_options_3args",
                receiveOptions.getPort(),
                LocaleTool.get(tls_key),
                thread.getOptions().getEncoding()

        );
        addLine(text);
    }

    @Override
    public @NotNull String getId() {
        return NAME;
    }

    @NotNull
    @Override
    public String getTitle() {
        return LocaleTool.get("message_receiver");
    }

    @Override
    public ImageIcon getIcon() {
        return IconThemeManager.getImageIcon("receive.png");
    }
}
