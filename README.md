# HL7 Inspector #

## What is this HL7 Inspector ? ###

[![Downloads](https://img.shields.io/badge/Downloads-gray)](https://bitbucket.org/crambow/hl7inspector/wiki/Home#markdown-header-downloads/)
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-orange.svg)](https://www.gnu.org/licenses/gpl-3.0.txt)
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/crambow/hl7inspector.svg)](https://Bitbucket.org/crambow/hl7inspector/issues)
[![Bitbucket pipeline](https://img.shields.io/bitbucket/pipelines/crambow/hl7inspector/develop.svg)](https://bitbucket.org/crambow/hl7inspector/addon/pipelines/home)

The HL7 Inspector is a useful open source tool for integration the HL7 Version 2 in a health care environmental.
It will help you to minimize the time for tuning the HL7 communication between systems such 
as HIS and RIS by analyzing and validating HL7 messages. 

### Features

* Parsing of HL7 messages
* Sending/receiving of messages on IP sockets (Including TLS)
* Reading of HL7 messages from (compressed) text/log files
* Simple message editor
* Full Unicode support
* Simple message validation support

### Release Notes

Release notes can be read [here](https://bitbucket.org/crambow/hl7inspector/src/master/release-notes.md). 

## License

The HL7 Inspector is distributed under GNU Public License, Version 3.0. For license terms, see COPYING.

## Prebuild binaries and setups

The easiest way to install the HL7 Inspector on Windows PC is Chocolatey. [Chocolatey](https://chocolatey.org/) is a software management solution.

Just enter the following commands in your console to install the latest version of HL7 Inspector

```cinst hl7inspector```
 
For other operating systems or a downloadable setup, you will find precompiled binaries and setups 
[here](https://bitbucket.org/crambow/hl7inspector/wiki/Home).

## How to compile only the source code

```
> git clone https://crambow@bitbucket.org/crambow/hl7inspector.git
> mvnw clean install
```

### How to build Windows binaries

```
> git clone https://crambow@bitbucket.org/crambow/hl7inspector.git
> mvnw clean install -P windows-setup
```

### How to build Windows portable

```
> git clone https://crambow@bitbucket.org/crambow/hl7inspector.git
> mvnw clean install -P windows-portable
```

### How to build Linux x64 binaries

```
> git clone https://crambow@bitbucket.org/crambow/hl7inspector.git
> mvnw clean install -P linux-distribution
```

### How to build MacOS binaries

Works only on Mac OS and linux systems

```
> git clone https://crambow@bitbucket.org/crambow/hl7inspector.git
> mvnw clean install -P macos-setup
```

## Contribution

### Git branches

* Master contains upcoming features for the next minor version
* Each released minor version gets his own branches. Only the latest branch will be supported with bug fixes.
* Branch "develop" is deprecated

