#!/usr/bin/env bash

#
# This script create a fancy DMG for the HL7 Inspector.
#
# Scripts runs only on MacOS and require installed "create-dmg" ( https://github.com/create-dmg/create-dmg ).
#

echo
read -p 'Enter version to package: ' APP_VERSION
echo -n 'Remote credential. Leave empty to skip upload. [username:token]: '
read -s BB_TOKEN
echo

#APP_VERSION=2.5.2
#URL=https://api.bitbucket.org/2.0/repositories/crambow/hl7inspector/downloads
URL=https://files.hl7inspector.com
NAME="HL7 Inspector"
APP_NAME="$NAME.app"
SOURCE_FOLDER="/Volumes/$NAME $APP_VERSION"
TARGET_IMAGE_FILE="./target/$NAME $APP_VERSION Installer.dmg"
ZIP_FILE="$NAME $APP_VERSION (MacOS-x64).zip"
LOCAL_ZIP_FILE="./target/$ZIP_FILE"

mkdir target

# Download zip package
URL_ZIP_FILE=$(echo $ZIP_FILE | sed 's/\ /\%20/g')
#=$(python -c "import urllib, sys; print urllib.quote(sys.argv[1])" "$ZIP_FILE")
if [[ ${APP_VERSION} == *"SNAPSHOT"* ]]; then
    URL_ZIP=${URL}/snapshot/v${APP_VERSION}/${URL_ZIP_FILE}
else
    URL_ZIP=${URL}/stable/v${APP_VERSION}/${URL_ZIP_FILE}
fi

echo "* Downloading ZIP package from ${URL_ZIP}..."
curl -L ${URL_ZIP} --output "$LOCAL_ZIP_FILE"
if [[ $? -ne 0 ]]; then
    echo "Download of ${URL_ZIP} failed."
    exit
fi

# Unzipping downloaded file
rm -r "./target/$APP_NAME"
echo "* Unzipping package file $LOCAL_ZIP_FILE..."
unzip "$LOCAL_ZIP_FILE" -d "./target"

# Creating image
echo "* Removing old file $TARGET_IMAGE_FILE, if exists..."
rm -f ${TARGET_IMAGE_FILE}
rm -f "rw.${TARGET_IMAGE_FILE}"

echo "* Creating new disk image file $TARGET_IMAGE_FILE"
create-dmg \
  --volname "$NAME Installer $APP_VERSION" \
  --volicon "./hli-app/src/setup/macosx/resources/AppIcon.icns" \
  --background "./hli-app/src/setup/macosx/resources/background.png" \
  --window-pos 200 120 \
  --window-size 800 600 \
  --icon-size 100 \
  --icon "$APP_NAME" 220 190 \
  --hide-extension "$APP_NAME" \
  --app-drop-link 580 190 \
  "${TARGET_IMAGE_FILE}" \
  "./target/${APP_NAME}"

if [[ -z "$BB_TOKEN" ]]; then
    echo "No credentials entered. Upload skipped."
else
    echo "* Uploading final disk image file ${TARGET_IMAGE_FILE} to ${URL}..."
    curl -s -u ${BB_TOKEN} -X POST ${URL} -F files=@"${TARGET_IMAGE_FILE}"

    #echo "* Delete old disk image file ${URL_OLD_IMAGE}..."
    #curl -s -u ${BB_TOKEN} -X DELETE ${URL_OLD_IMAGE}
fi
echo "* Done. Congratulation !!!"
