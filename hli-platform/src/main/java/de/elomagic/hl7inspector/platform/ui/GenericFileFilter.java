/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.PropertyKey;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.util.List;

public final class GenericFileFilter extends javax.swing.filechooser.FileFilter implements java.io.FileFilter, DirectoryStream.Filter<Path> {

    public static final GenericFileFilter ALL_FILES_FILTER = new GenericFileFilter(List.of("*"), "hl7", "all_files");
    public static final GenericFileFilter CDA_FILTER = new GenericFileFilter(List.of("xml"), "xml", "cda_files");
    public static final GenericFileFilter CSV_FILTER = new GenericFileFilter(List.of("csv"), "csv", "csv_file");
    public static final GenericFileFilter EXE_FILTER = new GenericFileFilter(List.of("exe"), "exe", "exe_file");
    public static final GenericFileFilter HL7_FILTER = new GenericFileFilter(List.of("hl7"), "hl7", "hl7_files");
    public static final GenericFileFilter HL7GEN_FILTER = new GenericFileFilter(List.of("hl7gen"), "hl7gen", "hl7gen_files");
    public static final GenericFileFilter DICOM_FILTER = new GenericFileFilter(List.of("dcm"), "dcm", "dicom_files");
    public static final GenericFileFilter PDF_FILTER = new GenericFileFilter(List.of("pdf"), "pdf", "pdf_files");
    public static final GenericFileFilter PLUGIN_FILTER = new GenericFileFilter(List.of("jar", "zip"), "jar", "plugin_files");
    public static final GenericFileFilter PNG_FILTER = new GenericFileFilter(List.of("png"), "png", "png_files");
    public static final GenericFileFilter PROFILE_FILTER = new GenericFileFilter(List.of("hip"), "hip", "hl7_definitions_profile");
    public static final GenericFileFilter TEXT_FILTER = new GenericFileFilter(List.of("txt"), "txt", "text_files");
    public static final GenericFileFilter XSLT_FILTER = new GenericFileFilter(List.of("xsl", "xslt"), "xsl", "xslt_files");

    private final String descriptionKey;
    private final List<String> fileExtensions;
    private final String defaultExtension;

    public GenericFileFilter(@NotNull List<String> fileExtensions, @NotNull String defaultExtension, @Nls @NotNull @PropertyKey(resourceBundle = LocaleTool.DEFAULT_BUNDLE) String descriptionKey) {
        this.fileExtensions = fileExtensions;
        this.defaultExtension = defaultExtension;
        this.descriptionKey = descriptionKey;
    }

    @Override
    public boolean accept(Path path) throws IOException {
        return accept(path.toFile());
    }

    /**
     * Whether the given file is accepted by this filter.
     */
    @Override
    public boolean accept(final File file) {
        if(file.isDirectory()) {
            return false;
        }

        if (fileExtensions.get(0).equals("*")) {
            return true;
        }

        String[] parts = file.getAbsolutePath().split("\\.");

        if(parts.length < 2) {
            return false;
        }

        String ext = parts[parts.length - 1].toLowerCase();
        return fileExtensions.contains(ext);
    }
    
    @Override
    public String getDescription() {
        return LocaleTool.get(descriptionKey);
    }

    public String getDefaultExtension() {
        return defaultExtension;
    }

}
