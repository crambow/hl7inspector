/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import javax.swing.table.AbstractTableModel;

public abstract class ArrayListModel<E> extends AbstractTableModel {

    private static final long serialVersionUID = -649617212368934188L;

    private int lockCount = 0;
    private transient List<E> table = new ArrayList<>();

    protected ArrayListModel() {
    }

    protected ArrayListModel(List<E> list) {
        this.table = list;
    }

    /**
     * Returns a sequential {@code Stream} with this model as its source.
     *
     * @return A sequential {@code Stream} over the elements in this model
     */
    public Stream<E> stream() {
        return table.stream();
    }

    /**
     * Will set reference to the given list.
     *
     * @param list
     */
    public void setList(@NotNull List<E> list) {
        this.table = list;
        fireTableDataChanged();
    }

    public void clear() {
        table.clear();
        fireTableDataChanged();
    }

    public int addRow(E object) {
        table.add(object);

        int l = table.size() - 1;

        if(lockCount == 0) {
            fireTableRowsInserted(l, l);
        }

        return l;
    }

    public void addAll(@NotNull List<E> list) {
        table.addAll(list);

        if(lockCount == 0) {
            fireTableRowsInserted(table.size() - list.size(), table.size());
        }
    }

    public void deleteRow(int rowIndex) {
        table.remove(rowIndex);

        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    public E getRow(int rowIndex) {
        return table.get(rowIndex);
    }

    public void lock() {
        lockCount++;
    }

    public void unlock() {
        lockCount--;
        if(lockCount == 0) {
            fireTableRowsInserted(table.size() - 1, table.size() - 1);
        }
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it should display. This method should be quick, as it is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return table.size();
    }
}
