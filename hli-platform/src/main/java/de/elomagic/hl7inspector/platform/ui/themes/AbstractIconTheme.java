/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.themes;

import com.alee.managers.style.Skin;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.io.ClassPathResource;

import javax.swing.ImageIcon;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public abstract class AbstractIconTheme implements Comparable<AbstractIconTheme> {

    private static final Logger LOGGER = LogManager.getLogger(AbstractIconTheme.class);

    @NotNull
    public abstract String getId();

    @NotNull
    protected abstract String getDisplayKey();

    @NotNull
    protected abstract String getResourcePath();

    @NotNull
    public abstract Class<? extends Skin> getSkin();

    @NotNull
    public String getDisplayName() {
        return LocaleTool.get(getDisplayKey());
    }

    @Override
    public String toString() {
        return getDisplayName();
    }

    @Nullable
    protected ImageIcon loadImageIcon(@NotNull String imageName) {
        ImageIcon icon = null;

        String name = getResourcePath() + "/" + imageName;
        try {
            icon = new ImageIcon(getResourceURL(name));
        } catch(Exception ex) {
            LOGGER.error("Unable to load image icon '{}'.", name, ex);
        }

        return icon;
    }

    private URL getResourceURL(@NotNull String name) throws IOException {
        ClassPathResource resource = new ClassPathResource(name);
        try {
            return resource.getURL();
        } catch (Exception ex) {
            LOGGER.error("Image icon '{}' not found. Loading surrogate image", name);
            name = "de/elomagic/hl7inspector/themes/missing.png";
            resource = new ClassPathResource(name);
            return resource.getURL();
        }
    }

    public @NotNull InputStream getRSyntaxTextAreaTheme() {
        return getClass().getResourceAsStream("/de/elomagic/hl7inspector/platform/ui/themes/editor-light-skin.xml");
    }

    @Override
    public int compareTo(AbstractIconTheme o) {
        return getDisplayName().compareTo(o.getDisplayName());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (o == this) {
            return true;
        }

        if (getClass() != o.getClass()) {
            return false;
        }

        AbstractIconTheme that = (AbstractIconTheme) o;

        return this.getDisplayName().equals(that.getDisplayName());
    }

    @Override
    public int hashCode() {
        return getDisplayName().hashCode();
    }
}
