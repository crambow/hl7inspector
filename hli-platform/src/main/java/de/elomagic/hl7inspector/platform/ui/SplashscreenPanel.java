/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

public class SplashscreenPanel extends JPanel {

    private static final Image backgroundImage = new ImageIcon(ApplicationProperties.getStartupImageResourceURL()).getImage();

    public SplashscreenPanel() {
        super(new BorderLayout());
        setOpaque(true);

        JLabel topSpacer = new JLabel();
        topSpacer.setPreferredSize(new Dimension(1, 200));

        JLabel leftSpacer = new JLabel();
        leftSpacer.setPreferredSize(new Dimension(120, 1));

        JLabel title = createLabel(LocaleTool.get("hl7_inspector") + " " + ApplicationProperties.getVersion() + " (" + ApplicationProperties.getBranchName() + ")");
        title.setFont(title.getFont().deriveFont(Font.BOLD));
        title.setFont(title.getFont().deriveFont(20f));

        JPanel centerPanel = new JPanel();
        centerPanel.setOpaque(false);
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        centerPanel.add(title);
        centerPanel.add(createLabel(LocaleTool.get("build_on_arg", ApplicationProperties.getBuildDate())));
        centerPanel.add(createLabel(" "));
        centerPanel.add(createLabel(LocaleTool.get("contact_arg", "hl7inspector.dev@elomagic.de")));
        centerPanel.add(createLabel(LocaleTool.get("developed_under_gnu_public_license")));
        centerPanel.add(createLabel(" "));
        centerPanel.add(createLabel(LocaleTool.get("runtime_version_arg", System.getProperty("java.version"))));
        centerPanel.add(createLabel(LocaleTool.get("vm_by_3args", System.getProperty("java.vm.name"), System.getProperty("java.vm.version"), System.getProperty("java.vendor"))));
        centerPanel.add(createLabel(" "));
        centerPanel.add(createLabel(LocaleTool.get("copyright")));

        add(topSpacer, BorderLayout.NORTH);
        add(leftSpacer, BorderLayout.WEST);
        add(centerPanel, BorderLayout.CENTER);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(backgroundImage, 0, 0, 640, 400, this);
    }

    private JLabel createLabel(@NotNull String text) {
        JLabel label = new JLabel(text);
        label.setForeground(Color.lightGray);
        label.setFont(Font.decode("Tahoma"));
        label.setFont(label.getFont().deriveFont(13f));

        return label;
    }

}
