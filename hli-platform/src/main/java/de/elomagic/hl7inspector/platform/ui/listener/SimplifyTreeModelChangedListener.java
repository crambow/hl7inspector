/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.listener;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

@FunctionalInterface
public interface SimplifyTreeModelChangedListener extends TreeModelListener {

    @Override
    default void treeNodesChanged(TreeModelEvent e) {
        treeModelChanged(e);
    }

    @Override
    default void treeNodesInserted(TreeModelEvent e) {
        treeModelChanged(e);
    }

    @Override
    default void treeNodesRemoved(TreeModelEvent e) {
        treeModelChanged(e);
    }

    @Override
    default void treeStructureChanged(TreeModelEvent e) {
        treeModelChanged(e);
    }

    void treeModelChanged(TreeModelEvent e);

}
