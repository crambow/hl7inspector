/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.SystemColor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JToolTip;
import javax.swing.SwingUtilities;
import javax.swing.plaf.metal.MetalToolTipUI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ExtendedToolTipUI extends MetalToolTipUI {

    private static final Logger LOGGER = LogManager.getLogger(ExtendedToolTipUI.class);
    private String[] strs;

    @Override
    public void paint(Graphics g, JComponent c) {
        FontMetrics metrics = g.getFontMetrics();
        Dimension size = c.getSize();
        g.setColor(SystemColor.info);
        g.fillRect(0, 0, size.width, size.height);
        g.setColor(SystemColor.infoText);
        if(strs != null) {
            for(int i = 0; i < strs.length; i++) {
                g.drawString(strs[i], 3, (metrics.getHeight()) * (i + 1));
            }
        }
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        FontMetrics metrics = c.getFontMetrics(c.getFont());
        String tipText = ((JToolTip)c).getTipText();
        if(tipText == null) {
            tipText = "";
        }
        BufferedReader br = new BufferedReader(new StringReader(tipText));
        String line;
        int maxW = 0;
        List<String> v = new ArrayList<>();
        try {
            while((line = br.readLine()) != null) {
                int width = SwingUtilities.computeStringWidth(metrics, line);
                maxW = Math.max(maxW, width);
                v.add(line);
            }
        } catch(IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        int lines = v.size();
        if(lines < 1) {
            strs = null;
            lines = 1;
        } else {
            strs = new String[lines];
            int i = 0;

            for(Iterator<String> it = v.iterator(); it.hasNext(); i++) {
                strs[i] = it.next();
            }
        }
        int height = metrics.getHeight() * lines;
        return new Dimension(maxW + 6, height + 4);
    }
}