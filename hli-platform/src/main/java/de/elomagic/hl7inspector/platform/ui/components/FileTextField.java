/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileTextField extends JPanel {

    public final JLabel caption;
    public final JTextField edFile;

    public FileTextField() {
        this(GenericFileFilter.ALL_FILES_FILTER);
    }

    public FileTextField(@NotNull GenericFileFilter...fileFilter) {
        super(new BorderLayout(4, 0));

        edFile = new JTextField();
        caption = new JLabel();
        caption.setLabelFor(edFile);
        JButton btChooseFolder = new JButton("...");

        btChooseFolder.addActionListener(e-> SimpleDialog.chooseFile(
                Paths.get(edFile.getText()),
                e2 -> {

                },
                fileFilter
        ));

        add(caption, BorderLayout.WEST);
        add(edFile, BorderLayout.CENTER);
        add(btChooseFolder, BorderLayout.EAST);
    }

    @Nullable
    public Path getFile() {
        return StringUtils.isBlank(edFile.getText()) ? null : Path.of(edFile.getText());
    }

    public FileTextField setFile(@Nullable Path path) {
        edFile.setText(path == null ? "" : path.toString());
        return this;
    }

    @Nullable
    public String getCaption() {
        return caption.getText();
    }

    public FileTextField setCaption(@Nullable String text) {
        caption.setText(text);
        return this;
    }

}
