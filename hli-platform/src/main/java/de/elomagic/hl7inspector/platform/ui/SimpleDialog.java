/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import com.alee.extended.filechooser.WebDirectoryChooser;
import com.alee.laf.window.WebDialog;
import com.alee.utils.SwingUtils;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.listener.PathSelectedListener;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;
import java.awt.BorderLayout;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.Objects;

public final class SimpleDialog {

    public static final int YES_OPTION = JOptionPane.YES_OPTION;
    public static final int NO_OPTION = JOptionPane.NO_OPTION;

    public static final String[] YES_NO_OPTION = new String[] { LocaleTool.get("yes"), LocaleTool.get("no") };

    public static class SelectionChosenEvent {

        private final Integer index;
        private final String option;
        private final boolean remember;

        public SelectionChosenEvent(int index, @NotNull String option, boolean remember) {
            this.index = index;
            this.option = option;
            this.remember = remember;
        }

        public int getIndex() {
            return index;
        }

        public String getOption() {
            return option;
        }

        public boolean isRemember() {
            return remember;
        }
    }

    @FunctionalInterface
    public interface SelectionChosenListener {
        void choose(SelectionChosenEvent event);
    }

    static class Dialog extends WebDialog<Dialog> {

        Dialog() {
            super(SimpleDialog.getParent());

            setModal(true);
            setLayout(new BorderLayout());
        }

    }

    private SimpleDialog() {
    }

    private static String getDetailedErrorMessage(@Nullable Throwable t) {
        if (t == null) {
            return null;
        }

        Throwable cause = t;
        String errorMessage = cause.getMessage();

        while (errorMessage == null && cause.getCause() != null) {
            cause = cause.getCause();
            errorMessage = cause.getMessage();
        }

        return errorMessage == null ? t.toString() : errorMessage;
    }

    public static String ask(String text, String def) {
        return JOptionPane.showInputDialog(getParent(), text, def);
    }

    public static void error(String text) {
        showExtendedDialog(LocaleTool.get("error"), text, JOptionPane.ERROR_MESSAGE);
    }

    public static void error(@NotNull Exception e) {
        String errorMessage = getDetailedErrorMessage(e);
        JOptionPane.showMessageDialog(getParent(), errorMessage + "\n\n" + getFormattedExceptionText(e, 20), LocaleTool.get("error"), JOptionPane.ERROR_MESSAGE);
    }

    public static void error(String text, @NotNull Exception ex) {
        String errorMessage = getDetailedErrorMessage(ex);

        JOptionPane.showMessageDialog(getParent(), text + "\n\n" + errorMessage + "\n" + getFormattedExceptionText(ex, 20), LocaleTool.get("error"), JOptionPane.ERROR_MESSAGE);
    }

    public static void info(String text) {
        JOptionPane.showMessageDialog(getParent(), text, LocaleTool.get("information"), JOptionPane.INFORMATION_MESSAGE);
    }

    public static void info(String title, String text) {
        showExtendedDialog(title, text, JOptionPane.INFORMATION_MESSAGE);
    }

    public static void warn(String text) {
        showExtendedDialog(LocaleTool.get("warning"), text, JOptionPane.WARNING_MESSAGE);
    }

    public static void warn(String title, String text) {
        showExtendedDialog(title, text, JOptionPane.WARNING_MESSAGE);
    }

    public static int confirmYesNo(String text) {
        Integer index = choose(text, YES_NO_OPTION, false).getIndex();
        return index == null ? NO_OPTION : index;
    }

    /**
     * Show a file chooser dialog.
     */
    public static void chooseFile(@Nullable Path initFile, @NonNls @Nullable String title, @NotNull PathSelectedListener listener, @Nullable FileFilter...fileFilters) {
        Path folder = initFile == null || initFile.getParent() == null ? Configuration.getInstance().getAppFilesLastUsedFolder() : initFile;
        folder = Files.isDirectory(folder) ? folder : folder.getParent();

        JFileChooser fc = new JFileChooser(folder.toFile());
        fc.setSelectedFile(initFile == null || Files.isDirectory(initFile) ? null : initFile.toFile());
        if (fileFilters != null && fileFilters.length > 0) {
            Arrays.stream(fileFilters)
                    .filter(Objects::nonNull)
                    .forEach(fc::addChoosableFileFilter);
            if (fileFilters[0] != null) {
                fc.setFileFilter(fileFilters[0]);
            }
        }
        fc.setDialogTitle(title == null ? LocaleTool.get("choose_file") : title);
        if(fc.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION) {
            fc.setVisible(false);

            Configuration.getInstance().setAppFilesLastUsedFolder(fc.getCurrentDirectory().toPath());

            listener.actionPerformed(new PathSelectedListener.PathSelectedEvent(fc.getSelectedFile().getAbsoluteFile().toPath()));
        }
    }

    /**
     * Show a file chooser dialog.
     */
    public static void chooseFile(@Nullable Path initFile, @NotNull PathSelectedListener listener, @Nullable FileFilter...fileFilters) {
        chooseFile(initFile, null, listener, fileFilters);
    }

    /**
     * Show a file saver dialog.
     */
    public static void saveFile(@Nullable Path initFile, @Nullable String title, @NotNull PathSelectedListener listener, @Nullable FileFilter...fileFilters) {
        Path folder = initFile == null || initFile.getParent() == null ? Configuration.getInstance().getAppFilesLastUsedFolder() : initFile;
        folder = Files.isDirectory(folder) ? folder : folder.getParent();

        JFileChooser fc = new JFileChooser(folder.toFile());
        fc.setSelectedFile(initFile == null || Files.isDirectory(initFile)  ? null : initFile.toFile());
        if (fileFilters != null && fileFilters.length > 0) {
            Arrays.stream(fileFilters).forEach(fc::addChoosableFileFilter);
            fc.setFileFilter(fileFilters[0]);
        }
        fc.setDialogTitle(title == null ? LocaleTool.get("save_as_dots") : title);
        if(fc.showSaveDialog(getParent()) == JFileChooser.APPROVE_OPTION) {
            fc.setVisible(false);

            Configuration.getInstance().setAppFilesLastUsedFolder(fc.getCurrentDirectory().toPath());

            listener.actionPerformed(new PathSelectedListener.PathSelectedEvent(fc.getSelectedFile().getAbsoluteFile().toPath()));
        }
    }

    /**
     * Show a folder chooser dialog.
     */
    public static void chooseDirectory(@Nullable Path initDirectory, @NotNull PathSelectedListener listener) {
        Path folder = initDirectory == null ? Configuration.getInstance().getAppFilesLastUsedFolder() : initDirectory;

        File result = WebDirectoryChooser.showDialog(getParent(), LocaleTool.get("choose_folder"), folder.toFile());
        if (result != null) {
            listener.actionPerformed(new PathSelectedListener.PathSelectedEvent(result.toPath()));
        }
    }

    /**
     * Show a folder chooser dialog.
     *
     * @return File path when user has approve selection otherwise initDirectory
     */
    public static Path chooseDirectory(@NotNull Path initDirectory) {
        File result = WebDirectoryChooser.showDialog(getParent(), LocaleTool.get("choose_folder"), initDirectory.toFile());

        return result == null ? initDirectory : result.toPath();
    }

    private static String getFormattedExceptionText(@NotNull Exception e, int maxLines) {
        String s = "";
        if(e.getMessage() != null) {
            s = LocaleTool.get("message_1arg", e.getMessage());
        }

        int lines = 0;
        for(int i = 0; i < e.getStackTrace().length && i < maxLines; i++) {
            s = s.concat("\n" + e.getStackTrace()[i].toString());
            lines++;
        }

        if(lines > e.getStackTrace().length) {
            s = s.concat("...");
        }

        return s;
    }

    private static Window getParent() {
        return KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow();
    }

    /**
     * Opens an user selection dialog.
     *
     * @param text Text which describe the selection
     * @param options Selection to choose
     * @param enableRemember When true, a remember checkbox is also visible
     * @param listener Executed when user has made selection
     * todo must be async
     */
    public static void choose(@NotNull String text, @NotNull String[] options, boolean enableRemember, @NotNull SelectionChosenListener listener) {
        listener.choose(choose(text, options, enableRemember));
    }

    /**
     * Opens an user selection dialog.
     *
     * @param text Text which describe the selection
     * @param options Selection to choose
     * @param enableRemember When true, a remember checkbox is also visible
     */
    private static SelectionChosenEvent choose(@NotNull String text, @NotNull String[] options, boolean enableRemember) {
        Dialog dialog = new Dialog();
        dialog.setTitle(LocaleTool.get("confirmation"));
        AtomicInteger result = new AtomicInteger(-1);

        JCheckBox cbDontAskAgain = new JCheckBox(LocaleTool.get("do_not_ask_me_again"));

        JButton[] buttons = IntStream.range(0, options.length)
                .mapToObj(i -> {
                    JButton b = new JButton(options[i]);
                    b.addActionListener(e -> {
                        result.set(i);
                        dialog.setVisible(false);
                    });
                    return b;
                })
                .toArray(JButton[]::new);

        SwingUtils.equalizeComponentsWidth(buttons);

        JPanel grid = FormBuilder.createBuilder()
                .addRowSpacer()

                .add(new JLabel(IconThemeManager.getImageIcon("question-32.png")), 1, 0, 3, 0)
                .addRow(new JLabel(text), 3, 1)

                .setColumnOffset(1)

                .add(new JLabel(""), 1, 1)
                .add(1, buttons)
                .addRow(new JLabel(""), 1, 1)

                .addRow(enableRemember ? cbDontAskAgain : new JLabel(""), 3, 1)

                .addRowSpacer()
                .build();

        dialog.add(grid, BorderLayout.CENTER);
        dialog.pack();
        dialog.center();
        dialog.setVisible(true);

        return new SelectionChosenEvent(result.get(), options[result.get()], enableRemember && cbDontAskAgain.isSelected());
    }

    private static void showExtendedDialog(String headerText, String message, int messageType) {
        String windowTitle;
        String i;

        switch(messageType) {
            case JOptionPane.ERROR_MESSAGE:
                windowTitle = LocaleTool.get("error");
                i = "critical-32.png";
                break;
            case JOptionPane.INFORMATION_MESSAGE:
                windowTitle = LocaleTool.get("information");
                i = "info-32.png";
                break;
            case JOptionPane.WARNING_MESSAGE:
                windowTitle = LocaleTool.get("warning");
                i = "warning-32.png";
                break;
            default:
                windowTitle = "Message";
                i = "info-32.png";
        }

        MyBaseDialog dlg = new MyBaseDialog();
        dlg.setTitle(windowTitle);

        dlg.setBannerTitle(headerText);
        dlg.setBannerIcon(IconThemeManager.getImageIcon(i));
        dlg.setDialogMode(MyBaseDialog.Mode.CLOSE);
        dlg.setSize(640, 400);
        dlg.setModal(true);
        dlg.center();

        JTextArea ta = new JTextArea(message);
        ta.setEditable(false);
        JScrollPane sp = new JScrollPane(ta);

        dlg.getContentPane().add(sp);

        dlg.ask();
    }

}
