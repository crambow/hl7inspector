/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.listener;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/**
 * Interface to simplify and handle only show and hide of component events.
 */
@FunctionalInterface
public interface SimplifyComponentVisibilityListener extends ComponentListener {

    @Override
    default void componentMoved(ComponentEvent e) {
        // noop
    }

    @Override
    default void componentResized(ComponentEvent e) {
        // noop
    }

    @Override
    default void componentShown(ComponentEvent e) {
        componentVisibilityChanged(e);
    }

    @Override
    default void componentHidden(ComponentEvent e) {
        componentVisibilityChanged(e);
    }

    void componentVisibilityChanged(ComponentEvent e);

}
