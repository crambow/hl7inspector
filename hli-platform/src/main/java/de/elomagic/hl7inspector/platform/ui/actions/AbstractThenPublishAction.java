/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.actions;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;

import java.awt.event.ActionEvent;
import java.util.function.Function;

@Component
public abstract class AbstractThenPublishAction extends AbstractMyAction<AbstractThenPublishAction> {

    private final transient EventMessengerProcessor eventMessengerProcessor;

    private transient Function<ActionEvent, Object> thenPublish;
    private Class<? extends ApplicationEvent> actionEventClass;

    protected @Autowired AbstractThenPublishAction(@NotNull EventMessengerProcessor eventMessengerProcessor) {
        super();

        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @Override
    public final void actionPerformed(@NotNull ActionEvent e) {

        Object event = null;

        if (thenPublish != null) {
             event = thenPublish.apply(e);
        }

        if (actionEventClass != null) {
            try {
                event = actionEventClass.getConstructor(Object.class).newInstance(e.getSource());
            } catch (Exception ex) {
                throw new IllegalStateException("Unexpected exception occur: " + ex.getMessage(), ex);
            }
        }

        if (event != null) {
            eventMessengerProcessor.publishEvent(event);
        }
    }

    public AbstractThenPublishAction thenPublish(@NotNull Function<ActionEvent, Object> thenPublish) {
        this.thenPublish = thenPublish;

        return this;
    }

    public AbstractThenPublishAction thenPublish(@NotNull Class<? extends ApplicationEvent> actionEventClass) {
        this.actionEventClass = actionEventClass;

        return this;
    }

}
