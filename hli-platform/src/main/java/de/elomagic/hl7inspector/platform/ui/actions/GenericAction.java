/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.actions;

import de.elomagic.hl7inspector.platform.ui.listener.GenericListener;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.PropertyKey;

import java.awt.event.ActionEvent;

public class GenericAction extends AbstractMyAction<GenericAction> {

    private final GenericListener listener;

    public GenericAction(@NotNull GenericListener listener) {
        super();

        this.listener = listener;
    }

    public GenericAction(@NotNull GenericListener listener, @Nls @NotNull @PropertyKey(resourceBundle = LocaleTool.DEFAULT_BUNDLE) String nameKey) {
        super();

        this.listener = listener;

        setName(nameKey);
    }

    public GenericAction(@NotNull GenericListener listener, @Nls @NotNull @PropertyKey(resourceBundle = LocaleTool.DEFAULT_BUNDLE) String nameKey, @NotNull String iconName) {
        super();

        this.listener = listener;

        setName(nameKey);
        setSmallIcon(iconName);
    }

    @Override
    public final void actionPerformed(@NotNull ActionEvent e) {
        listener.action(e);
    }
}
