/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;

public final class Html {

    public static final String BR = "<br>";
    public static final String HR = "<hr>";

    private Html() {

    }

    /**
     * Surrounds tex with a XML element.
     *
     * Note: No XML escaping will be done by this method.
     *
     * @param text Object to surround. Method {@link Object#toString()} in use.
     * @param elementName Name of the element
     * @param classes When set then class attribute with given value will be created
     * @return A string but never null;
     */
    public static String surroundWithElement(@Nullable @NonNls Object text, @NotNull @NonNls String elementName, @NonNls String...classes) {
        if (text == null) {
            return "<" + elementName + " />";
        }

        String result = "<" + elementName + createClassAttribute(classes) + ">";
        result += text.toString();
        result += "</" + elementName + ">";

        return result;
    }

    public static String createClassAttribute(@NonNls String...classes) {
        if (classes == null || classes.length == 0) {
            return "";
        }

        return " class=\"" + StringUtils.join(classes, " ") + "\"";
    }

    public static String surroundFontElement(@Nullable @NonNls Object text, @NotNull Color color) {
        if (text == null) {
            return "";
        }

        String result = createFontElement(color);
        result += text.toString();
        result += "</font>";

        return result;
    }

    public static String createFontElement(@NotNull Color color) {
        String result = "<font color=\"#";
        result += Integer.toHexString(color.getRGB() & 0xffffff);
        result += "\">";

        return result;
    }

}
