/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import com.alee.extended.overlay.WebOverlay;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.panel.WebPanel;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.listener.GenericListener;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.ui.themes.SkinExtensionStyles;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.formatter.DateOverlayFormatter;
import de.elomagic.hl7inspector.platform.utils.formatter.RegExOverlayFormatter;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.PropertyKey;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.DefaultFormatterFactory;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Enumeration;

public class UIFactory {

    private UIFactory() {
    }

    @NotNull
    public static JLabel createLabel(@NonNls @NotNull String text) {
        JLabel label = new JLabel(text);
        label.setBorder(BorderFactory.createEmptyBorder(1, 3, 1, 3));

        return label;
    }

    @NotNull
    public static JLabel createLabel(@NonNls @NotNull String text, String iconName) {
        JLabel label = createLabel(text);

        label.setIcon(IconThemeManager.getImageIcon(iconName));

        return label;
    }

    @NotNull
    public static JLabel createH2Label(@Nls @NotNull @PropertyKey(resourceBundle = LocaleTool.DEFAULT_BUNDLE) String textKey) {
        JLabel title = createLabel(LocaleTool.get(textKey));

        title.setBackground(UIManager.getColor("activeCaptionBorder"));
        title.setForeground(UIManager.getColor("activeCaptionText"));
        title.setFont(title.getFont().deriveFont(Font.BOLD));
        title.setOpaque(true);
        title.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));

        return title;
    }

    @NotNull
    public static JLabel createH1Label(@Nls @NotNull @PropertyKey(resourceBundle = LocaleTool.DEFAULT_BUNDLE) String textKey) {
        JLabel title = createLabel(LocaleTool.get(textKey));

        title.setFont(Font.decode("Arial"));
        title.setFont(title.getFont().deriveFont(Font.BOLD));
        title.setFont(title.getFont().deriveFont(Float.parseFloat("20")));
        title.setBorder(new EmptyBorder(0, 0, 4, 0));

        return title;
    }

    @NotNull
    public static JPanel createBanner(@Nullable String title, @Nullable Icon icon) {
        JLabel titleLabel = new JLabel();
        titleLabel.setOpaque(false);
        titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD));
        titleLabel.setText(title);

        JPanel nestedPane = new JPanel(new BorderLayout());
        nestedPane.setOpaque(false);
        nestedPane.add(BorderLayout.NORTH, titleLabel);

        JLabel iconLabel = new JLabel();
        iconLabel.setPreferredSize(new Dimension(50, 50));
        iconLabel.setIcon(icon);

        WebPanel b = new WebPanel();
        b.setLayout(new BorderLayout());
        b.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        b.setStyleId(SkinExtensionStyles.dialogBanner);
        b.add(BorderLayout.CENTER, nestedPane);
        b.add(BorderLayout.EAST, iconLabel);

        return b;
    }

    /**
     * Creates a flat button.
     *
     * @param c Button to create
     * @param text Caption of th ebutton
     * @return A new instance of a button
     */
    @NotNull
    public static <T extends AbstractButton> T createFlatIconButton(@NotNull Class<? extends T> c, @NotNull String text) {
        T result;
        try {
            result = c.getConstructor(String.class).newInstance(text);
            result.putClientProperty( StyleId.STYLE_PROPERTY, StyleId.buttonHover);
        } catch(ReflectiveOperationException e) {
            throw new UIFactoryException(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Creates a flat button.
     *
     * @param c Button to create
     * @param iconName Resource name
     * @param listener Will called even when button was clicked
     * @return A new instance of a button
     */
    @NotNull
    public static <T extends AbstractButton> T createFlatIconButton(@NotNull Class<? extends T> c, @NotNull String iconName, @NotNull GenericListener listener) {
        T result;
        try {
            GenericAction action = new GenericAction(listener).setSmallIcon(iconName);

            result = c.getConstructor(Action.class).newInstance(action);
            result.putClientProperty( StyleId.STYLE_PROPERTY, StyleId.buttonIconHover);
        } catch(ReflectiveOperationException e) {
            throw new UIFactoryException(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Creates a flat button.
     *
     * @param c Button to create
     * @param action Action which will be executed after clicking
     * @return A new instance of a button
     */
    @NotNull
    public static <T extends AbstractButton> T createFlatIconButton(@NotNull Class<? extends T> c, @NotNull AbstractAction action) {
        T result;
        try {
            result = c.getConstructor(Action.class).newInstance(action);
            result.putClientProperty( StyleId.STYLE_PROPERTY, StyleId.buttonIconHover);
        } catch(ReflectiveOperationException e) {
            throw new UIFactoryException(e.getMessage(), e);
        }

        return result;
    }

    @NotNull
    public static JComboBox<Font> createFontComboBox() {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font[] fonts = Arrays
                .stream(ge.getAvailableFontFamilyNames())
                .map(name -> new FontItem(name, Font.PLAIN, 12))
                .toArray(Font[]::new);

        JComboBox<Font> cbFont = new JComboBox<>(fonts);
        cbFont.putClientProperty(StyleId.STYLE_PROPERTY, StyleId.combobox);
        cbFont.setRenderer(new FontNameListCellRenderer());
        cbFont.setEditable(false);

        return cbFont;
    }

    @NotNull
    public static WebComboBox createCharsetComboBox() {
        WebComboBox cb = new WebComboBox(new Charset[]{
                StandardCharsets.ISO_8859_1 ,
                StandardCharsets.US_ASCII,
                StandardCharsets.UTF_8,
                StandardCharsets.UTF_16BE,
                StandardCharsets.UTF_16LE,
                StandardCharsets.UTF_16
        });
        try {
            cb.setSelectedItem(StandardCharsets.ISO_8859_1);
        } catch (Exception e) {
            cb.setSelectedItem(StandardCharsets.US_ASCII);
        }

        return cb;
    }

    /**
     * Wrapped an empty {@link JFormattedTextField} with an regular expression formatter which overlays with an error icon when validation failed.
     * @param textField A text field
     * @param regex Regular expression
     * @return Returns a web overlay container with the given text field
     */
    @NotNull
    public static WebOverlay wrapFormattedTextField(@NotNull JFormattedTextField textField, @NotNull String regex) {
        WebOverlay overlay = new WebOverlay();

        textField.setFocusLostBehavior(JFormattedTextField.PERSIST);
        textField.setFormatterFactory(new DefaultFormatterFactory(new RegExOverlayFormatter(overlay, regex)));
        overlay.setContent(textField);

        return overlay;
    }

    /**
     * Wrapped an empty {@link JFormattedTextField} with an date format which overlays with an error icon when validation failed.
     * @param textField A text field
     * @param dateFormat Date format
     * @return Returns a web overlay container with the given text field
     */
    @NotNull
    public static WebOverlay wrapFormattedTextField(@NotNull JFormattedTextField textField, @NotNull DateFormat dateFormat) {
        WebOverlay overlay = new WebOverlay();

        textField.setFocusLostBehavior(JFormattedTextField.PERSIST);
        textField.setFormatterFactory(new DefaultFormatterFactory(new DateOverlayFormatter(overlay, dateFormat)));
        overlay.setContent (textField);

        return overlay;
    }

    public static void setDefaultUIFont(){
        Enumeration<Object> keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof FontUIResource) {
                FontUIResource fr = (FontUIResource) value;
                FontUIResource nr = new FontUIResource("Arial", fr.getStyle(), fr.getSize());

                UIManager.put(key, nr);
            }
        }
    }

}
