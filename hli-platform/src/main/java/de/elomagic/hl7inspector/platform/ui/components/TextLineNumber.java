/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import com.alee.laf.panel.WebPanel;

import de.elomagic.hl7inspector.platform.ui.listener.SimplifyDocumentListener;
import de.elomagic.hl7inspector.platform.ui.themes.SkinExtensionStyles;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.CaretEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;

/**
 * Based and adopted from project https://github.com/wandora-team/wandora/blob/master/src/org/wandora/utils/swing/TextLineNumber.java
 */
public class TextLineNumber extends WebPanel {

    public static final float LEFT = 0.0f;
    public static final float CENTER = 0.5f;
    public static final float RIGHT = 1.0f;

    private static final Logger LOGGER = LogManager.getLogger(TextLineNumber.class);

    private static final Border OUTER = new MatteBorder(0, 0, 0, 1, Color.GRAY);

    private static final Color FONT_COLOR_LIGHT =  new Color(186, 74, 0);
    private static final Color FONT_COLOR_DARK = Color.WHITE;

    private static final int HEIGHT = Integer.MAX_VALUE - 1000000;

    //  Text component this TextTextLineNumber component is in sync with
    private final JTextComponent component;

    //  Properties that can be changed
    private boolean updateFont;
    private int borderGap;
    private float digitAlignment;
    private int minimumDisplayDigits;

    // Keep history information to reduce the number of times the component
    // needs to be repainted
    private int lastDigits;
    private double lastHeight;
    private int lastLine;

    /**
     * Create a line number component for a text component. This minimum display width will be based on 3 digits.
     *
     * @param component the related text component
     */
    public TextLineNumber(JTextComponent component) {
        this(component, 3);
    }

    /**
     * Create a line number component for a text component.
     *
     * @param component the related text component
     * @param minimumDisplayDigits the number of digits used to calculate the
     * minimum width of the component
     */
    public TextLineNumber(JTextComponent component, int minimumDisplayDigits) {
        super(SkinExtensionStyles.panelTextLines);

        this.component = component;

        setFont(component.getFont());

        setBorderGap(2);
        setDigitAlignment(RIGHT);
        setMinimumDisplayDigits(minimumDisplayDigits);

        component.getDocument().addDocumentListener((SimplifyDocumentListener) e -> documentChanged());
        component.addCaretListener(this::caretUpdate);
        component.addPropertyChangeListener("font", this::propertyChange);
    }

    /**
     * Gets the update font property
     *
     * @return the update font property
     */
    public boolean getUpdateFont() {
        return updateFont;
    }

    /**
     * Set the update font property. Indicates whether this Font should be
     * updated automatically when the Font of the related text component is
     * changed.
     *
     * @param updateFont when true update the Font and repaint the line numbers,
     * otherwise just repaint the line numbers.
     */
    public void setUpdateFont(boolean updateFont) {
        this.updateFont = updateFont;
    }

    /**
     * Gets the border gap
     *
     * @return the border gap in pixels
     */
    public int getBorderGap() {
        return borderGap;
    }

    /**
     * The border gap is used in calculating the left and right insets of the border. Default value is 5.
     *
     * @param borderGap the gap in pixels
     */
    public void setBorderGap(int borderGap) {
        this.borderGap = borderGap;
        Border inner = new EmptyBorder(0, borderGap, 0, borderGap);
        setBorder(new CompoundBorder(OUTER, inner));
        lastDigits = 0;
        setPreferredWidth();
    }

    /**
     * Gets the digit alignment.
     *
     * @return the alignment of the painted digits
     */
    public float getDigitAlignment() {
        return digitAlignment;
    }

    /**
     * Specify the horizontal alignment of the digits within the component.
     * Common values would be:
     * <ul>
     * <li>TextLineNumber.LEFT
     * <li>TextLineNumber.CENTER
     * <li>TextLineNumber.RIGHT (default)
     * </ul>
     *
     * @param digitAlignment Alignment
     */
    public void setDigitAlignment(float digitAlignment) {
        this.digitAlignment = digitAlignment > 1.0f ? 1.0f : digitAlignment < 0.0f ? -1.0f : digitAlignment;
    }

    /**
     * Gets the minimum display digits.
     *
     * @return the minimum display digits
     */
    public int getMinimumDisplayDigits() {
        return minimumDisplayDigits;
    }

    /**
     * Specify the minimum number of digits used to calculate the preferred width of the component. Default is 3.
     *
     * @param minimumDisplayDigits the number digits used in the preferred width calculation
     */
    public void setMinimumDisplayDigits(int minimumDisplayDigits) {
        this.minimumDisplayDigits = minimumDisplayDigits;
        setPreferredWidth();
    }

    /**
     * Calculate the width needed to display the maximum line number.
     */
    private void setPreferredWidth() {
        Element root = component.getDocument().getDefaultRootElement();
        int lines = root.getElementCount();
        int digits = Math.max(String.valueOf(lines).length(), minimumDisplayDigits);

        //  Update sizes when number of digits in the line number changes
        if (lastDigits != digits) {
            lastDigits = digits;
            FontMetrics fontMetrics = getFontMetrics(getFont());
            int width = fontMetrics.charWidth('0') * digits;
            Insets insets = getInsets();
            int preferredWidth = insets.left + insets.right + width;

            Dimension d = getPreferredSize();
            d.setSize(preferredWidth, HEIGHT);
            setPreferredSize(d);
            setSize(d);
        }
    }

    /**
     * Draw the line numbers.
     */
    @Override
    public void paintComponent(Graphics g) {
        try {
            if (g instanceof Graphics2D) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
            }

            super.paintComponent(g);

            FontMetrics fontMetrics = component.getFontMetrics(component.getFont());

            //	Determine the width of the space available to draw the line number
            FontMetrics fm = getFontMetrics(getFont());
            Insets insets = getInsets();
            int availableWidth = getSize().width - insets.left - insets.right;

            // Add space at the end to prevent counting of termination line break.
            String[] lines = StringUtils.split(component.getText() + " ", "\n");

            int lineNumber = 0;
            Rectangle2D rect = component.modelToView2D(0);
            double y = rect.getY() + rect.getHeight();

            for (String line : lines) {
                lineNumber++;

                if (isCurrentLine(lineNumber)) {
                    // Not pretty but it works
                    boolean lightSkin = getSkin().getId().contains("light");
                    g.setColor(lightSkin ? FONT_COLOR_LIGHT : FONT_COLOR_DARK);
                } else {
                    g.setColor(getForeground());
                }

                //  "X" and "Y" offsets for drawing the string.
                String text = Integer.toString(lineNumber);
                int stringWidth = fm.stringWidth(text);
                int x = getOffsetX(availableWidth, stringWidth) + insets.left;

                LineMetrics lineMetrics = fontMetrics.getLineMetrics(line, g);
                g.drawString(text, x, (int)Math.floor(y - lineMetrics.getDescent()));

                y += lineMetrics.getHeight() + 1;
            }
        } catch (BadLocationException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    /*
     * We need to know if the caret is currently positioned on the line we are about to paint so the line number can be highlighted.
     */
    private boolean isCurrentLine(int lineNumber) {
        int caretPosition = component.getCaretPosition();
        Element root = component.getDocument().getDefaultRootElement();
        int currentLine = root.getElementIndex(caretPosition);

        return currentLine == lineNumber-1;
    }

    /**
     * Determine the X offset to properly align the line number when drawn.
     */
    private int getOffsetX(int availableWidth, int stringWidth) {
        return (int) ((availableWidth - stringWidth) * digitAlignment);
    }

    private void caretUpdate(CaretEvent e) {
        //  Get the line the caret is positioned on

        int caretPosition = component.getCaretPosition();
        Element root = component.getDocument().getDefaultRootElement();
        int currentLine = root.getElementIndex(caretPosition);

        //  Need to repaint so the correct line number can be highlighted
        if (lastLine != currentLine) {
            repaint();
            lastLine = currentLine;
        }
    }

    /**
     * A document change may affect the number of displayed lines of text.
     * Therefore the lines numbers will also change.
     */
    private void documentChanged() {
        //  View of the component has not been updated at the time
        //  the DocumentEvent is fired
        SwingUtilities.invokeLater(() -> {
            try {
                int endPos = component.getDocument().getLength();
                Rectangle2D rect = component.modelToView2D(endPos);

                if (rect != null && rect.getY() != lastHeight) {
                    setPreferredWidth();
                    repaint();
                    lastHeight = rect.getY();
                }
            } catch (BadLocationException ex) { /* nothing to do */ }
        });
    }

    private void propertyChange(PropertyChangeEvent evt) {
        if (evt.getNewValue() instanceof Font) {
            if (updateFont) {
                Font newFont = (Font) evt.getNewValue();
                setFont(newFont);
                lastDigits = 0;
                setPreferredWidth();
            } else {
                repaint();
            }
        }
    }

    /**
     * Determines the ending row model position of the row that contains
     * the specified model position.  The component given must have a
     * size to compute the result.  If the component doesn't have a size
     * a value of -1 will be returned.
     *
     * @param c the editor
     * @param offs the offset in the document &gt;= 0
     * @return the position &gt;= 0 if the request can be computed, otherwise
     *  a value of -1 will be returned.
     * @exception BadLocationException if the offset is out of range
     */
    public static int getRowEnd(JTextComponent c, int offs) throws BadLocationException {
        Rectangle2D r = c.modelToView2D(offs);
        if (r == null) {
            return -1;
        }
        int n = c.getDocument().getLength();
        int lastOffs = offs;
        double y = r.getY();
        while ((r != null) && (y == r.getY())) {
            // Skip invisible elements
            if (r.getHeight() !=0) {
                offs = lastOffs;
            }
            lastOffs += 1;
            r = (lastOffs <= n) ? c.modelToView2D(lastOffs) : null;
        }
        return offs;
    }
}