/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import com.alee.laf.combobox.ComboBoxCellParameters;
import com.alee.laf.combobox.WebComboBoxRenderer;
import com.alee.laf.panel.WebPanel;
import com.alee.managers.style.StyleId;
import de.elomagic.hl7inspector.platform.ui.themes.SkinExtensionStyles;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

public class FontNameListCellRenderer extends WebComboBoxRenderer<Font, JList<Font>, ComboBoxCellParameters<Font, JList<Font>>> {

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

        JComponent c = (JComponent)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        Font font = (Font)value;

        JLabel labelName = new JLabel(font.getName());

        JLabel labelSample = new JLabel("HL7 Inspector, 你好");
        labelSample.setFont(font.deriveFont((float)c.getFont().getSize()));

        WebPanel panel = new WebPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        if (isSelected) {
            panel.setStyleId(SkinExtensionStyles.panelGradientY);
        }
        panel.putClientProperty(StyleId.STYLE_PROPERTY, c.getClientProperty(StyleId.STYLE_PROPERTY));
        panel.setOpaque(true);
        panel.setPreferredSize(c.getPreferredSize());
        panel.add(Box.createRigidArea(new Dimension(3, 0)));
        panel.add(labelName);
        panel.add(Box.createRigidArea(new Dimension(5, 0)));
        panel.add(new JLabel("-"));
        panel.add(Box.createRigidArea(new Dimension(5, 0)));
        panel.add(labelSample);

        return panel;

    }

}
