/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Helper class to migrate old configuration in new.
 */
public class ConfigurationMigrator {

    private enum KeyOld {

        AUTO_UPDATE_PERIOD("autoupdate-period", AppConfigurationKey.APP_UPDATE_CHECK_TIME_PERIOD.getKeyName(), false),
        AUTO_UPDATE_ASK("autoupdate-ask", AppConfigurationKey.APP_UPDATE_CHECK_TIME_ASK_USER.getKeyName(), false),
        AUTO_UPDATE_LAST_CHECK("autoupdate-last-check", AppConfigurationKey.APP_UPDATE_CHECK_TIME_LAST_CHECK.getKeyName(), false),

        APP_QUIT_WITHOUT_ASKING("application.quit.without.asking", AppConfigurationKey.APP_QUIT_WITHOUT_ASKING.getKeyName(), false),
        APP_LOCALE("application-locale", AppConfigurationKey.APP_LANGUAGE.getKeyName(), false),
        APP_ONE_INSTANCE("application-one-instance", AppConfigurationKey.APP_SINGLE_INSTANCE.getKeyName(), false),
        NETWORK_PROXY_MODE("network-proxy-mode", AppConfigurationKey.APP_NETWORK_PROXY_MODE.getKeyName(), false),
        NETWORK_PROXY_HOST("network-proxy-host", AppConfigurationKey.APP_NETWORK_PROXY_HOST.getKeyName(), false),
        NETWORK_PROXY_PORT("network-proxy-port", AppConfigurationKey.APP_NETWORK_PROXY_PORT.getKeyName(), false),

        APP_ICON_THEME("application-icon-theme", AppConfigurationKey.APP_UI_LAF_THEME.getKeyName(), false),

        DESKTOP_X("desktop.x", AppConfigurationKey.APP_UI_DESKTOP_POSITION_X.getKeyName(), false),
        DESKTOP_Y("desktop.y", AppConfigurationKey.APP_UI_DESKTOP_POSITION_Y.getKeyName(), false),
        DESKTOP_W("desktop.w", AppConfigurationKey.APP_UI_DESKTOP_DIMENSION_WIDTH.getKeyName(), false),
        DESKTOP_H("desktop.h", AppConfigurationKey.APP_UI_DESKTOP_DIMENSION_HEIGHT.getKeyName(), false),
        DESKTOP_TOOLBAR_POSITION("desktop-toolbar_position", AppConfigurationKey.APP_UI_TOOLBAR_ORIENTATION.getKeyName(), false),

        DESKTOP_VIEW_MODE("desktop-view-mode", AppConfigurationKey.APP_EDITOR_VIEW_MODE.getKeyName(), false),
        MESSAGE_EDITOR_FONT_NAME("message-editor-font-name", AppConfigurationKey.APP_EDITOR_TEXT_FONT_NAME.getKeyName(), false),
        MESSAGE_EDITOR_FONT_SIZE("message-editor-font-size", AppConfigurationKey.APP_EDITOR_TEXT_FONT_SIZE.getKeyName(), false),
        TREE_DISPLAY_MESSAGE_NODE("tree.display.message.node", AppConfigurationKey.APP_EDITOR_TREE_NODE_MESSAGE_FORMAT.getKeyName(), false),
        TREE_NODE_LENGTH("tree-node-length", AppConfigurationKey.APP_EDITOR_TREE_NODE_MAX_LENGTH.getKeyName(), false),
        TREE_FONT_NAME("tree-font-name", AppConfigurationKey.APP_EDITOR_TREE_FONT_NAME.getKeyName(), false),
        TREE_FONT_SIZE("tree-font-size", AppConfigurationKey.APP_EDITOR_TREE_FONT_SIZE.getKeyName(), false),

        EXTERNAL_FILE_VIEWER("external-file-viewer", AppConfigurationKey.APP_FILES_EXTERNAL_FILE_VIEWER.getKeyName(), false),
        WORKING_PATH("working-path", AppConfigurationKey.APP_FILES_LAST_USED_FOLDER.getKeyName(), false),
        RECENT_FILE("recent-file", AppConfigurationKey.APP_FILES_RECENTLY_OPENED.getKeyName(), true), // List of sub items
        DESKTOP_OPEN_FILES_MAX("desktop.openFiles.max", AppConfigurationKey.APP_FILES_MAX_OPEN.getKeyName(), false),
        IMPORT_OPTIONS_CHARSET("import-options-charset", AppConfigurationKey.APP_FILES_PARSER_CHARSET.getKeyName(), false),

        PHRASE_HISTORY("phrase-history", AppConfigurationKey.APP_SEARCH_PHRASE_HISTORY.getKeyName(), true), // List of sub items

        DEFAULT_PROFILE("profile-default", AppConfigurationKey.APP_PROFILES_SELECTED_FILE.getKeyName(), false),
        PROFILE_FILE("profile-file", AppConfigurationKey.APP_PROFILES_AVAILABLE_FILES.getKeyName(), true), // List of sub items [.xxx]
        PROFILE_NAME("profile-name", AppConfigurationKey.APP_PROFILES_AVAILABLE_NAMES.getKeyName(), true), // List of sub items [.xxx]
        PROFILE_DESCRIPTION("profile-description", AppConfigurationKey.APP_PROFILES_AVAILABLE_DESCRIPTIONS.getKeyName(), true), // List of sub items [.xxx]

        SENDER_OPTIONS_DEST("sender.options.destination", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_HOSTNAME_HISTORY.getKeyName(), true), // List of sub items
        SENDER_SET_CHARSET("sender.set.charset", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_CHARSET.getKeyName(), false),
        SENDER_SET_FRAME("sender.set.frame", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_FRAME.getKeyName(), false),
        SENDER_SET_HOSTNAME("sender.set.hostname", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_HOSTNAME.getKeyName(), false),
        SENDER_SET_PORT("sender.set.port", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_PORT.getKeyName(), false),
        SENDER_SET_REUSE("sender.set.reuse", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_KEEP_SOCKET.getKeyName(), false),
        SENDER_TLS_CERTIFICATES("sender.tls.certificates", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_SERVER_CERTIFICATES.getKeyName(), false),
        SENDER_TLS_ENABLED("sender.tls.enabled", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_TLS_ENABLED.getKeyName(), false),

        RECEIVER_PORT("receiver.port", ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_PORT.getKeyName(), false),
        RECEIVER_SOCKET_REUSE("receiver.socket.reuse", ToolsConfigurationKey.TOOLS_NETWORK_SENDER_KEEP_SOCKET.getKeyName(), false),
        RECEIVER_TLS_PRIVATE_KEY("receiver.tls.privateKey", ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_PRIVATE_KEY.getKeyName(), false),
        RECEIVER_TLS_PASSWORD("receiver.tls.password", ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_PASSWORD.getKeyName(), false),
        RECEIVER_TLS_ENABLED("receiver.tls.enabled", ToolsConfigurationKey.TOOLS_NETWORK_RECEIVER_TLS_ENABLED.getKeyName(), false),

        VALUE_PRESENTER_SERVER_PORT("value.presenter.server.port", ToolsConfigurationKey.TOOLS_HTTP_SERVER_PORT.getKeyName(), false),

        VALUE_PRESENTER_STRING_MAX_SIZE("value.presenter.string.max-size", ToolsConfigurationKey.TOOLS_VALUE_PRESENTER_PLAIN_AUTO_VIEW_MAX_SIZE.getKeyName(), false),
        VALUE_PRESENTER_CDA_XSLT_FILE("value.presenter.cda.xslt.file", ToolsConfigurationKey.TOOLS_VALUE_PRESENTER_XML_XSLT_FILE.getKeyName(), false);

        private final String key;
        private final String keyNew;
        private final boolean set;

        KeyOld(String key, String keyNew, boolean set) {
            this.key = key;
            this.keyNew = keyNew;
            this.set = set;
        }
        
        public String getKeyOld() {
            return key;
        }

        public String getKeyNew() {
            return keyNew;
        }

        public boolean isSet() {
            return set;
        }

        @Nullable
        public static KeyOld valueByName(@NotNull String keyName) {
            for (KeyOld key : values()) {
                if (key.getKeyOld().equals(keyName)) {
                    return key;
                }
            }

            return null;
        }
    }

    private static final Logger LOGGER = LogManager.getLogger(ConfigurationMigrator.class);

    public static boolean exists() {
        return Files.exists(getConfigurationFileOld());
    }

    @Nullable
    public static Properties migrate() throws IOException {
        if (!exists()) {
            return null;
        }

        Properties properties = new Properties();
        Properties propertiesOld = load();

        AtomicInteger atomicInteger = new AtomicInteger(0);

        // Map set type items
        Arrays.stream(KeyOld.values())
                .filter(KeyOld::isSet)
                .forEach(k -> {
                    atomicInteger.set(0);
                    for (int i = 0; i<99; i++) {
                        String keyOld = k.getKeyOld() + "." + i;
                        String keyAlt =String.format("%s.%03d", k.getKeyOld(), i);
                        String keyNew =String.format("%s.%03d", k.getKeyNew(), atomicInteger.getAndIncrement());

                        String value = propertiesOld.getProperty(keyOld, propertiesOld.getProperty(keyAlt));

                        propertiesOld.remove(keyOld);
                        propertiesOld.remove(keyAlt);

                        if (value == null) {
                            continue;
                        }

                        properties.put(keyNew, value);
                    }
                });

        // Now map the rest of only single type items
        for (Map.Entry<Object, Object> entry : propertiesOld.entrySet()) {
            KeyOld key = KeyOld.valueByName(entry.getKey().toString());
            if (key == null) {
                // Could be a list!
                LOGGER.warn("Key '{}' currently not supported or deprecated.", entry.getKey());
            } else {
                properties.put(key.getKeyNew(), entry.getValue().toString());
            }
        }

        LOGGER.info("!!! Rename old configuration file");
        Files.move(getConfigurationFileOld(), Paths.get(getConfigurationFileOld().toString() + ".deprecated"));

        return properties;
    }

    @NotNull
    private static Path getConfigurationFileOld(){
        return Paths.get(System.getProperty("user.home"), ".hl7inspector-2.1", "hl7inspector.properties");
    }

    private static Properties load() throws IOException {
        Path configurationFile = getConfigurationFileOld();
        Properties properties = new Properties();
        LOGGER.info("Reading old configuration from file '{}'", configurationFile);
        try (InputStream in = Files.newInputStream(configurationFile, StandardOpenOption.READ)) {
            properties.loadFromXML(in);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }

        return properties;
    }

}
