/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.themes;

import com.alee.managers.style.Skin;
import com.alee.skin.light.WebLightSkin;
import org.jetbrains.annotations.NotNull;

public class ClassicIconTheme extends AbstractIconTheme {

    @Override
    @NotNull
    public String getId() {
        return "classic-theme";
    }

    @Override
    @NotNull
    public String getDisplayKey() {
        return "theme_classic_text";
    }

    @Override
    @NotNull
    public String getResourcePath() {
        return "de/elomagic/hl7inspector/themes/classic";
    }

    @Override
    public @NotNull Class<? extends Skin> getSkin() {
        return WebLightSkin.class;
    }

}
