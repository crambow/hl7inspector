/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.actions;

import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.PropertyKey;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import java.util.Objects;

public abstract class AbstractMyAction<T extends AbstractMyAction<T>> extends AbstractAction {

    @NotNull
    public T setName(@Nls @NotNull @PropertyKey(resourceBundle = LocaleTool.DEFAULT_BUNDLE) String resourceKey, Object...args) {
        putValue(NAME, LocaleTool.get(resourceKey, args));
        return (T)this;
    }

    @NotNull
    public T setShortDescription(@Nls @NotNull @PropertyKey(resourceBundle = LocaleTool.DEFAULT_BUNDLE) String resourceKey, Object...args) {
        putValue(SHORT_DESCRIPTION, LocaleTool.get(resourceKey, args));
        return (T)this;
    }

    @NotNull
    public T setSmallIcon(@NotNull String iconName) {
        setSmallIcon(Objects.requireNonNull(IconThemeManager.getImageIcon(iconName)));
        return (T)this;
    }

    @NotNull
    public T setSmallIcon(@NotNull ImageIcon icon) {
        putValue(SMALL_ICON, icon);
        return (T)this;
    }

    @NotNull
    public T setEmptySmallIcon() {
        putValue(SMALL_ICON, IconThemeManager.getImageIcon("clear.png"));
        return (T)this;
    }


    @NotNull
    public T setAcceleratorKey(@NotNull KeyStroke keyStroke) {
        putValue(ACCELERATOR_KEY, keyStroke);
        return (T)this;
    }

    @NotNull
    public T setMnemonicKey(int key) {
        putValue(MNEMONIC_KEY, key);
        return (T)this;
    }


}
