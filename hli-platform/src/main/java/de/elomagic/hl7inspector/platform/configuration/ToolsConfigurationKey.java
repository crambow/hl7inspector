/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.configuration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.Charset;
import java.nio.file.Path;

public enum ToolsConfigurationKey implements ConfigurationKey {

    TOOLS_NETWORK_SENDER_SOURCE_MODE("tools.network.sender.source.mode", Integer.class,false),
    TOOLS_NETWORK_SENDER_SOURCE_FOLDER("tools.network.sender.source.folder", Path.class,false),
    TOOLS_NETWORK_SENDER_HOSTNAME_HISTORY("tools.network.sender.hostnameHistory", null,true), // List of sub items
    TOOLS_NETWORK_SENDER_CHARSET("tools.network.sender.charset", Charset.class,false),
    TOOLS_NETWORK_SENDER_FRAME("tools.network.sender.frame", null,false),
    TOOLS_NETWORK_SENDER_HOSTNAME("tools.network.sender.hostname", String.class,false),
    TOOLS_NETWORK_SENDER_PORT("tools.network.sender.port", Integer.class,false),
    TOOLS_NETWORK_SENDER_KEEP_SOCKET("tools.network.sender.keepSocket", Boolean.class,false),
    TOOLS_NETWORK_SENDER_TLS_SERVER_CERTIFICATES("tools.network.sender.tls.server.certificates", String.class,false),
    TOOLS_NETWORK_SENDER_TLS_ENABLED("tools.network.sender.tls.enabled", Boolean.class,false),
    TOOLS_NETWORK_SENDER_TLS_CLIENT_AUTH_ENABLED("tools.network.sender.tls.clientAuthentication.enabled", Boolean.class,false),
    TOOLS_NETWORK_SENDER_TLS_CLIENT_AUTH_PRIVATE_KEY("tools.network.sender.tls.clientAuthentication.privateKey", null,false),
    TOOLS_NETWORK_SENDER_TLS_CLIENT_AUTH_PASSWORD("tools.network.sender.tls.clientAuthentication.password", null,false),

    TOOLS_NETWORK_RECEIVER_PORT("tools.network.receiver.port", Integer.class,false),
    TOOLS_NETWORK_RECEIVER_TLS_PRIVATE_KEY("tools.network.receiver.tls.server.privateKey", null,false),
    TOOLS_NETWORK_RECEIVER_TLS_PASSWORD("tools.network.receiver.tls.server.password", null,false),
    TOOLS_NETWORK_RECEIVER_TLS_ENABLED("tools.network.receiver.tls.enabled", Boolean.class,false),
    TOOLS_NETWORK_RECEIVER_TLS_CLIENT_AUTH_ENABLED("tools.network.receiver.tls.clientAuthentication.enabled", Boolean.class,false),
    TOOLS_NETWORK_RECEIVER_TLS_CLIENT_AUTH_CERTIFICATES("tools.network.receiver.tls.clientAuthentication.certificates", String.class,false),

    TOOLS_HTTP_SERVER_PORT("tools.http.server.port", Integer.class,false),

    TOOLS_VALUE_PRESENTER_PLAIN_AUTO_VIEW_MAX_SIZE("tools.valuePresenter.plain.autoViewMaxSize", Integer.class,false),
    TOOLS_VALUE_PRESENTER_XML_XSLT_FILE("tools.valuePresenter.xml.xslt.file", Path.class,false);

    private final String keyName;
    private final boolean set;
    private final Class<?> classType;

    ToolsConfigurationKey(String keyName, Class<?> classType, boolean set) {
        this.keyName = keyName;
        this.classType = classType;
        this.set = set;
    }

    @NotNull
    @Override
    public String getKeyName() {
        return keyName;
    }

    @Override
    public boolean isSet() {
        return set;
    }

    /**
     * Returns data class type.
     *
     * In case of a set then it will return the class-type of an item.
     *
     * @return Returns the class type
     */
    @NotNull
    @Override
    public Class<?> getClassType() {
        return classType;
    }

    @Nullable
    public static ConfigurationKey valueByName(@NotNull String keyName) {
        for (ConfigurationKey key : values()) {
            if (key.getKeyName().equals(keyName)) {
                return key;
            }
        }

        return null;
    }

}
