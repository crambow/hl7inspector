/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import com.alee.laf.panel.WebPanel;
import com.alee.laf.window.WebDialog;
import com.alee.managers.style.StyleId;
import com.alee.utils.SwingUtils;

import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.ui.themes.SkinExtensionStyles;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MyBaseDialog extends WebDialog<MyBaseDialog> {

    public enum Mode {
        /**
         * Used to set the mode of the dialog to OK/CANCEL. When in this mode, OK and
         * Cancel buttons are automatically added to the dialog.
         */
        OK_CANCEL,
        /**
         * Used to set the mode of the dialog to OK/CANCEL. When in this mode, a
         * Close button is automatically added to the dialog.
         */
        CLOSE
    }

    private boolean cancelClicked;

    private JPanel banner;
    private JLabel iconLabel;
    private JLabel titleLabel;

    private JPanel contentPane;
    private JPanel buttonPane;

    public MyBaseDialog() {
        super(StyleId.frameDecorated, getFocusedWindow());
        buildUI();
    }

    public MyBaseDialog(String title, boolean modal) {
        super(StyleId.frameDecorated, getFocusedWindow(), title, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        buildUI();
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            center(getFocusedWindow());
            toFront();
        }

        super.setVisible(b);
    }

    public final JPanel getBanner() {
        return banner;
    }

    @Override
    public final Container getContentPane() {
        return contentPane;
    }

    public final Container getButtonPane() {
        return buttonPane;
    }

    public boolean ask() {
        toFront();
        setVisible(true);
        dispose();
        return !cancelClicked;
    }

    public void ok() {
        cancelClicked = false;
        setVisible(false);
    }

    public void cancel() {
        cancelClicked = true;
        setVisible(false);
    }

    public void setDialogMode(Mode mode) {
        buttonPane.removeAll();

        switch (mode) {
            case OK_CANCEL:
                JButton okButton = createButton(e -> ok(), "ok");
                buttonPane.add(okButton);
                buttonPane.add(createButton(e -> cancel(), "cancel"));
                getRootPane().setDefaultButton(okButton);
                break;
            case CLOSE:
                buttonPane.add(createButton(e -> cancel(), "close"));
                break;
        }

        SwingUtils.equalizeComponentsWidth(buttonPane.getComponents());
    }

    private JButton createButton(@NotNull ActionListener listener, @NotNull @Nls String key) {
        JButton b  = new JButton(LocaleTool.get(key));
        b.addActionListener(listener);
        return b;
    }

    private void buildUI() {
        setIconImages(ApplicationProperties.getIcons());

        banner = createBanner();

        contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout(8, 8));
        contentPane.setBorder(new EmptyBorder(3, 3, 3, 3));

        buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonPane.setBorder(new EmptyBorder(3, 3, 3, 3));

        JPanel contentPaneAndButtons = new JPanel();
        contentPaneAndButtons.setLayout(new BorderLayout());
        contentPaneAndButtons.setBorder(new EmptyBorder(4, 10, 10, 10));
        contentPaneAndButtons.add(contentPane, BorderLayout.CENTER);
        contentPaneAndButtons.add(buttonPane, BorderLayout.SOUTH);

        Container container = super.getContentPane();
        container.setLayout(new BorderLayout(0, 0));
        container.add(BorderLayout.NORTH, banner);
        container.add(BorderLayout.CENTER, contentPaneAndButtons);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        ((JComponent) container).registerKeyboardAction(
                e -> cancel(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_IN_FOCUSED_WINDOW);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cancel();
            }
        });

        setDialogMode(Mode.OK_CANCEL);
    }

    public void setBannerTitle(@Nullable String text) {
        titleLabel.setText(text);
    }

    public void setBannerIcon(@Nullable Icon icon) {
        iconLabel.setIcon(icon);
    }

    private JPanel createBanner() {
        titleLabel = new JLabel();
        titleLabel.setOpaque(false);
        titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD));

        JPanel nestedPane = new JPanel(new BorderLayout());
        nestedPane.setOpaque(false);
        nestedPane.add(BorderLayout.NORTH, titleLabel);

        iconLabel = new JLabel();
        iconLabel.setPreferredSize(new Dimension(50, 50));

        WebPanel b = new WebPanel();
        b.setLayout(new BorderLayout());
        b.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        b.setStyleId(SkinExtensionStyles.dialogBanner);
        b.add(BorderLayout.CENTER, nestedPane);
        b.add(BorderLayout.EAST, iconLabel);

        return b;
    }

    public static Window getFocusedWindow() {
        return KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow();
    }

}