/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.PropertyKey;

import java.text.MessageFormat;
import java.util.*;

/**
 * I18N tooling class.
 */
public final class LocaleTool {

    public static final String DEFAULT_BUNDLE = "de.elomagic.hl7inspector.locales";

    private LocaleTool() {
    }

    /**
     * Returns a Locale object build by the given locale text.
     *
     * @param locale Something like "de", "en_GB", "en-US" and so on
     * @param defaultLocale Will be returned instead when given 'locale' invalid
     * @return Returns null when text is null or empty
     */
    @Nullable
    public static Locale parse(@Nullable String locale, @Nullable Locale defaultLocale) {
        if(StringUtils.isBlank(locale)) {
            return defaultLocale;
        }

        String text = locale.replace("-", "_");

        if(text.contains("_")) {
            String[] items = text.split("_");

            if(items.length == 2) {
                return new Locale(items[0], items[1]);
            }

            return new Locale(items[0], items[1], items[2]);
        }

        return new Locale(text);
    }

    /**
     * Converts a {@link Locale} to a display string.
     *
     * @param locale Locale to present. If null then something like "Not defined" will be returned
     * @return A human readable text string
     */
    @NotNull
    public static String toDisplay(final Locale locale) {
        return get("locale_format", locale.getDisplayName(Locale.getDefault()),  locale.getDisplayName(Locale.ENGLISH));
    }

    @NotNull
    public static Set<Locale> getSupportedLocales() {
        Set<Locale> set = new TreeSet<>(Comparator.comparing(Locale::toLanguageTag));
        set.addAll(Arrays.asList(
                Locale.ENGLISH,
                Locale.GERMAN
        ));

        return set;
    }

    private static String getResourcePath() {
        return DEFAULT_BUNDLE.replace("\\.", "//");
    }

    @NotNull
    public static String get(@Nls @NotNull @PropertyKey(resourceBundle = DEFAULT_BUNDLE) String key, Object...args) {
        String text = ResourceBundle.getBundle(getResourcePath()).getString(key);

        if (args.length != 0) {
            text = MessageFormat.format(text, args);
        }

        return text;
    }

}

