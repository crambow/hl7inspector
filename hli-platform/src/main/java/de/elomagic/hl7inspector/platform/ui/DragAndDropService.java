/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.dnd.DropTarget;
import java.nio.file.Path;
import java.util.List;

@Component
public class DragAndDropService {

    private static final Logger LOGGER = LogManager.getLogger(DragAndDropService.class);

    private final EventMessengerProcessor eventMessengerProcessor;

    public @Autowired DragAndDropService(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    public void addMessageDropSupport(java.awt.Component c) {
        try {
            DropTarget dropTarget = new DropTarget();
            dropTarget.setComponent(c);
            dropTarget.addDropTargetListener(new DragAndDropListener() {
                @Override
                public void handleStringDropped(java.awt.Component target, String text) {
                    eventMessengerProcessor.publishEvent(new TextDroppedEvent(this, text));
                }

                @Override
                public void handleFilesDropped(java.awt.Component target, List<Path> files) {
                    SwingUtilities.getWindowAncestor(target).toFront();
                    eventMessengerProcessor.publishEvent(new FilesDroppedEvent(this, files));
                }
            });
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

}
