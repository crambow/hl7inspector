/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils.security;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcaPKCS8Generator;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8EncryptorBuilder;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.OutputEncryptor;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.util.io.pem.PemGenerationException;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemObjectGenerator;
import org.bouncycastle.util.io.pem.PemWriter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.StringReader;
import java.io.Writer;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class PemTool {

    private static final String PRIVATE_KEY = "PRIVATE KEY";

    private PemTool() {
    }

    @NotNull
    public static X509Certificate[] readCertificates(@Nullable String pem) throws CertificateException, IOException {
        List<X509Certificate> results = new ArrayList<>();

        if (StringUtils.isNotBlank(pem)) {
            JcaX509CertificateConverter converter = new JcaX509CertificateConverter();
            converter.setProvider(new BouncyCastleProvider());

            PEMParser parser = new PEMParser(new StringReader(pem));
            Object o = parser.readObject();
            while (o != null) {
                if (o instanceof X509CertificateHolder) {
                    X509CertificateHolder xch = (X509CertificateHolder)o;
                    results.add(converter.getCertificate(xch));
                }

                o = parser.readObject();
            }
        }

        return results.toArray(new X509Certificate[0]);
    }

    /**
     * Returns private key
     * @param pem PEM encoded key
     * @param password if required the password of the key
     * @return Thr private key or null when key not found in PEM
     * @throws IOException
     * @throws GeneralSecurityException
     * @throws OperatorCreationException
     * @throws PKCSException Thrown when unable to parse PEM string
     */
    @Nullable
    public static PrivateKey readPrivateKey(@NotNull String pem, @Nullable char[] password) throws IOException, GeneralSecurityException, OperatorCreationException, PKCSException {
        PEMParser parser = new PEMParser(new StringReader(pem));
        Object o = parser.readObject();

        JcaPEMKeyConverter converter = new JcaPEMKeyConverter();

        if (o == null) {
            return null;
        }
        if (o instanceof PKCS8EncryptedPrivateKeyInfo) {
            PKCS8EncryptedPrivateKeyInfo pki = (PKCS8EncryptedPrivateKeyInfo) o;

            JceOpenSSLPKCS8DecryptorProviderBuilder builder = new JceOpenSSLPKCS8DecryptorProviderBuilder();
            builder.setProvider(new BouncyCastleProvider());

            InputDecryptorProvider decryptorProvider = builder.build(password);
            PrivateKeyInfo privateKeyInfo = pki.decryptPrivateKeyInfo(decryptorProvider);
            return converter.getPrivateKey(privateKeyInfo);
        } else if (o instanceof PrivateKeyInfo) {
            PrivateKeyInfo pki = (PrivateKeyInfo) o;
            return converter.getPrivateKey(pki);
        } else {
            throw new GeneralSecurityException("Unsupported key info class " + o.getClass().getName());
        }
    }

    public static void writePrivateKey(@NotNull PrivateKey privateKey, @Nullable char[] password, @NotNull Writer out) throws PemGenerationException, OperatorCreationException {
        try (PemWriter writer = new PemWriter(out)) {
            PemObjectGenerator gen;
            if (password == null || password.length == 0) {
                gen = new PemObject(PRIVATE_KEY, privateKey.getEncoded());
            } else {
                OutputEncryptor encryptor = new JceOpenSSLPKCS8EncryptorBuilder(PKCSObjectIdentifiers.pbeWithSHAAnd3_KeyTripleDES_CBC)
                        //.setProvider(BouncyCastleProvider.PROVIDER_NAME)
                        .setRandom(new SecureRandom())
                        .setPassword(password)
                        .build();

                gen = new JcaPKCS8Generator(privateKey, encryptor);
            }

            writer.writeObject(gen);
            writer.flush();
        } catch (IOException e) {
            throw new SecurityRuntimeException(e.getMessage(), e);
        }
    }

    public static void writeCertificate(@NotNull X509Certificate certificate, @NotNull Writer out) {
        try (PemWriter writer = new PemWriter(out)) {
            writer.writeObject( new PemObject("CERTIFICATE", certificate.getEncoded()));
            writer.flush();
        } catch (IOException | CertificateEncodingException e) {
            throw new SecurityRuntimeException(e.getMessage(), e);
        }
    }

    public static void writePrivateKeyAndCertificate(@NotNull PrivateKey privateKey, @Nullable char[] password, @NotNull X509Certificate certificate, @NotNull Writer out) throws PemGenerationException, OperatorCreationException {
        writePrivateKey(privateKey, password, out);
        writeCertificate(certificate, out);
    }

    /**
     * Returns a human readable certificates summary.
     *
     * @param pem Base64 PEM encoded string
     * @return Returns a string
     * @throws CertificateException
     * @throws IOException
     */
    public static String getCertificateSummary(String pem) throws CertificateException, IOException {
        X509Certificate[] certificates = PemTool.readCertificates(pem);

        StringBuilder sb = new StringBuilder();

        for (X509Certificate c : certificates) {
            sb.append("Subject: ").append(c.getSubjectX500Principal().getName()).append("\n")
                    .append("Issuer: ").append(c.getIssuerX500Principal().getName()).append("\n")
                    .append("Not After: ").append(c.getNotAfter()).append("\n")
                    .append("Serial: ").append(c.getSerialNumber()).append("\n\n");
        }

        return sb.toString();
    }

}
