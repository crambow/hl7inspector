/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.find.ui;

import com.alee.extended.image.WebImage;
import com.alee.laf.button.WebButton;
import com.alee.laf.button.WebToggleButton;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.text.WebTextField;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.ui.find.DocumentFindMethods;
import de.elomagic.hl7inspector.platform.ui.find.ui.actions.CloseFindAction;
import de.elomagic.hl7inspector.platform.ui.find.ui.actions.FindCaseSensitiveAction;
import de.elomagic.hl7inspector.platform.ui.find.ui.actions.FindHighlightAction;
import de.elomagic.hl7inspector.platform.ui.find.ui.actions.FindNextAction;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyDocumentListener;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyKeyPressListener;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;

import org.jetbrains.annotations.NotNull;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import java.awt.Dimension;
import java.awt.event.KeyEvent;

/**
 * UI component at the bottom of the main frame.
 */
public class FindBar extends WebPanel {

    private final WebTextField editPhrase = new WebTextField(30);
    private final JButton btNext = new WebButton(StyleId.buttonIconHover, new FindNextAction(e -> findNext()));
    private final JToggleButton btHighlight = new WebToggleButton(StyleId.buttonIconHover, new FindHighlightAction(e -> refreshHighlightPhrases()));
    private JToggleButton cbCaseSensitive;

    private transient DocumentFindMethods documentFindMethods;

    public FindBar() {
        initUI();
    }

    private void initUI() {
        setBorder(BorderFactory.createEmptyBorder());

        JButton btClose = UIFactory.createFlatIconButton(JButton.class, new CloseFindAction(e -> setVisible(false)));

        editPhrase.setLeadingComponent(new WebImage(IconThemeManager.getImage("find.png")));
        editPhrase.getDocument().addDocumentListener((SimplifyDocumentListener) e -> refreshHighlightPhrases());
        editPhrase.addKeyListener((SimplifyKeyPressListener) e -> {
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                setVisible(false);
            } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                findNext();
            }
        });

        cbCaseSensitive = new WebToggleButton(StyleId.buttonIconHover, new FindCaseSensitiveAction(e -> {
            refreshHighlightPhrases();
            editPhrase.requestFocus();
        }));

        FormBuilder.createBuilder(this)
                .noBorder()
                .noInsets()
                .add(editPhrase, 1, 0)
                .add(btNext, 1, 0)
                .add(btHighlight, 1, 0)
                .add(cbCaseSensitive, 1, 0)
                .addColumnSpacer()
                .add(btClose, 1, 0);

        btClose.setPreferredSize(new Dimension(getPreferredSize().height, getPreferredSize().height));
    }

    public void setPresenter(@NotNull DocumentFindMethods methods) {
        this.documentFindMethods = methods;

        btHighlight.setVisible(documentFindMethods.supportHighlightPhrase());
    }

    private void findNext() {
        String phrase = editPhrase.getText();

        if (phrase == null) {
            return;
        }

        documentFindMethods.findNextPhrase(phrase, cbCaseSensitive.isSelected());
    }

    private void refreshHighlightPhrases() {
        documentFindMethods.highlightPhrase(editPhrase.getText(), cbCaseSensitive.isSelected());
    }

    @Override
    public void requestFocus() {
        editPhrase.requestFocus();
    }

    @Override
    public void setVisible(boolean value) {
        boolean visible = isVisible();

        super.setVisible(value);

        firePropertyChange("visible", visible, value);

        if(value) {
            editPhrase.selectAll();
            editPhrase.requestFocus();
        } else if (documentFindMethods != null) {
            documentFindMethods.highlightPhrase(null, false);
        }
    }

}
