/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.actions.ClearAllAction;
import de.elomagic.hl7inspector.platform.ui.actions.SaveFileAsAction;
import de.elomagic.hl7inspector.platform.utils.Html;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.StringEscapeUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.text.html.HTMLDocument;
import java.awt.Color;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.text.MessageFormat;

@Component
@Scope("prototype")
public class HighlighterPanel extends JEditorPane {

    private static final Logger LOGGER = LogManager.getLogger(HighlighterPanel.class);
    private static final String PATTERN = "<HTML><BODY><font face=\"Courier New, Tahoma, Arial\" size=\"3\">{0}</font></BODY></HTML>";
    private static final String SPECIAL_PATTERN = "<font color=\"fuchsia\">&lt;0x{0}&gt;</font>";

    private final transient Configuration configuration = Configuration.getInstance();

    private StringBuilder buffer = new StringBuilder();
    private boolean changed = false;

    @PostConstruct
    private void init() {
        setContentType("text/html");
        setEditable(false);
        setDoubleBuffered(true);
        putClientProperty(StyleId.STYLE_PROPERTY, StyleId.editorpane);

        Color c = getForeground();
        String rgb = String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
        ((HTMLDocument)getDocument()).getStyleSheet().addRule("body { color: " + rgb + "; }");
        ((HTMLDocument)getDocument()).getStyleSheet().addRule(".hl1 { color: black; background-color: #f0ad4e; }");
        ((HTMLDocument)getDocument()).getStyleSheet().addRule(".hl2 { color: white; background-color: #ff00ff; }");
        ((HTMLDocument)getDocument()).getStyleSheet().addRule(".err { color: red; }");

        JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.add(new SaveFileAsAction(e -> saveLog()));
        popupMenu.add(new ClearAllAction(e -> clear()));

        setComponentPopupMenu(popupMenu);
    }

    public void addBytes(@NotNull byte[] data, @Nullable Charset encoding) {
        if (encoding == null) {
            for (byte b : data) {
                buffer.append(MessageFormat.format(SPECIAL_PATTERN, String.format("%02x", b)));
            }
        } else {
            String text = new String(data, encoding);
            buffer.append(StringEscapeUtils.escapeHtml(text, SPECIAL_PATTERN));
        }

        changed = true;

        refreshScreen();
    }

    public void addLine(String line, boolean escape) {
        if(buffer.lastIndexOf(Html.BR) != (buffer.length() - 4)) {
            buffer.append(Html.BR);
        }

        String text = escape ? StringEscapeUtils.escapeHtml(line) : line;

        buffer.append(text).append(Html.BR);
        changed = true;

        refreshScreen();
    }

    public void addLine(String value) {
        addLine(value, true);
    }

    public void clear() {
        buffer = new StringBuilder();
        changed = true;

        refreshScreen();
    }

    public void saveLog() {
        /*
         * TODO Dialog must be top most. Actual it will be topped by receive/send window.
         */
        JFileChooser fc = new JFileChooser(configuration.getAppFilesLastUsedFolder().toFile());
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setDialogTitle(LocaleTool.get("save_log"));
        fc.setSelectedFile(configuration.getAppFilesLastUsedFolder().resolve("hl7_receive_log.txt").toFile());
        fc.addChoosableFileFilter(GenericFileFilter.TEXT_FILTER);

        if(fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();

            if(file.exists() && SimpleDialog.confirmYesNo(LocaleTool.get("file_already_exists_overwrite")) == JOptionPane.YES_OPTION) {
                try {
                    try (FileOutputStream fout = new FileOutputStream(file, false); BufferedOutputStream bout = new BufferedOutputStream(fout)) {
                        bout.write(getText().getBytes());
                        bout.flush();
                    }
                } catch(Exception ex) {
                    LOGGER.error("Writing log file failed.", ex);
                }
            }
        }
    }

    private void refreshScreen() {
        SwingUtilities.invokeLater(() -> {
            if(changed) {
                changed = false;
                String s = getText();

                String newValue = MessageFormat.format(PATTERN, buffer.toString());

                if(!s.equals(newValue)) {
                    setText(newValue);
                }
            }
        });
    }

}
