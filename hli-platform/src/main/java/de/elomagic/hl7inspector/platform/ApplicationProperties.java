/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform;

import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.io.ClassPathResource;

import java.awt.Image;
import java.io.InputStream;
import java.net.URL;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Support as component bean not required.
 */
public class ApplicationProperties {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationProperties.class);

    private static final ApplicationProperties INSTANCE = new ApplicationProperties();
    private final Properties properties = new Properties();

    private ApplicationProperties() {
        read();
    }

    @NotNull
    private Properties read() {
        String resource = "meta.properties";
        try {
            try (InputStream in = new ClassPathResource(resource).getInputStream()) {
                properties.load(in);
                return properties;
            }
        } catch(Exception e) {
            LOGGER.error("Unable to load resource \"{}\".", resource, e);
            return properties;
        }
    }

    @NotNull
    public static String getVersion() {
        return INSTANCE.properties.getProperty("application.version", "unknown");
    }

    public static boolean isMasterRelease() {
        return !INSTANCE.properties.getProperty("application.version", "unknown").contains("-");
    }

    @NotNull
    public static String getBranchName() {
        return INSTANCE.properties.getProperty("application.branch.name", "");
    }

    @Nullable
    public static ZonedDateTime getBuildDate() {
        String value = INSTANCE.properties.getProperty("application.build.date",null);
        return value == null ? null : ZonedDateTime.parse(value);
    }

    public static String getReleaseNotesUrl() { return INSTANCE.properties.getProperty("application.releaseNotes.url", ""); }

    public static String getHomepageManifestUrl() {
        return INSTANCE.properties.getProperty("homepage.manifest.url");
    }

    public static String getHomepageManifestSnapshotUrl() {
        return INSTANCE.properties.getProperty("homepage.manifest.snapshot.url");
    }

    public static String getHomepageUrl() {
        return INSTANCE.properties.getProperty("homepage.url");
    }

    public static String getHomepageBugTrackerUrl() { return INSTANCE.properties.getProperty("homepage.bug.tracker.url"); }

    public static String getHL7InspectorNeoUrl() {
        return INSTANCE.read().getProperty("hl7inspector.url");
    }

    /**
     * Helper method.
     *
     * Identify channel by using snapshot mode.
     *
     * @return Returns RELEASE or CHANNEL
     */
    public static String getChannel() {
        return isMasterRelease() ? "RELEASE" : "SNAPSHOT";
    }

    public static URL getStartupImageResourceURL() {
        return ApplicationProperties.class.getResource(INSTANCE.read().getProperty("application.startup.image.resourcePath"));
    }

    public static List<Image> getIcons() {
        return Arrays.asList(
                IconThemeManager.getImage(INSTANCE.read().getProperty("application.icons.size16")),
                IconThemeManager.getImage(INSTANCE.read().getProperty("application.icons.size32")),
                IconThemeManager.getImage(INSTANCE.read().getProperty("application.icons.size64")));
    }
}
