/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.events;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Event publisher.
 *
 * The benefit comparing to the {@link ApplicationEventPublisher} is that on publishing to non singleton beans,
 * only existing beans will be notified and no new beans will created.
 * <p>
 * See also https://reflectoring.io/spring-bean-lifecycle or <br/>
 * https://stackoverflow.com/questions/29743320/how-exactly-does-the-spring-beanpostprocessor-work
 */
public class EventMessengerProcessor implements DestructionAwareBeanPostProcessor {

    private static final Logger LOGGER = LogManager.getLogger(EventMessengerProcessor.class);

    private final Map<Class<?>, Map<Object, Set<Method>>> eventMap = new HashMap<>();

    private final ApplicationEventPublisher applicationEventPublisher;

    public @Autowired EventMessengerProcessor(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, @NotNull String beanName) throws BeansException {
        for (Method method : bean.getClass().getMethods()) {
            for (EventListener annotation : method.getDeclaredAnnotationsByType(EventListener.class)) {
                Class<?>[] eventClasses = annotation.value();
                if (eventClasses.length == 0 && method.getParameterCount() == 1) {
                    Class<?> eventClass = method.getParameterTypes()[0];
                    register(eventClass, bean, method);
                } else if (eventClasses.length > 0 && method.getParameterCount() == 0) {
                    Arrays.stream(eventClasses).forEach(eventClass -> register(eventClass, bean, method));
                } else {
                    throw new IllegalArgumentException("Unsupported event listener declaration for method " + method);
                }
            }
        }

        return bean;
    }

    @Override
    public void postProcessBeforeDestruction(@NotNull Object bean, @NotNull String beanName) throws BeansException {
        for (Map.Entry<Class<?>, Map<Object, Set<Method>>> eventEntry : eventMap.entrySet()) {
            if (eventEntry.getValue().remove(bean) != null) {
                LOGGER.trace("Event handlers successful removed from bean '{}'", bean);
            }
        }
    }

    private void register(@NotNull Class<?> eventClass, @NotNull Object bean, @NotNull Method method) {
        LOGGER.trace("Registering event handler '{}'", method);
        Map<Object, Set<Method>> beanMethodsMap = eventMap.getOrDefault(eventClass, new HashMap<>());

        Set<Method> methods = beanMethodsMap.getOrDefault(bean, new HashSet<>());
        methods.add(method);

        beanMethodsMap.put(bean, methods);
        eventMap.put(eventClass, beanMethodsMap);
    }

    private String getEventingMode() {
        return System.getProperty("application.system.eventingMode", "modern");
    }

    public void publishEvent(@NotNull Object event) {
        if (!"modern".equals(getEventingMode())) {
            applicationEventPublisher.publishEvent(event);
        } else {
            LOGGER.trace("Publishing event '{}' in modern way.", event);
            Class<?> eventClass = event.getClass();
            do {
                if (eventMap.containsKey(eventClass)) {
                    Map<Object, Set<Method>> beansMap = eventMap.get(eventClass);

                    for (Map.Entry<Object, Set<Method>> entry : beansMap.entrySet()) {
                        Object bean = entry.getKey();
                        entry.getValue().forEach(method -> {
                            try {
                                if (method.getParameterCount() == 0) {
                                    method.invoke(bean);
                                } else if (method.getParameterCount() == 1) {
                                    method.invoke(bean, event);
                                } else {
                                    throw new IllegalAccessException("Unsupported method call.");
                                }
                            } catch (Exception ex) {
                                throw new IllegalArgumentException(ex.getMessage(), ex);
                            }
                        });
                    }
                }
                eventClass = eventClass.getSuperclass();
            } while (eventClass != Object.class);
        }
    }

}
