/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.configuration;

import de.elomagic.hl7inspector.platform.io.gson.GsonTool;
import de.elomagic.hl7inspector.platform.ui.ListModel;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;
import de.elomagic.hl7inspector.platform.utils.SortedProperties;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public final class Configuration implements Serializable {

    private static final String CONFIG_FILE = "hl7inspector.properties";
    private static final String KEY_SET_FORMAT = "%s.%03d";

    private static final Logger LOGGER = LogManager.getLogger(Configuration.class);
    private static final Configuration INSTANCE = new Configuration();

    private final ListModel<String> destinationModel = new ListModel<>(10, true);
    private final transient Set<Path> recentOpenedFileModel = new LinkedHashSet<>();
    private final Properties properties = new SortedProperties();
    private boolean restartRequired = false;

    public enum ToolbarPosition {

        TOP(SwingConstants.HORIZONTAL, BorderLayout.NORTH),
        LEFT(SwingConstants.VERTICAL, BorderLayout.WEST),
        RIGHT(SwingConstants.VERTICAL, BorderLayout.EAST);

        private final int orientation;
        private final String position;

        ToolbarPosition(int orientation, String position) {
            this.orientation = orientation;
            this.position = position;
        }

        public int getOrientation() {
            return orientation;
        }

        public String getPosition() {
            return position;
        }

        public static ToolbarPosition findByPosition(String position) {
            return Arrays.stream(ToolbarPosition.values())
                    .filter(p -> p.getPosition().equals(position))
                    .findFirst()
                    .orElse(ToolbarPosition.TOP);
        }
    }

    public enum ViewMode {
        TREE, EDITOR
    }

    private Configuration() {
        super();

        LOGGER.debug("Instancing StartupProperties class");

        try {
            load();

            createRecentDestinationModel();
            createRecentOpenedFileModel();
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
            SimpleDialog.error(e);
        }
    }

    @NotNull
    public static Configuration getInstance() {
        return INSTANCE;
    }

    private void load() throws IOException {
        Path configurationFile = getConfigurationFile(false);
        if(Files.exists(configurationFile)) {
            LOGGER.info("Reading configuration from file '{}'", configurationFile);
            try (InputStream in = Files.newInputStream(configurationFile, StandardOpenOption.READ)) {
                properties.load(in);
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage(), ex);
                throw ex;
            }
        } else if (ConfigurationMigrator.exists()) {
            // Remove this after couple of years? may be 2022/2023?
            properties.putAll(ConfigurationMigrator.migrate());
        } else {
            LOGGER.info("Configuration file '{}' not found. Is this your first start on this computer?", configurationFile);
        }
    }

    public void save() {
        try {
            commitRecentDestination();
            commitRecentOpenedFileModel();

            Path configurationFile = getConfigurationFile(true);
            LOGGER.info("Saving configuration in file '{}'.", configurationFile);
            try (OutputStream out = Files.newOutputStream(configurationFile)) {
                properties.store(out, "HL7 Inspector properties");
                out.flush();
            }
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
            SimpleDialog.error(e.getMessage(), e);
        }
    }

    private void validateSetStatus(@NotNull ConfigurationKey key) {
        if (key.isSet()) {
            throw new IllegalArgumentException("Key " + key + " does support only a set of values.");
        }
    }

    public <T> T getProperty(@NotNull ConfigurationKey key, @NotNull Class<? extends T> clazz, @Nullable T defaultValue) {
        validateSetStatus(key);

        String value = properties.getProperty(key.getKeyName());
        try {

            if (value == null) {
                return defaultValue;
            }

            if (key.getClassType() != clazz && key.getClassType() != null) {
                LOGGER.warn("Getting property with different type classes. Method parameter '{}' <> key '{}'", clazz, key.getClassType());
            }

            // TODO Add enum support

            if (clazz == String.class) {
                return (T) value;
            } else if (clazz == Boolean.class) {
                return (T) (Boolean) (value.toLowerCase().startsWith("t"));
            } else if (clazz == Integer.class) {
                return (T) (Integer) Integer.parseInt(value);
            } else if (clazz == Locale.class) {
                return (T) LocaleTool.parse(value, (Locale) defaultValue);
            } else if (clazz == Path.class) {
                return (T) Paths.get(value);
            } else if (clazz == Charset.class) {
                return (T) Charset.forName(value);
            } else if (clazz == URL.class) {
                return (T) new URL(value);
            } else if (clazz == URI.class) {
                return (T) new URI(value);
            } else if (key.getClassType() == null) {
                return GsonTool.read(new StringReader(value), clazz);
            } else {
                throw new IllegalArgumentException("Class type '" + clazz + "' not supported yed .");
            }
        } catch (Exception ex) {
            LOGGER.error("Error occur during getting property {}. Value was '{}'. Return given default value {}", key, value, defaultValue);
            return defaultValue;
        }
    }

    public void setProperty(@NotNull ConfigurationKey key, @Nullable Object value) {
        validateSetStatus(key);

        if (value == null) {
            properties.remove(key.getKeyName());
        } else {
            properties.setProperty(key.getKeyName(), value.toString());
        }
    }

    /**
     * Returns the value of a property set with starting keyPrefix + "." + [keyPrefix] (3 digits!).
     * @param keyPrefix String of key prefix
     * @param index Index in the list
     */
    public void setProperty(@NotNull ConfigurationKey keyPrefix, int index, @Nullable String value) {
        if (!keyPrefix.isSet()) {
            throw new IllegalArgumentException("Key " + keyPrefix + " doesn't support a set of values.");
        }

        if (value == null) {
            properties.remove(keyPrefix.getKeyName());
        } else {
            properties.setProperty(String.format(KEY_SET_FORMAT, keyPrefix.getKeyName(), index), value);
        }
    }

    /**
     * Returns the value of a property set with starting keyPrefix + "." + [keyPrefix] (3 digits!).
     *
     * @param keyPrefix String of key prefix
     * @param keyIndex Index in the list
     * @return Returns the value or null if not exists
     */
    @Nullable
    public String getValueOfKeyWithPrefix(@NotNull ConfigurationKey keyPrefix, int keyIndex) {
        if (!keyPrefix.isSet()) {
            throw new IllegalArgumentException("Key " + keyPrefix + " doesn't support a set of values.");
        }

        return properties.getProperty(String.format(KEY_SET_FORMAT, keyPrefix.getKeyName(), keyIndex));
    }

    /**
     * Returns a list properties with starting key of [keyPrefix] + "." .
     *
     * @param keyPrefix String of key prefix
     */
    @NotNull
    private List<String> getValuesOfKeysWithPrefix(@NotNull ConfigurationKey keyPrefix) {
        return getValuesOfKeysWithPrefix(keyPrefix, Integer.MAX_VALUE);
    }

    /**
     * Returns a list properties with starting key of [keyPrefix] + "." .
     *
     * @param keyPrefix String of key prefix
     * @param limit Max values to set.
     */
    @NotNull
    public List<String> getValuesOfKeysWithPrefix(@NotNull ConfigurationKey keyPrefix, int limit) {
        // Result must be ordered !!!
        return properties.stringPropertyNames()
                .stream()
                .sorted()
                .filter(key -> key.startsWith(keyPrefix.getKeyName() + "."))
                .limit(limit)
                .map(properties::getProperty)
                .collect(Collectors.toList());
    }


    /**
     * Add keys with values to the properties.
     *
     * @param keyPrefix Key prefix w/o a dot.
     * @param values NULL values in the list will be ignored
     */
    public void setValuesByKeyPrefix(@NotNull ConfigurationKey keyPrefix, @Nullable List<?> values) {
        removeKeysWithPrefix(keyPrefix);

        if (values == null) {
            return;
        }

        AtomicInteger atomicInteger = new AtomicInteger(0);

        values.stream()
                .filter(Objects::nonNull)
                .sorted()
                .forEach(item -> setProperty(keyPrefix, atomicInteger.incrementAndGet()-1, item.toString()));
    }

    /**
     * Removes properties with starting key of [keyPrefix] + "." .
     *
     * @param keyPrefix String of key prefix
     * @return Return count of removed keys
     */
    public long removeKeysWithPrefix(@NotNull ConfigurationKey keyPrefix) {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        properties.stringPropertyNames()
                .stream()
                .filter(key -> key.startsWith(keyPrefix.getKeyName() + "."))
                .forEach(key -> {
                    properties.remove(key);
                    atomicInteger.incrementAndGet();
                });

        LOGGER.debug("{} keys with prefix '{}' removed.", atomicInteger.get(), keyPrefix.getKeyName());
        return atomicInteger.get();
    }

    @NotNull
    private Path getConfigurationFile(boolean createPath) throws IOException {
        Path path = PlatformTool.getUsersHL7InspectorHome().resolve(CONFIG_FILE);

        if (Files.notExists(path.getParent()) && createPath) {
            LOGGER.debug("Creating folder '{}'.", path.getParent());
            Files.createDirectories(path.getParent());
        }

        return path;
    }

    private void createRecentOpenedFileModel() {
        recentOpenedFileModel.clear();

        try {
            getValuesOfKeysWithPrefix(AppConfigurationKey.APP_FILES_RECENTLY_OPENED)
                    .stream()
                    .filter(StringUtils::isNotBlank)
                    .map(Paths::get)
                    .filter(Files::exists)
                    .forEachOrdered(recentOpenedFileModel::add);
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private void commitRecentOpenedFileModel() {
        setValuesByKeyPrefix(AppConfigurationKey.APP_FILES_RECENTLY_OPENED, Arrays.asList(recentOpenedFileModel.toArray()));
    }

    public boolean isRestartRequired() {
        return restartRequired;
    }

    public void setRestartRequired(boolean restartRequired) {
        this.restartRequired = this.restartRequired || restartRequired;
    }

    @NotNull
    public Set<Path> getRecentOpenedFileModel() {
        return recentOpenedFileModel;
    }

    public void setPhrases(@NotNull List<String> phrases) {
        setValuesByKeyPrefix(AppConfigurationKey.APP_SEARCH_PHRASE_HISTORY, phrases);
    }

    @NotNull
    public List<String> getPhrases() {
        return getValuesOfKeysWithPrefix(AppConfigurationKey.APP_SEARCH_PHRASE_HISTORY, 10);
    }

    public void putPhrase(String phrase) {
        List<String> v = getPhrases();

        v.remove(phrase);

        while(v.size() > 9) {
            v.remove(9);
        }

        v.add(0, phrase);

        setPhrases(v);
    }

    /**
     * Don't call this method directly.
     * <br>
     * Use <code>de.elomagic.hl7inspector.shared.services.ProfileService</code>> instead

     * @return Returns the default profile file or null
     */
    @Nullable
    public Path getDefaultProfile() {
        return getProperty(AppConfigurationKey.APP_PROFILES_SELECTED_FILE, Path.class, null);
    }

    /**
     * Don't call this method directly.
     * <br>
     * Use <code>de.elomagic.hl7inspector.shared.services.ProfileService</code>> instead

     * @param file The default profile file
     */
    public void setDefaultProfile(@Nullable Path file) {
        setProperty(AppConfigurationKey.APP_PROFILES_SELECTED_FILE, file);
    }

    private void createRecentDestinationModel() {
        try {
            destinationModel.clear();
            destinationModel.addAll(getValuesOfKeysWithPrefix(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_HOSTNAME_HISTORY));
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @NotNull
    public ListModel<String> getRecentDestinationModel() {
        return destinationModel;
    }

    private void commitRecentDestination() {
        setValuesByKeyPrefix(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_HOSTNAME_HISTORY, destinationModel);
    }

    @NotNull
    public Path getAppFilesLastUsedFolder() {
        return getProperty(AppConfigurationKey.APP_FILES_LAST_USED_FOLDER, Path.class, PlatformTool.getHL7DocumentFolder());
    }

    public void setAppFilesLastUsedFolder(@Nullable Path path) {
        setProperty(AppConfigurationKey.APP_FILES_LAST_USED_FOLDER, path == null ? null : path.toAbsolutePath().toString());
    }

}
