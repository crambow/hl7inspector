/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils.formatter;

import com.alee.api.data.BoxOrientation;
import com.alee.extended.overlay.AlignedOverlay;
import com.alee.extended.overlay.WebOverlay;
import com.alee.laf.label.WebLabel;
import com.alee.managers.tooltip.TooltipWay;

import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;

import javax.swing.JFormattedTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultFormatter;
import java.awt.Insets;

public class AbstractOverlayFormatter extends DefaultFormatter {

    public static final String ID_FORMATTER = "^([a-z])([a-z]|[0-9])*$";

    private final WebOverlay overlay;
    private final WebLabel overlayLabel = new WebLabel(IconThemeManager.getImageIcon("error-12.png"));

    public AbstractOverlayFormatter(@NotNull WebOverlay overlay) {
        super();

        setOverwriteMode(false);

        this.overlay = overlay;

        overlayLabel.setToolTip(LocaleTool.get("invalid_input_please_correct_them"), TooltipWay.down);
    }

    @Override
    public void install(JFormattedTextField ftf) {
        super.install(ftf);

        if (overlay.getContent() == null) {
            overlay.setContent(ftf);
        }
    }

    protected void showOverlay() {
        SwingUtilities.invokeLater (() -> {
            if (overlay.getOverlayCount () == 0) {
                overlay.addOverlay (
                        new AlignedOverlay(
                                overlayLabel,
                                BoxOrientation.right,
                                BoxOrientation.top,
                                new Insets(0, 0, 0, 3)
                        )
                );
            }
        });
    }

    protected void removeOverlay() {
        if (overlay.getOverlayCount () > 0) {
            overlay.removeOverlay ( overlayLabel );
        }
    }

}
