/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.logging.log4j.LogManager;

import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public interface DragAndDropListener extends DropTargetListener {

    void handleStringDropped(Component target, String text);

    void handleFilesDropped(Component target, List<Path> files);

    @Override
    default void dragEnter(DropTargetDragEvent dtde) {
        Transferable tr = dtde.getTransferable();
        if (!tr.isDataFlavorSupported(DataFlavor.javaFileListFlavor)
                && !tr.isDataFlavorSupported(DataFlavor.stringFlavor)) {
            dtde.rejectDrag();
        }
    }

    @Override
    default void dragOver(DropTargetDragEvent dtde) {
        // noop
    }

    @Override
    default void dropActionChanged(DropTargetDragEvent dtde) {
        // noop
    }

    @Override
    default void dragExit(DropTargetEvent dte) {
        // noop
    }

    @Override
    default void drop(DropTargetDropEvent dtde) {
        try {
            Transferable tr = dtde.getTransferable();
            if (tr.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);

                @SuppressWarnings("unchecked")
                List<Path> files = ((List<File>)tr.getTransferData(DataFlavor.javaFileListFlavor))
                        .stream()
                        .map(File::toPath)
                        .collect(Collectors.toList());

                handleFilesDropped(dtde.getDropTargetContext().getComponent(), files);
            } else if (tr.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);

                String text = tr.getTransferData(DataFlavor.stringFlavor).toString();

                handleStringDropped(dtde.getDropTargetContext().getComponent(), text);
            }
        } catch (Exception ex) {
            LogManager.getLogger(DragAndDropListener.class).error(ex.getMessage(), ex);
            dtde.rejectDrop();
            SimpleDialog.error(LocaleTool.get("error_during_parsing_text"), ex);
        }
    }


}
