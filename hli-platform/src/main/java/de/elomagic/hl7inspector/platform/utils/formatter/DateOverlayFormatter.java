/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils.formatter;

import com.alee.extended.overlay.WebOverlay;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;

public class DateOverlayFormatter extends AbstractOverlayFormatter {

    private final DateFormat dateFormat;

    public DateOverlayFormatter(@NotNull WebOverlay overlay, @NotNull DateFormat dateFormat) {
        super(overlay);

        this.dateFormat = dateFormat;
    }

    @Override
    public Object stringToValue(String text) throws ParseException {
        if (dateFormat != null) {
            try {
                dateFormat.parse(text);
                removeOverlay();
                return super.stringToValue(text);
            } catch (Exception ex) {
                showOverlay();
                throw new ParseException("Date pattern '" + dateFormat + "' didn't match", 0);
            }
        }

        return text;
    }

}
