/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import com.alee.laf.window.WindowMethodsImpl;

import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import java.awt.BorderLayout;
import java.awt.Color;

public class SplashscreenFrame extends JFrame {

    private final JLabel statusLabel = new JLabel(
            LocaleTool.get("for_educational_purposes_only") +
                    " - " +
                    LocaleTool.get("starting_app"));
    private transient Timer timer;

    public SplashscreenFrame() {
        setSize(640, 400);
        setUndecorated(true);
        setAlwaysOnTop(true);
        setIconImages(ApplicationProperties.getIcons());
        setTitle(LocaleTool.get("starting_app"));
        WindowMethodsImpl.center(this);

        statusLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        statusLabel.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
        statusLabel.setFont(statusLabel.getFont().deriveFont(10f));
        statusLabel.setForeground(Color.lightGray);

        JPanel p = new SplashscreenPanel();
        p.add(statusLabel, BorderLayout.SOUTH);

        setContentPane(p);
    }

    public void setStatusText(String text) {
        statusLabel.setText(text);
    }

    @Override
    public void setVisible(boolean b) {
        if (timer != null && timer.isRunning()) {
            timer.stop();
        }

        if (b) {
            timer = new Timer(2000, e -> statusLabel.setText(statusLabel.getText() + "."));
            timer.setRepeats(true);
            timer.start();
        }

        super.setVisible(b);
    }
}
