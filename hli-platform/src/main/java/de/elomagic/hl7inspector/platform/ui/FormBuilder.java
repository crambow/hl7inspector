/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.ui.themes.SkinExtensionStyles;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Arrays;

/**
 * By default, a form layout has three columns.
 * <ul></ul>
 * <li>First column for the label</li>
 * <li>Second for the component</li>
 * <li>Third as reserve, if the component has to extend over the whole row</li>
 * </ul>
 */
public class FormBuilder {

    private final JPanel grid;
    private int row = 0;
    private int col = 0;
    private int columnOffset = 0;
    private Insets insets = new Insets( 2, 4, 2, 4);

    private FormBuilder() {
        this(new JPanel());
        grid.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    }

    private FormBuilder(@NotNull JPanel grid) {
        this.grid = grid;
        grid.setLayout(new GridBagLayout());
        grid.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    }

    private GridBagConstraints createGridBagConstraint(int x, int y, int columnSpan, double columnWeight) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = x;
        c.gridy = y;
        c.gridwidth = columnSpan;
        c.weightx = columnWeight;
        c.fill = columnWeight == 0 ? GridBagConstraints.NONE : GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.WEST;
        c.insets = insets;

        return c;
    }

    private GridBagConstraints createGridBagConstraint(int x, int y, int columnSpan, double columnWeight, int rowSpan, int rowWeight) {
        GridBagConstraints c = createGridBagConstraint(x, y, columnSpan, columnWeight);
        c.gridheight = rowSpan;
        c.weighty = rowWeight;

        if (columnWeight == 0 && rowWeight == 0) {
            c.fill = GridBagConstraints.NONE;
        } else if (columnWeight == 0) {
            c.fill = GridBagConstraints.VERTICAL;
        } else if (rowWeight == 0) {
            c.fill = GridBagConstraints.HORIZONTAL;
        } else {
            c.fill = GridBagConstraints.BOTH;
        }

        c.insets = insets;

        return c;
    }

    public static FormBuilder createBuilder() {
        return new FormBuilder();
    }

    public static FormBuilder createBuilder(JPanel grid) {
        return new FormBuilder(grid);
    }

    public FormBuilder setColumnOffset(int columnOffset) {
        this.columnOffset = columnOffset;
        return this;
    }

    public FormBuilder setRow(int row) {
        this.row = row;
        return this;
    }

    public FormBuilder nextRow() {
        row++;
        return this;
    }

    public FormBuilder setColumnSize(int column , int size) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = column;
        c.insets = new Insets(0, 0, 0, size);

        grid.add(new JLabel(""), c);

        col = 0;

        return this;
    }

    public FormBuilder addColumnSpacer() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = col++;
        c.weightx = 1;

        grid.add(new JLabel(""), c);

        return this;
    }

    public FormBuilder addColumnSpacer(int width) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = col++;
        c.insets = new Insets(0, width, 0, 0);

        grid.add(new JLabel(""), c);

        return this;
    }

    public FormBuilder skipColumns(int columnCount) {
        col += columnCount;
        return this;
    }

    public FormBuilder setBorder(Border border) {
        grid.setBorder(border);
        return this;
    }

    /**
     * Set an empty border to the grid.
     *
     * @return Itself
     */
    public FormBuilder noBorder() {
        grid.setBorder(BorderFactory.createEmptyBorder());
        return this;
    }

    /**
     * Set an empty insets.
     * <br>
     * Note: Should be called before adding cells!
     *
     * @return Iitself
     */
    public FormBuilder noInsets() {
        insets = new Insets(0, 0, 0, 0);
        return this;
    }

    public FormBuilder setInsets(@NotNull Insets insets) {
        this.insets = insets;
        return this;
    }

    public FormBuilder addHeader(@Nls @NotNull String resourceKey, int columnSpan) {
        grid.add(UIFactory.createH1Label(resourceKey), createGridBagConstraint(columnOffset, row++, columnSpan, 0));
        col = 0;
        return this;
    }

    public FormBuilder addSection(@Nls @NotNull String resourceKey, int columnSpan) {
        JLabel label = UIFactory.createH2Label(resourceKey);
        label.putClientProperty(StyleId.STYLE_PROPERTY, SkinExtensionStyles.labelGradient);
        grid.add(label, createGridBagConstraint(columnOffset, row++, columnSpan, 1));
        col = 0;
        return this;
    }

    public FormBuilder addSection(@Nls @NotNull String resourceKey) {
        return addSection(resourceKey, 3);
    }

    /**
     * Add a label at next column and current row w/o increasing row number.
     *
     * @param resourceKey Resource key of the text for the label
     * @param columnSpan Columns to span
     * @return Itself
     */
    public FormBuilder add(@Nls @NotNull String resourceKey, int columnSpan) {
        grid.add(createLabel(resourceKey), createGridBagConstraint(col + columnOffset, row, columnSpan, 0));
        col += columnSpan;
        return this;
    }

    /**
     * Add a label and component at next column and current row w/o increasing row number.
     *
     * @param c The component to add
     * @param columnSpan Columns to span
     * @param columnWeight Columns weight ratio
     * @return Itself
     */
    public FormBuilder add(@NotNull Component c, int columnSpan, double columnWeight) {
        grid.add(c, createGridBagConstraint(col + columnOffset, row, columnSpan, columnWeight));
        col += columnSpan;

        return this;
    }

    /**
     * Adds a component to the current row.
     *
     * @param c The component to add
     * @param columnSpan Columns to span
     * @param columnWeight Column weight ratio
     * @param rowSpan Rows to span
     * @param rowWeight Row weight ratio
     * @return Itself
     */
    public FormBuilder add(@NotNull Component c, int columnSpan, double columnWeight, int rowSpan, int rowWeight) {
        grid.add(c, createGridBagConstraint(col + columnOffset, row, columnSpan, columnWeight, rowSpan, rowWeight));
        col += columnSpan;

        return this;
    }

    /**
     * Add a label and component at next column and current row w/o increasing row number.
     *
     * @param resourceKey Resource key of the text for the label
     * @param c The component to add
     * @param columnSpan Columns to span
     * @param columnWeight Columns weight ratio
     * @return Itself
     */
    public FormBuilder add(@Nls @NotNull String resourceKey, @NotNull Component c, int columnSpan, double columnWeight) {
        grid.add(createLabel(resourceKey), createGridBagConstraint(col + columnOffset, row, 1, 0));
        col += 1;

        grid.add(c, createGridBagConstraint(col + columnOffset, row, columnSpan, columnWeight));
        col += columnSpan;

        return this;
    }

    public FormBuilder addRowSpacer() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = row++;
        c.weighty = 1;

        grid.add(new JLabel(""), c);

        col = 0;

        return this;
    }

    public FormBuilder addRowSpacer(int height) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = row++;
        c.insets = new Insets(height, 0, 0, 0);

        grid.add(new JLabel(""), c);

        col = 0;

        return this;
    }

    /**
     * Add a row and reset column index.
     *
     * @return Itself
     */
    public FormBuilder addRow() {
        row++;
        col = 0;

        return this;
    }

    /**
     * Adds a label and a component to the current row and increase row number.
     *
     * @param resourceKey Resource key of the text for the label
     * @param c The component to add
     * @return Itself
     */
    public FormBuilder addRow(@Nls @NotNull String resourceKey, @NotNull Component c) {
        return addRow(resourceKey, c, 1, 0);
    }

    /**
     * Adds a label to the current row and increase row number.
     *
     * @param resourceKey Resource key of the text for the label
     * @param columnSpan Columns to span
     * @return Itself
     */
    public FormBuilder addRow(@Nls @NotNull String resourceKey, int columnSpan) {
        grid.add(createLabel(resourceKey), createGridBagConstraint(col + columnOffset, row++, columnSpan, 0));
        col = 0;

        return this;
    }

    /**
     * Adds a label and a component to the current row and increase row number.
     *
     * @param resourceKey Resource key of the text for the label
     * @param c The component to add
     * @param columnSpan Columns to span
     * @param columnWeight Columns weight ratio
     * @return Itself
     */
    public FormBuilder addRow(@Nls @NotNull String resourceKey, @NotNull Component c, int columnSpan, double columnWeight) {
        grid.add(createLabel(resourceKey), createGridBagConstraint(col + columnOffset, row, 1, 0));
        grid.add(c, createGridBagConstraint(col + 1 + columnOffset, row++, columnSpan, columnWeight));
        col = 0;

        return this;
    }

    /**
     * Adds a component to the current row and increase row number.
     *
     * Component starts in column 0
     *
     * @param c The component to add
     * @param columnSpan Columns to span
     * @param columnWeight Columns weight ratio
     * @return Itself
     */
    public FormBuilder addRow(@NotNull Component c, int columnSpan, double columnWeight) {
        grid.add(c, createGridBagConstraint(col + columnOffset, row++, columnSpan, columnWeight));
        col = 0;

        return this;
    }

    /**
     * Adds components to the current row.
     *
     * @param components Components to add
     * @param columnSpan Columns to span
     * @return Itself
     */
    public FormBuilder add(int columnSpan, @NotNull Component... components) {
        JPanel p = new JPanel(new FlowLayout());
        Arrays.stream(components).forEach(p::add);

        add(p, columnSpan, 0);

        return this;
    }

    /**
     * Adds a component to the current row and increase row number by rowSpan.
     *
     * @param c The component to add
     * @param columnSpan Columns to span
     * @param columnWeight Columns weight ratio
     * @param rowSpan Rows to span
     * @param rowWeight Row weight ratio
     * @return Itself
     */
    public FormBuilder addRow(@NotNull Component c, int columnSpan, double columnWeight, int rowSpan, int rowWeight) {
        grid.add(c, createGridBagConstraint(col + columnOffset, row, columnSpan, columnWeight, rowSpan, rowWeight));
        col = 0;
        row += rowSpan;

        return this;
    }

    /**
     * Adds components to the current row and increase row number by rowSpan.
     *
     * @param components Components to add
     * @param columnSpan Columns to span
     * @return Itself
     */
    public FormBuilder addRow(int columnSpan, @NotNull Component... components) {
        JPanel p = new JPanel(new FlowLayout());
        Arrays.stream(components).forEach(p::add);

        addRow(p, columnSpan, 0);

        return this;
    }

    /**
     * Builds a {@link JPanel}.
     *
     * @return A {@link JPanel} with a {@link GridBagLayout}
     */
    @NotNull
    public JPanel build() {
        return grid;
    }

    @NotNull
    private JLabel createLabel(@NotNull String resourceKey) {
        return UIFactory.createLabel(LocaleTool.get(resourceKey));
    }

}
