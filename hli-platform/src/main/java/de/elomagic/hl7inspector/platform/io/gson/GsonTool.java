/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.io.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class GsonTool {

    private static final Logger LOGGER = LogManager.getLogger(GsonTool.class);

    private GsonTool() {
    }

    public static Gson createGsonInstance() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(Charset.class, new CharsetAdapter())
                .registerTypeAdapter(Path.class, new PathAdapter())
                .create();
    }

    /**
     * Reads a UTF-8 encoded JSON file into an object of given class.
     *
     * @param in Stream to read from
     * @param clazz Class to read into
     * @param <T> Type of class
     * @return Returns new instance of class
     * @throws IOException Thrown when unable to read the file
     */
    public static <T> T read(@NotNull InputStream in, @NotNull Class<? extends T> clazz) throws IOException {
        try (Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8)) {
            return read(reader, clazz);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    /**
     * Reads a UTF-8 encoded JSON file into an object of given class.
     *
     * @param reader Stream to read from
     * @param clazz Class to read into
     * @param <T> Type of class
     * @return Returns new instance of class
     */
    public static <T> T read(@NotNull Reader reader, @NotNull Class<? extends T> clazz) {
        try {
            return createGsonInstance().fromJson(reader, clazz);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }


    /**
     * Reads a UTF-8 encoded JSON file into an object of given class.
     *
     * @param file File to read
     * @param clazz Class to read into
     * @param <T> Type of class
     * @return Returns new instance of class
     * @throws IOException Thrown when unable to read the file
     */
    public static <T> T read(@NotNull Path file, @NotNull Class<? extends T> clazz) throws IOException {
        try (BufferedReader r = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
            return createGsonInstance().fromJson(r, clazz);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    /**
     * Writes a object as UTF-8 encoded JSON string into a file.
     *
     * @param o Object to write
     * @param out Stream to write into
     * @throws IOException Thrown when unable to create or write file.
     */
    public static void write(@NotNull Object o, @NotNull OutputStream out) throws IOException {
        try (BufferedWriter w = new BufferedWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8)); JsonWriter writer = new JsonWriter(w)) {
            createGsonInstance().toJson(o, o.getClass(), writer);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    /**
     * Writes a object as UTF-8 encoded JSON string into a file.
     *
     * @param o Object to write
     * @param path File to write into
     * @throws IOException Thrown when unable to create or write file.
     */
    public static void write(@NotNull Object o, @NotNull Path path) throws IOException {
        try (BufferedWriter w = Files.newBufferedWriter(path, StandardCharsets.UTF_8); JsonWriter writer = new JsonWriter(w)) {
            createGsonInstance().toJson(o, o.getClass(), writer);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

}
