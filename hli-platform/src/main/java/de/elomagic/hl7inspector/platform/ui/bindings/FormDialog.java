/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.bindings;

import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class FormDialog<T> extends MyBaseDialog implements FormBinder<T> {

    private static final Logger LOGGER = LogManager.getLogger(FormDialog.class);

    protected FormDialog() {
    }

    protected FormDialog(String title, boolean modal) {
        super(title, modal);
    }

    @Override
    public void ok() {
        try {
            if (validateForm()) {
                commit();

                super.ok();
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

}
