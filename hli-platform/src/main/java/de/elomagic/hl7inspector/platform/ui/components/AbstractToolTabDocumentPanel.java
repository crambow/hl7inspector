/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import com.alee.extended.panel.WebComponentPane;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.ImageIcon;

/**
 * Usually used for tabs in the bottom desktop.
 */
public abstract class AbstractToolTabDocumentPanel extends WebComponentPane {

    @NotNull
    public String getId() {
        return getClass().getName();
    }

    /**
     * Returns the located name of the tab.
     *
     * @return A located string.
     */
    @NotNull
    public abstract String getTitle();

    /**
     * Returns the icon of the tab.
     *
     * @return A ImageIcon or null when no icon should be visible.
     */
    @Nullable
    public abstract ImageIcon getIcon();

}
