/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils.formatter;

import com.alee.extended.overlay.WebOverlay;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExOverlayFormatter extends AbstractOverlayFormatter {

    private final Pattern pattern;

    public RegExOverlayFormatter(@NotNull WebOverlay overlay, @NotNull String regex) {
        super(overlay);

        this.pattern = Pattern.compile(regex);
    }

    @Override
    public Object stringToValue(String text) throws ParseException {
        if (pattern != null) {
            Matcher matcher = pattern.matcher(text);

            if (matcher.matches()) {
                removeOverlay();
                return super.stringToValue(text);
            }

            showOverlay();

            throw new ParseException("Pattern didn't match", 0);
        }
        return text;
    }

}
