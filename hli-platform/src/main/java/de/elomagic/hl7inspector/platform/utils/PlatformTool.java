/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils;

import de.elomagic.hl7inspector.platform.ui.SimpleDialog;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.Desktop;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Some platform tools to make it more OS independent.
 */
public final class PlatformTool {

    private static final String USER_HOME = "user.home";
    public static final String USER_HOME_FOLDER = ".hl7inspector";

    private static final Logger LOGGER = LogManager.getLogger(PlatformTool.class);

    private PlatformTool() {
    }

    public static boolean isMacOS() {
        String os = System.getProperty("os.name").toLowerCase();
        return os.toLowerCase().startsWith("mac");
    }

    public static boolean isWindowsOS() {
        String os = System.getProperty("os.name").toLowerCase();
        return os.toLowerCase().startsWith("win");
    }

    public static Set<Path> getLoggingFolder() {
        LoggerContext context = LoggerContext.getContext();
        org.apache.logging.log4j.core.config.Configuration configuration = context.getConfiguration();

        return configuration
                .getAppenders()
                .values()
                .stream()
                .filter(a -> a instanceof RollingFileAppender)
                .map(a -> (RollingFileAppender)a)
                .map(RollingFileAppender::getFileName)
                .map(Paths::get)
                .map(Path::getParent)
                .collect(Collectors.toSet());
    }

    /**
     * Returns temporary folder of the HL7 Inspector.
     *
     * Folder will be created if doesn't exists.
     *
     * Paths.get(System.getProperty("java.io.tmpdir")).resolve("hl7inspector");
     *
     * @return The path but never null;
     * @throws IOException Thrown when unable to create folder.
     */
    @NotNull
    public static Path getTempAppFolder() throws IOException {
        Path path = Paths.get(System.getProperty("java.io.tmpdir")).resolve("hl7inspector");
        Files.createDirectories(path);
        return path;
    }

    /**
     * Returns the users home of the HL7 Inspector.
     *
     * Usually used for persisting configuration.
     *
     * Folder will be created if doesn't exists.
     *
     * Paths.get(System.getProperty("user.home")).resolve(".hl7inspector");
     *
     * @return The path but never null;
     * @throws IOException Thrown when unable to create folder.
     */
    @NotNull
    public static Path getUsersHL7InspectorHome() throws IOException {
        Path path = Paths.get(System.getProperty(USER_HOME), USER_HOME_FOLDER);
        Files.createDirectories(path);
        return path;
    }


    /**
     * Returns the users plugins folder of the HL7 Inspector.
     *
     * Folder will be created if doesn't exists.
     *
     * getUsersHL7InspectorHome().resolve("plugins");
     *
     * @return The path but never null;
     * @throws IOException Thrown when unable to create folder.
     */
    @NotNull
    public static Path getUserPluginsFolder() throws IOException {
        Path path = getUsersHL7InspectorHome().resolve("plugins");
        Files.createDirectories(path);
        return path;
    }

    /**
     * Returns temporary folder for not persisted message files (In memory files).
     *
     * Folder will be created if doesn't exists.
     *
     * @return The path but never null;
     * @throws IOException Thrown when unable to create folder.
     */
    @NotNull
    public static Path getInMemoryFolder() throws IOException {
        Path path = getTempAppFolder().resolve("inmemory");
        Files.createDirectories(path);
        return path;
    }

    @NotNull
    public static Path getHL7DocumentFolder() {

        Path path = Paths.get(System.getProperty(USER_HOME));

        if (isWindowsOS()) {
            try {
                Process p = Runtime.getRuntime().exec("reg query \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders\" /v personal");
                p.waitFor();

                try (InputStream in = p.getInputStream()) {
                    byte[] b = new byte[in.available()];
                    in.read(b);
                    String myDocuments = new String(b).split("\\s\\s+")[4];
                    path = Paths.get(myDocuments);
                }
            } catch(Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else  if(isMacOS()) {
            path = path.resolve("Documents");
        }

        path = path.resolve("HL7 Inspector");
        if (!Files.exists(path)) {
            LOGGER.info("Folder '{}' doesn't exist. Return parent folder.", path);
            path = path.getParent();
        }

        return path;
    }

    public static void browse(@NotNull String address) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(URI.create(address));
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                SimpleDialog.error(e.getMessage());
            }
        }
    }

    public static void showFolder(@NotNull Path folder) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.OPEN)) {
            try {
                desktop.open(folder.toFile());
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                SimpleDialog.error(e.getMessage());
            }
        }
    }

    @Nullable
    public static Process openFileWith(@NotNull Path executable, @NotNull Path file) {
        try {
            Runtime rt = Runtime.getRuntime();
            String[] cmd = {executable.toString(), file.toString()};
            return rt.exec(cmd);
        } catch(Exception ee) {
            LOGGER.error(ee.getMessage(), ee);
            SimpleDialog.error(LocaleTool.get("unable_to_open_external_file_viewer_editor_application"), ee);
            return null;
        }
    }

    public static void openLogFiles() {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.OPEN)) {
            PlatformTool.getLoggingFolder()
                    .forEach(p -> {
                        try {
                            LOGGER.debug("Opening folder \"{}\"", p);
                            desktop.open(p.toFile());
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                            SimpleDialog.error(e.getMessage());
                        }

                    });
        }
    }

}
