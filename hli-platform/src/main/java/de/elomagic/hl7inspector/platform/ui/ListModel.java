/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import org.jetbrains.annotations.NotNull;

import javax.swing.AbstractListModel;
import javax.swing.MutableComboBoxModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListModel <E> extends AbstractListModel<E> implements List<E>, MutableComboBoxModel<E> {

    private final transient List<E> list = new ArrayList<>();

    private transient Object selectedObject;

    private int limit;
    private boolean unique;

    public ListModel() {
        this.limit = Integer.MAX_VALUE;
    }

    public ListModel(int limit, boolean unique) {
        this.limit = limit;
        this.unique = unique;
    }

    public ListModel(int limit) {
        this.limit = limit;
    }

    public ListModel(boolean unique) {
        this.limit = Integer.MAX_VALUE;
        this.unique = unique;
    }

    /**
     * Check that the limit is not exceeded and cut off from the beginning if required.
     *
     * @param toAdd Items to add
     */
    private void careLimit(int toAdd) {
        if (limit == Integer.MAX_VALUE) {
            return;
        }

        int newMaxSize = limit - toAdd;

        while (list.size() > newMaxSize) {
            remove(0);
        }
    }

    /**
     * Keep care that given item not in the list when unique is enabled.
     *
     * @param index Index before taking care
     * @param items Item to remove
     * @return New index
     */
    @SafeVarargs
    private int careUnique(int index, E... items) {
        if (!unique || items == null) {
            return index;
        }

        for (E item : items) {
            int i;
            while ((i = indexOf(item)) != -1) {
                if (index > i) {
                    index--;
                }

                remove(i);
            }
        }

        return index;
    }


    /**
     * Keep care that given item not in the list when unique is enabled.
     *
     * @param items Item to remove
     */
    @SafeVarargs
    private void careUnique(E... items) {
        careUnique(0, items);
    }

    public void setLimit(int limit) {
        this.limit = limit;
        careLimit(0);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @NotNull
    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        if (index == -1) {
            return false;
        }

        remove(index);

        return true;
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public void clear() {
        int index = list.size()-1;
        list.clear();

        selectedObject = null;

        if (index >= 0) {
            fireIntervalRemoved(this, 0, index);
        }
    }

    @Override
    public boolean add(E item) {
        return addAll(Math.min(list.size(), limit-1), Collections.singletonList(item));
    }

    @Override
    public void add(int index, E item) {
        addAll(Math.min(list.size(), limit-1), Collections.singletonList(item));
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends E> c) {
        return addAll(Math.min(list.size(), limit-1), c);
    }

    @Override
    public boolean addAll(int index, @NotNull Collection<? extends E> c) {
        if (c.size() == 0) {
            return false;
        }

        careLimit(c.size());
        index = careUnique(index, (E[])c.toArray());

        boolean result = list.addAll(index, c);
        fireIntervalAdded(this, index, index + c.size() - 1);

        if (list.size() == 1 && selectedObject == null && list.get(0) != null) {
            setSelectedItem(list.get(0));
        }

        return result;
    }

    @Override
    public E set(int index, E item) {

        // TODO careUnique(item);

        int i = list.indexOf(item);

        E result = list.set(index, item);

        fireContentsChanged(this, index, index);

        if (i != -1) {
            setSelectedItem(list.get(index));
        }

        return result;
    }

    @Override
    public E remove(int index) {
        if (index == 0) {
            setSelectedItem(list.size() == 1 ? null : getElementAt(index + 1));
        } else {
            setSelectedItem(getElementAt(index - 1));
        }

        E result = list.remove(index);
        fireIntervalRemoved(this, index, index);
        return result;
    }

    /**
     * Remove items.
     *
     * @param c Collections of items
     * @return Returns true when at least one of given items was removed.
     */
    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        long count = c.stream().filter(this::remove).count();

        return count != 0;
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException("Currently not implemented yet.");
    }

    @Override
    public E get(int index) {
        return list.get(index);
    }

    /**
     * Returns item of index.
     *
     * Compare to other methods, this method return null instead throwing a {@link IndexOutOfBoundsException}.
     *
     * @param index Index of the item
     *
     * @return Returns item or null
     */
    @Override
    public E getElementAt(int index) {
        if (index >= 0 && index < list.size()) {
            return list.get(index);
        } else {
            return null;
        }
    }

    /**
     * Returns the index of the first occurrence.
     *
     * @param o Item to search for
     * @return The index or -1 if this list does not contain the element
     */
    @Override
    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    @NotNull
    @Override
    public ListIterator<E> listIterator() {
        return list.listIterator();
    }

    @NotNull
    @Override
    public ListIterator<E> listIterator(int index) {
        return list.listIterator(index);
    }

    @NotNull
    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex, toIndex);
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public void addElement(E item) {
        add(item);

        if ( list.size() == 1 && selectedObject == null && item != null ) {
            setSelectedItem(item);
        }
    }

    @Override
    public void insertElementAt(E item, int index) {
        add(index, item);
    }

    /**
     * Remove item from list.
     *
     * @param anObject Item to remove
     */
    @Override
    public void removeElement(Object anObject) {
        int index = indexOf(anObject);
        if (index != -1) {
            removeElementAt(index);
        }
    }

    @Override
    public void removeElementAt(int index) {
        remove(index);
    }

    @Override
    public void setSelectedItem(Object anObject) {
        if ((selectedObject != null && !selectedObject.equals( anObject )) ||
                selectedObject == null && anObject != null) {
            selectedObject = anObject;
            fireContentsChanged(this, -1, -1);
        }
    }

    @Override
    public Object getSelectedItem() {
        return selectedObject;
    }
}
