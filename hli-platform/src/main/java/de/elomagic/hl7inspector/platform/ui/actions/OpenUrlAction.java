/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.actions;

import com.alee.managers.icon.Icons;

import de.elomagic.hl7inspector.platform.utils.PlatformTool;

import org.jetbrains.annotations.NotNull;

import javax.swing.ImageIcon;
import java.net.URL;

public class OpenUrlAction extends GenericAction {

    public OpenUrlAction(@NotNull String url, @NotNull String nameKey) {

        super(e -> PlatformTool.browse(url));

        setName(nameKey);
        setSmallIcon((ImageIcon)Icons.globe.getIcon());
    }

    public OpenUrlAction(@NotNull URL url, @NotNull String nameKey) {
        this(url.toString(), nameKey);
    }

}
