/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import com.alee.laf.list.WebList;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.ui.ListModel;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ButtonBar extends JScrollPane {

    static class ButtonRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(
                JList<?> list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {
            return (JToggleButton)value;
        }
    }

    private final WebList list;
    private final ListModel<JToggleButton> model = new ListModel<>();

    public ButtonBar() {
        putClientProperty(StyleId.STYLE_PROPERTY, StyleId.scrollpaneTransparentHoveringExtending);
        setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);

        list = new WebList();
        list.putClientProperty(StyleId.STYLE_PROPERTY, StyleId.list);

        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setCellRenderer(new ButtonBar.ButtonRenderer());
        list.setModel(model);

        list.addListSelectionListener(e -> selectButton((JToggleButton)list.getSelectedValue()));

        setViewportView(list);
    }

    public void addItem(@NotNull String text, @Nullable String description, @NotNull Icon icon, @NotNull ActionListener listener) {
        JToggleButton button = new JToggleButton(text);
        button.putClientProperty( StyleId.STYLE_PROPERTY, StyleId.togglebuttonIconHover);
        button.addActionListener(listener);
        button.setIcon(icon);
        button.setToolTipText(description == null ? text : description);
        button.setBorder(BorderFactory.createEmptyBorder(10, 4, 10, 4));
        button.setVerticalTextPosition(SwingConstants.BOTTOM);
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        button.setRolloverEnabled(true);
        button.setOpaque(false);

        model.add(button);

        getHorizontalScrollBar().setValue(20);
    }

    private void selectButton(JToggleButton b) {
        b.setSelected(true);
        b.doClick();

        model.stream()
                .filter(button -> b != button)
                .forEach(button -> button.setSelected(false));
    }

}
