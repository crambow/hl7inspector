/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils.security;

import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509ExtensionUtils;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCSException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

public final class CertificateTool {

    private static final char[] DEFAULT_PASSPHRASE = "secret".toCharArray();

    private CertificateTool() {
    }

    /**
     *
     * @param privateKey Base64 PEM encoded private key and certificates
     * @param password Password for private key
     * @return Returns key managers
     * @throws OperatorCreationException
     * @throws GeneralSecurityException
     * @throws PKCSException Thrown when unable to parse PEM string
     * @throws IOException
     */
    @NotNull
    public static KeyManager[] getKeyManagers(String privateKey, char[] password) throws OperatorCreationException, GeneralSecurityException, PKCSException, IOException {
        PrivateKey key = PemTool.readPrivateKey(privateKey, password);
        X509Certificate[] certificates = PemTool.readCertificates(privateKey);

        KeyStore keystore = KeyStore.getInstance("JKS");
        keystore.load(null);
        keystore.setKeyEntry("alias", key, DEFAULT_PASSPHRASE, certificates);

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(keystore, DEFAULT_PASSPHRASE);

        return kmf.getKeyManagers();
    }

    /**
     *
     * @param pem Base64 PEM encoded certificates
     * @return Returns trust managers
     * @throws GeneralSecurityException
     * @throws IOException
     */
    @NotNull
    public static TrustManager[] getTrustManagers(@NotNull String pem) throws GeneralSecurityException, IOException {
        X509Certificate[] certificates = PemTool.readCertificates(pem);

        KeyStore keystore = KeyStore.getInstance("JKS");
        keystore.load(null);
        for (int i=0; i<certificates.length; i++) {
            keystore.setCertificateEntry("alias" + i, certificates[i]);
        }

        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(keystore);

        return tmf.getTrustManagers();
    }

    /**
     * Creates a RSA private key with 4096 key size.
     *
     * @return Returns a key pair
     * @throws NoSuchAlgorithmException
     */
    @NotNull
    public static KeyPair createRSAKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(4096);
        return keyPairGenerator.generateKeyPair();
    }

    /**
     * Creates a self signed certificate.
     * <p>
     * Duration 5 years
     *
     * @param keyPair
     * @param hashAlgorithm
     * @return Returns a self signed certificate
     * @throws CertificateException
     * @throws OperatorCreationException
     * @throws CertIOException
     */
    @NotNull
    public static X509Certificate createSelfSignedCertificate(KeyPair keyPair, String hashAlgorithm) throws CertificateException, OperatorCreationException, CertIOException {
        int daysDuration = 5 * 365;

        Instant now = Instant.now();
        Date notBefore = Date.from(now);
        Date notAfter = Date.from(now.plus(Duration.ofDays(daysDuration)));

        ContentSigner contentSigner = new JcaContentSignerBuilder(hashAlgorithm).build(keyPair.getPrivate());
        X509v3CertificateBuilder certificateBuilder =
                new JcaX509v3CertificateBuilder(
                        new X500Name("CN=HL7 Inspector Application"),
                        BigInteger.valueOf(now.toEpochMilli()),
                        notBefore,
                        notAfter,
                        new X500Name("CN=Self signed test certificate"),
                        keyPair.getPublic())
                        .addExtension(Extension.subjectKeyIdentifier, false, createSubjectKeyId(keyPair.getPublic()))
                        .addExtension(Extension.authorityKeyIdentifier, false, createAuthorityKeyId(keyPair.getPublic()))
                        .addExtension(Extension.basicConstraints, true, new BasicConstraints(true));

        return new JcaX509CertificateConverter().setProvider(new BouncyCastleProvider()).getCertificate(certificateBuilder.build(contentSigner));
    }

    /**
     * Creates a self signed PEM encoded certificate by using SHA256withRSA algorithm.
     *
     * @param password Password
     * @return Returns Base64 PEM encoded string
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws OperatorCreationException
     */
    @NotNull
    public static String createSelfSignedPemEncodedCertificate(char[] password) throws GeneralSecurityException, IOException, OperatorCreationException {
        KeyPair keyPair = CertificateTool.createRSAKeyPair();

        X509Certificate certificate = CertificateTool.createSelfSignedCertificate(keyPair, "SHA256withRSA");

        StringWriter w = new StringWriter();

        PemTool.writePrivateKeyAndCertificate(keyPair.getPrivate(), password, certificate, w);

        return w.toString();
    }

    @NotNull
    private static SubjectKeyIdentifier createSubjectKeyId(final PublicKey publicKey) throws OperatorCreationException {
        SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfo.getInstance(publicKey.getEncoded());
        DigestCalculator digCalc = new BcDigestCalculatorProvider().get(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1));

        return new X509ExtensionUtils(digCalc).createSubjectKeyIdentifier(publicKeyInfo);
    }

    @NotNull
    private static AuthorityKeyIdentifier createAuthorityKeyId(final PublicKey publicKey) throws OperatorCreationException {
        SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfo.getInstance(publicKey.getEncoded());
        DigestCalculator digCalc = new BcDigestCalculatorProvider().get(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1));

        return new X509ExtensionUtils(digCalc).createAuthorityKeyIdentifier(publicKeyInfo);
    }

    /**
     * Creates a SSL context.
     *
     * @param privateKey Base64 PEM encoded private key.
     * @param privateKeyPassword Private key password if private key itself is encrypted
     * @param trustedCertificates Base64 PEM encoded trsuted certificates.
     * @return Returns a SSL context
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws PKCSException Thrown when unable to parse PEM string
     * @throws OperatorCreationException
     */
    @NotNull
    public static SSLContext createSSLContext(
            @Nullable String privateKey,
            @Nullable char[] privateKeyPassword,
            @Nullable String trustedCertificates) throws GeneralSecurityException, IOException, PKCSException, OperatorCreationException {

        KeyManager[] km = privateKey == null ? null : getKeyManagers(privateKey, privateKeyPassword);
        TrustManager[] tm = trustedCertificates == null ? null : getTrustManagers(trustedCertificates);

        SSLContext context = SSLContext.getInstance("TLS");
        context.init(km, tm, null);

        return context;
    }

}
