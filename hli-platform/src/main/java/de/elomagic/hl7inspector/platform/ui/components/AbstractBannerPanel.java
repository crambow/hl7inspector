/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinder;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import java.awt.BorderLayout;

/**
 * A JPanel with a {@link BorderLayout} with heading Banner.
 *
 * @param <T>
 */
public abstract class AbstractBannerPanel<T> extends JPanel implements FormBinder<T> {

    protected AbstractBannerPanel() {
        super(new BorderLayout(0, 6));

        JPanel banner = UIFactory.createBanner(getTitle(), getIcon());

        add(banner, BorderLayout.NORTH);
        add(new JRootPane(), BorderLayout.CENTER);
    }

    public abstract Icon getIcon();

    public abstract String getTitle();

    public abstract String getDescription();

    @Override
    public String toString() {
        return getTitle();
    }

}
