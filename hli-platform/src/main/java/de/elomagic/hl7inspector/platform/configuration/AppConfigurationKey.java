/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.configuration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Locale;

public enum AppConfigurationKey implements ConfigurationKey {

    APP_UPDATE_CHECK_TIME_PERIOD("application.updateCheck.timePeriod", Integer.class, false),
    APP_UPDATE_CHECK_TIME_ASK_USER("application.updateCheck.askUser", Boolean.class,false),
    APP_UPDATE_CHECK_TIME_LAST_CHECK("application.updateCheck.lastCheck", null, false),

    APP_QUIT_WITHOUT_ASKING("application.quitWithoutAsking", Boolean.class,false),
    APP_LANGUAGE("application.language", Locale.class,false),
    APP_SINGLE_INSTANCE("application.singleInstance", Boolean.class,false),
    APP_NETWORK_PROXY_MODE("application.network.proxy.mode", Integer.class,false),
    APP_NETWORK_PROXY_HOST("application.network.proxy.host", String.class,false),
    APP_NETWORK_PROXY_PORT("application.network.proxy.port", Integer.class,false),

    APP_UI_LAF_THEME("application.ui.laf.theme", null,false),

    APP_UI_DESKTOP_POSITION_X("application.ui.desktop.position.x", Integer.class, false),
    APP_UI_DESKTOP_POSITION_Y("application.ui.desktop.position.y", Integer.class, false),
    APP_UI_DESKTOP_DIMENSION_WIDTH("application.ui.desktop.dimension.width", Integer.class, false),
    APP_UI_DESKTOP_DIMENSION_HEIGHT("application.ui.desktop.dimension.height", Integer.class, false),
    APP_UI_TOOLBAR_ORIENTATION("application.ui.toolbar.orientation", null,false),

    APP_EDITOR_VIEW_MODE("application.editor.viewMode", null,false),
    APP_EDITOR_TEXT_FONT_NAME("application.editor.text.font.name", String.class, false),
    APP_EDITOR_TEXT_FONT_SIZE("application.editor.text.font.size", Integer.class, false),
    APP_EDITOR_TREE_NODE_MESSAGE_FORMAT("application.editor.tree.node.messageFormat", String.class,false),
    APP_EDITOR_TREE_NODE_MAX_LENGTH("application.editor.tree.node.maxLength", Integer.class, false),
    APP_EDITOR_TREE_FONT_NAME("application.editor.tree.font.name", String.class, false),
    APP_EDITOR_TREE_FONT_SIZE("application.editor.tree.font.size", Integer.class, false),

    APP_FILES_EXTERNAL_FILE_VIEWER("application.files.externalFileViewer", Path.class,false),
    APP_FILES_LAST_USED_FOLDER("application.files.lastUsedFolder", Path.class,false),
    APP_FILES_RECENTLY_OPENED("application.files.recentlyOpened", null,true), // List of sub items
    APP_FILES_MAX_OPEN("application.files.maxOpen", Integer.class,false),
    APP_FILES_PARSER_CHARSET("application.files.parser.charset", Charset.class,false),

    APP_SEARCH_PHRASE_HISTORY("application.search.phraseHistory", null,true), // List of sub items

    APP_PLUGIN_REPOSITORIES_ULS("application.plugin.repositories_urls", URL.class, true),

    APP_PROFILES_SELECTED_FILE("application.profiles.selected.file", null,false),
    APP_PROFILES_AVAILABLE_FILES("application.profiles.available.files", null,true), // List of sub items [.xxx]
    APP_PROFILES_AVAILABLE_NAMES("application.profiles.available.names", null,true), // List of sub items [.xxx]
    APP_PROFILES_AVAILABLE_DESCRIPTIONS("application.profiles.available.descriptions", null,true); // List of sub items [.xxx]

    private final String keyName;
    private final boolean set;
    private final Class<?> classType;

    AppConfigurationKey(String keyName, Class<?> classType, boolean set) {
        this.keyName = keyName;
        this.classType = classType;
        this.set = set;
    }

    @NotNull
    @Override
    public String getKeyName() {
        return keyName;
    }

    @Override
    public boolean isSet() {
        return set;
    }

    /**
     * Returns data class type.
     *
     * In case of a set then it will return the class type of an item.
     *
     * @return Returns the class type
     */
    @NotNull
    @Override
    public Class<?> getClassType() {
        return classType;
    }

    @Nullable
    public static ConfigurationKey valueByName(@NotNull String keyName) {
        for (ConfigurationKey key : values()) {
            if (key.getKeyName().equals(keyName)) {
                return key;
            }
        }

        return null;
    }

}
