/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.themes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class IconThemeManager {

    private static final IconThemeManager INSTANCE = new IconThemeManager();

    private static final Logger LOGGER = LogManager.getLogger(IconThemeManager.class);

    private AbstractIconTheme currentTheme = new ClassicIconTheme();

    public Set<AbstractIconTheme> getSupportedThemesImpl() {
        return new TreeSet<>(Arrays.asList(
                new ClassicIconTheme(),
                new Year2020Theme(),
                new OutlineTheme(),
                new OutlineDarkTheme()
        ));
    }

    @NotNull
    public AbstractIconTheme getThemeByIdImpl(@Nullable @NonNls String id) {
        return getSupportedThemes()
                .stream()
                .filter(theme -> theme.getId().equals(id))
                .findFirst()
                .orElseGet(() ->  {
                    LOGGER.error("Theme ID {} not found. Fallback to outline theme.", id);
                    return new OutlineTheme();
                });
    }

    public static Set<AbstractIconTheme> getSupportedThemes() {
        return INSTANCE.getSupportedThemesImpl();
    }

    public static AbstractIconTheme getTheme() {
        return INSTANCE.currentTheme;
    }

    public static void setTheme(AbstractIconTheme theme) {
        INSTANCE.currentTheme = theme;
    }

    @NotNull
    public static AbstractIconTheme getThemeById(@Nullable @NonNls String id) {
        return INSTANCE.getThemeByIdImpl(id);
    }

    /**
     *
     * @param name Name of the image. Like: "hl7inspector-32.png"
     * @return The icon or null when not found
     */
    @Nullable
    public static ImageIcon getImageIcon(@NotNull @NonNls String name) {
        return INSTANCE.currentTheme.loadImageIcon(name);
    }

    public static Image getImage(@NotNull String name) {
        ImageIcon imageIcon = getImageIcon(name);
        return imageIcon == null ? null : imageIcon.getImage();
    }


}
