/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.events;

import de.elomagic.hl7inspector.platform.ui.components.AbstractToolTabDocumentPanel;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationEvent;

/**
 * Event that request to open an tool tab window at the bottom of the desktop.
 */
public class OpenToolTabDocumentEvent extends ApplicationEvent {

    private Class<? extends AbstractToolTabDocumentPanel> clazz;
    private AbstractToolTabDocumentPanel component;
    private Boolean visible;

    public OpenToolTabDocumentEvent(@NotNull Object source, @NotNull AbstractToolTabDocumentPanel component) {
        super(source);

        this.component = component;
    }

    public OpenToolTabDocumentEvent(@NotNull Object source, @NotNull Class<? extends AbstractToolTabDocumentPanel> clazz) {
        super(source);

        this.clazz = clazz;
    }

    public OpenToolTabDocumentEvent(@NotNull Object source, @NotNull AbstractToolTabDocumentPanel component, boolean visible) {
        super(source);

        this.component = component;
        this.visible = visible;
    }

    public OpenToolTabDocumentEvent(@NotNull Object source, @NotNull Class<? extends AbstractToolTabDocumentPanel> clazz, boolean visible) {
        super(source);

        this.clazz = clazz;
        this.visible = visible;
    }

    @Nullable
    public AbstractToolTabDocumentPanel getComponent() {
        return component;
    }

    @Nullable
    public Class<? extends AbstractToolTabDocumentPanel> getClazz() {
        return clazz;
    }

    @Nullable
    public Boolean getVisible() {
        return visible;
    }
}