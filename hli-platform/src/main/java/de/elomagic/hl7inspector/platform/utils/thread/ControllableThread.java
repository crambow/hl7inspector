/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils.thread;

import de.elomagic.hl7inspector.platform.events.BeforeApplicationExitEvent;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.List;

@Scope("prototype")
public abstract class ControllableThread<S extends ThreadEvent<? extends ControllableThread<S, E>>, E extends ThreadEvent<? extends ControllableThread<S, E>>> extends Thread {

    private final List<ThreadAfterStartListener<S>> afterListener = new ArrayList<>();
    private final List<ThreadBeforeEndListener<E>> beforeListener = new ArrayList<>();

    protected ControllableThread() {
        setName(getClass().getSimpleName() + "-" + getName());
    }

    protected ControllableThread(@NotNull String name) {
        setName(name);
    }

    /**
     * Override this method if you want to be notified when application going to terminate.
     */
    protected void requestTermination() {
        // noop
    }

    public void addAfterThreadStartsListener(ThreadAfterStartListener<S> listener) {
        afterListener.add(listener);
    }

    public void removeAfterThreadStartsListener(ThreadAfterStartListener<S> listener) {
        afterListener.remove(listener);
    }

    public void addBeforeThreadEndListener(ThreadBeforeEndListener<E> listener) {
        beforeListener.add(listener);
    }

    public void removeBeforeThreadEndListener(ThreadBeforeEndListener<E> listener) {
        beforeListener.remove(listener);
    }

    protected void fireAfterStartListener(S event) {
        afterListener.forEach(l -> l.afterThreadStarted(event));
    }

    protected void fireBeforeEndListener(E event) {
        beforeListener.forEach(l -> l.beforeThreadStarts(event));
    }

    @EventListener(BeforeApplicationExitEvent.class)
    public void handleBeforeApplicationExitEvent() {
        requestTermination();
    }

}
