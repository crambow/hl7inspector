/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils;

public class MetricBean {

    private int count = 0;
    private long min = Long.MAX_VALUE;
    private long max = Long.MIN_VALUE;
    private long total = 0;
    private long latestMeasure = Long.MIN_VALUE;

    /**
     * Returns count of added measures.
     *
     * @return Returns count
     */
    public int getCount() {
        return count;
    }

    /**
     * Returns minimum measure.
     *
     * @return Returns measure
     */
    public long getMin() {
        return min;
    }

    /**
     * Returns maximum measure.
     *
     * @return Returns measure
     */
    public long getMax() {
        return max;
    }

    /**
     * Returns latest add measure.
     *
     * @return Returns measure
     */
    public long getLatest() {
        return latestMeasure;
    }

    /**
     * Returns average.
     *
     * @return Returns average or -1 when not measure was added
     */
    public long getAverage() {
        return count == 0 ? -1 : (total / count);

    }

    public void add(long measure) {
        latestMeasure = measure;
        count++;
        total += measure;

        min = Math.min(min, measure);
        max = Math.max(max, measure);
    }

}
