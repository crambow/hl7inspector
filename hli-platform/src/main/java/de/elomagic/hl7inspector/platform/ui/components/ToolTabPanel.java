/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import com.alee.extended.tab.DocumentData;
import com.alee.extended.tab.WebDocumentPane;

import de.elomagic.hl7inspector.platform.events.OpenToolTabDocumentEvent;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.SwingConstants;
import java.lang.reflect.InvocationTargetException;

/**
 * Tool panel which will group all tool feature like sending, receiving, etc.
 */
@Component
public class ToolTabPanel extends WebDocumentPane<DocumentData<?>> {

    private static final Logger LOGGER = LogManager.getLogger(ToolTabPanel.class);

    private final transient BeanFactory beanFactory;

    @Autowired
    public ToolTabPanel(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @PostConstruct
    private void initUI() {
        setVisible(false);
        setTabbedPaneCustomizer(tabbedPane -> tabbedPane.setTabPlacement (SwingConstants.BOTTOM ));
    }

    @EventListener
    public void handleOpenToolTabDocumentEvent(OpenToolTabDocumentEvent e) {
        try {
            AbstractToolTabDocumentPanel c = e.getClazz() == null ? e.getComponent() : createComponent(e.getClazz());

            if (c == null) {
                throw new IllegalArgumentException("Unable to handle event. Event doesn't contains a component or component class.");
            } else if (e.getVisible() == null ? getDocument(c.getId()) == null : e.getVisible()) {
                openDocument(new DocumentData<>(c.getId(), c.getTitle(), c));
            } else {
                closeDocument(c.getId());
            }

            setVisible(!getDocuments().isEmpty());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private AbstractToolTabDocumentPanel createComponent(Class<? extends AbstractToolTabDocumentPanel> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        if (clazz.isAnnotationPresent(Component.class)) {
            Component annotation = clazz.getAnnotation(Component.class);
            String name = annotation.value();

            if (StringUtils.isBlank(name)) {
                return beanFactory.getBean(clazz);
            } else {
                return BeanFactoryAnnotationUtils.qualifiedBeanOfType(beanFactory, clazz, name);
            }
        } else {
            return clazz.getConstructor().newInstance();
        }
    }
}
