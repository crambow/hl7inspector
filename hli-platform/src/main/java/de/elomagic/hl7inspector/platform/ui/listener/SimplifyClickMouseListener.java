/*
 * HL7 Inspector - Platform
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Platform.
 *
 * HL7 Inspector - Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Platform. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.listener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Interface to simplify and handle only mouse click events.
 */
@FunctionalInterface
public interface SimplifyClickMouseListener extends MouseListener {

    @Override
    default void mousePressed(MouseEvent e) {
        // noop
    }

    @Override
    default void mouseReleased(MouseEvent e) {
        // noop
    }

    @Override
    default  void mouseEntered(MouseEvent e) {
        // noop
    }

    @Override
    default void mouseExited(MouseEvent e) {
        // noop
    }

}
