/*
 * Copyright 2020-present Carsten Rambow
 *
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/gpl.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector.configuration;

import de.elomagic.hl7inspector.platform.configuration.AppConfigurationKey;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.configuration.ToolsConfigurationKey;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

class ConfigurationTest {

    @Test
    void testGetSetByKeys() {

        List<String> v = Arrays.asList("a", "b", "z");

        Configuration.getInstance().setPhrases(v);

        Assertions.assertEquals(v, Configuration.getInstance().getPhrases());

    }

    /**
     * This test check data type casting compare to test of module "shared".
     *
     * @throws Exception
     */
    @Test
    void testGetProperty() throws Exception {

        Configuration c = Configuration.getInstance();

        // String.class
        c.setProperty(AppConfigurationKey.APP_NETWORK_PROXY_HOST, "abc");
        Assertions.assertEquals("abc", c.getProperty(AppConfigurationKey.APP_NETWORK_PROXY_HOST, String.class, null));

        // Boolean.class
        c.setProperty(AppConfigurationKey.APP_QUIT_WITHOUT_ASKING, true);
        Assertions.assertTrue(c.getProperty(AppConfigurationKey.APP_QUIT_WITHOUT_ASKING, Boolean.class, null));
        c.setProperty(AppConfigurationKey.APP_QUIT_WITHOUT_ASKING, false);
        Assertions.assertFalse(c.getProperty(AppConfigurationKey.APP_QUIT_WITHOUT_ASKING, Boolean.class, null));
        Assertions.assertFalse(c.getProperty(AppConfigurationKey.APP_QUIT_WITHOUT_ASKING, Boolean.class, null));

        // Integer.class
        c.setProperty(AppConfigurationKey.APP_NETWORK_PROXY_PORT, 12345);
        Assertions.assertEquals(12345, c.getProperty(AppConfigurationKey.APP_NETWORK_PROXY_PORT, Integer.class, null));
        Assertions.assertEquals("12345", c.getProperty(AppConfigurationKey.APP_NETWORK_PROXY_PORT, String.class, null));

        // Locale.class
        c.setProperty(AppConfigurationKey.APP_LANGUAGE, Locale.CANADA_FRENCH.toLanguageTag());
        Assertions.assertEquals(Locale.CANADA_FRENCH, c.getProperty(AppConfigurationKey.APP_LANGUAGE, Locale.class, null));
        Assertions.assertEquals("fr-CA", c.getProperty(AppConfigurationKey.APP_LANGUAGE, String.class, null));
        c.setProperty(AppConfigurationKey.APP_LANGUAGE, Locale.CANADA.toLanguageTag());
        Assertions.assertEquals(Locale.CANADA, c.getProperty(AppConfigurationKey.APP_LANGUAGE, Locale.class, null));
        c.setProperty(AppConfigurationKey.APP_LANGUAGE, Locale.getDefault().toLanguageTag());

        // Path.class
        c.setAppFilesLastUsedFolder(Paths.get(System.getProperty("user.home")));
        Assertions.assertEquals(Paths.get(System.getProperty("user.home")), c.getProperty(AppConfigurationKey.APP_FILES_LAST_USED_FOLDER, Path.class, null));
        Assertions.assertEquals(System.getProperty("user.home"), c.getProperty(AppConfigurationKey.APP_FILES_LAST_USED_FOLDER, String.class, null));

        // Charset.class
        c.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_CHARSET, StandardCharsets.UTF_8.name());
        Assertions.assertEquals(StandardCharsets.UTF_8, c.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_CHARSET, Charset.class, null));
        c.setProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_CHARSET, StandardCharsets.ISO_8859_1.name());
        Assertions.assertEquals(StandardCharsets.ISO_8859_1, c.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_CHARSET, Charset.class, null));
        Assertions.assertEquals("ISO-8859-1", c.getProperty(ToolsConfigurationKey.TOOLS_NETWORK_SENDER_CHARSET, String.class, null));

//        } else if(key.getClassType() == null) {
//            return GsonTool.read(new StringReader(value), clazz);
//        } else {
//            throw new IllegalArgumentException("Class type '" + clazz + "' not supported yed .");
//        }

    }
}
