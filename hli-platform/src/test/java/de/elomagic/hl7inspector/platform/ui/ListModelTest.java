package de.elomagic.hl7inspector.platform.ui;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ListModelTest {

    @Test
    void insertElementAt() {

        ListModel<String> model = new ListModel<>(true);
        model.add("item1");
        model.add("item1");
        Assertions.assertEquals(1, model.size());

        model.add("item2");
        model.add("item3");
        model.add("item3");
        Assertions.assertEquals(3, model.size());

        model.add("item2");
        Assertions.assertEquals(3, model.size());

    }
}