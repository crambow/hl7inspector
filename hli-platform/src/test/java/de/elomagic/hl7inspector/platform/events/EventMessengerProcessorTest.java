package de.elomagic.hl7inspector.platform.events;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EventMessengerProcessorTest {

    EventMessengerProcessor processor = new EventMessengerProcessor(null);

    @Test
    void testProcessorTestBean() throws Exception {

        System.setProperty("application.system.eventingMode", "modern");

        TestBean bean = new TestBean();

        processor.postProcessBeforeInitialization(bean, "testBean");

        processor.publishEvent(new TestEvent(100));

        Assertions.assertEquals(101, bean.getCounter());

        processor.postProcessBeforeDestruction(bean, "testBean");

    }

}