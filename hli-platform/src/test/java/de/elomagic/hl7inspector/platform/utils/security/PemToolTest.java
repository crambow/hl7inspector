package de.elomagic.hl7inspector.platform.utils.security;

import de.elomagic.hl7inspector.platform.TestTool;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.pkcs.PKCSException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.StringWriter;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

class PemToolTest {

    @Test
    void testReadCertificates() throws Exception {
        String pem = TestTool.readResource("/private-key_certificate.pem");

        X509Certificate certificate = PemTool.readCertificates(pem)[0];

        Assertions.assertEquals("CN=testcertificate,L=Karlsruhe,ST=Germany,C=DE", certificate.getIssuerX500Principal().getName());
    }

    @Test
    void testReadPrivateKey() throws Exception {
        String pem = TestTool.readResource("/private-key.pem");

        PrivateKey key = PemTool.readPrivateKey(pem, "changeme".toCharArray());

        Assertions.assertEquals("RSA", key.getAlgorithm());
        Assertions.assertEquals("PKCS#8", key.getFormat());
    }

    @Test
    void testWritePrivateKey() throws Exception {
        KeyPair keyPair = CertificateTool.createRSAKeyPair();

        StringWriter writer = new StringWriter();

        char[] password = "secret".toCharArray();

        PemTool.writePrivateKey(keyPair.getPrivate(), password, writer);

        String pem = StringUtils.trim(writer.toString());

        Assertions.assertTrue(pem.startsWith("-----BEGIN ENCRYPTED PRIVATE KEY-----") && pem.endsWith("-----END ENCRYPTED PRIVATE KEY-----"), pem);

        // Test invalid password
        Assertions.assertThrows(PKCSException.class, () -> PemTool.readPrivateKey(writer.toString(), "wrongPassword".toCharArray()));

        PrivateKey key = PemTool.readPrivateKey(writer.toString(), password);
        Assertions.assertEquals("RSA", key.getAlgorithm());
        Assertions.assertEquals("PKCS#8", key.getFormat());
    }

}