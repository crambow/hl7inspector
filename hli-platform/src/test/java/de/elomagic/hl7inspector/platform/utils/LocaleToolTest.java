/*
 * Copyright 2020-present Carsten Rambow
 *
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/gpl.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector.platform.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

class LocaleToolTest {

    @Test
    void testGetSupportedLocales() {
        Set<Locale> locales = LocaleTool.getSupportedLocales();

        Iterator<Locale> it = locales.iterator();

        Assertions.assertEquals(2, locales.size());
        Assertions.assertEquals(Locale.GERMAN, it.next());
        Assertions.assertEquals(Locale.ENGLISH, it.next());

        System.out.println("Locale:" + Locale.GERMANY.toLanguageTag());
    }

    @Test
    void testGetBundle() {
        Calendar c = Calendar.getInstance();
        c.set(2000, Calendar.OCTOBER, 20, 11, 10, 9);
        Date date = c.getTime();

        String s = LocaleTool.get("app_title", 1, 2, 3, date);

        assertTrue(s.startsWith("1 2 (3 Channel, "));
    }
}
