package de.elomagic.hl7inspector.platform.utils.formatter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

class RegExOverlayFormatterTest {

    @Test
    @Disabled
    void testConstructor() {
        Pattern pattern = Pattern.compile(".*(\\$\\{counter}).*");

        Assertions.assertTrue(pattern.matcher("bjkfgdjfg${counter}jklgdfljkgf").matches());
        Assertions.assertTrue(pattern.matcher("${counter}jklgdfljkgf").matches());
        Assertions.assertTrue(pattern.matcher("bjkfgdjfg${counter}").matches());
        Assertions.assertTrue(pattern.matcher("${counter}").matches());
        Assertions.assertFalse(pattern.matcher("bjkfgdjfgjklgdfljkgf").matches());
        Assertions.assertFalse(pattern.matcher("bjkfgdjfcounter}gjklgdfljkgf").matches());
        Assertions.assertFalse(pattern.matcher("bjkfgdjf{counter}gjklgdfljkgf").matches());
        Assertions.assertFalse(pattern.matcher("bjkfgdjf${countergjklgdfljkgf").matches());
    }

}
