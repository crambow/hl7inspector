package de.elomagic.hl7inspector.platform.utils.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCSException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

class CertificateToolTest {

    Logger LOGGER = LogManager.getLogger(CertificateToolTest.class);

    static final char[] PASSWORD = "TestSecret".toCharArray();
    ExecutorService executor = Executors.newSingleThreadExecutor();

    @Test
    void testGetKeyManagers() throws GeneralSecurityException, IOException, OperatorCreationException, PKCSException {

        String pem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);

        KeyManager[] km = CertificateTool.getKeyManagers(pem, PASSWORD);

        Assertions.assertEquals(1, km.length);

    }

    @Test
    void testGetTrustManagers() throws GeneralSecurityException, IOException, OperatorCreationException, PKCSException {

        String pem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);

        TrustManager[] tm = CertificateTool.getTrustManagers(pem);

        Assertions.assertEquals(1, tm.length);

    }

    @Test
    void testSelfSignedCertificate() throws GeneralSecurityException, IOException, OperatorCreationException, PKCSException {

        String pem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);

        Assertions.assertNotNull(pem);

        X509Certificate[] certificates = PemTool.readCertificates(pem);
        Assertions.assertEquals(1, certificates.length);

        X509Certificate c = certificates[0];
        Assertions.assertTrue(c.getIssuerX500Principal().getName().contains("HL7 Inspector"));

        PrivateKey key = PemTool.readPrivateKey(pem, PASSWORD);
        Assertions.assertEquals("PKCS#8", key.getFormat());
        Assertions.assertEquals("RSA", key.getAlgorithm());

    }

    @Test
    void testCreateSSLContext() throws Exception {

        String serverPem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);
        SSLContext serverContext = CertificateTool.createSSLContext(serverPem, PASSWORD, null);

        // Check TLS positive fail
        ServerSocket server1 = serverContext.getServerSocketFactory().createServerSocket(0);
        final Future<Boolean> future1 = createFuture(server1);
        TimeUnit.SECONDS.sleep(1);
        LOGGER.debug("Server running on port {}", server1.getLocalPort());

        final Socket socket1 = CertificateTool.createSSLContext(null, null, null).getSocketFactory().createSocket("localhost", server1.getLocalPort());
        Assertions.assertThrows(Exception.class, () -> socket1.getOutputStream().write(55));
        //socket.getOutputStream().flush();
        //Assertions.assertThrows(Exception.class, () -> new Socket("localhost", server.getLocalPort()));
        Assertions.assertThrows(Exception.class, () -> future1.get(5, TimeUnit.SECONDS));
        socket1.close();
        server1.close();

        // Test simple TLS connection
        LOGGER.debug("Test simple TLS connection w/o client authentication");
        ServerSocket server2= serverContext.getServerSocketFactory().createServerSocket(0);
        Future<Boolean> future = createFuture(server2);
        TimeUnit.SECONDS.sleep(1);
        LOGGER.debug("Server running on port {}", server2.getLocalPort());

        Socket socket = CertificateTool.createSSLContext(null, null, serverPem).getSocketFactory().createSocket("localhost", server2.getLocalPort());
        socket.getOutputStream().write(55);
        socket.getOutputStream().flush();
        Assertions.assertEquals(66, socket.getInputStream().read());
        Assertions.assertTrue(future.get(5, TimeUnit.SECONDS));
        socket.close();
        server2.close();
    }

    @Test
    void testClientAuthenticationPositiveFail1() throws Exception {
        LOGGER.debug("Test TLS connection with client authentication (Positive fail / No client certificate)");
        String serverPem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);
        String clientPem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);
        SSLServerSocket server3 = (SSLServerSocket)CertificateTool.createSSLContext(serverPem, PASSWORD, clientPem).getServerSocketFactory().createServerSocket(0);
        server3.setNeedClientAuth(true);
        Future<Boolean> future = createFuture(server3);
        TimeUnit.SECONDS.sleep(1);
        LOGGER.debug("Server running on port {}", server3.getLocalPort());

        Socket socket = CertificateTool.createSSLContext(null, null, serverPem).getSocketFactory().createSocket("localhost", server3.getLocalPort());
        //socket.getOutputStream().write(55);
        //socket.getOutputStream().flush();
        Assertions.assertThrows(SSLHandshakeException.class, () -> socket.getInputStream().read());
        Assertions.assertThrows(Exception.class, () -> future.get(5, TimeUnit.SECONDS));
        socket.close();
        server3.close();
    }

    @Test
    void testClientAuthenticationPositiveFail2() throws Exception {
        LOGGER.debug("Test TLS connection with client authentication (Positive fail / Different client certificate)");
        String serverPem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);
        String clientPem1 = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);
        String clientPem2 = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);
        SSLServerSocket server4 = (SSLServerSocket)CertificateTool.createSSLContext(serverPem, PASSWORD, clientPem2).getServerSocketFactory().createServerSocket(0);
        server4.setNeedClientAuth(true);
        Future<Boolean> future = createFuture(server4);
        TimeUnit.SECONDS.sleep(1);
        LOGGER.debug("Server running on port {}", server4.getLocalPort());

        Socket socket = CertificateTool.createSSLContext(clientPem1, PASSWORD, serverPem).getSocketFactory().createSocket("localhost", server4.getLocalPort());
        Assertions.assertThrows(SSLException.class, () -> socket.getOutputStream().write(55));
        //socket.getOutputStream().flush();
        //Assertions.assertThrows(SSLHandshakeException.class, () -> socket.getInputStream().read());
        Assertions.assertThrows(Exception.class, () -> future.get(5, TimeUnit.SECONDS));
        socket.close();
        server4.close();
    }

    @Test
    void testClientAuthentication() throws Exception {
        LOGGER.debug("Test TLS connection with client authentication");
        String serverPem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);
        String clientPem = CertificateTool.createSelfSignedPemEncodedCertificate(PASSWORD);
        SSLServerSocket server4 = (SSLServerSocket)CertificateTool.createSSLContext(serverPem, PASSWORD, clientPem).getServerSocketFactory().createServerSocket(0);
        server4.setNeedClientAuth(true);
        Future<Boolean> future = createFuture(server4);
        TimeUnit.SECONDS.sleep(1);
        LOGGER.debug("Server running on port {}", server4.getLocalPort());

        Socket socket = CertificateTool.createSSLContext(clientPem, PASSWORD, serverPem).getSocketFactory().createSocket("localhost", server4.getLocalPort());
        socket.getOutputStream().write(55);
        socket.getOutputStream().flush();
        Assertions.assertEquals(66, socket.getInputStream().read());
        Assertions.assertTrue(future.get(5, TimeUnit.SECONDS));
        socket.close();
        server4.close();
    }

    private Future<Boolean> createFuture(ServerSocket server) {
        return executor.submit(() -> {
            try {
                LOGGER.debug("Waiting for connection...");
                Socket socket = server.accept();
                LOGGER.debug("Connection established.");

                int b = socket.getInputStream().read();
                socket.getOutputStream().write(66);
                socket.getOutputStream().flush();

                LOGGER.debug("Finishing future...");

                return 55 == b;
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                throw ex;
            }
        });
    }

}