package de.elomagic.hl7inspector.platform.events;

public class TestEvent {

    private final int value;

    public TestEvent(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
