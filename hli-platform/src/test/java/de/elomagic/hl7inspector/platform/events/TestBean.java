package de.elomagic.hl7inspector.platform.events;

import org.springframework.context.event.EventListener;

public class TestBean {

    private int counter;

    public int getCounter() {
        return counter;
    }

    @EventListener(TestEvent.class)
    public void handleEmptyMethodEvent() {
        counter++;
    }

    @EventListener()
    public void handleMethodParameterEvent(TestEvent event) {
        counter += event.getValue();
    }

}
