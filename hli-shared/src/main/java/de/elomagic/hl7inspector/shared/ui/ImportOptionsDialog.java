/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.ui;

import com.alee.laf.combobox.WebComboBox;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.bindings.FormDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions.StreamFormat;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;

import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.nio.charset.Charset;
import java.util.Objects;

@Component
public class ImportOptionsDialog extends FormDialog<ImportOptions> {

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private final MessageFramePanel framePanel = new MessageFramePanel();

    private transient ImportOptions importOption;

    private final JLabel lblSource = new JLabel("Source can be a file or an other streamable source");
    private final JLabel lblFileSize = new JLabel("?");
    private final JCheckBox cbClearBuffer = new JCheckBox(LocaleTool.get("clear_workspace"));
    private final JCheckBox cbReadBottom = new JCheckBox(LocaleTool.get("read_from_bottom"));
    private final JCheckBox cbValidate = new JCheckBox(LocaleTool.get("validate_message"));
    private final WebComboBox cbCharset = UIFactory.createCharsetComboBox();
    private final JRadioButton rbModeAuto = new JRadioButton(new ParseModeAction(LocaleTool.get("auto_detect"), "AUTO"));
    private final JRadioButton rbModeParse = new JRadioButton(new ParseModeAction(LocaleTool.get("line_stream"), "PARSE"));
    private final JRadioButton rbModeStream = new JRadioButton(new ParseModeAction(LocaleTool.get("message_stream"), "FRAMED"));
    private final ButtonGroup btGrpMode = new ButtonGroup();
    private final JCheckBox cbRegExpr = new JCheckBox(LocaleTool.get("use_regular_expression"));
    private final JCheckBox cbNegateReg = new JCheckBox(LocaleTool.get("negate_result"));
    private final JCheckBox cbCaseSense = new JCheckBox(LocaleTool.get("case_sensitive"));
    private JComboBox<String> cbPhrase;

    @PostConstruct
    private void init() {
        getBanner().setVisible(false);

        setTitle(LocaleTool.get("import_options"));
        setModal(true);

        lblSource.setFont(lblSource.getFont().deriveFont(Font.BOLD));

        btGrpMode.add(rbModeAuto);
        btGrpMode.add(rbModeStream);
        btGrpMode.add(rbModeParse);

        rbModeAuto.setSelected(true);

        cbPhrase = new JComboBox<>(new DefaultComboBoxModel<>(Configuration.getInstance().getPhrases().toArray(new String[0])));
        cbPhrase.setSelectedIndex(-1);
        cbPhrase.setEditable(true);

        JPanel panel = FormBuilder.createBuilder()
                .noBorder()
                .addSection("source", 7)
                .addRow("name", lblSource, 7, 1)
                .addRow("size", lblFileSize, 7, 1)
                .addSection("options", 7)
                .add(cbClearBuffer, 2, 0)
                .addRow(cbReadBottom, 4, 0)
                .add("char_encoding", cbCharset, 1, 0)
                .addRow(cbValidate, 3, 1)
                .addSection("parser_mode", 7)
                .addRow(rbModeAuto, 3, 0)
                .addRow(rbModeParse, 3, 0)
                .addRow(rbModeStream, 3, 0)
                .addRow(framePanel, 7, 1)
                .addSection("filter", 7)
                .add(cbCaseSense, 2, 0)
                .add(cbNegateReg, 2, 0)
                .addRow(cbRegExpr, 2, 0)
                .addRow("phrase", cbPhrase, 6, 1)

                .addRowSpacer()
                .build();

        getContentPane().add(panel, BorderLayout.CENTER);

        setSize(800, getPreferredSize().height);
        setLocationRelativeTo(getOwner());
        getButtonPane().requestFocus();
    }

    @Override
    public void bindBean(@NotNull ImportOptions bean) {
        importOption = bean;

        framePanel.bindBean(importOption.getFrame());

        cbCaseSense.setSelected(bean.isCaseSensitive());
        cbClearBuffer.setSelected(bean.isClearBuffer());
        rbModeAuto.setSelected(bean.getImportMode() == StreamFormat.AUTO_DETECT);
        rbModeStream.setSelected(bean.getImportMode() == StreamFormat.FRAMED);
        rbModeParse.setSelected(bean.getImportMode() == StreamFormat.TEXT_LINE);
        cbNegateReg.setSelected(bean.isNegReg());
        cbReadBottom.setSelected(bean.isReadBottom());
        cbValidate.setSelected(bean.isValidate());
        cbCharset.setSelectedItem(bean.getEncoding());
        cbRegExpr.setSelected(bean.isUseRegExpr());

        updateParseModeButtons(bean.getImportMode() != StreamFormat.TEXT_LINE);

        lblSource.setText(bean.getSourceDescription());
        lblSource.setToolTipText(bean.getSourceDescription());

        lblFileSize.setText(bean.getFileSize() == -1 ? "?" : "" + bean.getFileSize());

        cbPhrase.setSelectedItem("");
        cbPhrase.setFocusable(true);

        cbPhrase.requestFocusInWindow();
    }

    @Override
    public void commit() {
        importOption.setCaseSensitive(cbCaseSense.isSelected());
        importOption.setClearBuffer(cbClearBuffer.isSelected());
        importOption.setFileSize(NumberUtils.toInt(lblFileSize.getText(), -1));
        importOption.setImportMode(rbModeParse.isSelected() ? StreamFormat.TEXT_LINE : (rbModeStream.isSelected() ? StreamFormat.FRAMED : StreamFormat.AUTO_DETECT));
        importOption.setNegReg(cbNegateReg.isSelected());
        importOption.setPhrase((cbPhrase.getSelectedItem() == null) ? "" : cbPhrase.getSelectedItem().toString());
        importOption.setReadBottom(cbReadBottom.isSelected());
        importOption.setValidate(cbValidate.isSelected());
        importOption.setEncoding((Charset)cbCharset.getSelectedItem());
        importOption.setUseRegExpr(cbRegExpr.isSelected());

        framePanel.commit();

        if(!Objects.toString(cbPhrase.getSelectedItem(), "").isEmpty()) {
            Configuration.getInstance().putPhrase(cbPhrase.getSelectedItem().toString());
        }

        configuration.setImportOptionsBean(importOption);
    }

    class ParseModeAction extends AbstractAction {
        public ParseModeAction(String name, String cmd) {
            super(name);

            putValue(Action.ACTION_COMMAND_KEY, cmd);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            boolean b = !e.getActionCommand().equals("PARSE");
            updateParseModeButtons(b);
        }
    }

    private void updateParseModeButtons(boolean enabledFrame) {
        framePanel.setEnabled(enabledFrame);
    }
}
