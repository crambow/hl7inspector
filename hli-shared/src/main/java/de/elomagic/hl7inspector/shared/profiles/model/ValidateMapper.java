/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.profiles.model;

import com.google.gson.annotations.JsonAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import de.elomagic.hl7inspector.shared.profiles.model.adapter.ValidatorStatusXmlAdapter;
import de.elomagic.hl7inspector.shared.profiles.model.adapter.ValidatorGsonStatusAdapter;

import org.jetbrains.annotations.NotNull;

@XmlRootElement(name = "validate")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidateMapper {

    @XmlElement(name = "length")
    @XmlJavaTypeAdapter(ValidatorStatusXmlAdapter.class)
    @JsonAdapter(ValidatorGsonStatusAdapter.class)
    private ValidateStatus mapLength = ValidateStatus.WARN;
    @XmlElement(name = "deprecated")
    @XmlJavaTypeAdapter(ValidatorStatusXmlAdapter.class)
    @JsonAdapter(ValidatorGsonStatusAdapter.class)
    private ValidateStatus mapDeprecated = ValidateStatus.INFO;
    @XmlElement(name = "conditional")
    @XmlJavaTypeAdapter(ValidatorStatusXmlAdapter.class)
    @JsonAdapter(ValidatorGsonStatusAdapter.class)
    private ValidateStatus mapConditional = ValidateStatus.INFO;
    @XmlElement(name = "required")
    @XmlJavaTypeAdapter(ValidatorStatusXmlAdapter.class)
    @JsonAdapter(ValidatorGsonStatusAdapter.class)
    private ValidateStatus mapRequired = ValidateStatus.ERROR;
    @XmlElement(name = "item-missing")
    @XmlJavaTypeAdapter(ValidatorStatusXmlAdapter.class)
    @JsonAdapter(ValidatorGsonStatusAdapter.class)
    private ValidateStatus mapItemMissing = ValidateStatus.ERROR;
    @XmlElement(name = "definition-not-found")
    @XmlJavaTypeAdapter(ValidatorStatusXmlAdapter.class)
    @JsonAdapter(ValidatorGsonStatusAdapter.class)
    private ValidateStatus mapDefNotFound = ValidateStatus.WARN;
    @XmlElement(name = "repetition")
    @XmlJavaTypeAdapter(ValidatorStatusXmlAdapter.class)
    @JsonAdapter(ValidatorGsonStatusAdapter.class)
    private ValidateStatus mapRepetition = ValidateStatus.ERROR;
    @XmlElement(name = "invalidFormat")
    @XmlJavaTypeAdapter(ValidatorStatusXmlAdapter.class)
    @JsonAdapter(ValidatorGsonStatusAdapter.class)
    private ValidateStatus invalidFormat = ValidateStatus.ERROR;
    @NotNull
    public ValidateStatus getMapLength() {
        return mapLength;
    }

    public void setMapLength(@NotNull ValidateStatus map) {
        mapLength = map;
    }

    @NotNull
    public ValidateStatus getMapDeprecated() {
        return mapDeprecated;
    }

    public void setMapDeprecated(@NotNull ValidateStatus map) {
        mapDeprecated = map;
    }

    @NotNull
    public ValidateStatus getMapConditional() {
        return mapConditional;
    }

    public void setMapConditional(@NotNull ValidateStatus map) {
        mapConditional = map;
    }

    @NotNull
    public ValidateStatus getMapRequired() {
        return mapRequired;
    }

    public void setMapRequired(@NotNull ValidateStatus map) {
        mapRequired = map;
    }

    @NotNull
    public ValidateStatus getMapItemMiss() {
        return mapItemMissing;
    }

    public void setMapItemMiss(@NotNull ValidateStatus map) {
        mapItemMissing = map;
    }

    @NotNull
    public ValidateStatus getMapDefNotFound() {
        return mapDefNotFound;
    }

    public void setMapDefNotFound(@NotNull ValidateStatus map) {
        mapDefNotFound = map;
    }

    @NotNull
    public ValidateStatus getMapRepetition() {
        return mapRepetition;
    }

    public void setMapRepetition(@NotNull ValidateStatus map) {
        mapRepetition = map;
    }

    @NotNull
    public ValidateStatus getInvalidFormat() {
        return invalidFormat;
    }

    public void setInvalidFormat(@NotNull ValidateStatus invalidFormat) {
        this.invalidFormat = invalidFormat;
    }
}
