/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7.model;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.Objects;
import java.util.UUID;

public class MessageMETA {

    public enum SourceType {
        FILE("file"),
        IP_SOCKET("ip_socket"),
        IN_MEMORY("in_memory");

        private final String key;

        SourceType(String resourceKey) {
            this.key = resourceKey;
        }

        public String toDisplay() {
            return LocaleTool.get(key);
        }
    }

    private final String id = UUID.randomUUID().toString();
    // Each file, independent if it's a in memory message, must a (temporary) file
    private Path file;
    // Each file, independent if it's a in memory message, must a encoding
    private Charset encoding;
    private boolean inMemory;
    private Path sourceFile;
    private SourceType sourceType;
    private boolean changed;
    private FileTime modifiedTime;

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public Path getFile() {
        return file;
    }

    public void setFile(Path file) {
        this.file = file;
    }

    @NotNull
    public Charset getEncoding() {
        return encoding;
    }

    public void setEncoding(Charset encoding) {
        this.encoding = encoding;
    }

    public boolean isInMemory() {
        return inMemory;
    }

    public void setInMemory(boolean inMemory) {
        this.inMemory = inMemory;
    }

    /**
     * Returns the source file where message comes from.
     *
     * Can be differ when message is one of more messages from a log file.
     *
     * @return The source file or null when message received via socket.
     */
    @Nullable
    public Path getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(@Nullable Path sourceFile) {
        this.sourceFile = sourceFile;
    }

    @NotNull
    public SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(@NotNull SourceType sourceType) {
        this.sourceType = sourceType;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    /**
     * Returns last modified time of the file.
     *
     * @return Returns time only when message exists as a file otherwise null
     */
    @Nullable
    public FileTime getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(@Nullable FileTime modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * Check if file exists.
     *
     * @return Returns true when property 'file' is set and file also exists.
     */
    public boolean isFileExists() {
        return file != null && Files.exists(file);
    }

    /**
     * Returns modify status of a message file.
     *
     * @return Returns true if the file was changed in the meanwhile.
     * @throws IOException Thrown {@link IOException} when unable to get modify time from file
     */
    public boolean isFileModified() throws IOException {
        if (file == null || modifiedTime == null) {
            return false;
        }

        return Files.notExists(file) || modifiedTime.compareTo(Files.getLastModifiedTime(file)) != 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageMETA that = (MessageMETA) o;
        return file.equals(that.file);
    }

    @Override
    public int hashCode() {
        return Objects.hash(file);
    }

}
