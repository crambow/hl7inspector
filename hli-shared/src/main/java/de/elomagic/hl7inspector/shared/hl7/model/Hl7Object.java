/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7.model;

import de.elomagic.hl7inspector.platform.utils.StringEscapeUtils;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.profiles.validate.ValidateResult;
import de.elomagic.hl7inspector.shared.ui.HL7TreeNode;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @param <N> Child class type of this class
 */
public abstract class Hl7Object<N extends Hl7Object> implements HL7TreeNode<N>, Iterable<N> {

    private static final Logger LOGGER = LogManager.getLogger(Hl7Object.class);
    private final List<N> objList = new ArrayList<>();
    private Delimiters delimiters = new Delimiters();
    private String validationText = "";
    private String description = "";
    private ValidateResult val = null;
    private String htmlText = null;
    private Message root = null;
    private Hl7Object<?> parent = null;

    public void parse(@NotNull String text, @NotNull Delimiters del) {
        this.delimiters = del;

        parse(text);
    }

    public void parse(@Nullable String text) {
        // FEATURE Dynamic encoding support (Customize encoding)

        clear();

        if (StringUtils.isBlank(text)) {
            return;
        }

        try {
            StringBuilder subText = new StringBuilder();
            int p = 0;

            while(p < text.length()) {
                char c = text.charAt(p);

                if(!(text.startsWith("MSH" + delimiters.getDelimiters()) && (p == 6)) && (c == Delimiters.DEFAULT_ESCAPE_CHAR)) {
                    // Wenn Escapezeichen kommt dann nächstes Zeichen nicht interpretieren
//                    subText = subText.append(c);
//                    p++;
//                    if (text.length() > p) {
//                        subText = subText.append(c);
//                    }

                    // Wenn Escapezeichen kommt dann nächstes Zeichen nicht interpretieren

                    do {
                        subText.append(c);
                        p++;

                        if(text.length() > p) {
                            c = text.charAt(p);
                        }
                    } while((text.length() > p) && (c != Delimiters.DEFAULT_ESCAPE_CHAR));
                    if(c == Delimiters.DEFAULT_ESCAPE_CHAR) {
                        subText.append(c);
                    }
                } else {
                    if(c == getSubDelimiter()) {
                        // Wenn neues Feld dann altes in Array sichern
//            add(subText, not ((subText = (COMPONENT_CHAR + REPEATION_CHAR + ESCAPE_CHAR + SUBCOMPONENT_CHAR)) and (p == 9) and (copy(text, 1, 3) = 'MSH')));

//                        if (subText.eq)

                        if((c == Delimiters.DEFAULT_FIELD) && subText.toString().equals("MSH") && objList.isEmpty()) {
                            add("MSH");
                            add("" + Delimiters.DEFAULT_FIELD);
                            add((N)new EncodingObject());
                            p += 5;
                        } else {
                            add(subText.toString());
                        }

                        subText = new StringBuilder();
                    } else {
                        subText.append(c);
                    }
                }

                p++;
            }

            if(subText.length() != 0) {
                add(subText.toString());
            }
        } catch(Exception ex) {
            LOGGER.error("HL7 message parsing error occured", ex);
        }
    }

    @NotNull
    @Override
    public Iterator<N> iterator() {
        return objList.iterator();
    }

    public void clear() {
        objList.clear();
        description = "";

        resetTreeData(this);
    }

    @NotNull
    public Delimiters getDelimiters() {
        return delimiters;
    }

    private void add(@NotNull N obj) {
        obj.setRoot(getRoot());
        obj.setHL7Parent(this);
        objList.add(obj);
    }

    @NotNull
    private N add(@NotNull String text) throws ReflectiveOperationException {
        Class<N> child = getChildClass();
        if(child == null) {
            throw new IllegalArgumentException("Child items are not allowed for this item type");
        }

        N obj = child.getConstructor().newInstance();
        obj.setRoot(getRoot());
        obj.setHL7Parent(this);
        obj.parse(text, delimiters);
        objList.add(obj);

        return obj;
    }

    @Nullable
    public String getValidationText() {
        return validationText;
    }

    public void setValidationText(@Nullable String value) {
        validationText = value;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description.trim();
    }

    @Nullable
    public ValidateResult getValidateStatus() {
        return val;
    }

    public void setValidateStatus(@Nullable ValidateResult v) {
        val = v;
    }

    @NotNull
    public String toHtmlEscapedString() {
        if(htmlText == null) {
            htmlText = StringEscapeUtils.escapeHtml(asText());
        }

        return htmlText;
    }

    @NotNull
    public String asText() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < objList.size(); i++) {
            if(!(HL7ObjectType.is(this, HL7ObjectType.SEGMENT) && (i == 1) && "MSH".equals(get(0).asText()))) {
                sb.append(objList.get(i).asText());
                sb.append(getSubDelimiter());
            }
        }

        if((sb.length() != 0) && !HL7ObjectType.is(this, HL7ObjectType.MESSAGE)) {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString();
    }

    public boolean isSinglePath() {
        boolean r = objList.isEmpty();
        if(objList.size() == 1) {
            r = objList.get(0).isSinglePath();
        }

        return r;
    }

    /**
     * Returns the parent HL7Model of this node.
     *
     * Keep in mind! This is the HL7 parent and not necessarily identical to the parent of the tree!
     *
     * @see HL7TreeNode#getParent()
     */
    public Hl7Object<?> getHL7Parent() {
        return parent;
    }

    protected void setHL7Parent(Hl7Object<?> parent) {
        this.parent = parent;
    }

    public void setRoot(Message value) {
        root = value;
    }

    @NotNull
    public Message getRoot() {
        return root;
    }

    /**
     * Returns character to delimit sub object.
     *
     * @return A character or 0 when last leaf is reached
     */
    public abstract char getSubDelimiter();

    @Nullable
    public abstract Class<N> getChildClass();

    /**
     * Returns type of this class.
     *
     * Please use it instead of {@link #getClass()}
     *
     * @return Returns type of HL7 object
     */
    @NotNull
    public abstract HL7ObjectType getType();

    public int size() {
        return size(false);
    }

    public int size(boolean compressed) {
        if (compressed) {
            int r = 0;

            for (int i = 0; i < size(); i++) {
                if (!get(i).isEmpty()) {
                    r++;
                }
            }
            return r;
        } else {
            return objList.size();
        }
    }

    @NotNull
    public N get(int index) {
        return get(index, false);
    }

    @NotNull
    public N get(int index, boolean compressed) {
        if (compressed) {
            int idx = -1;
            for (int i = 0; i < size(); i++) {
                if (!get(i).isEmpty()) {
                    idx++;
                }

                if (idx == index) {
                    return objList.get(i);
                }
            }
        } else {
            return objList.get(index);
        }

        throw new IndexOutOfBoundsException();
    }

    /**
     * Returns the absolute index and not the index when compress is enabled
     *
     * @return The index
     */
    public int getIndex() {
        return getHL7Parent() == null ? -1 : getHL7Parent().indexOf(this, false);
    }

    public int indexOf(Hl7Object<?> value, boolean compressed) {
        if (compressed) {
            int idx = indexOf(value, false);
            int r = 0;
            for (int i = 0; i < idx; i++) {
                if (!get(i).isEmpty()) {
                    r++;
                }
            }
            return r;
        } else {
            return objList.indexOf(value);
        }
    }

    public boolean isEmpty() {
        return objList.isEmpty();
    }

    @Nullable
    public HL7Path toPath() {
        String segment = null;
        int segmentIndex = -1;
        Integer field = null;
        Integer repetitionField = null;
        int component = 1;
        int subcomponent = 1;

        Hl7Object<?> o = this;

        do {
            switch(o.getType()) {
                case SEGMENT:
                    segment = o.get(0).asText();
                    segmentIndex = o.getIndex();
                    break;
                case FIELD:
                    repetitionField = o.getIndex() + 1;
                    break;
                case REPETITION_FIELD:
                    field = o.getIndex();
                    break;
                case COMPONENT:
                    component = o.getIndex() + 1;
                    break;
                case SUB_COMPONENT:
                    subcomponent = o.getIndex() + 1;
                    break;
            }

            o = o.getHL7Parent();
        } while (o != null);

        return segment == null ? null : new HL7Path(segment, segmentIndex, field, repetitionField,component, subcomponent);
    }

    protected static void resetTreeData(@Nullable Hl7Object<?> o) {
        while(o != null) {
            o.htmlText = null;
            o.description = "";
            o.val = null;
            o.validationText = "";

            o = o.getHL7Parent();
        }
    }

}
