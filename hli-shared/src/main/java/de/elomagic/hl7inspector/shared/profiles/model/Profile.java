/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.profiles.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.Nullable;

@XmlRootElement(name = "hl7inspector-profile")
@XmlAccessorType(XmlAccessType.NONE)
public class Profile {

    private String description = "";
    private String name = "";
    private List<DataElement> dataElementList = new ArrayList<>();
    private List<SegmentItem> segmentList = new ArrayList<>();
    private List<DataTypeItem> dataTypeList = new ArrayList<>();
    private List<TableItem> tableDataList = new ArrayList<>();
    private ValidateMapper validateMapper = new ValidateMapper();

    private final Map<String, DataElement> dataElementMap = new HashMap<>();
    private final Map<String, DataTypeItem> dataTypeMap = new HashMap<>();
    private final Map<String, SegmentItem> segmentMap = new HashMap<>();
    private final Map<String, TableItem> tableDataMap = new HashMap<>();

    @XmlElement(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElementWrapper(name = "data-elements")
    @XmlElement(name = "data-element")
    public List<DataElement> getDataElementList() {
        return dataElementList;
    }

    public void setDataElementList(List<DataElement> dataElementList) {
        this.dataElementList = dataElementList;
    }

    @XmlElementWrapper(name = "segments")
    @XmlElement(name = "segment")
    public List<SegmentItem> getSegmentList() {
        return segmentList;
    }

    public void setSegmentList(List<SegmentItem> segmentList) {
        this.segmentList = segmentList;
    }

    @XmlElementWrapper(name = "data-types")
    @XmlElement(name = "data-type")
    public List<DataTypeItem> getDataTypeList() {
        return dataTypeList;
    }

    public void setDataTypeList(List<DataTypeItem> dataTypeList) {
        this.dataTypeList = dataTypeList;
    }

    @XmlElementWrapper(name = "table-data")
    @XmlElement(name = "table")
    public List<TableItem> getTableDataList() {
        return tableDataList;
    }

    public void setTableDataList(List<TableItem> tableDataList) {
        this.tableDataList = tableDataList;
    }

    @XmlElement(name = "validate")
    public ValidateMapper getValidateMapper() {
        return validateMapper;
    }

    public void setValidateMapper(ValidateMapper validateMapper) {
        this.validateMapper = validateMapper;
    }

    public void reindex() {
        dataElementMap.clear();
        dataTypeMap.clear();
        segmentMap.clear();
        tableDataMap.clear();
    }

    @Nullable
    public DataElement getDataElement(String segmentType, int indexOfField) {
        if(dataElementMap.size() != dataElementList.size()) {
            dataElementMap.clear();
            dataElementList.forEach(de -> dataElementMap.put(de.getSegment() + "-" + de.getSequence(), de));
        }

        return dataElementMap.get(segmentType + '-' + indexOfField);
    }

    @Nullable
    public DataTypeItem getDataType(String dataType, int index) {
        if(dataTypeMap.size() != dataTypeList.size()) {
            dataTypeMap.clear();
            dataTypeList.forEach(t -> dataTypeMap.put(t.getParentDataType() + '-' + t.getIndex(), t));
        }

        return dataTypeMap.get(dataType + '-' + index);
    }

    @Nullable
    public SegmentItem getSegment(String segmentType) {
        if(segmentMap.size() != segmentList.size()) {
            segmentMap.clear();
            segmentList.forEach(item -> segmentMap.put(item.getId(), item));
        }

        return segmentMap.get(segmentType);
    }

    @Nullable
    public TableItem getTableData(String tableId, String value) {
        if(tableDataMap.size() != tableDataList.size()) {
            tableDataMap.clear();
            tableDataList.forEach(item -> tableDataMap.put(item.getId() + '-' + item.getValue(), item));
        }

        return tableDataMap.get(tableId + '-' + value);
    }

    public boolean containsParentDataType(String dataType) {
        return dataTypeList
                .stream()
                .anyMatch(dt -> dt.getParentDataType().equals(dataType));
    }

    public boolean containsTableData(String tableId) {
        return tableDataList
                .stream()
                .anyMatch(ti -> ti.getId().equals(tableId));
    }

    public List<String> validate() {
        List<String> result = new ArrayList<>();

        // Validate data types
        for(DataTypeItem dt : dataTypeList) {
            if((dt.getIndex() == 0) && !dt.getParentDataType().trim().isEmpty()) {
                result.add(LocaleTool.get("error_in_data_type_definition_index_invalid_must_be_greater_then_0_1arg", dt.getParentDataType()));
            }
            // todo Validation of order missing

            if(dt.getParentDataType().trim().isEmpty() && (dt.getIndex() != 0)) {
                result.add(LocaleTool.get("error_in_data_type_definition_parent_data_type_not_set_1arg", dt.getParentDataTypeName()));
            }

            if(!dt.getTable().trim().isEmpty()) {
                if(!containsTableData(dt.getTable())) {
                    result.add(LocaleTool.get("error_in_data_type_definition_table_definition_not_found_3arg", dt.getParentDataType(), dt.getIndex(), dt.getTable()));
                }
            }

            if(!dt.getDataType().trim().isEmpty()) {
                if(!containsParentDataType(dt.getDataType())) {
                    result.add(LocaleTool.get("error_in_data_type_definition_data_type_definition_not_found_3arg", dt.getParentDataType(), dt.getIndex(), dt.getDataType()));
                }
            }
        }

        // Validate data elements
        for(DataElement de : dataElementList) {
            if(de.getSegment().trim().isEmpty()) {
                result.add(LocaleTool.get("error_in_data_element_definition_segment_not_set_1arg", de.getName()));
            } else {
                if(getSegment(de.getSegment().trim()) == null) {
                    result.add(LocaleTool.get("error_in_data_element_definition_segment_definition_not_found_3arg", de.getSegment(), de.getSequence(), de.getSegment()));
                }
            }

            if(de.getSequence() == 0) {
                result.add(LocaleTool.get("error_in_data_element_definition_sequence_invalid_must_be_greater_then_1arg", de.getSegment()));
            }
            // todo Validation of order missing

            if(de.getItem().isEmpty()) {
                result.add(LocaleTool.get("error_in_data_element_definition_item_id_not_set_1arg", de.getSegment()));
            }

            if(!de.getDataType().trim().isEmpty()) {
                if(!containsParentDataType(de.getDataType())) {
                    result.add(LocaleTool.get("error_in_data_element_definition_data_type_definition_not_found_3arg", de.getSegment(), de.getSequence(), de.getDataType()));
                }
            } else {
                result.add(LocaleTool.get("error_in_data_element_definition_data_type_not_set_2arg", de.getSegment(), de.getSequence()));
            }

            if(!de.getTable().trim().isEmpty()) {
                if(!containsTableData(de.getTable())) {
                    result.add(LocaleTool.get("error_in_data_element_definition_table_definition_not_found_3arg", de.getSegment(), de.getSequence(), de.getTable()));
                }
            }

        }

        return result;
    }
}
