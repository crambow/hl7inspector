/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.profiles.validate;

import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.profiles.model.ValidateStatus;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

public class ValidateResult {

    private final ValidateStatus status;
    private HL7Path path;
    private String text = "";

    public ValidateResult(@NotNull ValidateStatus status) {
        this.status = status;
    }

    public ValidateResult(@NotNull ValidateStatus status, HL7Path path, @Nls String text) {
        this.status = status;
        this.path = path;
        this.text = text;
    }

    @NotNull
    public ValidateStatus getStatus() {
        return status;
    }

    public HL7Path getPath() {
        return path;
    }

    public String getText() {
        return text;
    }

}
