/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.ui;

import com.alee.managers.style.StyleId;
import com.alee.managers.style.StyleManager;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.actions.ClearAllAction;
import de.elomagic.hl7inspector.platform.ui.actions.SaveFileAsAction;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import java.awt.Component;

public class MonitorToolBar extends JToolBar {

    public MonitorToolBar(CharacterMonitorTabPanel dialog) {
        super();

        setOrientation(SwingConstants.VERTICAL);
        setFloatable(false);
        setRollover(true);
        StyleManager.setStyleId(this, StyleId.toolbarUndecorated);

        add(UIFactory.createFlatIconButton(JButton.class, new ClearAllAction(e -> dialog.clear())));
        add(UIFactory.createFlatIconButton(JButton.class, new SaveFileAsAction(e -> dialog.saveLog())));
    }

    @Override
    public Component add(Component comp) {
        if (AbstractButton.class.isAssignableFrom(comp.getClass())) {
            AbstractButton button = (AbstractButton) comp;
            button.setText(null);
        }

        return super.add(comp);
    }

}
