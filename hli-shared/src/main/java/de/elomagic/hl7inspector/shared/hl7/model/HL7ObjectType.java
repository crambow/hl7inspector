/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7.model;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum HL7ObjectType {

    MESSAGE("message"),
    SEGMENT("segment"),
    FIELD("field"),
    REPETITION_FIELD("repetition-field"),
    COMPONENT("component"),
    SUB_COMPONENT("subcomponent");

    private final String resourceKey;

    HL7ObjectType(@Nls String resourceKey) {

        this.resourceKey = resourceKey;

    }

    public String getResourceKey() {
        return resourceKey;
    }

    @Nullable
    public static HL7ObjectType get(@Nullable Object object) {
        if (!(object instanceof Hl7Object)) {
            return null;
        }

        return ((Hl7Object<?>)object).getType();
    }

    public static boolean is(@Nullable Object object, @NotNull HL7ObjectType type) {
        if (!(object instanceof Hl7Object)) {
            return false;
        }

        return ((Hl7Object<?>)object).getType() == type;
    }
}
