/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.ui;

import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinder;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;

import java.awt.FlowLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

public class MessageFramePanel extends JPanel implements FormBinder<Frame> {

    private final JComboBox<String> cbbStartChar = new JComboBox<>();
    private final JComboBox<String> cbbStopChar1 = new JComboBox<>();
    private final JComboBox<String> cbbStopChar2 = new JComboBox<>();

    private Frame frame;

    public MessageFramePanel() {
        initUI();
    }

    private void initUI() {
        String[] model = {
            "0x00 - NUL",
            "0x01 - SOH",
            "0x02 - STX",
            "0x03 - ETX",
            "0x04 - EOT",
            "0x05 - ENQ",
            "0x06 - ACK",
            "0x07 - BEL",
            "0x08 - BS",
            "0x09 - HT",
            "0x0a - LF",
            "0x0b - VT",
            "0x0c - FF",
            "0x0d - CR",
            "0x0e - SO",
            "0x0f - SI",
            "0x10 - DLE",
            "0x11 - DC1",
            "0x12 - DC2",
            "0x13 - DC3",
            "0x14 - DC4",
            "0x15 - NAK",
            "0x16 - SYN",
            "0x17 - ETB",
            "0x18 - CAN",
            "0x19 - EM",
            "0x1a - SUB",
            "0x1b - ESC",
            "0x1c - FS",
            "0x1d - GS",
            "0x1e - RS",
            "0x1f - US",
            "-"
        };

        cbbStartChar.setModel(new DefaultComboBoxModel<>(model));
        cbbStopChar1.setModel(new DefaultComboBoxModel<>(model));
        cbbStopChar2.setModel(new DefaultComboBoxModel<>(model));

        setLayout(new FlowLayout(FlowLayout.LEFT));

        add(UIFactory.createLabel(LocaleTool.get("start_char_label")));
        add(cbbStartChar);

        add(UIFactory.createLabel(LocaleTool.get("stop_char1_label")));
        add(cbbStopChar1);

        add(UIFactory.createLabel(LocaleTool.get("stop_char2_label")));
        add(cbbStopChar2);
    }

    @NotNull
    public Frame getMessageFrame() {
        byte startByte = cbbStartChar.getSelectedIndex() < 32 ? (byte) cbbStartChar.getSelectedIndex() : Frame.DEFAULT_START;
        byte stopByte1 = cbbStopChar1.getSelectedIndex() < 32 ? (byte) cbbStopChar1.getSelectedIndex() : Frame.DEFAULT_STOP1;
        Byte stopByte2 = cbbStopChar2.getSelectedIndex() < 32 ? (byte) cbbStopChar2.getSelectedIndex() : Frame.DEFAULT_STOP2;

        return new Frame(startByte, stopByte1, stopByte2);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        cbbStartChar.setEnabled(enabled);
        cbbStopChar1.setEnabled(enabled);
        cbbStopChar2.setEnabled(enabled);
    }

    @Override
    public void bindBean(@NotNull Frame frame) {
        this.frame = frame;

        cbbStartChar.setSelectedIndex(frame.getStartByte());
        cbbStopChar1.setSelectedIndex(frame.getStopBytesLength() == 0 ? 32 : frame.getStopBytes()[0]);
        cbbStopChar2.setSelectedIndex(frame.getStopBytesLength() < 2 ? 32 : frame.getStopBytes()[1]);
    }

    @Override
    public void commit() {
        frame.setStartByte(cbbStartChar.getSelectedIndex() < 32 ? (byte)cbbStartChar.getSelectedIndex() : Frame.DEFAULT_START);

        byte stopByte1 = cbbStopChar1.getSelectedIndex() < 32 ? (byte) cbbStopChar1.getSelectedIndex() : Frame.DEFAULT_STOP1;
        Byte stopByte2 = cbbStopChar2.getSelectedIndex() < 32 ? (byte) cbbStopChar2.getSelectedIndex() : Frame.DEFAULT_STOP2;
        frame.setStopBytes(stopByte1, stopByte2);
    }
}
