/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7.model;

import org.jetbrains.annotations.NotNull;

public class Delimiters {

    /**
     * Default segment separator: 0x0d
     */
    public static final Character DEFAULT_SEGMENT_SEPARATOR = 0x0d;

    public static final char DEFAULT_REPETITION = '~';
    public static final char DEFAULT_FIELD = '|';
    public static final char DEFAULT_COMPONENT = '^';
    public static final char DEFAULT_ESCAPE_CHAR = '\\';
    public static final char DEFAULT_SUPCOMPONENT = '&';

    public char repetitionDelimiter = '~';
    public char fieldDelimiter = '|';
    public char componentDelimiter = '^';
    public char escapeCharacter = '\\';
    public char subcomponentDelimiter = '&';

    public Delimiters() {
    }

    public Delimiters(@NotNull String value) {
        if(value.length() < 5) {
            throw new IllegalArgumentException("Delimiter string must at least 5 character long");
        }

        fieldDelimiter = value.charAt(0);
        componentDelimiter = value.charAt(1);
        repetitionDelimiter = value.charAt(2);
        escapeCharacter = value.charAt(3);
        subcomponentDelimiter = value.charAt(4);
    }

    /**
     * Returns delimiters as a HL7 valid string in correct order.
     *
     * @return A string
     */
    @NotNull
    public String getDelimiters() {
        return "" + fieldDelimiter + componentDelimiter + repetitionDelimiter + escapeCharacter + subcomponentDelimiter;
    }

    /**
     * Returns delimiters as a string.
     *
     * Use this method ONLY for debug output. Use {@link Delimiters#getDelimiters()} instead.
     *
     * @see Delimiters#toString()
     *
     * @return A string
     */
    @Override
    public String toString() {
        return getDelimiters();
    }
}
