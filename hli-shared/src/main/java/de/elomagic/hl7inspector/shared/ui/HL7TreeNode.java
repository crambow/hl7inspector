/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.ui;

import com.alee.api.annotations.Nullable;

import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.HL7ObjectType;

import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public interface HL7TreeNode<N extends Hl7Object> extends TreeNode {

    /**
     * Returns the child TreeNode at index childIndex.
     */
    default N getChildAt(int childIndex) {
        if(HL7ObjectType.is(this, HL7ObjectType.SEGMENT)) {
            childIndex++; // Filter segment from fields
        }

        N result = get(childIndex, "t".equals(System.getProperty(COMPRESSED_KEY, "f")));

        if(HL7ObjectType.is(result, HL7ObjectType.REPETITION_FIELD) && (result.getChildCount() == 1)) {
            result = (N)result.getChildAt(0);
        }

        return result;
    }

    /**
     * Returns the number of children TreeNodes of this node.
     */
    default int getChildCount() {
        int result = size("t".equals(System.getProperty(COMPRESSED_KEY, "f")));

        if(HL7ObjectType.is(this, HL7ObjectType.SEGMENT) && (result > 0)) {
            result--;
        }

        return isSinglePath() && !HL7ObjectType.is(this, HL7ObjectType.REPETITION_FIELD) ? 0 : result;
    }

    /**
     * Returns the parent TreeNode of this tree node.
     *
     * Keep in mind! This is the tree parent and not necessarily identical to the parent of the HL7 model!
     *
     * @see Hl7Object#getHL7Parent()
     */
    default HL7TreeNode<?> getParent() {
        HL7TreeNode<?> result = getHL7Parent();

        if(HL7ObjectType.is(result, HL7ObjectType.REPETITION_FIELD) && (result.size(false) == 1)) {
            result = result.getParent();
        }

        return result;
    }

    /**
     * Returns the index of node in children nodes.
     */
    default int getIndex(TreeNode node) {
        for(int i = 0; i < getChildCount(); i++) {
            if(getChildAt(i).equals(node)) {
                return i;
            }
        }

        return -1;
    }

    default boolean getAllowsChildren() {
        return getChildClass() != null;
    }

    /**
     * Returns true if the receiver is a leaf.
     */
    default boolean isLeaf() {
        return !isSinglePath();
    }

    /**
     * Returns the children of the receiver as an Enumeration.
     */
    default Enumeration<? extends TreeNode> children() {
        List<TreeNode> result = new ArrayList<>();

        for(int i = 0; i < getChildCount(); i++) {
            result.add(getChildAt(i));
        }

        return Collections.enumeration(result);
    }

    // Following Must be implemented by subclass

    String COMPRESSED_KEY = Hl7Object.class.getName().concat(".compressed");

    Class<N> getChildClass();

    /**
     * Returns the HL7 logical parent and not of the tree model.
     *
     * @see HL7TreeNode#getParent()
     *
     * @return The parent. Is null when this instance of type Message
     */
    @Nullable
    Hl7Object<?> getHL7Parent();

    int size(boolean compressed);

    boolean isSinglePath();

    N get(int index, boolean compressed);

}
