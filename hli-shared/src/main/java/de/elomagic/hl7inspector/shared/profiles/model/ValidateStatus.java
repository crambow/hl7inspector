/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.profiles.model;

import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import javax.swing.ImageIcon;
import java.util.Arrays;

public enum ValidateStatus {

    OK(0),
    INFO(1),
    WARN(2),
    ERROR(3);

    private final int status;

    ValidateStatus(int status) {
        this.status = status;
    }

    public static ValidateStatus valueOf(Integer value) {
        return Arrays.stream(values())
                .filter(vr -> vr.status == value)
                .findFirst()
                .orElse(OK);
    }

    public int getStatus() {
        return status;
    }

    public ImageIcon getIcon() {
        ImageIcon icon;

        switch(this) {
            case OK:
                icon = IconThemeManager.getImageIcon("check.png");
                break;
            case INFO:
                icon = IconThemeManager.getImageIcon("info.png");
                break;
            case WARN:
                icon = IconThemeManager.getImageIcon("warning.png");
                break;
            case ERROR:
                icon = IconThemeManager.getImageIcon("error.png");
                break;
            default:
                icon = IconThemeManager.getImageIcon("hole_white.gif");
                break;
        }

        return icon;
    }

    @Override
    public String toString() {
        String s;

        switch(this) {
            case INFO:
                s = LocaleTool.get("info");
                break;
            case WARN:
                s = LocaleTool.get("warning");
                break;
            case ERROR:
                s = LocaleTool.get("error");
                break;
            default:
                s = LocaleTool.get("ok");
                break;
        }

        return s;
    }

}
