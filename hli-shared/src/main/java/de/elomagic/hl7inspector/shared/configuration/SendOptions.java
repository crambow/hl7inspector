/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.configuration;

import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

public class SendOptions implements Serializable {

    // 0 = Selected messages, 1 = Messages from folder
    private int sourceMode = 0;
    private transient Path sourceFolder;
    private Frame frame = new Frame();
    private String host = "localhost";
    private int port = 2100;
    private transient Charset encoding = StandardCharsets.ISO_8859_1;
    private boolean keepSocket = true;
    private final transient TlsSendOptions tlsOptions = new TlsSendOptions();

    public int getSourceMode() {
        return sourceMode;
    }

    public void setSourceMode(int sourceMode) {
        this.sourceMode = sourceMode;
    }

    @Nullable
    public Path getSourceFolder() {
        return sourceFolder;
    }

    public void setSourceFolder(@Nullable Path sourceFolder) {
        this.sourceFolder = sourceFolder;
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Charset getEncoding() {
        return encoding;
    }

    public void setEncoding(Charset encoding) {
        this.encoding = encoding;
    }

    public boolean isKeepSocket() {
        return keepSocket;
    }

    public void setKeepSocket(boolean keepSocket) {
        this.keepSocket = keepSocket;
    }

    public TlsSendOptions getTlsOptions() {
        return tlsOptions;
    }

}
