/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7;

import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HL7Encoder {

    private HL7Encoder() {
    }

    public static String encodeString(@Nullable String value, @NotNull Delimiters delimiters) {
        if (StringUtils.isEmpty(value)) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            if(c == delimiters.componentDelimiter) {
                sb.append(delimiters.escapeCharacter);
                sb.append('S');
                sb.append(delimiters.escapeCharacter);
            } else if(c == delimiters.escapeCharacter) {
                sb.append(delimiters.escapeCharacter);
                sb.append('E');
                sb.append(delimiters.escapeCharacter);
            } else if(c == delimiters.fieldDelimiter) {
                sb.append(delimiters.escapeCharacter);
                sb.append('F');
                sb.append(delimiters.escapeCharacter);
            } else if(c == delimiters.repetitionDelimiter) {
                sb.append(delimiters.escapeCharacter);
                sb.append('R');
                sb.append(delimiters.escapeCharacter);
            } else if(c == delimiters.subcomponentDelimiter) {
                sb.append(delimiters.escapeCharacter);
                sb.append('T');
                sb.append(delimiters.escapeCharacter);
            } else if(c == 13) {
                sb.append(".br");
            } else if(c < 32) {
                String v = String.format("%02X", (int)c);

                sb.append(delimiters.escapeCharacter);
                sb.append('X');
                sb.append(v);
                sb.append(delimiters.escapeCharacter);
//            no ascii } else if (c > 63) {
//            no 8bit } else if (c > 255) {
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }
}