/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.configuration;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.Nullable;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

public class ImportOptions {

    public enum StreamFormat {
        AUTO_DETECT, FRAMED, TEXT_LINE
    }

    private boolean readBottom = false;
    private boolean useRegExpr = false;
    private boolean negReg = false;
    private String phrase = "";
    private StreamFormat importMode = StreamFormat.AUTO_DETECT;
    private Frame frame = new Frame();
    private boolean clearBuffer = false;
    private Path sourceFile;
    private String sourceDescription = LocaleTool.get("name");
    private long fileSize = -1;
    private boolean caseSensitive = false;
    private Charset charset = StandardCharsets.ISO_8859_1;
    private boolean validate = false;

    public boolean isReadBottom() {
        return readBottom;
    }

    public void setReadBottom(boolean readBottom) {
        this.readBottom = readBottom;
    }

    public boolean isUseRegExpr() {
        return useRegExpr;
    }

    public void setUseRegExpr(boolean useRegExpr) {
        this.useRegExpr = useRegExpr;
    }

    public boolean isNegReg() {
        return negReg;
    }

    public void setNegReg(boolean negReg) {
        this.negReg = negReg;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public StreamFormat getImportMode() {
        return importMode;
    }

    public void setImportMode(StreamFormat importMode) {
        this.importMode = importMode;
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public boolean isClearBuffer() {
        return clearBuffer;
    }

    public void setClearBuffer(boolean clearBuffer) {
        this.clearBuffer = clearBuffer;
    }

    /**
     * Returns the source file of the message(s).
     *
     * @return The source file or null when message from clipboard or text was dropped
     */
    @Nullable
    public Path getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(@Nullable Path sourceFile) {
        this.sourceFile = sourceFile;
    }

    public String getSourceDescription() {
        return sourceDescription;
    }

    public void setSourceDescription(String sourceDescription) {
        this.sourceDescription = sourceDescription;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public Charset getEncoding() {
        return charset;
    }

    /**
     * Supported Encoders: US-ASCII Seven-bit ASCII, a.k.a. ISO646-US, a.k.a. the Basic Latin block of the Unicode character set ISO-8859-1 ISO Latin Alphabet No. 1, a.k.a. ISO-LATIN-1 UTF-8 Eight-bit
     * UCS Transformation Format UTF-16BE Sixteen-bit UCS Transformation Format, big-endian byte order UTF-16LE Sixteen-bit UCS Transformation Format, little-endian byte order UTF-16
     */
    public void setEncoding(Charset charset) {
        this.charset = charset;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}
