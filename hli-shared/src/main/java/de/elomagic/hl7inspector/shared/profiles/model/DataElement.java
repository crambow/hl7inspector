/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.profiles.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data-element")
@XmlAccessorType(XmlAccessType.NONE)
public final class DataElement {

    private String name = "";
    private String item = "";
    private String segment = "";
    private int seq = 0;
    private String chapter = "";
    private int len = 0;
    private String dataType = "";
    private String repeat = "N";
    private String table = "";

    public DataElement() {
    }

    //Name	Item#	Seg	Seq#	Chp	Len	DT	Rep	Qty	Table
    public DataElement(String id, String dataType, String name, int len, String table) {
        this.item = id;
        this.dataType = dataType;
        this.name = name;
        this.len = len;
        setTable(table);
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "item")
    public String getItem() {
        return item;
    }

    public void setItem(String id) {
        try {
            this.item = Integer.toString(Integer.parseInt(id));
        } catch(Exception e) {
            this.item = id;
        }
    }

    @XmlElement(name = "segment")
    public String getSegment() {
        return segment;
    }

    public void setSegment(String value) {
        this.segment = value.trim();
    }

    @XmlElement(name = "sequence")
    public int getSequence() {
        return seq;
    }

    public void setSequence(int s) {
        this.seq = s;
    }

    @XmlElement(name = "chapter")
    public String getChapter() {
        return chapter;
    }

    public void setChapter(String value) {
        this.chapter = value;
    }

    @XmlElement(name = "length")
    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    @XmlElement(name = "data-type")
    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType.trim();
    }

    @XmlElement(name = "repeatable")
    public String getRepeatable() {
        return repeat;
    }

    public void setRepeatable(String value) {
        this.repeat = value.toUpperCase();

        if(repeat.startsWith("Y")) {
            repeat = "Y";
        } else if(repeat.startsWith("N")) {
            repeat = "N";
        }
    }

    @XmlElement(name = "table")
    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        try {
            this.table = Integer.toString(Integer.parseInt(table));
        } catch(Exception e) {
            this.table = table.trim();
        }
    }

    public int getRepeatableCount() {
        int c;

        try {
            c = Integer.parseInt(repeat);
        } catch(Exception e) {
            c = (repeat.startsWith("Y")) ? Integer.MAX_VALUE : 1;
        }

        return c;
    }
}
