/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.desktop;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.pf4j.ExtensionPoint;

public interface SettingsUIDescriptor extends ExtensionPoint {

    abstract class AbstractSettingsPanel extends AbstractBannerPanel<Configuration> implements ExtensionPoint {

        public void bindBean(@NotNull Configuration bean) {
            load();
        }

        protected abstract void load();

    }

    @Nullable
    default Class<? extends AbstractSettingsPanel> getComponentClass() {
        return null;
    }

}
