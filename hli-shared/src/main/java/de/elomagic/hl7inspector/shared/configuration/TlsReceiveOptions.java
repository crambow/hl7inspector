/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.configuration;

import org.jetbrains.annotations.Nullable;

public class TlsReceiveOptions {

    private boolean enabled;
    private String privateKey;
    private char[] password;

    private boolean clientAuthenticationEnabled;
    private String clientAuthenticationCertificates;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    @Nullable
    public char[] getPassword() {
        return password;
    }

    public void setPassword(@Nullable char[] password) {
        this.password = password;
    }

    public boolean isClientAuthenticationEnabled() {
        return clientAuthenticationEnabled;
    }

    public void setClientAuthenticationEnabled(boolean clientAuthenticationEnabled) {
        this.clientAuthenticationEnabled = clientAuthenticationEnabled;
    }

    @Nullable
    public String getClientAuthenticationCertificates() {
        return clientAuthenticationCertificates;
    }

    public void setClientAuthenticationCertificates(@Nullable String clientAuthenticationCertificates) {
        this.clientAuthenticationCertificates = clientAuthenticationCertificates;
    }

}
