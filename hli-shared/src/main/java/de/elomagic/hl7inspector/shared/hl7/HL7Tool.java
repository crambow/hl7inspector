/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7;

import de.elomagic.hl7inspector.platform.utils.TextSelection;
import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.Segment;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class HL7Tool {

    // Line feed = "\n" = LF = 0x0a
    private static final Character ASCII_LF = 0x0a;
    // Carriage return  = "\r" = CR = 0x0d
    private static final Character ASCII_CR = 0x0d;

    private HL7Tool() {
    }

    /**
     * Exchanhge invalid segment separator by a default segment separator.
     *
     * @param message Message to normalize.
     * @return Returns the normalize message or null when parameter was also null
     */
    @Nullable
    public static String normalizeSegmentSeparator(@Nullable String message) {
        if (message == null) {
            return null;
        }

        String result = message.replace("" + ASCII_LF + ASCII_CR, "" + Delimiters.DEFAULT_SEGMENT_SEPARATOR);
        result = result.replace("" + ASCII_CR + ASCII_LF, "" + Delimiters.DEFAULT_SEGMENT_SEPARATOR);
        result = result.replace(ASCII_LF.toString(), "" + Delimiters.DEFAULT_SEGMENT_SEPARATOR);

        return result;
    }

    /**
     * Translate absolute position into a X/Y coordination system.
     *
     * @param message A HL7 message
     * @param cursorPosition Cursor position: Row = Y: Starts from 0, Column = X: Start also from 0
     * @return Returns a HL7 path or null
     */
    @Nullable
    public static HL7Path getHL7Path(@NotNull String message, @NotNull Point cursorPosition) {
        if (!StringUtils.startsWith(message, "MSH")) {
            return null;
        }

        String[] segments = message.split("\n");
        if (segments.length <= cursorPosition.y) {
            return null;
        }

        String segment = segments[cursorPosition.y];
        if (segment.length() < 8) {
            return null;
        }

        String segmentType = segment.substring(0, 3);
        if (StringUtils.isBlank(segmentType)) {
            return null;
        }

        Delimiters delimiters = new Delimiters(message.substring(3, 8));
        String segmentPart = segment.substring(0, cursorPosition.x);

        int fieldIndex = StringUtils.splitPreserveAllTokens(segmentPart, delimiters.fieldDelimiter).length - 1;
        if (segment.startsWith("MSH")) {
            if (cursorPosition.x == 3) {
                return new HL7Path("MSH", 0, 1);
            } else if (fieldIndex == 1) {
                return new HL7Path("MSH", 0, 2);
            } else if (fieldIndex > 1) {
                fieldIndex++;
            }
        }

        if (fieldIndex <= 0) {
            return new HL7Path(segmentType, cursorPosition.y);
        }

        String fieldPart = segmentPart.substring(segmentPart.lastIndexOf(delimiters.fieldDelimiter) + 1, cursorPosition.x);
        int repIndex = Math.max(1, StringUtils.splitPreserveAllTokens(fieldPart, delimiters.repetitionDelimiter).length);
        String repPart = fieldPart.substring(fieldPart.lastIndexOf(delimiters.repetitionDelimiter) + 1);
        int compIndex = Math.max(1, StringUtils.splitPreserveAllTokens(repPart, delimiters.componentDelimiter).length);
        String compPart = fieldPart.substring(repPart.lastIndexOf(delimiters.componentDelimiter) + 1);
        int subCompIndex = Math.max(1, StringUtils.splitPreserveAllTokens(compPart, delimiters.subcomponentDelimiter).length);

//        if (repIndex == 0) {
//            return segmentType;
//        }

//        String result = segmentType + "-" + fieldIndex;

//        if (repIndex > 1) {
//            result += "(" + repIndex + ")";
//        }

        //result += "." + compIndex + "." + subCompIndex;
        return new HL7Path(segmentType, cursorPosition.y, fieldIndex, repIndex, compIndex, subCompIndex);
    }

    /**
     * Translate a HL7 path into text selection.
     *
     * @param message A text based HL7 message
     * @param path A HL7 path but never null
     */
    @NotNull
    public static TextSelection getTextSelection(@Nullable String message, @Nullable HL7Path path) {
        if (StringUtils.isBlank(message) || path == null) {
            return new TextSelection(-1, -1);
        }

        Delimiters delimiters = getDelimiters(message, new Delimiters());

        int start = 0;
        int end = Integer.MAX_VALUE;

        String[] segments = message.split("\n");
        String segment = "";
        int segmentIndex = 0;
        for (String s : segments) {
            if (path.getSegmentIndex() == segmentIndex) {
                end = start + s.length();
                segment = s;
                break;
            }
            start += s.length() + 1;
            segmentIndex++;
        }

        if (path.getField() == null) {
            return new TextSelection(start, end-start);
        }

        // Field
        StringBuilder sb = new StringBuilder("\\").append(delimiters.fieldDelimiter);
        String[] fields = StringUtils.splitPreserveAllTokens(segment, sb.toString());
        String field = "";
        for (int i=0; i<fields.length; i++) {
            field = fields[i];
            end = start + field.length();

            if (i == path.getField()) {
                break;
            }

            start += field.length();
            start += 1; // delimiter char
        }

        if (path.getRepetitionField() == null) {
            return new TextSelection(start, end-start);
        }

        // RepField
        sb = new StringBuilder("\\").append(delimiters.repetitionDelimiter);
        String[] reps = StringUtils.splitPreserveAllTokens(field, sb.toString());
        String rep = "";
        for (int i=0; i<reps.length; i++) {
            rep = reps[i];
            end = start + rep.length();

            if (i == path.getRepetitionField()-1) {
                break;
            }

            start += rep.length();
            start += 1; // delimiter char
        }

        if (path.getComponent() == null) {
            return new TextSelection(start, end-start);
        }

        // Comp
        sb = new StringBuilder("\\").append(delimiters.componentDelimiter);
        String[] comps = StringUtils.splitPreserveAllTokens(rep, sb.toString());
        String comp = "";
        for (int i=0; i<comps.length; i++) {
            comp = comps[i];
            end = start + comp.length();

            if (i == path.getComponent()-1) {
                break;
            }

            start += comp.length();
            start += 1; // delimiter char
        }

        if (path.getSubcomponent() == null) {
            return new TextSelection(start, end-start);
        }

        // SubComps
        sb = new StringBuilder("\\").append(delimiters.subcomponentDelimiter);
        String[] subs = StringUtils.splitPreserveAllTokens(comp, sb.toString());
        String sub = "";
        for (int i=0; i<subs.length; i++) {
            sub = subs[i];
            end = start + sub.length();

            if (i == path.getSubcomponent()-1) {
                break;
            }

            start += sub.length();
            start += 1; // delimiter char
        }

        return new TextSelection(start, end-start);
    }

    /**
     * MSH|^~\&|ADT-HIS||HL7IN
     *
     * @param message The HL7 message
     * @param defaultDelimiters Fallback delimiters
     * @return Returns the delimiters of the message or null when no delimiters was found.
     */
    public static Delimiters getDelimiters(@Nullable String message, @Nullable Delimiters defaultDelimiters) {
        if (StringUtils.startsWith(message, "MSH|") && StringUtils.length(message) > 8) {
            return new Delimiters(message.substring(3));
        } else {
            return defaultDelimiters;
        }
    }

    /**
     * Returns the last <b>available</b> {@link Hl7Object} (leaf/node) of the {@link Message} independent if parameter {@link HL7Path} has a more detailed path.
     *
     * @param message HL7 message
     * @param path Path of the HL7 object
     * @return Return the HL7 object or null when not exists in the message tree
     */
    @Nullable
    public static Hl7Object<?> toHL7Object(@Nullable Message message, @Nullable HL7Path path) {
        TreePath p = toTreePath(message, path);

        if (p == null) {
            return null;
        }

        return (Hl7Object<?>)p.getLastPathComponent();
    }

    /**
     * Returns {@link TreePath} with the last <b>available</b> leaf/node of the message independent of parameter {@link HL7Path} has detailed path.
     *
     * @param tn HL7 object
     * @return Return the path or null when not exists in the tree
     */
    @Nullable
    public static TreePath toTreePath(@Nullable Hl7Object<?> tn) {
        if (tn == null) {
            return null;
        }

        TreeNode treeNode = tn;

        List<Object> nodes = new ArrayList<>();
        do {
            nodes.add(treeNode);
        } while ((treeNode = treeNode.getParent()) != null);

        Collections.reverse(nodes);

        return nodes.isEmpty() ? null : new TreePath(nodes.toArray());
    }

    /**
     * Returns a tree path of the given HL7 object.
     * TODO Must merged with method above
     * @param o Hl7Object
     * @return TreePath
     */
    @NotNull
    public static TreePath createTreePath(@NotNull final Hl7Object<?> o) {
        Hl7Object<?> child = o;
        List<Object> path = new ArrayList<>();
        while(child.getHL7Parent() != null) {
            path.add(child.getHL7Parent());
            child = child.getHL7Parent();
        }

        Collections.reverse(path);

        return new TreePath(path.toArray());
    }

    /**
     * Returns a {@link TreePath} with the last <b>existing</b> {@link Hl7Object} (leaf/node) of the message independent of parameter {@link HL7Path} has detailed path.
     *
     * @param message The HL7 message
     * @param path HL7 path
     * @return Returns at least the message path or null when message or path is null
     */
    @Nullable
    public static TreePath toTreePath(@Nullable Message message, @Nullable HL7Path path) {
        if (path == null || message == null) {
            return null;
        }

        TreePath result = new TreePath(message);

        Segment segment = message.get(path.getSegmentIndex());
        result = result.pathByAddingChild(segment);

        // Field
        if ((path.getField() == null) || (path.getField() == 0)) {
            return result;
        }
        Hl7Object<?> fieldOrRep = segment.get(path.getField());
        if (fieldOrRep.size() > 1) {
            result = result.pathByAddingChild(fieldOrRep);
        }

        // Rep Field
        if ((path.getRepetitionField() == null) || (path.getRepetitionField() == 0) || (fieldOrRep.isEmpty())) {
            return result;
        }

        Hl7Object<?> rep = fieldOrRep.get(path.getRepetitionField()-1);
        result = result.pathByAddingChild(rep);

        // Component
        if ((path.getComponent() == null) || (path.getComponent() == 0) || (rep.size() < 2)) {
            return result;
        }

        Hl7Object<?> comp = rep.get(path.getComponent()-1);
        result = result.pathByAddingChild(comp);

        // Sub-Component
        if ((path.getSubcomponent() == null) || (path.getSubcomponent() == 0) || (comp.size() < 2)) {
            return result;
        }

        Hl7Object<?> subComponent = comp.get(path.getSubcomponent() - 1);
        return result.pathByAddingChild(subComponent);
    }

}
