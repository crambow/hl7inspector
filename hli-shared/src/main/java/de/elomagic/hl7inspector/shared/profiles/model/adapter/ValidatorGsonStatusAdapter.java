/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.profiles.model.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.elomagic.hl7inspector.shared.profiles.model.ValidateStatus;

import java.io.IOException;

public class ValidatorGsonStatusAdapter extends TypeAdapter<ValidateStatus> {

    @Override
    public void write(JsonWriter out, ValidateStatus value) throws IOException {
        out.value(value == null ? ValidateStatus.OK.getStatus() : value.getStatus());
    }

    @Override
    public ValidateStatus read(JsonReader in) throws IOException {
        return ValidateStatus.valueOf(in.nextInt());
    }

}
