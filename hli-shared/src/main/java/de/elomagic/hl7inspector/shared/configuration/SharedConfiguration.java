/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.configuration;

import de.elomagic.hl7inspector.platform.configuration.AppConfigurationKey;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.FontItem;
import de.elomagic.hl7inspector.platform.ui.themes.AbstractIconTheme;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.ui.themes.OutlineDarkTheme;

import org.apache.commons.lang3.EnumUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.Font;
import java.awt.Rectangle;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

public final class SharedConfiguration {

    private static final SharedConfiguration INSTANCE = new SharedConfiguration();

    private final Configuration configuration = Configuration.getInstance();

    private SharedConfiguration() {
    }

    @NotNull
    public static SharedConfiguration getInstance() {
        return INSTANCE;
    }


    @NotNull
    public Calendar getLastUpdateCheck() throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(new SimpleDateFormat("yyyy.MM.dd").parse(configuration.getProperty(AppConfigurationKey.APP_UPDATE_CHECK_TIME_LAST_CHECK, String.class, "1980.01.01")));
        return c;
    }

    public void setLastUpdateCheck(@NotNull Calendar c) {
        configuration.setProperty(AppConfigurationKey.APP_UPDATE_CHECK_TIME_LAST_CHECK, new SimpleDateFormat("yyyy.MM.dd").format(c.getTime()));
    }

    public boolean isAutoUpdateAsk() {
        return configuration.getProperty(AppConfigurationKey.APP_UPDATE_CHECK_TIME_ASK_USER, Boolean.class, true);
    }

    public void setAutoUpdateAsk(boolean value) {
        configuration.setProperty(AppConfigurationKey.APP_UPDATE_CHECK_TIME_ASK_USER, value);
    }

    /**
     * Returns period for application update checks.
     *
     * @return Number of days between automatically checks. -1 means disabled. 0 means every on application start
     */
    public int getAutoUpdatePeriod() {
        return configuration.getProperty(AppConfigurationKey.APP_UPDATE_CHECK_TIME_PERIOD, Integer.class, 30);
    }

    /**
     * Set period for application update checks.
     *
     * @param period  Number of days between automatically checks. -1 means disabled. 0 means every on application start
     */
    public void setAutoUpdatePeriod(int period) {
        configuration.setProperty(AppConfigurationKey.APP_UPDATE_CHECK_TIME_PERIOD, period);
    }

    public Locale getAppLocale() {
        return configuration.getProperty(AppConfigurationKey.APP_LANGUAGE, Locale.class, Locale.getDefault());
    }

    public void setAppLocale(@Nullable Locale locale) {
        configuration.setRestartRequired(!Objects.equals(getAppLocale(), locale));
        configuration.setProperty(AppConfigurationKey.APP_LANGUAGE, locale == null ? Locale.ENGLISH.toLanguageTag() : locale.toLanguageTag());
    }

    @NotNull
    public AbstractIconTheme getIconTheme() {
        String id = configuration.getProperty(AppConfigurationKey.APP_UI_LAF_THEME, String.class, null);

        if (id == null) {
            return new OutlineDarkTheme();
        }

        return IconThemeManager.getThemeById(id);
    }

    public void setIconTheme(@Nullable AbstractIconTheme iconTheme) {
        configuration.setRestartRequired(!Objects.equals(getIconTheme(), iconTheme));
        configuration.setProperty(AppConfigurationKey.APP_UI_LAF_THEME, iconTheme == null ? new OutlineDarkTheme().getId() : iconTheme.getId());
    }

    public boolean isOneInstance() {
        return configuration.getProperty(AppConfigurationKey.APP_SINGLE_INSTANCE, Boolean.class, false);
    }

    public void setOneInstance(boolean value) {
        configuration.setProperty(AppConfigurationKey.APP_SINGLE_INSTANCE, value);
    }

    public boolean isQuitWithoutAsking() {
        return configuration.getProperty(AppConfigurationKey.APP_QUIT_WITHOUT_ASKING, Boolean.class, false);
    }

    public void setQuitWithoutAsking(boolean value) {
        configuration.setProperty(AppConfigurationKey.APP_QUIT_WITHOUT_ASKING, value);
    }


    public Path getExternalFileViewer() {
        return configuration.getProperty(AppConfigurationKey.APP_FILES_EXTERNAL_FILE_VIEWER, Path.class, null);
    }

    public void setExternalFileViewer(@Nullable Path value) {
        configuration.setProperty(AppConfigurationKey.APP_FILES_EXTERNAL_FILE_VIEWER, value == null ? null : value.toAbsolutePath().toString());
    }

    public int getProxyMode() {
        return configuration.getProperty(AppConfigurationKey.APP_NETWORK_PROXY_MODE, Integer.class, 1);
    }

    public void setProxyMode(int mode) {
        configuration.setProperty(AppConfigurationKey.APP_NETWORK_PROXY_MODE, mode);
    }

    @NotNull
    public String getProxyHost() {
        return configuration.getProperty(AppConfigurationKey.APP_NETWORK_PROXY_HOST, String.class, "");
    }

    public void setProxyHost(String host) {
        configuration.setProperty(AppConfigurationKey.APP_NETWORK_PROXY_HOST, host);
    }

    public int getProxyPort() {
        return configuration.getProperty(AppConfigurationKey.APP_NETWORK_PROXY_PORT, Integer.class, 0);
    }

    public void setProxyPort(int port) {
        configuration.setProperty(AppConfigurationKey.APP_NETWORK_PROXY_PORT, port);
    }

    public int getTreeNodeLength() {
        return configuration.getProperty(AppConfigurationKey.APP_EDITOR_TREE_NODE_MAX_LENGTH, Integer.class, 128);
    }

    public void setTreeNodeLength(int nodeLen) {
        configuration.setProperty(AppConfigurationKey.APP_EDITOR_TREE_NODE_MAX_LENGTH, nodeLen);
    }

    @NotNull
    public Font getAppEditorTreeFont() {
        String name = configuration.getProperty(AppConfigurationKey.APP_EDITOR_TREE_FONT_NAME, String.class, "Arial");
        int size = configuration.getProperty(AppConfigurationKey.APP_EDITOR_TREE_FONT_SIZE, Integer.class, 12);
        return new FontItem(name, Font.PLAIN, size);
    }

    public void setAppEditorTreeFont(@Nullable Font font) {
        configuration.setProperty(AppConfigurationKey.APP_EDITOR_TREE_FONT_NAME, font == null ? null : font.getName());
        configuration.setProperty(AppConfigurationKey.APP_EDITOR_TREE_FONT_SIZE, font == null ? null : font.getSize());
    }

    public String getTreeMessageNodeFormat() {
        return configuration.getProperty(AppConfigurationKey.APP_EDITOR_TREE_NODE_MESSAGE_FORMAT, String.class,"<font color=\"#404040\"><B>%i</B></font>%m<font color=\"#ff00ff\"><B>###</B></font>");
    }

    public void setTreeMessageNodeFormat(@Nullable String value) {
        configuration.setProperty(AppConfigurationKey.APP_EDITOR_TREE_NODE_MESSAGE_FORMAT, value);
    }

    @NotNull
    public Rectangle getDesktopBoundaries() {
        return new Rectangle(
                configuration.getProperty(AppConfigurationKey.APP_UI_DESKTOP_POSITION_X, Integer.class, 0),
                configuration.getProperty(AppConfigurationKey.APP_UI_DESKTOP_POSITION_Y, Integer.class, 0),
                configuration.getProperty(AppConfigurationKey.APP_UI_DESKTOP_DIMENSION_WIDTH, Integer.class, 800),
                configuration.getProperty(AppConfigurationKey.APP_UI_DESKTOP_DIMENSION_HEIGHT, Integer.class, 600)
        );
    }

    public void setDesktopBoundaries(@NotNull Rectangle boundaries) {
        configuration.setProperty(AppConfigurationKey.APP_UI_DESKTOP_POSITION_X, boundaries.x);
        configuration.setProperty(AppConfigurationKey.APP_UI_DESKTOP_POSITION_Y, boundaries.y);
        configuration.setProperty(AppConfigurationKey.APP_UI_DESKTOP_DIMENSION_WIDTH, boundaries.width);
        configuration.setProperty(AppConfigurationKey.APP_UI_DESKTOP_DIMENSION_HEIGHT, boundaries.height);
    }

    @NotNull
    public Configuration.ToolbarPosition getToolbarPosition() {
        String value = configuration.getProperty(AppConfigurationKey.APP_UI_TOOLBAR_ORIENTATION, String.class, Configuration.ToolbarPosition.TOP.name());
        return EnumUtils.getEnum(Configuration.ToolbarPosition.class, value, Configuration.ToolbarPosition.TOP);
    }

    public void setToolbarPosition(@Nullable Configuration.ToolbarPosition position) {
        configuration.setProperty(AppConfigurationKey.APP_UI_TOOLBAR_ORIENTATION, position == null ? null : position.name());
    }

    @NotNull
    public Configuration.ViewMode getAppEditorViewMode() {
        String value = configuration.getProperty(AppConfigurationKey.APP_EDITOR_VIEW_MODE, String.class, Configuration.ViewMode.EDITOR.name());
        return EnumUtils.getEnum(Configuration.ViewMode.class, value, Configuration.ViewMode.EDITOR);
    }

    public void setAppEditorViewMode(@Nullable Configuration.ViewMode mode) {
        configuration.setProperty(AppConfigurationKey.APP_EDITOR_VIEW_MODE, mode == null ? null : mode.name());
    }

    /**
     * Returns a new instance of configured import options.
     *
     * @return New instance
     */
    @NotNull
    public ImportOptions getImportOptions() {
        ImportOptions bean = new ImportOptions();
        bean.setEncoding(configuration.getProperty(AppConfigurationKey.APP_FILES_PARSER_CHARSET, Charset.class, StandardCharsets.ISO_8859_1));

        return bean;
    }

    public void setImportOptionsBean(@NotNull ImportOptions bean) {
        configuration.setProperty(AppConfigurationKey.APP_FILES_PARSER_CHARSET, bean.getEncoding().name());
    }

    @NotNull
    public Font getAppEditorTextFont() {
        String name = configuration.getProperty(AppConfigurationKey.APP_EDITOR_TEXT_FONT_NAME, String.class, Font.MONOSPACED);
        int size = configuration.getProperty(AppConfigurationKey.APP_EDITOR_TEXT_FONT_SIZE, Integer.class, 12);
        return new FontItem(name, Font.PLAIN, size);
    }

    public void setAppEditorTextFont(@Nullable Font font) {
        configuration.setProperty(AppConfigurationKey.APP_EDITOR_TEXT_FONT_NAME, font == null ? null : font.getName());
        configuration.setProperty(AppConfigurationKey.APP_EDITOR_TEXT_FONT_SIZE, font == null ? null : font.getSize());
    }

    public int getOpenFilesMaximum() {
        return configuration.getProperty(AppConfigurationKey.APP_FILES_MAX_OPEN, Integer.class, 1000);
    }

    public void setOpenFilesMaximum(int size) {
        configuration.setProperty(AppConfigurationKey.APP_FILES_MAX_OPEN, size);
    }

    @NotNull
    public List<URL> getPluginRepositoriesUrls() {
        List<String> values = configuration.getValuesOfKeysWithPrefix(AppConfigurationKey.APP_PLUGIN_REPOSITORIES_ULS, 10);

        if (values.isEmpty()) {
            values.add("https://bitbucket.org/crambow/hl7inspector-plugin-repository/raw/main/");
        }

        return values.stream()
                .map(v -> { try {
                    return new URL(v);
                } catch (Exception ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }}).collect(Collectors.toList());
    }

    public void setPluginRepositoriesUrls(@Nullable List<URL> urls) {
        configuration.removeKeysWithPrefix(AppConfigurationKey.APP_PLUGIN_REPOSITORIES_ULS);

        if (urls == null || urls.isEmpty()) {
            return;
        }

        configuration.setValuesByKeyPrefix(AppConfigurationKey.APP_PLUGIN_REPOSITORIES_ULS, urls);
    }

}
