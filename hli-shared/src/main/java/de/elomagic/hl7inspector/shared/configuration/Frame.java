/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.configuration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

/**
 * Defines the frame when exchanging messages via MLLP.
 *
 * <b>{START-FRAME}</b>message<b>{STOP-BYTE1, [STOP-BYTE2]}</b>
 */
public class Frame implements Serializable {

    private byte start;
    private byte[] stops;

    /**
     * Default SB (Start Block character). 0x0b
     */
    public static final Byte DEFAULT_START = 0x0b;
    /**
     * Default EB (End Block character) 0x1c
     */
    public static final Byte DEFAULT_STOP1 = 0x1c;
    /**
     * Default CR (Carriage Return) 0x0d
     */
    public static final Byte DEFAULT_STOP2 = 0x0d;

    public Frame() {
        start = 0xb;
        stops = new byte[] {0x1c, 0xd};
    }

    public Frame(byte startFrame, @NotNull byte[] stopFrames) {
        start = startFrame;
        stops = stopFrames;
    }

    public Frame(byte startByte, byte stopByte1, @Nullable Byte stopByte2) {
        start = startByte;

        setStopBytes(stopByte1, stopByte2);

        stops = new byte[stopByte2 == null ? 1 : 2];
        stops[0] = stopByte1;

        if(stops.length > 1) {
            stops[1] = stopByte2;
        }
    }

    public byte getStartByte() {
        return start;
    }

    public void setStartByte(byte start) {
        this.start = start;
    }

    public void setStopBytes(byte stopByte1, @Nullable Byte stopByte2) {
        if (stopByte2 == null) {
            stops = new byte[] {stopByte1};
        } else {
            stops = new byte[] {stopByte1, stopByte2};
        }
    }

    /**
     *
     * @return Returns array of stop bytes.
     *
     * @see Frame#getStopBytesLength()
     */
    @NotNull
    public byte[] getStopBytes() {
        return stops;
    }

    public int getStopBytesLength() {
        return stops.length;
    }

}
