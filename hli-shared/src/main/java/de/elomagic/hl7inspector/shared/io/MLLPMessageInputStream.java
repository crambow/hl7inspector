/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.io;

import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.platform.io.IOBytesReadListener;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.MetricBean;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class MLLPMessageInputStream implements Closeable {

    private static final Logger LOGGER = LogManager.getLogger(MLLPMessageInputStream.class);

    private static final int END_OF_STREAM_BYTE = -1;

    private final BufferedInputStream in;
    private final Frame frame;
    private final Charset encoding;

    private ByteArrayOutputStream buffer;

    private final List<IOBytesReadListener> listener = new ArrayList<>();

    private long bytesReads;
    private MetricBean metric = new MetricBean();

    public MLLPMessageInputStream(@NotNull InputStream in, @NotNull Frame frame, @NotNull Charset encoding) {
        this.in = new BufferedInputStream(in);
        this.frame = frame;
        this.encoding = encoding;
    }

    /**
     * Reads a HL7 message from the stream.
     *
     * @return Returns the message or null when the end of the stream has been reached regularly
     * @throws IOException Some unexpected was happened
     * @throws UnexpectedEndOfStreamException Throw when socket was closed when receiving bytes in between of the frame
     */
    @Nullable
    public Message readMessage() throws IOException {
        boolean done = false;

        StopWatch stopWatch = StopWatch.createStarted();

        if (!waitForStartFrame()) {
            return null;
        }

        buffer = new ByteArrayOutputStream();
        LOGGER.debug("Reading message including stop frame");
        while (!done) {
            LOGGER.trace("Waiting for a byte...");
            int b = in.read();
            LOGGER.printf(Level.TRACE, "Byte 0x%02X received", b);

            if (b == END_OF_STREAM_BYTE) {
                throw new UnexpectedEndOfStreamException();
            } else {
                bytesReads++;
                buffer.write(b);
                done = checkOnStopBytes();
            }
        }

        LOGGER.debug("Stop frame matched");

        byte[] data = Arrays.copyOf(buffer.toByteArray(), buffer.size() - frame.getStopBytesLength());
        fireBytesReceived(data, encoding);
        fireBytesReceived(Arrays.copyOfRange(buffer.toByteArray(), buffer.size()-frame.getStopBytesLength(), buffer.size()), null);

        String text = new String(data, encoding);
        if (!text.endsWith("" + Delimiters.DEFAULT_SEGMENT_SEPARATOR)) {
            fireStringReceived(LocaleTool.get("warning_last_segment_have_no_segment_termination_char_0x0d"));
            text += Delimiters.DEFAULT_SEGMENT_SEPARATOR;
        }

        stopWatch.stop();
        metric.add(stopWatch.getTime());

        Message result = new Message();
        result.parse(text);
        result.getMeta().setEncoding(encoding);

        return result;
    }

    /**
     * Wait for the start frame byte.
     *
     * @return Returns true when start byte caught and false when end of stream reached
     * @throws IOException Thrown when unable to read bytes from the stream
     */
    private boolean waitForStartFrame() throws IOException {
        int b;
        do {
            LOGGER.trace("Waiting for a start frame byte...");
            b = in.read();
            LOGGER.printf(Level.TRACE, "Byte 0x%02X received", b);
            bytesReads++;

            if (b == END_OF_STREAM_BYTE) {
                // End of stream reached
                return false;
            } else {
                fireByteReceived((byte)b);
            }

        } while(b !=frame.getStartByte());

        return true;
    }

    /**
     * Checks buffer on trailing stop frame
     *
     * @return Returns true when the last bytes match the stop frame byte(s).
     */
    private boolean checkOnStopBytes() {
        boolean result = false;

        // Check on stop frame
        if(buffer.size() >= frame.getStopBytesLength()) {
            result = Arrays.equals(frame.getStopBytes(), 0, frame.getStopBytesLength(),
                    buffer.toByteArray(), buffer.size()-frame.getStopBytesLength(), buffer.size());
        }

        return result;
    }

    @Override
    public void close() throws IOException {
        in.close();
    }

    public long getBytesReads() {
        return bytesReads;
    }

    /**
     * Returns some metrics.
     *
     * @return Returns metrics in milliseconds
     */
    @NotNull
    public MetricBean getMetric() {
        return metric;
    }

    public void setMetric(@NotNull MetricBean metric) {
        this.metric = metric;
    }

    public void addListener(@NotNull IOBytesReadListener value) {
        listener.add(value);
    }

    public void removeListener(@NotNull IOBytesReadListener value) {
        listener.remove(value);
    }

    private void fireStringReceived(@NotNull String s) {
        fireBytesReceived(s.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
    }

    private void fireBytesReceived(byte[] data, Charset encoding) {
        CompletableFuture.runAsync(() -> {
            for (IOBytesReadListener l : listener) {
                l.bytesRead(this, data, encoding);
            }
        });
    }

    private void fireByteReceived(byte b) {
        CompletableFuture.runAsync(() -> {
            for (IOBytesReadListener l : listener) {
                l.bytesRead(this, new byte[]{b}, null);
            }
        });
    }

}
