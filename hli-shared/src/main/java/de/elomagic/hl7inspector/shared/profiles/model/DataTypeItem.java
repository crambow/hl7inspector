/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.profiles.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data-type")
@XmlAccessorType(XmlAccessType.NONE)
public final class DataTypeItem {

    private int len = 0;
    private int index = 0;
    private String description = "";
    private String dataType = "";
    private String parentDataType = "";
    private String opt = "O";
    private String chapter = "";
    private String parentDataTypeName = "";
    private String table = "";

    public DataTypeItem() {
    }

    public DataTypeItem(String parentDataType, int index, String desc, String dataType) {
        setParentDataType(parentDataType);
        setIndex(index);
        setDescription(desc);
        setDataType(dataType);
    }

    @XmlElement(name = "length")
    public int getLength() {
        return len;
    }

    public void setLength(int l) {
        this.len = l;
    }

    public void setLength(String l) {
        try {
            this.len = Integer.parseInt(l);
        } catch(Exception e) {
            this.len = 0;
        }
    }

    @XmlElement(name = "index")
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setIndex(String index) {
        try {
            this.index = Integer.parseInt(index);
        } catch(Exception e) {
            this.index = 0;
        }
    }

    @XmlElement(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement(name = "datatype")
    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType.trim();
    }

    @XmlElement(name = "parent")
    public String getParentDataType() {
        return parentDataType;
    }

    public void setParentDataType(String dataType) {
        this.parentDataType = dataType.trim();
    }

    @XmlElement(name = "opt")
    public String getOptionality() {
        return opt;
    }

    public void setOptionality(String value) {
        opt = value.trim();
    }

    @XmlElement(name = "chapter")
    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    @XmlElement(name = "parent-name")
    public String getParentDataTypeName() {
        return parentDataTypeName;
    }

    public void setParentDataTypeName(String parentDataTypeName) {
        this.parentDataTypeName = parentDataTypeName.trim();
    }

    @XmlElement(name = "table")
    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        try {
            this.table = Integer.valueOf(table).toString();
        } catch(Exception ex) {
            this.table = table.trim();
        }
    }
}
