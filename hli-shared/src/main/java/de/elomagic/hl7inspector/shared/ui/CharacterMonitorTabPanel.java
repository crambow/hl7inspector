/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.ui;

import de.elomagic.hl7inspector.platform.utils.thread.ThreadStatusListener;
import de.elomagic.hl7inspector.platform.ui.components.AbstractToolTabDocumentPanel;
import de.elomagic.hl7inspector.platform.ui.components.HighlighterPanel;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.nio.charset.Charset;

@Component(CharacterMonitorTabPanel.NAME)
public class CharacterMonitorTabPanel extends AbstractToolTabDocumentPanel implements ThreadStatusListener {

    public static final String NAME = "CharacterMonitor";

    private MonitorToolBar monitorToolBar;

    private final HighlighterPanel highlighterPane;

    @Autowired
    public CharacterMonitorTabPanel(HighlighterPanel highlighterPane) {
        this.highlighterPane = highlighterPane;
    }

    @PostConstruct
    private void initCharacterMonitorUI() {
        setLayout(new BorderLayout());

        monitorToolBar = new MonitorToolBar(this);
        JScrollPane sp = new JScrollPane(highlighterPane);

        add(monitorToolBar, BorderLayout.WEST);
        add(sp, BorderLayout.CENTER);

        setPreferredSize(new Dimension(400, 200));
    }

    @Override
    public @NotNull String getId() {
        return NAME;
    }

    @Override
    @NotNull
    public String getTitle() {
        return LocaleTool.get("parser_log");
    }

    @Override
    public ImageIcon getIcon() {
        return IconThemeManager.getImageIcon("trace.png");
    }

    public void addBytes(@NotNull byte[] data, @Nullable Charset encoding) {
        highlighterPane.addBytes(data, encoding);
    }

    public void addLine(String value) {
        highlighterPane.addLine(value);
    }

    public void addLine(String value, boolean escape) {
        highlighterPane.addLine(value, escape);
    }

    public void clear() {
        highlighterPane.clear();
    }

    public void saveLog() {
        highlighterPane.saveLog();
    }

    public String getText() {
        return highlighterPane.getText();
    }

    public MonitorToolBar getToolBar() {
        return monitorToolBar;
    }
    // Interface IOThreadListener

    @Override
    public void bytesSend(@NotNull Object source, @NotNull byte[] data, @Nullable Charset encoding) {
        addBytes(data, encoding);
    }

    @Override
    public void bytesRead(@NotNull Object source, @NotNull byte[] data, @Nullable Charset encoding) {
        addBytes(data, encoding);
    }

    @Override
    public void status(Thread source, String status) {
        addLine(status);
    }
}
