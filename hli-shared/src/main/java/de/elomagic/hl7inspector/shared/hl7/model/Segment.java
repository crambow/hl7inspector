/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7.model;

import org.jetbrains.annotations.NotNull;

public class Segment extends Hl7Object<RepetitionField> {

    @Override
    @NotNull
    public HL7ObjectType getType() {
        return HL7ObjectType.SEGMENT;
    }

    @Override
    public char getSubDelimiter() {
        return Delimiters.DEFAULT_FIELD;
    }

    @Override
    public Class<RepetitionField> getChildClass() {
        return RepetitionField.class;
    }

    @Override
    public Message getParent() {
        return (Message)super.getParent();
    }
}
