/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7.model;

import org.jetbrains.annotations.NotNull;

public class Subcomponent extends Hl7Object {

    private String value = "";

    public Subcomponent() {
    }

    public Subcomponent(String value) {
        this.parse(value);
    }

    @Override
    @NotNull
    public String asText() {
        return value;
    }

    @Override
    public void parse(String text) {
        value = text == null ? "" : text;

        resetTreeData(this);
    }

    @Override
    @NotNull
    public HL7ObjectType getType() {
        return HL7ObjectType.SUB_COMPONENT;
    }

    @Override
    public char getSubDelimiter() {
        return (char)0;
    }

    @Override
    public Class<?> getChildClass() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return value.isEmpty();
    }

    @Override
    public Component getParent() {
        return (Component)super.getParent();
    }
}
