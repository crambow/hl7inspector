/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.events;

import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.hl7.model.Message;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationEvent;

import java.awt.*;

public class MessageNodeSelectionChangedEvent extends ApplicationEvent {

    private final transient Message message;
    private final HL7Path path;
    private final Point cursorPosition;

    public MessageNodeSelectionChangedEvent(@NotNull Object source, @Nullable Message message, @Nullable HL7Path path, @Nullable Point cursorPosition) {
        super(source);

        this.message = message;
        this.path = path;
        this.cursorPosition = cursorPosition;
    }

    @Nullable
    public Message getMessage() {
        return message;
    }

    @Nullable
    public HL7Path getPath() {
        return path;
    }

    @Nullable
    public Point getCursorPosition() {
        return cursorPosition;
    }

}
