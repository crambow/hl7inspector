/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.io;

import de.elomagic.hl7inspector.shared.hl7.model.MessageMETA;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.Objects;
import java.util.UUID;

public class MessageFile {

    public enum SourceType {
        FILE("file"),
        IP_SOCKET("ip_socket"),
        IN_MEMORY("in_memory");

        private final String key;

        SourceType(String resourceKey) {
            this.key = resourceKey;
        }

        public String toDisplay() {
            return LocaleTool.get(key);
        }
    }

    private final String id = UUID.randomUUID().toString();
    // Each file, independent if it's a in memory message, must a (temporary) file
    private Path file;
    private boolean inMemory;
    private Path sourceFile;
    private MessageMETA.SourceType sourceType;

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public Path getFile() {
        return file;
    }

    public void setFile(Path file) {
        this.file = file;
    }

    public boolean isInMemory() {
        return inMemory;
    }

    public void setInMemory(boolean inMemory) {
        this.inMemory = inMemory;
    }

    /**
     * Returns the source file where message comes from.
     *
     * Can be differ when message is one of more messages from a log file.
     *
     * @return The source file or null when message received via socket.
     */
    @Nullable
    public Path getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(@Nullable Path sourceFile) {
        this.sourceFile = sourceFile;
    }

    @NotNull
    public MessageMETA.SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(@NotNull MessageMETA.SourceType sourceType) {
        this.sourceType = sourceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageFile that = (MessageFile) o;
        return Objects.equals(file, that.file);
    }

    @Override
    public int hashCode() {
        return Objects.hash(file);
    }

}
