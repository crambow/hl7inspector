/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.io;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.io.IOBytesWriteListener;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class MessageOutputStream extends OutputStream {

    private static final Logger LOGGER = LogManager.getLogger(MessageOutputStream.class);

    private final BufferedOutputStream out;
    private final Charset encoding;

    private final List<IOBytesWriteListener> listener = new ArrayList<>();

    private long bytesWritten;

    public MessageOutputStream(@NotNull OutputStream out, @NotNull Charset encoding) {
        this.out = new BufferedOutputStream(out);
        this.encoding = encoding;
    }

    public void writeMessage(@NotNull Message message) throws IOException {
        byte[] data = message.asText().getBytes(encoding);
        write(data);
        fireBytesSend(data, encoding);
        flush();
    }

    public long getBytesWritten() {
        return bytesWritten;
    }

    public void addListener(@NotNull IOBytesWriteListener value) {
        listener.add(value);
    }

    public void removeListener(@NotNull IOBytesWriteListener value) {
        listener.remove(value);
    }

    protected void fireBytesSend(byte[] data, Charset encoding) {
        CompletableFuture.runAsync(() -> {
            for(IOBytesWriteListener l : listener) {
                l.bytesSend(this, data, encoding);
            }
        });
    }

    protected void fireByteSend(byte b) {
        CompletableFuture.runAsync(() -> {
            for (IOBytesWriteListener l : listener) {
                l.bytesSend(this, new byte[]{b}, null);
            }
        });
    }

    @Override
    public void write(int b) throws IOException {
        LOGGER.printf(Level.TRACE, "Writing byte 0x%02X", b);
        out.write(b);
        bytesWritten++;
    }

    @Override
    public void flush() throws IOException {
        LOGGER.trace("Flushing stream");
        out.flush();
    }

    @Override
    public void close() throws IOException {
        LOGGER.trace("Closing stream");
        out.close();
    }

}
