/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.io;

import de.elomagic.hl7inspector.shared.hl7.HL7Tool;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.MessageMETA;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class MessageFileIO {

    public static final String VAR_COUNTER = "${counter}";

    private MessageFileIO() {
    }

    /**
     *
     * @param counter
     * @param digits Digits in the filename
     * @param targetFolder Path to the target folder.
     * @param filenamePattern Pattern must include ${counter}
     * @return Returns the filename
     */
    public static Path createFilename(int counter, int digits, @NotNull Path targetFolder, @NotNull String filenamePattern) {
        String c = Integer.toString(counter);

        c = StringUtils.leftPad(c, digits, "0");

        String filename = filenamePattern.replace(MessageFileIO.VAR_COUNTER, c);

        return targetFolder.resolve(filename);
    }

    public static Path createFilename(int counter, int maxCounter, @NotNull Path targetFolder, @NotNull String prefix, @NotNull String suffix) {
        String filename = Integer.toString(counter);

        filename = prefix + StringUtils.leftPad(filename, Integer.toString(maxCounter).length(), "0") + suffix;

        return targetFolder.resolve(filename);
    }

    /**
     * Writes message with UTF-8 encoding to a file
     *
     * @param message HL7 message
     * @param file The File. If exists then it will be overridden. If folder not exists, it will be created
     * @throws IOException Thrown when unable to write.
     */
    public static void writeMessage(@NotNull Message message, @NotNull Path file) throws IOException {
        Files.createDirectories(file.getParent());
        Files.writeString(file, message.asText(), message.getMeta().getEncoding(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);

        message.getMeta().setModifiedTime(Files.getLastModifiedTime(file));
    }

    /**
     * Reads and parse a HL7 message file.
     *
     * @param file File which MUST contains one HL7 message
     * @param charset Charset to use for reading
     * @return Returns the HL7 message
     * @throws IOException Thrown when unable to read the file
     */
    @NotNull
    public static Message readMessage(@NotNull Path file, @NotNull Charset charset) throws IOException {
        String text = FileUtils.readFileToString(file.toFile(), charset);

        // Normalize segment delimiters
        text = HL7Tool.normalizeSegmentSeparator(text);

        Message message = new Message();
        message.parse(text);

        boolean inmemory = file.startsWith(PlatformTool.getInMemoryFolder());

        MessageMETA meta = message.getMeta();
        meta.setSourceFile(file);
        meta.setFile(file);
        meta.setModifiedTime(Files.getLastModifiedTime(file));
        meta.setEncoding(charset);
        meta.setInMemory(inmemory);
        meta.setSourceType(inmemory ? MessageMETA.SourceType.IN_MEMORY : MessageMETA.SourceType.FILE);

        return message;
    }

    /**
     * Re-Reads and parse a HL7 message file.
     *
     * @param message Message to re-read
     * @throws IOException Thrown when unable to read the file
     */
    public static void rereadMessage(@NotNull Message message) throws IOException {
        String text = Files.readString(message.getMeta().getFile(), message.getMeta().getEncoding());

        message.parse(text);

        MessageMETA meta = message.getMeta();
        meta.setModifiedTime(Files.getLastModifiedTime(message.getMeta().getFile()));
    }

}
