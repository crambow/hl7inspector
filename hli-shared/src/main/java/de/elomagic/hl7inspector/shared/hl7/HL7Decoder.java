/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7;

import de.elomagic.hl7inspector.platform.utils.Html;
import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HL7Decoder {

    private HL7Decoder() {
    }

    /**
     * Returns a HTML formatted decoded HL7 string without HTML tag frame.
     */
    public static String decodeString(@Nullable String encodedValue, @NotNull Delimiters delimiters) throws HL7ValueException {
        return decodeString(encodedValue, delimiters, false);
    }

    /**
     * Returns a HTML formatted decoded HL7 string without HTML tag frame.
     */
    public static String decodeString(@Nullable String encodedValue, @NotNull Delimiters delimiters, boolean noWrap) throws HL7ValueException {
        if (StringUtils.isEmpty(encodedValue)) {
            return "";
        }

        StringBuilder result = new StringBuilder();

        int i = 0;
        while(i < encodedValue.length()) {
            char c = encodedValue.charAt(i);

            if (c != delimiters.escapeCharacter) {
                result.append(c);
                i++;
                continue;
            }

            i++;
            int li = encodedValue.indexOf(String.valueOf(delimiters.escapeCharacter), i);

            if(li == -1) {
                throw new HL7ValueException("Invalid escape sequence in HL7 object");
            }

            String seq = encodedValue.substring(i, li);
            i = li;

            if(seq.isEmpty()) {
                i++;
                continue;
            }

            char control = seq.charAt(0);
            String value = seq.substring(1);

            switch(control) {
                // Start highlighting
                case 'H':
                    result.append("<B>");
                    break;
                // normal text (end highlighting)
                case 'N':
                    result.append("</B>");
                    break;
                // field separator
                case 'F':
                    result.append(delimiters.fieldDelimiter);
                    break;
                // component separator
                case 'S':
                    result.append(delimiters.componentDelimiter);
                    break;
                // subcomponent separator
                case 'T':
                    result.append(delimiters.subcomponentDelimiter);
                    break;
                // repetition separator
                case 'R':
                    result.append(delimiters.repetitionDelimiter);
                    break;
                // escape character
                case 'E':
                    result.append(delimiters.escapeCharacter);
                    break;
                // hexadecimal data
                case 'X':
                    result.append(((char)Integer.parseInt(value, 16)));
                    break;
//                            case 'Z': { /* TODO "Locally escape sequence" support */; break; }
//                            case 'C': { /* TODO "Single byte character set" support */; break; }        // single byte character set
//                            case 'M': { /* TODO "Multi byte character set" support */; break; }         // multi byte character set
                case '.':
                    if(value.equals("br")) {
                        result.append(Html.BR);
                    } else if(value.equals("sp")) {
                        result.append(Html.BR);
                    } else if(value.equals("fi")) {
                        if(noWrap) {
                            result.append("</nobr>");
                        }
                        noWrap = false;
                    } else if(value.equals("nf")) {
                        result.append("<nobr>");
                        noWrap = true;
                    } else if(value.startsWith("in")) {
                        // Indent <number> of spaces, where <number> is a positive or negative integer. This command cannot appear after the first printable character of a line.
                    } else if(value.startsWith("ti")) {
                        // Temporarily indent <number> of spaces where number is a positive or negative integer. This command cannot appear after the first printable character of a line.
                    } else if(value.startsWith("sk")) {
                        // Skip <number> spaces to the right. Example .sk+3   OR  .sk-2
                    } else if(value.equals("ce")) {
                        // End current output line and center the next line.
                    } else {
                        throw new HL7ValueException("Unsupported escape sequence '" + seq + "' in hl7 object");
                    }
                    break;
                default:
                    throw new HL7ValueException("Unsupported escape sequence '" + seq + "' in hl7 object");
            }

            i++;
        }

        return result.toString();
    }

}
