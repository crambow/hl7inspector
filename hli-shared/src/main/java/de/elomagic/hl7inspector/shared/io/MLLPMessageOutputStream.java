/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.io;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.platform.utils.MetricBean;

import org.apache.commons.lang3.time.StopWatch;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class MLLPMessageOutputStream extends MessageOutputStream {

    private final Frame frame;
    private MetricBean metric = new MetricBean();

    public MLLPMessageOutputStream(@NotNull OutputStream out, @NotNull Frame frame, @NotNull Charset encoding) {
        super(out, encoding);
        this.frame = frame;
    }

    @Override
    public void writeMessage(@NotNull Message message) throws IOException {
        StopWatch watch = StopWatch.createStarted();
        write(frame.getStartByte());
        fireByteSend(frame.getStartByte());

        super.writeMessage(message);

        write(frame.getStopBytes());

        watch.stop();
        metric.add(watch.getTime());

        fireBytesSend(frame.getStopBytes(), null);

        flush();
    }

    /**
     * Returns some metrics.
     *
     * @return Returns metrics in milliseconds
     */
    @NotNull
    public MetricBean getMetric() {
        return metric;
    }

    public void setMetric(@NotNull MetricBean metric) {
        this.metric = metric;
    }

}
