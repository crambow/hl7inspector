/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.profiles;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class ProfileFile implements Serializable {

    private final transient Path path;
    private String name;
    private String description;

    public ProfileFile(@NotNull Path path) {
        this.path = path;
    }

    public ProfileFile(@NotNull Path path, String name, String description) {
        this.path = path;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @NotNull
    public Path getPath() {
        return path;
    }

    public boolean exists() {
        return Files.exists(path);
    }

    public boolean delete() throws IOException {
        return Files.deleteIfExists(path);
    }

    public String getAbsolutePath() {
        return path.toString();
    }

    @Override
    public String toString() {
        return "ProfileFile{" +
                "path=" + path +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfileFile that = (ProfileFile) o;
        return Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }
}
