/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.ui;

import com.alee.laf.scroll.WebScrollPane;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.DragAndDropService;
import de.elomagic.hl7inspector.platform.ui.FontUtils;
import de.elomagic.hl7inspector.platform.ui.HighlightCaret;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.components.TextLineNumber;
import de.elomagic.hl7inspector.platform.ui.find.DocumentFindMethods;
import de.elomagic.hl7inspector.platform.ui.find.ui.FindBar;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyDocumentListener;
import de.elomagic.hl7inspector.platform.ui.themes.AbstractIconTheme;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.TextSelection;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.hl7inspector.shared.events.MessageEditedEvent;
import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.shared.events.ShowDocumentFindBarEvent;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.hl7.HL7Tool;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.ui.actions.CopyTextAction;
import de.elomagic.hl7inspector.shared.ui.actions.CutTextAction;
import de.elomagic.hl7inspector.shared.ui.actions.PasteTextAction;
import de.elomagic.hl7inspector.shared.ui.actions.RedoAction;
import de.elomagic.hl7inspector.shared.ui.actions.ShowDocumentFindBarAction;
import de.elomagic.hl7inspector.shared.ui.actions.UndoAction;
import de.elomagic.hl7inspector.shared.ui.facades.CutCopyPasteFacade;
import de.elomagic.hl7inspector.shared.ui.facades.RefreshUIFacade;
import de.elomagic.hl7inspector.shared.ui.facades.UndoRedoFacade;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.TokenTypes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.undo.UndoManager;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Component("BasicMessageEditor")
@Scope("prototype")
public abstract class AbstractMessageEditor extends WebScrollPane implements RefreshUIFacade, CutCopyPasteFacade, UndoRedoFacade, DocumentFindMethods {

    private static final Logger LOGGER = LogManager.getLogger(AbstractMessageEditor.class);

    public static final String STYLE_NAME = "application/hl7v2";

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private final UndoManager undoManager = new UndoManager();

    private final transient BeanFactory beanFactory;
    private final transient EventMessengerProcessor eventMessengerProcessor;
    private final transient DragAndDropService dragAndDropService;

    private transient Message message;
    private TextLineNumber lineNumbersPanel;
    private RSyntaxTextArea editMessage;
    private FindBar findBar;

    protected int suppressEvent = 0;
    private int suppressCaretListener = 0;
    private int suppressSetMessage = 0;

    protected @Autowired AbstractMessageEditor(BeanFactory beanFactory, EventMessengerProcessor eventMessengerProcessor, DragAndDropService dragAndDropService) {
        super(StyleId.scrollpaneTransparent);
        this.beanFactory = beanFactory;
        this.eventMessengerProcessor = eventMessengerProcessor;
        this.dragAndDropService = dragAndDropService;
    }

    @PostConstruct
    private void initTextEditorUI() {
        setBorder(BorderFactory.createEmptyBorder());

        AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
        atmf.putMapping(STYLE_NAME, HL7MessageTokenStyle.class.getName());

        editMessage = new RSyntaxTextArea();
        editMessage.setCaret(new HighlightCaret());
        editMessage.setSyntaxEditingStyle(STYLE_NAME);
        editMessage.getSyntaxScheme().getStyle(TokenTypes.RESERVED_WORD).foreground = Color.MAGENTA;
        editMessage.setAutoIndentEnabled(false);
        editMessage.setTabsEmulated(true);
        editMessage.setTabSize(0);
        editMessage.setFont(configuration.getAppEditorTextFont());
        editMessage.getDocument().addUndoableEditListener(evt -> undoManager.addEdit(evt.getEdit()));
        editMessage.getDocument().addDocumentListener((SimplifyDocumentListener) e -> fireMessageEditedEvent());
        // Will be called by JDK twice when set text. One for removing old test and one for settings new text. Dam it.
        editMessage.addCaretListener(e -> {
            if (suppressCaretListener > 0) {
                return;
            }

            Point p = getCursorPosition(e.getDot());
            LOGGER.trace("Cursor position: {}", p);

            HL7Path path = HL7Tool.getHL7Path(editMessage.getText(), p);
            LOGGER.trace("HL7 path: {}", path);
            fireCursorChangedEvent(new MessageNodeSelectionChangedEvent(this, message, path, p));
        });
        editMessage.addMouseWheelListener(e -> {
            if (e.isControlDown()) {
                FontUtils.adjustSize(editMessage, (float) e.getPreciseWheelRotation());
            }
        });

        setTheme(configuration.getIconTheme());

        dragAndDropService.addMessageDropSupport(editMessage);

        lineNumbersPanel = new TextLineNumber(editMessage);
        lineNumbersPanel.setUpdateFont(true);

        findBar = new FindBar();
        findBar.setVisible(false);
        findBar.setPresenter(this);
        findBar.addPropertyChangeListener("visible", e -> {
            boolean visible = (boolean)e.getNewValue();
            setColumnHeaderView(visible ? findBar : null);
            if (!visible) {
                editMessage.requestFocus();
            }
        });

        setRowHeaderView(lineNumbersPanel);
        setViewportView(editMessage);
    }

    public void bindMessage(@Nullable Message message) {
        if (suppressSetMessage > 0) {
            return;
        }

        this.message = message;
        String m = message == null ? null : message.asText().replace('\r', '\n');
        suppressCaretListener++;
        try {
            HL7Path p = getPath();
            editMessage.setText(m);
            selectPath(p);
        } finally {
            suppressCaretListener--;
        }
    }

    @Override
    public void setComponentPopupMenu(@Nullable JPopupMenu popupMenu) {
        editMessage.setComponentPopupMenu(popupMenu);

        ActionMap amap = editMessage.getActionMap();
        InputMap imap = editMessage.getInputMap(JComponent.WHEN_FOCUSED);

        if (popupMenu == null) {
            imap.clear();
            amap.clear();

            return;
        }

        getActions(popupMenu)
                .forEach(a -> {
                    String actionName = (String)a.getValue(Action.NAME);
                    KeyStroke ks = (KeyStroke)a.getValue(Action.ACCELERATOR_KEY);

                    imap.put(ks, actionName);
                    amap.put(actionName, a);
                });
    }

    private Set<Action> getActions(JPopupMenu menu) {
        return Arrays.stream(menu.getComponents())
                .filter(c -> JMenuItem.class.isAssignableFrom(c.getClass()))
                .map(c -> (JMenuItem)c)
                .filter(c -> c.getComponentCount() == 0)
                .map(AbstractButton::getAction)
                .collect(Collectors.toSet());
    }

    private void setTheme(@NotNull AbstractIconTheme theme) {
        try {
            Font font = editMessage.getFont();
            Theme t = Theme.load(theme.getRSyntaxTextAreaTheme());
            t.apply(editMessage);
            editMessage.setFont(font);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @Override
    public void setVisible(boolean visible) {
        boolean currentStatus = isVisible();

        super.setVisible(visible);

        if (visible && !currentStatus) {
            editMessage.setFont(configuration.getAppEditorTextFont());
            editMessage.requestFocus();
        }
    }

    /**
     * Returns the associated {@link HL7Path} from the current cursor position.
     *
     * @return The {@link HL7Path} or null when unable to locate HL7 path.
     */
    @Nullable
    public HL7Path getPath() {
        Point p = getCursorPosition(editMessage.getCaret().getDot());
        return HL7Tool.getHL7Path(editMessage.getText(), p);
    }

    public void selectPath(@Nullable HL7Path path) {
        if (suppressEvent > 0) {
            return;
        }

        suppressCaretListener++;
        suppressEvent++;
        try {
            TextSelection selection = HL7Tool.getTextSelection(editMessage.getText(), path);
            editMessage.select(selection.getStartPosition(), selection.getStopPosition());
        } finally {
            suppressEvent--;
            suppressCaretListener--;
        }
    }

    @NotNull
    private Point getCursorPosition(int totalPosition) {
        String text = editMessage.getText();
        String subtext = text.substring(0, Math.min(totalPosition, text.length()));
        int y = StringUtils.countMatches(subtext, '\n');
        int lastIndex = subtext.lastIndexOf('\n');

        return new Point(totalPosition - lastIndex - 1, y);
    }

    /**
     * Registers the given observer to begin receiving notifications
     * when changes are made to the document.
     *
     * @param listener the observer to register
     * @see Document#removeDocumentListener
     */
    public void addDocumentListener(DocumentListener listener) {
        editMessage.getDocument().addDocumentListener(listener);
    }

    /**
     * Unregisters the given observer from the notification list
     * so it will no longer receive change updates.
     *
     * @param listener the observer to register
     * @see Document#addDocumentListener
     */
    public void removeDocumentListener(DocumentListener listener) {
        editMessage.getDocument().removeDocumentListener(listener);
    }

    private boolean getLineNumberVisible() {
        return lineNumbersPanel.isVisible();
    }

    private void setLineNumberVisible(boolean visible) {
        lineNumbersPanel.setVisible(visible);
    }

    public void insertUndoRedo(@NotNull JPopupMenu popupMenu) {
        popupMenu.add(new UndoAction(this));
        popupMenu.add(new RedoAction(this));
    }

    public void insertCopyPasteToPopupMenu(@NotNull JPopupMenu popupMenu) {
        popupMenu.add(new CutTextAction(this));
        popupMenu.add(new CopyTextAction(this));
        popupMenu.add(new PasteTextAction(this));
    }

    public void insertFindMenu(@NotNull JPopupMenu popupMenu) {
        popupMenu.add(beanFactory.getBean(ShowDocumentFindBarAction.class));
    }

    private void fireCursorChangedEvent(MessageNodeSelectionChangedEvent event) {
        suppressEvent++;
        try {
            eventMessengerProcessor.publishEvent(event);
        } finally {
            suppressEvent--;
        }
    }

    private void fireMessageEditedEvent() {
        if (suppressCaretListener > 0) {
            return;
        }

        LOGGER.debug("Message edited: {}", this);
        message.parse(editMessage.getText().replace("\n", "\r"));

        suppressCaretListener++;
        suppressSetMessage++;
        suppressEvent++;
        try {
            HL7Path path  = HL7Tool.getHL7Path(
                    editMessage.getText(),
                    getCursorPosition(editMessage.getSelectionStart()));

            MessageEditedEvent event = new MessageEditedEvent(this, message, path);

            eventMessengerProcessor.publishEvent(event);
        } catch (Exception ex) {
            LOGGER.info("Unable to parse edited message: {}", ex.getMessage(), ex);
        } finally {
            suppressEvent--;
            suppressSetMessage--;
            suppressCaretListener--;
        }
    }

    /**
     * Insert text at the current cursor position.
     */
    public void insertText(@Nullable String snippet) throws BadLocationException {
        int offset = editMessage.getCaretPosition();
        editMessage.getDocument().insertString(offset, snippet, null);
    }

    public void refresh(Hl7Object<?> o) {
        if (suppressEvent > 0) {
            return;
        }

        int offset = editMessage.getCaretPosition();

        suppressCaretListener++;
        suppressSetMessage++;
        suppressEvent++;
        try {
            // We don't have to use message from event because it must the same instance we already bind.
            String m = message == null ? null : message.asText().replace('\r', '\n');
            editMessage.setText(m);

            editMessage.setCaretPosition(offset);
        } finally {
            suppressEvent--;
            suppressSetMessage--;
            suppressCaretListener--;
        }
    }

    @Override
    public void undo() {
        if (undoManager.canUndo()) {
            undoManager.undo();
        }
    }

    @Override
    public void redo() {
        if (undoManager.canRedo()) {
            undoManager.redo();
        }
    }

    @Override
    public void cut() {
        editMessage.cut();
    }

    @Override
    public void copy() {
        editMessage.copy();
    }

    @Override
    public void paste() {
        editMessage.paste();
    }

    @EventListener(ShowDocumentFindBarEvent.class)
    public void handleShowDocumentFindBarEvent() {
        if (isShowing()) {
            findBar.setVisible(true);
        }
    }

    @Override
    public void findNextPhrase(@NotNull String phrase, boolean caseSensitive) {
        try {
            Document doc = editMessage.getDocument();
            int p = editMessage.getSelectionStart();
            String text = doc.getText(p + 1, doc.getLength() - p);
            text = caseSensitive ? text : text.toUpperCase();
            phrase = caseSensitive ? phrase : phrase.toUpperCase();

            int index = text.indexOf(phrase);

            if (index == -1) {
                SimpleDialog.info(LocaleTool.get("end_of_message_reached"));
            } else {
                int nextPos = index + p + 1;
                editMessage.select(nextPos, nextPos + phrase.length());
            }
        } catch (Exception ex) {
            SimpleDialog.error(ex);
        }
    }

}
