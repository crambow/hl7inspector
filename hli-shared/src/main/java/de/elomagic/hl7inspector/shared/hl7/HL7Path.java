/*
 * HL7 Inspector - Shared
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector - Shared.
 *
 * HL7 Inspector - Shared is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector - Shared is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector - Shared. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.shared.hl7;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

public class HL7Path implements Serializable {

    private final String segment;
    private final int segmentIndex;
    private final Integer field;
    private final Integer repetitionField;
    private final Integer component;
    private final Integer subcomponent;

    /**
     *
     * @param segment Name of the segment
     * @param segmentIndex Index of the segment in the message. Start from 0.
     * @param field Position of the field. Starts from 1 or null when not known.
     * @param repetitionField Position of the repetition field. Starts from 1 or null when not known.
     * @param component Position of the component. Starts from 1 or null when not known.
     * @param subcomponent Position of the sub-component. Starts from 1 or null when not known.
     */
    public HL7Path(@NotNull String segment, int segmentIndex, @Nullable Integer field, @Nullable Integer repetitionField, @Nullable Integer component, @Nullable Integer subcomponent) {
        this.segment = segment;
        this.segmentIndex = segmentIndex;
        this.field = field;
        this.repetitionField = repetitionField;
        this.component = component;
        this.subcomponent = subcomponent;
    }

    /**
     *
     * @param segment Name of the segment
     * @param segmentIndex Index of the segment in the message. Start from 0.
     * @param field Position of the field. Starts from 1 or null when not known.
     */
    public HL7Path(@NotNull String segment, int segmentIndex, @Nullable Integer field) {
        this(segment, segmentIndex, field, null, null, null);
    }

    /**
     *
     * @param segment Name of the segment
     * @param segmentIndex Index of the segment in the message. Start from 0.
     */
    public HL7Path(@NotNull String segment, int segmentIndex) {
        this(segment, segmentIndex,null);
    }

    /**
     * Returns the name of the segment.
     *
     * <i>I say it again, don't use the segment name to get the position of a segment in a message!</i>
     *
     * @return The segment name
     */
    public String getSegmentName() {
        return segment;
    }

    /**
     * Returns the index of the segment in the message.
     *
     * @return Index start from 0
     */
    public int getSegmentIndex() {
        return segmentIndex;
    }

    /**
     * Returns position of the field.
     *
     * @return Starts from 1 or null when not known.
     */
    public Integer getField() {
        return field;
    }

    /**
     * Returns position of the repetition field.
     *
     * @return Starts from 1 or null when not known.
     */
    public Integer getRepetitionField() {
        return repetitionField;
    }

    /**
     * Returns position of the component.
     *
     * @return Starts from 1 or null when not known.
     */
    public Integer getComponent() {
        return component;
    }

    /**
     * Returns position of the sub-component.
     *
     * @return Starts from 1 or null when not known.
     */
    public Integer getSubcomponent() {
        return subcomponent;
    }

    public String asText() {
        String result = segment + "[" + segmentIndex + "]";

        result += field == null ? "" : ("." + field);
        result += repetitionField == null ? "" : ("[" + repetitionField + "]");
        result += component == null ? "" : ("." + component);
        result += subcomponent == null ? "": "." + subcomponent;

        return result;
    }

    @Override
    public String toString() {
        return "HL7Path{" +
                "segment='" + segment + '\'' +
                ", segmentIndex=" + segmentIndex +
                ", field=" + field +
                ", repetitionField=" + repetitionField +
                ", component=" + component +
                ", subcomponent=" + subcomponent +
                '}';
    }
}
