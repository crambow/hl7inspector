package de.elomagic.hl7inspector.shared.hl7;

import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class HL7DecoderTest {

    @Test
    void testDecode() throws Exception {
        Assertions.assertEquals("abcdefg", HL7Decoder.decodeString("abcdefg", new Delimiters()));
        Assertions.assertEquals("DILANTIN & NORVASC", HL7Decoder.decodeString("DILANTIN \\T\\ NORVASC", new Delimiters()));
        Assertions.assertEquals("Pierre DuRho^ne & Cie", HL7Decoder.decodeString("Pierre DuRho\\S\\ne \\T\\ Cie", new Delimiters()));
    }

}
