/*
 * Copyright 2011-present Carsten Rambow
 * 
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.gnu.org/licenses/gpl.txt
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector.shared.hl7.model;

import de.elomagic.hl7inspector.shared.TestTool;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

class MessageTest {

    @Test
    void testParseNonDefaultDelimters() throws Exception {
        System.out.println("parseNonDefaultDelimters");

        String messageText = TestTool.readMessageResource("/NonDefaultDelimiters.HL7");

        Message instance = new Message();
        instance.parse(messageText, new Delimiters("|^~\\`"));
        char expResult = 0x0d;
        assertEquals(expResult, instance.getSubDelimiter());
    }

    @Test
    void testGetSubDelimiter() {
        System.out.println("getSubDelimiter");
        Message instance = new Message();
        char expResult = 0x0d;
        assertEquals(expResult, instance.getSubDelimiter());
    }

    @Test
    void testGetChildClass() {
        System.out.println("getChildClass");
        Message instance = new Message();
        Class<Segment> result = instance.getChildClass();
        assertEquals(Segment.class, result);
    }

    @Test
    void testIndexOfName() throws IOException {
        System.out.println("indexOfName");

        String messageText = TestTool.readMessageResource("/NonDefaultDelimiters.HL7");

        Message instance = new Message();
        instance.parse(messageText, new Delimiters("|^~\\`"));

        int result = instance.indexOfName("DG1");
        assertEquals(4, result);

        result = instance.indexOfName("XYZ");
        assertEquals(-1, result);
    }

    @Test
    void testGetSegment() throws IOException {
        System.out.println("getSegment");

        String messageText = TestTool.readMessageResource("/NonDefaultDelimiters.HL7");

        Message instance = new Message();
        instance.parse(messageText, new Delimiters("|^~\\`"));

        Segment result = instance.getFirstSegment("PV1");
        assertEquals("PV1", result.get(0).asText());
    }

}
