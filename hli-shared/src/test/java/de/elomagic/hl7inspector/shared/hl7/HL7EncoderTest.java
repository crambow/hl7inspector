package de.elomagic.hl7inspector.shared.hl7;

import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class HL7EncoderTest {

    @Test
    void testHex() {
        Assertions.assertEquals("00", String.format("%02X", 0));
        Assertions.assertEquals("0C", String.format("%02X", 12));
    }

    @Test
    void testEncode() throws Exception {
        Assertions.assertEquals("abcdefg", HL7Encoder.encodeString("abcdefg", new Delimiters()));
        Assertions.assertEquals("DILANTIN \\T\\ NORVASC", HL7Encoder.encodeString("DILANTIN & NORVASC", new Delimiters()));
        Assertions.assertEquals("Pierre DuRho\\S\\ne \\T\\ Cie", HL7Encoder.encodeString("Pierre DuRho^ne & Cie", new Delimiters()));
    }

}
