package de.elomagic.hl7inspector.shared.hl7;

import de.elomagic.hl7inspector.shared.TestTool;
import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.utils.TextSelection;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HL7ToolTest {

    TreePath getPath(Hl7Object value) {
        List<Object> v = new ArrayList<>();

        TreeNode o = value;
        v.add(o);

        while(o.getParent() != null) {
            o = o.getParent();
            v.add(0, o);
        }

        return new TreePath(v.toArray());
    }

    @Test
    void testPlayground() throws Exception {

        //String message = TestTool.readMessageResource("/A08-UTF8-CR.hl7");

        String segmentPart = "PID|||4711";

        Delimiters delimiters = new Delimiters();

        int index = StringUtils.splitPreserveAllTokens(segmentPart, "\\" + delimiters.fieldDelimiter).length;

        assertEquals(4, index);
    }

    @Test
    void testGetHL7Position() throws Exception {
        // MSH|^~\&|ADT-HIS||HL7INSPECTOR||20060101103700||ADT^A08|1|P|2.3
        // EVN|A01|20060101||
        // PID||||4711|母鹿^约翰~Doe^John||19700220|M|||Sesamstreet 17^76137^上海^CN|||||GS|EV||||
        // PV1||I|S2^13^3^CHI^^21||||||||||||||||1234|||||||||||||||||||||||||200601010930||||||||
        // OBX|1|HD|^SR Instance UID||1.2.3.4.5.6.7.8.88.43||||||F|||||2^Dampf^Hans^^^^^^^^^^PN
        // OBX|2|EI|^Document Identifier||50^RIS-A
        // OBX|3|HD|^Study Instance UID|1|1.2.3.4.5.6.7.8.10||||||F|||||2^Dampf^Hans^^^^^^^^^^PN
        String message = TestTool.readMessageResource("/A08-UTF8-CR.hl7").replace('\r', '\n');

        HL7Path path = HL7Tool.getHL7Path(message, new Point(11, 2));

        assertEquals("PID", path.getSegmentName());
        assertEquals(2, path.getSegmentIndex());
        assertEquals(4, path.getField());
        assertEquals(1, path.getRepetitionField() );
        assertEquals(1, path.getComponent());
        assertEquals(1, path.getSubcomponent());

        path = HL7Tool.getHL7Path(message, new Point(8, 2));
        assertEquals(2, path.getSegmentIndex());
        assertEquals(4, path.getField());
        assertEquals(1, path.getRepetitionField() );
        assertEquals(1, path.getComponent());
        assertEquals(1, path.getSubcomponent());

        path = HL7Tool.getHL7Path(message, new Point(21, 2));
        assertEquals(2, path.getSegmentIndex());
        assertEquals(5, path.getField());
        assertEquals(2, path.getRepetitionField() );
        assertEquals(1, path.getComponent());
        assertEquals(1, path.getSubcomponent());

        path = HL7Tool.getHL7Path(message, new Point(22, 2));
        assertEquals(2, path.getSegmentIndex());
        assertEquals(5, path.getField());
        assertEquals(2, path.getRepetitionField() );
        assertEquals(2, path.getComponent());
        assertEquals(1, path.getSubcomponent());

        path = HL7Tool.getHL7Path(message, new Point(23, 2));
        assertEquals(2, path.getSegmentIndex());
        assertEquals(5, path.getField());
        assertEquals(2, path.getRepetitionField() );
        assertEquals(2, path.getComponent());
        assertEquals(1, path.getSubcomponent());

        path = HL7Tool.getHL7Path(message, new Point(22, 6));
        assertEquals(6, path.getSegmentIndex());
        assertEquals(3, path.getField());
        assertEquals(1, path.getRepetitionField() );
        assertEquals(2, path.getComponent());
        assertEquals(1, path.getSubcomponent());
    }

    @Test
    void testGetTextSelection() throws Exception {
        // MSH|^~\&|ADT-HIS||HL7INSPECTOR||20060101103700||ADT^A08|1|P|2.3
        // EVN|A01|20060101||
        // PID||||4711|母鹿^约翰~Doe^John||19700220|M|||Sesamstreet 17^76137^上海^CN|||||GS|EV||||
        // PV1||I|S2^13^3^CHI^^21||||||||||||||||1234|||||||||||||||||||||||||200601010930||||||||
        // OBX|1|HD|^SR Instance UID||1.2.3.4.5.6.7.8.88.43||||||F|||||2^Dampf^Hans^^^^^^^^^^PN
        // OBX|2|EI|^Document Identifier||50^RIS-A
        // OBX|3|HD|^Study Instance UID|1|1.2.3.4.5.6.7.8.10||||||F|||||2^Dampf^Hans^^^^^^^^^^PN
        String message = TestTool.readMessageResource("/A08-UTF8-CR.hl7").replace('\r', '\n');

        TextSelection ts1 = HL7Tool.getTextSelection(message, new HL7Path("PID", 2));
        Assertions.assertEquals(83, ts1.getStartPosition());
        Assertions.assertEquals(81, ts1.getLength());

        // 母鹿^约翰~Doe^John
        TextSelection ts2 = HL7Tool.getTextSelection(message, new HL7Path("PID", 2, 5));
        Assertions.assertEquals(95, ts2.getStartPosition());
        Assertions.assertEquals(14, ts2.getLength());

        // 3
        TextSelection ts3 = HL7Tool.getTextSelection(message, new HL7Path("PV1", 3, 3, 1, 3, null));
        Assertions.assertEquals(178, ts3.getStartPosition());
        Assertions.assertEquals(179, ts3.getStopPosition());

        // ""
        TextSelection ts4 = HL7Tool.getTextSelection(message, new HL7Path("EVN", 1, 3, 1, 1, 1));
        Assertions.assertEquals(81, ts4.getStartPosition());
        Assertions.assertEquals(0, ts4.getLength());

        TextSelection ts5 = HL7Tool.getTextSelection(message, new HL7Path("OBX", 6, 5, 1, 1, 1));
        //Assertions.assertEquals(81, ts5.getStartPosition());
        Assertions.assertEquals(18, ts5.getLength());
    }

    @Test
    void testGetDelimiters() throws Exception {
        // MSH|^~\&|ADT-HIS||HL7INSPECTOR||20060101103700||ADT^A08|1|P|2.3
        // EVN|A01|20060101||
        // PID||||4711|母鹿^约翰~Doe^John||19700220|M|||Sesamstreet 17^76137^上海^CN|||||GS|EV||||
        // PV1||I|S2^13^3^CHI^^21||||||||||||||||1234|||||||||||||||||||||||||200601010930||||||||
        String message = TestTool.readMessageResource("/A08-UTF8-CR.hl7").replace('\r', '\n');

        Delimiters d = HL7Tool.getDelimiters(message, new Delimiters("abcde"));
        Assertions.assertEquals('|', d.fieldDelimiter);
        Assertions.assertEquals('^', d.componentDelimiter);
        Assertions.assertEquals('\\', d.escapeCharacter);
        Assertions.assertEquals('&', d.subcomponentDelimiter);
    }

    @Test
    void testToHL7Object() throws Exception {
        // MSH|^~\&|ADT-HIS||HL7INSPECTOR||20060101103700||ADT^A08|1|P|2.3
        // EVN|A01|20060101||
        // PID||||4711|母鹿^约翰~Doe^John||19700220|M|||Sesamstreet 17^76137^上海^CN|||||GS|EV||||
        // PV1||I|S2^13^3^CHI^^21||||||||||||||||1234|||||||||||||||||||||||||200601010930||||||||
        Message m = TestTool.readMessage("/A08-UTF8-CR.hl7");

        //String segment, int segmentIndex, @Nullable Integer field, @Nullable Integer repetitionField, @Nullable Integer component, @Nullable Integer subcomponent) {
        Assertions.assertEquals("Doe", HL7Tool.toHL7Object(m, new HL7Path("PID", 2, 5, 2, 1, null)).asText());
        Assertions.assertEquals("76137", HL7Tool.toHL7Object(m, new HL7Path("PID", 2, 11, 1, 2, null)).asText());

        Hl7Object<?> o1 = HL7Tool.toHL7Object(m, new HL7Path("PID", 2, 11, 1, 2, null));
        Hl7Object<?> o2 = HL7Tool.toHL7Object(m, new HL7Path("PID", 2, 11, 1, 2, 1));
        Assertions.assertEquals(o1, o2);
    }

    @Test
    void testToTreePath_Hl7Object() throws Exception {
        // MSH|^~\&|ADT-HIS||HL7INSPECTOR||20060101103700||ADT^A08|1|P|2.3
        // EVN|A01|20060101||
        // PID||||4711|母鹿^约翰~Doe^John||19700220|M|||Sesamstreet 17^76137^上海^CN|||||GS|EV||||
        // PV1||I|S2^13^3^CHI^^21||||||||||||||||1234|||||||||||||||||||||||||200601010930||||||||
        Message m = TestTool.readMessage("/A08-UTF8-CR.hl7");

        //String segment, int segmentIndex, @Nullable Integer field, @Nullable Integer repetitionField, @Nullable Integer component, @Nullable Integer subcomponent) {
        Hl7Object<?> o = HL7Tool.toHL7Object(m, new HL7Path("PID", 2, 5, 2, 1, 1));

        Object[] p1 = HL7Tool.toTreePath(o).getPath();

        Assertions.assertArrayEquals(getPath(o).getPath(), p1);
    }

    @Test
    void testToTreePath_Message_HL7Path() throws Exception {
        // MSH|^~\&|ADT-HIS||HL7INSPECTOR||20060101103700||ADT^A08|1|P|2.3
        // EVN|A01|20060101||
        // PID||||4711|母鹿^约翰~Doe^John||19700220|M|||Sesamstreet 17^76137^上海^CN|||||GS|EV||||
        // PV1||I|S2^13^3^CHI^^21||||||||||||||||1234|||||||||||||||||||||||||200601010930||||||||
        Message m = TestTool.readMessage("/A08-UTF8-CR.hl7");

        //String segment, int segmentIndex, @Nullable Integer field, @Nullable Integer repetitionField, @Nullable Integer component, @Nullable Integer subcomponent) {
        HL7Path path = new HL7Path("PID", 2, 7, 1, null, null);
        Object[] p2 = getPath(HL7Tool.toHL7Object(m, path)).getPath();
        Object[] p1 = HL7Tool.toTreePath(m, path).getPath();
        Assertions.assertArrayEquals(p2, p1);

        path = new HL7Path("PID", 2, 2, 1, null, null);
        p2 = getPath(HL7Tool.toHL7Object(m, path)).getPath();
        p1 = HL7Tool.toTreePath(m, path).getPath();
        Assertions.assertArrayEquals(p2, p1);
    }

    @Test
    void normalizeSegmentSeparator() {
        Assertions.assertNull(HL7Tool.normalizeSegmentSeparator(null));
        Assertions.assertEquals("abcde\rabcd", HL7Tool.normalizeSegmentSeparator("abcde\rabcd"));
        Assertions.assertEquals("abcde\rabcd", HL7Tool.normalizeSegmentSeparator("abcde\nabcd"));
        Assertions.assertEquals("abcde\rabcd", HL7Tool.normalizeSegmentSeparator("abcde\n\rabcd"));
        Assertions.assertEquals("abcde\rabcd", HL7Tool.normalizeSegmentSeparator("abcde\r\nabcd"));
    }
}