/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.configuration.ui;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.nio.file.Paths;

@Component
public class ExternalToolsPanel extends AbstractBannerPanel<Configuration> {

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private JTextField editExternalApp;

    @PostConstruct
    public void initUI() {
        editExternalApp = new JTextField();
        JButton btChooseExternalApp = new JButton(new FileChooseAction());

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                .addSection("external_viewers", 3)
                .add("text_viewer", editExternalApp, 1, 1)
                .addRow(btChooseExternalApp, 1, 0)
                .addRowSpacer()
                .build();

        add(grid, BorderLayout.CENTER);
    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("open-external-32.png");
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("external_tools");
    }

    @Override
    public String getDescription() {
        return LocaleTool.get("external_tools");
    }

    @Override
    public void bindBean(@NotNull Configuration bean) {
        editExternalApp.setText(configuration.getExternalFileViewer() == null ? "" : configuration.getExternalFileViewer().toAbsolutePath().toString());
    }

    @Override
    public void commit() {
        configuration.setExternalFileViewer(editExternalApp.getText().isEmpty() ? null : Paths.get(editExternalApp.getText()));
    }

    class FileChooseAction extends AbstractAction {
        public FileChooseAction() {
            super("...", null);

            putValue(Action.SHORT_DESCRIPTION, LocaleTool.get("choose_external_application"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            SimpleDialog.chooseFile(
                    Paths.get(editExternalApp.getText()),
                    LocaleTool.get("choose_external_application"),
                    d -> editExternalApp.setText(d.getPath().toString()),
                    GenericFileFilter.EXE_FILTER
            );
        }
    }
}
