/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.configuration.ui;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.LocaleDisplayWrapper;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.themes.AbstractIconTheme;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.Objects;

@Component
public class GeneralOptionPanel extends AbstractBannerPanel<Configuration> {

    private final SharedConfiguration configuration = SharedConfiguration.getInstance();

    private JCheckBox cbOneInstance;
    private JComboBox<LocaleDisplayWrapper> cbLocale;
    private JComboBox<AbstractIconTheme> cbIconTheme;
    private JCheckBox cbQuitWithoutAsking;
    private JRadioButton rbNoProxy;
    private JRadioButton rbSystemProxy;
    private JRadioButton rbHttpProxy;
    private JTextField editProxyHost;
    private JTextField editProxyPort;
    private JComboBox<String> cbCheckPeriod;
    private JCheckBox cbAskBeforeCheck;

    @PostConstruct
    public void initUI() {
        cbOneInstance = new JCheckBox();
        cbLocale = new JComboBox<>(LocaleTool.getSupportedLocales().stream().map(LocaleDisplayWrapper::new).toArray(LocaleDisplayWrapper[]::new));
        cbIconTheme = new JComboBox<>(IconThemeManager.getSupportedThemes().toArray(new AbstractIconTheme[0]));
        cbQuitWithoutAsking = new JCheckBox();

        rbNoProxy = new JRadioButton(new ProxyModeAction(LocaleTool.get("no_proxy"), "NO_PROXY"));
        rbSystemProxy = new JRadioButton(new ProxyModeAction(LocaleTool.get("use_system_proxy"), "SYS_PROXY"));
        rbHttpProxy = new JRadioButton(new ProxyModeAction(LocaleTool.get("http_proxy"), "USE_PROXY"));
        ButtonGroup btnGrp = new ButtonGroup();
        btnGrp.add(rbNoProxy);
        btnGrp.add(rbSystemProxy);
        btnGrp.add(rbHttpProxy);

        cbCheckPeriod = new JComboBox<>(new String[] {
                LocaleTool.get("never"),
                LocaleTool.get("every_startup"),
                LocaleTool.get("once_every_day"),
                LocaleTool.get("once_every_week"),
                LocaleTool.get("once_every_two_week"),
                LocaleTool.get("once_every_month")});
        cbCheckPeriod.setEditable(false);
        editProxyHost = new JTextField();
        editProxyPort = new JTextField();

        cbAskBeforeCheck = new JCheckBox();

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                .addSection("application", 2)
                .addRow("one_instance", cbOneInstance)
                .addRow("language", cbLocale, 1, 1)
                .addRow("icon_theme_label", cbIconTheme, 1, 1)
                .addRow("quit_without_asking", cbQuitWithoutAsking)

                .addSection("proxy", 2)
                .addRow("web_proxy", rbNoProxy, 1, 1)
                .setColumnOffset(1)
                .addRow(rbSystemProxy, 1, 1)
                .addRow(rbHttpProxy, 1, 1)
                .setColumnOffset(0)
                .addRow("proxy_host", editProxyHost, 1, 1)
                .addRow("proxy_port", editProxyPort, 1, 1)

                .addSection("auto_update", 2)
                .addRow("check_period", cbCheckPeriod, 1, 1)
                .addRow("ask_before_check", cbAskBeforeCheck)

                .addRowSpacer()
                .build();

        add(grid, BorderLayout.CENTER);
    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("preferences-desktop-32.png");
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("general");
    }

    @Override
    public String getDescription() {
        return LocaleTool.get("general_options");
    }

    @Override
    public void bindBean(@NotNull Configuration bean) {
        cbOneInstance.setSelected(configuration.isOneInstance());
        cbLocale.setSelectedItem(new LocaleDisplayWrapper(configuration.getAppLocale()));
        cbIconTheme.setSelectedItem(configuration.getIconTheme());
        cbQuitWithoutAsking.setSelected(configuration.isQuitWithoutAsking());

        switch(configuration.getProxyMode()) {
            case 1:
                rbSystemProxy.setSelected(true);
                break;

            case 2:
                rbHttpProxy.setSelected(true);
                break;

            default:
                rbNoProxy.setSelected(true);
                break;
        }
        editProxyHost.setText(configuration.getProxyHost());
        editProxyPort.setText(Integer.toString(configuration.getProxyPort()));
        updateProxyModeButtons(configuration.getProxyMode() == 2);

        switch(configuration.getAutoUpdatePeriod()) {
            case 0:
                cbCheckPeriod.setSelectedIndex(1);
                break;

            case 1:
                cbCheckPeriod.setSelectedIndex(2);
                break;

            case 7:
                cbCheckPeriod.setSelectedIndex(3);
                break;

            case 14:
                cbCheckPeriod.setSelectedIndex(4);
                break;

            case 30:
                cbCheckPeriod.setSelectedIndex(5);
                break;

            default:
                cbCheckPeriod.setSelectedIndex(0);
                break;

        }
        cbAskBeforeCheck.setSelected(configuration.isAutoUpdateAsk());
    }

    @Override
    public void commit() {
        configuration.setOneInstance(cbOneInstance.isSelected());
        configuration.setAppLocale(((LocaleDisplayWrapper) Objects.requireNonNull(cbLocale.getSelectedItem())).getLocale());
        configuration.setIconTheme((AbstractIconTheme)cbIconTheme.getSelectedItem());
        configuration.setQuitWithoutAsking(cbQuitWithoutAsking.isSelected());

        if(rbNoProxy.isSelected()) {
            configuration.setProxyMode(0);
        } else if(rbSystemProxy.isSelected()) {
            configuration.setProxyMode(1);
        } else if(rbHttpProxy.isSelected()) {
            configuration.setProxyMode(2);
        } else {
            configuration.setProxyMode(0);
        }

        configuration.setProxyHost(editProxyHost.getText());
        try {
            configuration.setProxyPort(Integer.parseInt(editProxyPort.getText()));
        } catch(Exception e) {
            configuration.setProxyPort(0);
        }

        switch(cbCheckPeriod.getSelectedIndex()) {
            case 1:
                configuration.setAutoUpdatePeriod(0);
                break;

            case 2:
                configuration.setAutoUpdatePeriod(1);
                break;

            case 3:
                configuration.setAutoUpdatePeriod(7);
                break;

            case 4:
                configuration.setAutoUpdatePeriod(14);
                break;

            case 5:
                configuration.setAutoUpdatePeriod(30);
                break;

            default:
                configuration.setAutoUpdatePeriod(-1);
                break;

        }
        configuration.setAutoUpdateAsk(cbAskBeforeCheck.isSelected());
    }

    class ProxyModeAction extends AbstractAction {
        public ProxyModeAction(String name, String cmd) {
            super(name);

            putValue(Action.ACTION_COMMAND_KEY, cmd);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            boolean b = e.getActionCommand().equals("USE_PROXY");
            updateProxyModeButtons(b);

        }
    }

    private void updateProxyModeButtons(boolean enabled) {
        editProxyHost.setEnabled(enabled);
        editProxyPort.setEnabled(enabled);
    }
}
