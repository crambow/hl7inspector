/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.configuration.ui;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyChangeListener;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;

import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Font;

@Component
public class EditorViewPanel extends AbstractBannerPanel<Configuration> {

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private JComboBox<Font> cbFont;
    private JTextField edFontSize;
    private JLabel editFontSample;

    @PostConstruct
    public void initUI() {
        cbFont = UIFactory.createFontComboBox();
        cbFont.addActionListener(e -> updateSample());

        edFontSize = new JTextField();
        edFontSize.getDocument().addDocumentListener((SimplifyChangeListener) e -> updateSample());

        editFontSample = new JLabel(LocaleTool.get("text_sample"));

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                .addSection("font")
                .addRow("name", cbFont, 1, 1)
                .addRow("size", edFontSize, 1, 0.3)
                .addRow("example", editFontSample, 1, 1)
                .addRowSpacer()
                .build();

        add(grid, BorderLayout.CENTER);
    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("editor-32.png");
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("editor");
    }

    @Override
    public String getDescription() {
        return null;
    }

    private void updateSample() {
        Font font = (Font)cbFont.getSelectedItem();
        font = font == null ? null : font.deriveFont(NumberUtils.toFloat(edFontSize.getText(), 12));
        editFontSample.setFont(font);
    }

    @Override
    public void bindBean(@NotNull Configuration bean) {
        Font font = configuration.getAppEditorTextFont();
        cbFont.setSelectedItem(font);
        edFontSize.setText(Integer.toString(font.getSize()));
    }

    @Override
    public void commit() {
        Font font = (Font)cbFont.getSelectedItem();
        configuration.setAppEditorTextFont(font == null ? null : font.deriveFont(NumberUtils.toFloat(edFontSize.getText(), 12)));
    }

}
