/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.configuration.ui;

import com.alee.extended.link.WebLink;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.desktop.SettingsUIDescriptor;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.Icon;
import javax.swing.JPanel;
import java.awt.BorderLayout;

@Component
@Scope("prototype")
public class ToolsSummaryPanel extends AbstractBannerPanel<Configuration> {

    private final transient BeanFactory beanFactory;
    private final transient ListableBeanFactory listableBeanFactory;
    private final transient EventMessengerProcessor eventMessengerProcessor;

    public @Autowired ToolsSummaryPanel(BeanFactory beanFactory, ListableBeanFactory listableBeanFactory, EventMessengerProcessor eventMessengerProcessor) {
        this.beanFactory = beanFactory;
        this.listableBeanFactory = listableBeanFactory;
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @PostConstruct
    public void initUI() {
        FormBuilder builder = FormBuilder.createBuilder()
                .noBorder();

        listableBeanFactory
                .getBeansOfType(SettingsUIDescriptor.class)
                .values()
                .stream()
                // TODO Sort by title/name
                //.sorted((d1, d2) -> d1.getComponentClass(). )
                .map(SettingsUIDescriptor::getComponentClass)
                .forEach(c -> builder.addRow(createLink(c), 1, 1));

        JPanel grid = builder
                .addRowSpacer()
                .build();

        add(grid, BorderLayout.CENTER);
    }

    private WebLink createLink(Class<? extends AbstractBannerPanel<Configuration>> clazz) {
        AbstractBannerPanel<Configuration> p = beanFactory.getBean(clazz);

        WebLink link =  new WebLink(p.getTitle());
        link.addActionListener(e -> eventMessengerProcessor.publishEvent(new SelectSettingEvent(this, clazz)));
        return link;
    }

    @Override
    public void bindBean(@NotNull Configuration bean) {
        // noop
    }

    @Override
    public void commit() {
        // noop
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("tools");
    }

    @Override
    public String getDescription() {
        return LocaleTool.get("tools");
    }

}
