/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.configuration.ui;

import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.tree.TreeCellArea;
import com.alee.laf.tree.TreeNodeParameters;
import com.alee.laf.tree.TreeToolTipProvider;
import com.alee.laf.tree.WebTree;
import com.alee.laf.tree.WebTreeCellRenderer;
import com.alee.laf.tree.WebTreeModel;
import com.alee.laf.tree.WebTreeNode;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.bindings.FormDialog;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.ui.themes.SkinExtensionStyles;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.desktop.SettingsUIDescriptor;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.Icon;
import javax.swing.JTree;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope("prototype")
public class OptionsDialog extends FormDialog<Configuration> {

    private final Configuration configuration = Configuration.getInstance();

    private final transient BeanFactory beanFactory;
    private final transient ListableBeanFactory listableBeanFactory;

    private final Map<Class<? extends AbstractBannerPanel<Configuration>>, WebTreeNode<?, AbstractBannerPanel<Configuration>>> classNodesMap = new HashMap<>();

    private WebTree<WebTreeNode<?, AbstractBannerPanel<Configuration>>> tree;
    protected AbstractBannerPanel<Configuration> selectedPanel = null;

    static class GenericWebTreeCellRenderer extends WebTreeCellRenderer<WebTreeNode<?, AbstractBannerPanel<?>>, WebTree<WebTreeNode<?, AbstractBannerPanel<Configuration>>>, TreeNodeParameters<WebTreeNode<?, AbstractBannerPanel<?>>, WebTree<WebTreeNode<?, AbstractBannerPanel<Configuration>>>> > {
        @Override
        protected Icon iconForValue(TreeNodeParameters p) {
            if (p.isLeaf()) {
                return IconThemeManager.getImageIcon("clear.png");
            }

            return IconThemeManager.getImageIcon(p.isExpanded() ? "node-open.png" : "node-close.png");
        }
    }

    public @Autowired OptionsDialog(BeanFactory beanFactory, ListableBeanFactory listableBeanFactory) {
        super(LocaleTool.get("settings"), true);

        this.beanFactory = beanFactory;
        this.listableBeanFactory = listableBeanFactory;
    }

    @PostConstruct
    public void initUI() {
        getBanner().setVisible(false);

        tree = new WebTree<>(SkinExtensionStyles.treeStyleLineNone);
        tree.setRootVisible(false);
        tree.setModel(createModel());
        tree.setCellRenderer(new GenericWebTreeCellRenderer());
        tree.addTreeSelectionListener(e -> select(tree.getNodeForPath(e.getNewLeadSelectionPath()).getUserObject()));
        tree.setToolTipProvider(new TreeToolTipProvider<>() {
            @Override
            protected String getToolTipText(JTree component, TreeCellArea<WebTreeNode<?, AbstractBannerPanel<Configuration>>, JTree> area) {
                return area.getValue(tree).getUserObject().getDescription();
            }
        });
        tree.expandAll();
        tree.selectFirstVisibleLeafNode();

        WebScrollPane scrollPane = new WebScrollPane(tree);
        scrollPane.setPreferredSize(new Dimension(200, 10));

        getContentPane().add(scrollPane, BorderLayout.WEST);


        setSize(1024, 600);
        center(getOwner());
    }

    private WebTreeModel<WebTreeNode<?, ? extends AbstractBannerPanel<Configuration>>> createModel() {
        WebTreeNode<?, ? extends AbstractBannerPanel<Configuration>> root = new WebTreeNode<>(null);
        for(Class<? extends AbstractBannerPanel<Configuration>> clazz : getPanelClasses()) {
            addNode(root, clazz);
        }

        WebTreeNode<?, ? extends AbstractBannerPanel<Configuration>> toolsNode = classNodesMap.get(ToolsSummaryPanel.class);
        listableBeanFactory
                .getBeansOfType(SettingsUIDescriptor.class)
                .values()
                .stream()
                // TODO Sort by title/name
                //.sorted((d1, d2) -> d1.getComponentClass(). )
                .map(SettingsUIDescriptor::getComponentClass)
                .forEach(c -> addNode(toolsNode, c));

        return new WebTreeModel<>(root);
    }

    private void addNode(WebTreeNode<?, ?> root, Class<? extends AbstractBannerPanel<Configuration>> clazz) {
        AbstractBannerPanel<Configuration> p = beanFactory.getBean(clazz);

        WebTreeNode<?, AbstractBannerPanel<Configuration>> node = new WebTreeNode<>(p);
        root.add(node);

        classNodesMap.put(clazz, node);
    }

    @EventListener
    public void handleSelectSettingEvent(SelectSettingEvent event) {
        select(classNodesMap.get(event.getSettingPanelClass()).getUserObject());
    }

    private void select(AbstractBannerPanel<Configuration> optionPanel) {
        if(!optionPanel.equals(selectedPanel)) {
            for(int i = 0; i < getContentPane().getComponentCount(); i++) {
                if(getContentPane().getComponent(i) instanceof AbstractBannerPanel) {
                    getContentPane().remove(i);
                }
            }

            selectedPanel = optionPanel;

            getContentPane().add(selectedPanel, BorderLayout.CENTER);
            tree.setSelectedNode(classNodesMap.get(selectedPanel.getClass()));

            selectedPanel.revalidate();
            selectedPanel.repaint();
        }
    }

    protected List<Class<? extends AbstractBannerPanel<Configuration>>> getPanelClasses() {
        return List.of(
                GeneralOptionPanel.class,
                TreeViewPanel.class,
                EditorViewPanel.class,
                ExternalToolsPanel.class,
                ToolsSummaryPanel.class);
    }

    @Override
    public boolean ask() {
        bindBean(Configuration.getInstance());

        boolean result = super.ask();

        if (result && configuration.isRestartRequired()) {
            SimpleDialog.info(LocaleTool.get("restart_required"));
        }

        return result;
    }

    public void bindBean(@NotNull Configuration bean) {
        classNodesMap.values().forEach(n -> n.getUserObject().bindBean(bean));
    }

    public void commit() {
        classNodesMap.values().forEach(n -> n.getUserObject().commit());
    }
}
