/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.configuration.ui;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;

import org.jetbrains.annotations.NotNull;
import org.pf4j.PluginDescriptor;
import org.pf4j.PluginManager;
import org.pf4j.PluginWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

@Component
public class PluginsPanel extends AbstractBannerPanel<Configuration> {

    private JList<PluginDescriptor> list;
    private final transient PluginManager pluginManager;

    public @Autowired PluginsPanel(PluginManager pluginManager) {
        this.pluginManager = pluginManager;
    }

    @PostConstruct
    public void initUI() {

        JButton btAdd = new JButton(LocaleTool.get("add"));
        btAdd.addActionListener(a -> addPlugin());
        JButton btRemove = new JButton(LocaleTool.get("remove"));
        btRemove.addActionListener(a -> removeSelectedPlugin());

        list = new JList<>();
        list.setCellRenderer(new PluginCellRenderer());

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                .addRow(2, btAdd, btRemove)
                .addRow(list, 2, 1, 1, 1)
                .build();

        add(grid, BorderLayout.CENTER);

    }

    @Override
    public void bindBean(@NotNull Configuration bean) {
        DefaultListModel<PluginDescriptor> model = new DefaultListModel<>();
        model.addAll(pluginManager
                .getPlugins()
                .stream()
                .map(PluginWrapper::getDescriptor)
                .collect(Collectors.toList()));

        list.setModel(model);
    }

    @Override
    public void commit() {
        // noop
    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("plugin-32.png");
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("plugins");
    }

    @Override
    public String getDescription() {
        return null;
    }

    private void addPlugin() {
        SimpleDialog.chooseFile(
                null,
                "plugin",
                l -> addPlugin(l.getPath()),
                GenericFileFilter.PLUGIN_FILTER);
    }

    private void addPlugin(Path pluginFile) {
        try {
            Path targetFile = PlatformTool.getUserPluginsFolder().resolve(pluginFile.getFileName());
            Files.copy(pluginFile, targetFile);
        } catch (Exception ex) {
            SimpleDialog.error(ex);
        }
    }

    private void removeSelectedPlugin() {
        pluginManager.deletePlugin(list.getSelectedValue().getPluginId());
    }

}
