/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.configuration.ui;

import com.alee.laf.list.ListCellParameters;
import com.alee.laf.list.WebListCellRenderer;

import org.pf4j.PluginDescriptor;

import javax.swing.BorderFactory;
import javax.swing.JList;
import java.awt.Component;
import java.text.MessageFormat;

public class PluginCellRenderer extends WebListCellRenderer<PluginDescriptor, JList<PluginDescriptor>, ListCellParameters<PluginDescriptor, JList<PluginDescriptor>>> {

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        PluginDescriptor pd = (PluginDescriptor)value;

        String pattern = "<html><b>{0}</b><br><small>{1}</small><br><small>{2}</small></<html>";

        String text = MessageFormat.format(pattern, pd.getPluginDescription(), pd.getVersion(), pd.getPluginId());

        setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setText(text);

        return this;
    }

}
