/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.configuration.ui;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyChangeListener;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;

import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;

@Component
public class TreeViewPanel extends AbstractBannerPanel<Configuration> {

    private final SharedConfiguration configuration = SharedConfiguration.getInstance();

    private JCheckBox btNodeLength;
    private JSpinner btSpinNodeLength;
    private JComboBox<Font> cbFont;
    private JTextField edFontSize;
    private JTextField edTreeMessageNodeFormat;
    private JLabel editFontSample;

    @PostConstruct
    public void initUI() {
        btNodeLength = new JCheckBox(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btSpinNodeLength.setEnabled(btNodeLength.isSelected());
            }
        });

        btSpinNodeLength = new JSpinner();

        edTreeMessageNodeFormat = new JTextField();
        // TODO Set tooltip with valid parameters
        edTreeMessageNodeFormat.addActionListener(e -> {
            // TODO Update sample text field
        });

        JLabel edTreeMessageNodeFormatSample = new JLabel();

        cbFont = UIFactory.createFontComboBox();
        cbFont.addActionListener(e -> updateSample());

        edFontSize = new JTextField();
        edFontSize.getDocument().addDocumentListener((SimplifyChangeListener) e -> updateSample());

        editFontSample = new JLabel(LocaleTool.get("text_sample"));

        JPanel grid = FormBuilder.createBuilder()
                .noBorder()
                .addSection("view_options", 2)
                .addRow("node_text_length_limit", btNodeLength, 1, 0)
                .addRow("length", btSpinNodeLength, 1, 0)
                .addRow("message_node_format", edTreeMessageNodeFormat, 1, 1)
                .addRow("message_node_sample", edTreeMessageNodeFormatSample, 1, 1)
                .addSection("font", 2)
                .addRow("name", cbFont, 1, 1)
                .addRow("size", edFontSize, 1, 0.3)
                .addRow("example", editFontSample, 1, 1)
                .addRowSpacer()
                .build();

        add(grid, BorderLayout.CENTER);
    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("tree-32.png");
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("tree");
    }

    @Override
    public String getDescription() {
        return LocaleTool.get("tree_view_selections");
    }

    private void updateSample() {
        Font font = (Font)cbFont.getSelectedItem();
        font = font == null ? null : font.deriveFont(NumberUtils.toFloat(edFontSize.getText(), 12));
        editFontSample.setFont(font);
    }

    @Override
    public void bindBean(@NotNull Configuration bean) {
        btNodeLength.setSelected(configuration.getTreeNodeLength() != 0);
        edTreeMessageNodeFormat.setText(configuration.getTreeMessageNodeFormat());
        btSpinNodeLength.setValue(configuration.getTreeNodeLength() == 0 ? 200 : configuration.getTreeNodeLength());
        btSpinNodeLength.setEnabled(configuration.getTreeNodeLength() != 0);
        cbFont.setSelectedItem(configuration.getAppEditorTreeFont());
        edFontSize.setText(Integer.toString(configuration.getAppEditorTreeFont().getSize()));
    }

    @Override
    public void commit() {
        Font font = (Font)cbFont.getSelectedItem();

        configuration.setTreeNodeLength(btNodeLength.isSelected() ? Integer.parseInt(btSpinNodeLength.getValue().toString()) : 0);
        configuration.setTreeMessageNodeFormat(edTreeMessageNodeFormat.getText());
        configuration.setAppEditorTreeFont(font == null ? null : font.deriveFont(NumberUtils.toFloat(edFontSize.getText(), 12)));
    }
}
