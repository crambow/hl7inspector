/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector;

import com.alee.laf.WebLookAndFeel;
import com.alee.managers.settings.SettingsManager;
import com.alee.managers.style.StyleManager;

import de.elomagic.hl7inspector.app.HL7InspectorSpringConfiguration;
import de.elomagic.hl7inspector.app.desktop.Desktop;
import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.events.ApplicationStartedEvent;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.SplashscreenFrame;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.ui.themes.SkinDarkExtension;
import de.elomagic.hl7inspector.platform.ui.themes.SkinLightExtension;
import de.elomagic.hl7inspector.platform.updatecheck.UpdateCheckerService;
import de.elomagic.hl7inspector.platform.utils.InstanceManagerThread;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.spps.bc.SimpleCrypt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.NotNull;
import org.pf4j.PluginManager;
import org.pf4j.update.PluginInfo;
import org.pf4j.update.UpdateManager;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;

import java.awt.EventQueue;
import java.security.Security;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Main class.
 */
public class Hl7Inspector {

    public static final String APPLICATION_NAME = "HL7 Inspector";
    private static final Logger LOGGER = LogManager.getLogger(Hl7Inspector.class);
    private static final SharedConfiguration CONFIGURATION = SharedConfiguration.getInstance();

    /**
     * Creates a new instance of Main.
     */
    private Hl7Inspector() {
    }

    private void initLF() {
        try {
            // Install WebLaF as application LaF
            WebLookAndFeel.install(CONFIGURATION.getIconTheme().getSkin());
            StyleManager.addExtensions(new SkinDarkExtension(), new SkinLightExtension());

            if (PlatformTool.isMacOS()) {
                System.setProperty("apple.laf.useScreenMenuBar", "true");
            }
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
            SimpleDialog.error(e.getMessage(), e);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Hl7Inspector().start(args);
    }

    private void start(String[] args) {

        LOGGER.info("Starting HL7 Inspector {}. Starting arguments={}", ApplicationProperties.getVersion(), args);

        //UIFactory.setDefaultUIFont();
        SimpleCrypt.init();

        // Set i18n
        Locale.setDefault(CONFIGURATION.getAppLocale());

        SplashscreenFrame splashscreenFrame = new SplashscreenFrame();
        splashscreenFrame.setVisible(true);

        Security.addProvider(new BouncyCastleProvider());

        SettingsManager.setDefaultSettingsDirName(PlatformTool.USER_HOME_FOLDER);
        SettingsManager.setSaveOnChangeDelay(1000);

        if(CONFIGURATION.isOneInstance() && InstanceManagerThread.lookupInstance(args)) {
            System.exit(0);
        }

        initLF();

        IconThemeManager.setTheme(CONFIGURATION.getIconTheme());

        var ctx = new SpringApplicationBuilder(
                HL7InspectorSpringConfiguration.class)
                .headless(false)
                .run(args);

        EventQueue.invokeLater(() -> {
            try {
                var pluginManager = ctx.getBean(PluginManager.class);
                var desktop = ctx.getBean(Desktop.class);
                var updateCheckerService= ctx.getBean(UpdateCheckerService.class);
                var instanceManagerThread= ctx.getBean(InstanceManagerThread.class);
                var enviroment = ctx.getBean(Environment.class);

                boolean developerProfileActive = enviroment.acceptsProfiles(Profiles.of("dev"));

                instanceManagerThread.setMainWindow(desktop);

                instanceManagerThread.start();

                desktop.setVisible(true);

                splashscreenFrame.setVisible(false);

                LOGGER.debug("Plugin folders: {}", pluginManager.getPluginsRoots());
                if (!developerProfileActive) {
                    keepSystemUpToDate(pluginManager);
                }

                if (updateCheckerService.nextCheckReached()
                        && (CONFIGURATION.isAutoUpdateAsk() || (SimpleDialog.confirmYesNo(LocaleTool.get("check_for_updates_in_channel_1arg", ApplicationProperties.getChannel())) == 0))) {
                    updateCheckerService.checkAsync();
                } else if (!ApplicationProperties.isMasterRelease()) {
                    SimpleDialog.info(
                            LocaleTool.get("your_running_on_snapshot"),
                            LocaleTool.get(
                                    "please_report_bug",
                                    LocaleTool.get("help"),
                                    LocaleTool.get("open_bug_report_page")));
                }

                ctx.publishEvent(new ApplicationStartedEvent(this, args));
            } catch(Exception e) {
                LOGGER.error(e.getMessage(), e);
                SimpleDialog.error(e.getMessage(), e);
            } finally {
                splashscreenFrame.setVisible(false);
            }
        });
    }

    private void keepSystemUpToDate(@NotNull PluginManager pluginManager) {

        // create update manager
        AtomicInteger i = new AtomicInteger(0);
        UpdateManager updateManager = new UpdateManager(pluginManager);
        CONFIGURATION.getPluginRepositoriesUrls().forEach(u -> updateManager.addRepository(Integer.toString(i.incrementAndGet()), u));

        // >> keep system up-to-date <<
        boolean systemUpToDate = true;

        // check for updates
        if (updateManager.hasUpdates()) {
            List<PluginInfo> updates = updateManager.getUpdates();
            LOGGER.debug("Found {} updates", updates.size());
            for (PluginInfo plugin : updates) {
                LOGGER.debug("Found update for plugin '{}'", plugin.id);
                PluginInfo.PluginRelease lastRelease = updateManager.getLastPluginRelease(plugin.id);
                String lastVersion = lastRelease.version;
                String installedVersion = pluginManager.getPlugin(plugin.id).getDescriptor().getVersion();
                LOGGER.debug("Update plugin '{}' from version {} to version {}", plugin.id, installedVersion, lastVersion);
                boolean updated = updateManager.updatePlugin(plugin.id, lastVersion);
                if (updated) {
                    LOGGER.debug("Updated plugin '{}'", plugin.id);
                } else {
                    LOGGER.error("Cannot update plugin '{}'", plugin.id);
                    systemUpToDate = false;
                }
            }
        } else {
            LOGGER.debug("No updates found");
        }

        /*
        // check for available (new) plugins
        if (updateManager.hasAvailablePlugins()) {
            List<PluginInfo> availablePlugins = updateManager.getAvailablePlugins();
            LOGGER.debug("Found {} available plugins", availablePlugins.size());
            for (PluginInfo plugin : availablePlugins) {
                LOGGER.debug("Found available plugin '{}'", plugin.id);
                PluginInfo.PluginRelease lastRelease = updateManager.getLastPluginRelease(plugin.id);
                String lastVersion = lastRelease.version;
                LOGGER.debug("Install plugin '{}' with version {}", plugin.id, lastVersion);
                boolean installed = updateManager.installPlugin(plugin.id, lastVersion);
                if (installed) {
                    LOGGER.debug("Installed plugin '{}'", plugin.id);
                } else {
                    LOGGER.error("Cannot install plugin '{}'", plugin.id);
                    systemUpToDate = false;
                }
            }
        } else {
            LOGGER.debug("No available plugins found");
        }
         **/

        if (systemUpToDate) {
            LOGGER.debug("System up-to-date");
        }


    }


}
