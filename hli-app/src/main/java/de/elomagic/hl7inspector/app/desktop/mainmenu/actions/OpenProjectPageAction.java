/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.mainmenu.actions;

import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.ui.actions.AbstractThenDoAction;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;

import org.springframework.stereotype.Component;

@Component
public class OpenProjectPageAction extends AbstractThenDoAction {

    public OpenProjectPageAction() {
        setName("visit_project_page_dots");
        setShortDescription("visit_project_page_on_bitbucket");
        setSmallIcon("clear.png");

        thenDo(e -> PlatformTool.browse(ApplicationProperties.getHomepageUrl()));
    }

}
