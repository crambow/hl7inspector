/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop;

import com.alee.extended.dock.SidebarButtonVisibility;
import com.alee.extended.dock.WebDockablePane;
import com.alee.managers.settings.Configuration;

import de.elomagic.hl7inspector.app.desktop.messagelist.MessageListPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MessageBorderPanel extends WebDockablePane {

    private final MessageListPanel messageListPanel;
    private final ViewDetailSplitPanel viewDetailSplitPanel;

    public @Autowired MessageBorderPanel(MessageListPanel messageListPanel, ViewDetailSplitPanel viewDetailSplitPanel) {
        this.messageListPanel = messageListPanel;
        this.viewDetailSplitPanel = viewDetailSplitPanel;
    }

    @PostConstruct
    private void initUI() {
        setSidebarButtonVisibility(SidebarButtonVisibility.always);
        registerSettings(new Configuration("DetailPanel","state"));

        addFrame(messageListPanel);
        setContent(viewDetailSplitPanel);
    }
}
