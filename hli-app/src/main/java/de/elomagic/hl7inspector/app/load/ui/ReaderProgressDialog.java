/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.load.ui;

import com.alee.laf.window.WebDialog;

import de.elomagic.hl7inspector.app.desktop.messagelist.MessageListModel;
import de.elomagic.hl7inspector.app.desktop.messagelist.MessageListPanel;
import de.elomagic.hl7inspector.app.load.MessageImportEvent;
import de.elomagic.hl7inspector.app.load.MessageImportListener;
import de.elomagic.hl7inspector.app.load.MessageImportThread;
import de.elomagic.hl7inspector.app.profiles.validate.ValidatorService;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.ui.CharacterMonitorTabPanel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

@Component
public class ReaderProgressDialog extends WebDialog<ReaderProgressDialog> implements MessageImportListener {

    private static final Logger LOGGER = LogManager.getLogger(ReaderProgressDialog.class);

    private final JLabel lblSource = new JLabel();
    private final JLabel lblSize = new JLabel("?");
    private final JLabel lblMessages = new JLabel("0");
    private final JLabel lblBytes = new JLabel("0");
    private final JPanel buttonPanel = new JPanel(new FlowLayout());
    private final JProgressBar bar = new JProgressBar(SwingConstants.HORIZONTAL);
    private final JButton btAbort = new JButton(LocaleTool.get("abort"));

    private final transient BeanFactory beanFactory;
    private transient Message firstMessage;
    private int messagesAppended;
    private boolean userAbort = false;


    private @Autowired MessageListPanel messageListPanel;

    private @Autowired
    MessageListModel messageListModel;
    @Autowired
    @Qualifier(CharacterMonitorTabPanel.NAME)
    private CharacterMonitorTabPanel inputTrace;

    @Autowired
    public ReaderProgressDialog(BeanFactory beanFactory) {
        super(MyBaseDialog.getFocusedWindow());

        this.beanFactory = beanFactory;
    }

    @PostConstruct
    private void init() {
        setTitle(LocaleTool.get("file_reading_progress"));
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setIconImage(IconThemeManager.getImage("hl7inspector-32.png"));
        setLayout(new BorderLayout());

        lblSource.getFont().deriveFont(Font.BOLD);
        bar.setIndeterminate(true);

        btAbort.addActionListener(e ->  userAbort = true);

        buttonPanel.add(btAbort);

        JPanel panel = FormBuilder.createBuilder()
                .addSection("source", 10)
                .addRow("name", lblSource, 9, 1)
                .addRow("size", lblSize, 9, 1)
                .addRowSpacer(10)

                .addSection("progress", 10)
                .add("message", lblMessages, 1, 0)
                .addRow("size", lblBytes)
                .addRow(bar, 10, 1)
                .addRow(buttonPanel, 10, 1)

                .addRowSpacer()
                .build();

        getContentPane().add(panel, BorderLayout.CENTER);

        pack();

        setSize(400, getPreferredSize() == null ? 230 : getPreferredSize().height);
    }

    private boolean hasToFilter(Message message, ImportOptions options) {
        boolean ignore = false;
        if(!options.getPhrase().isEmpty()) {
            String m = options.isCaseSensitive() ? message.asText() : message.asText().toUpperCase();
            String phrase = options.isCaseSensitive() ? options.getPhrase() : options.getPhrase().toUpperCase();

            if(!options.isUseRegExpr()) {
                boolean found = (m.contains(phrase));
                ignore = ((!found && !options.isNegReg())
                        || found && options.isNegReg());
            } else {
                boolean found = m.matches(phrase);

                ignore = ((!found && !options.isNegReg())
                        || found && options.isNegReg());
            }
        }

        return ignore;
    }

    public void importStarted(MessageImportEvent event) {
        messagesAppended = 0;
    }

    public void importDone(MessageImportEvent event) {
        SwingUtilities.invokeLater(() -> setVisible(false));

        int index = firstMessage == null ? -1 : messageListModel.indexOf(firstMessage);
        if ( index != -1) {
            messageListPanel.getSelectionModel().setSelectionInterval(index, index);
        }

        inputTrace.addLine(LocaleTool.get("import_done"));
        if(!event.getSource().isAlive() && userAbort) {
            SimpleDialog.info(LocaleTool.get("import_abort_by_user"));
        }
    }

    @Override
    public void bytesRead(byte[] data, Charset encoding) {
        inputTrace.addBytes(data, encoding);
    }

    @Override
    public void messageRead(MessageImportEvent event) {
        ImportOptions options = event.getSource().getOptions();

        if (userAbort) {
            event.getSource().requestTermination();
        }

        inputTrace.addLine(LocaleTool.get("catch_message"));
        long bytesRead = event.getBytesRead();

        if(hasToFilter(event.getMessage(), event.getSource().getOptions())) {
            inputTrace.addLine(LocaleTool.get("ignore_message_filter_active"));
        } else {
            Message message = event.getMessage();

            // Have to decouple otherwise strange painting issues can occur ???
            messageListModel.add(message);

            firstMessage = firstMessage == null ? message : firstMessage;

            if(messageListModel.size() < SharedConfiguration.getInstance().getOpenFilesMaximum()) {
                messagesAppended++;
            } else if(!options.isReadBottom()) {
                event.getSource().requestTermination();
            }

            if(options.isValidate()) {
                try {
                    beanFactory.getBean(ValidatorService.class).validate(message);
                } catch(Exception ee) {
                    LOGGER.error(ee.getMessage(), ee);
                }
            }
        }

        SwingUtilities.invokeLater(() -> {
            lblSource.setText(options.getSourceDescription());
            lblSize.setText(Long.toString(options.getFileSize()));
            lblBytes.setText(Long.toString(bytesRead));
            lblMessages.setText(Integer.toString(messagesAppended));
            bar.setValue(messagesAppended);
        });
    }

    public void startImport(@NotNull InputStream in, @NotNull ImportOptions options) {
        // Can not moved into "importStarted" otherwise dialog freeze occur.
        setVisible(true);

        MessageImportThread thread = beanFactory.getBean(MessageImportThread.class);
        thread.setInputStream(new BufferedInputStream(in));
        thread.setOptions(options);
        thread.addListener(this);
        thread.addAfterThreadStartsListener(this::importStarted);
        thread.addBeforeThreadEndListener(this::importDone);
        thread.start();
    }

}
