/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop;

import com.alee.extended.tab.DocumentData;
import com.alee.extended.tab.WebDocumentPane;

import de.elomagic.hl7inspector.app.desktop.messagelist.events.MessageRemovedEvent;
import de.elomagic.hl7inspector.app.editor.events.ActionViewCompressedEvent;
import de.elomagic.hl7inspector.shared.events.MessageEditedEvent;
import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.app.editor.ui.MessageEditorDocument;
import de.elomagic.hl7inspector.shared.hl7.HL7Tool;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.app.profiles.ProfileChangedEvent;
import de.elomagic.hl7inspector.app.profiles.validate.MessageValidatedEvent;
import de.elomagic.hl7inspector.shared.ui.facades.*;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.events.DocumentOpenedEvent;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.events.OpenDocumentEvent;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.awt.*;
import java.util.function.Consumer;

/**
 * Tab container for documents of the center desktop.
 */
@Component
public class DocumentsPanel extends WebDocumentPane<DocumentData<?>> {

    private final transient EventMessengerProcessor eventMessengerProcessor;

    public @Autowired DocumentsPanel(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @PostConstruct
    private void initUI() {
        setPreferredSize(new Dimension(300, 300));
    }

    @EventListener
    public void handleShowDocumentEvent(OpenDocumentEvent e) {
        openDocument(new DocumentData<>(e.getId(), e.getTitle(), e.getComponent()));

        eventMessengerProcessor.publishEvent(new DocumentOpenedEvent(this,e.getId(), e.getComponent(), e.getTitle()));
    }

    @EventListener
    public void handleMessageRemovedEvent(MessageRemovedEvent e) {
        closeDocument(e.getMessage().getId());
    }

    @EventListener
    public void handleMessageEdited(MessageEditedEvent e) {
        Hl7Object<?> o = e.getObject() == null ? HL7Tool.toHL7Object(e.getMessage(), e.getPath()) : e.getObject();

        // May be we have to iterate over all documents or not?
        doWithSelectedComponent(RefreshUIFacade.class, c -> c.refresh(o));
    }

    @EventListener
    public void handleMessageNodeSelectionChangedEvent(MessageNodeSelectionChangedEvent e) {
        // May be we have to iterate over all documents or not?
        doWithSelectedComponent(SelectPathFacade.class, c -> c.selectPath(e.getPath()));
    }

    @EventListener
    public void handleProfileChangedEvent(ProfileChangedEvent event) {
        // May be we have to iterate over all documents or not?
        doWithSelectedComponent(SetProfileFacade.class, c -> c.setProfile(event.getCurrentProfile()));
    }

    @EventListener(MessageValidatedEvent.class)
    public void handleMessageValidatedEvent() {
        doWithSelectedComponent(MessageEditorDocument.class, java.awt.Component::repaint);
    }

    @EventListener
    public void handleActionViewCompressedEvent(ActionViewCompressedEvent e) {
        doWithSelectedComponent(SetCompactTreeViewFacade.class, c -> c.setCompactView(e.getCompressed()));
    }

    @Nullable
    public Configuration.ViewMode getSelectedIndex() {
        return getSelectedDocument() == null ? null : Configuration.ViewMode.valueOf(getSelectedDocument().getId());
    }

    @Nullable
    private java.awt.Component getSelectedDocumentComponent() {
        return  getSelectedDocument() == null ? null : getSelectedDocument().getComponent();
    }

    public boolean isSelectedComponentClass(@NotNull Class<?> clazz) {
        return getSelectedDocumentComponent() != null && clazz.isInstance(getSelectedDocumentComponent());
    }

    /**
     * Return the selected message document as class.
     *
     * @return Returns the document or null when selected document not of type {@link MessageEditorDocument}
     */
    @Nullable
    private <C> C getSelectedComponentAsClass(@NotNull Class<? extends C> clazz) {
        return getSelectedDocument() == null || !isSelectedComponentClass(clazz) ? null : (C)getSelectedDocument().getComponent();
    }

    public void doWithSelectedMessage(@NotNull Consumer<Message> f) {
        MessageEditorDocument messageEditorDocument = getSelectedComponentAsClass(MessageEditorDocument.class);

        if (messageEditorDocument != null) {
            f.accept(messageEditorDocument.getMessage());
        }
    }

    public <C> void doWithSelectedComponent(@NotNull Class<? extends C> clazz, @NotNull Consumer<C> f) {
        C c = getSelectedComponentAsClass(clazz);

        if (c != null) {
            f.accept(c);
        }
    }

}
