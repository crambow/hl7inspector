/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.load;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.MessageMETA;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions;
import de.elomagic.hl7inspector.platform.utils.thread.ControllableThread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Scope("prototype")
public class MessageImportThread extends ControllableThread<MessageImportEvent, MessageImportEvent> {

    private static final Logger LOGGER = LogManager.getLogger(MessageImportThread.class);

    private InputStream in;
    private ImportOptions options;
    private final List<MessageImportListener> listener = new ArrayList<>();
    private final AtomicBoolean terminating = new AtomicBoolean(false);
    private MessageFileInputStream parser;
    private int count = 0;

    @PostConstruct
    private void init() {
        setPriority(MIN_PRIORITY);
    }

    public void addListener(MessageImportListener value) {
        listener.add(value);
    }

    public void removeListener(MessageImportListener value) {
        listener.remove(value);
    }

    /**
     * Set the input stream of messages.
     *
     * This could be a file or a IP stream.
     *
     * @param in Input stream
     */
    public void setInputStream(@NotNull InputStream in) {
        this.in = in;
    }

    public ImportOptions getOptions() {
        return options;
    }

    public void setOptions(@NotNull ImportOptions options) {
        this.options = options;
    }

    @Override
    public void run() {
        fireAfterStartListener(new MessageImportEvent(this, null, 0));
        try {
            parser = new MessageFileInputStream(in, options.getEncoding(), options.getImportMode(), options.getFrame());
            parser.addListener((o, b, e) -> fireBytesRead(b, e));

            // Note: To detect, that the file doesn't contains more then one message, the logic must keep one message
            // till read of end before firing an event otherwise the meta data would going invalid.

            Message previousMessage = null;
            Message message;

            do {
                message = parser.readMessage();
                count++;

                if (previousMessage != null && message != null) {
                    previousMessage.getMeta().setFile(null);
                    previousMessage.getMeta().setEncoding(null);
                    previousMessage.getMeta().setInMemory(true);
                    previousMessage.getMeta().setSourceType(MessageMETA.SourceType.IN_MEMORY);

                    fireMessageReadEvent(previousMessage);
                }

                if(message != null) {
                    message.getMeta().setSourceFile(options.getSourceFile());
                    message.getMeta().setFile(count > 1 ? null : options.getSourceFile());
                    message.getMeta().setEncoding(count > 1 ? null : options.getEncoding());
                    message.getMeta().setInMemory(count > 1 || options.getSourceFile() == null);
                    message.getMeta().setSourceType(count > 1 ? MessageMETA.SourceType.IN_MEMORY : MessageMETA.SourceType.FILE);

                    previousMessage = message;
                }
            } while(!terminating.get() && message != null);

            if (previousMessage != null) {
                fireMessageReadEvent(previousMessage);
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        } finally {
            fireBeforeEndListener(new MessageImportEvent(this, null, parser.getBytesRead()));
        }
    }

    public void requestTermination() {
        terminating.set(true);
    }

    private void fireMessageReadEvent(Message message) {
        try {
            listener.forEach(l -> l.messageRead(new MessageImportEvent(this, message, parser.getBytesRead())));
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private void fireBytesRead(@NotNull byte[] data, @Nullable Charset encoding) {
        listener.forEach(l -> l.bytesRead(data, encoding));
    }

}
