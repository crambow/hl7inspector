/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.editor.ui;

import de.elomagic.hl7inspector.app.desktop.messagelist.actions.OpenFileExternalAction;
import de.elomagic.hl7inspector.app.editor.ui.actions.ViewCompressedAction;
import de.elomagic.hl7inspector.app.save.ui.actions.SaveMessageFileAsAction;
import de.elomagic.hl7inspector.app.ui.MessageEditorPanel;
import de.elomagic.hl7inspector.app.ui.actions.ShowParserWindowAction;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.actions.RevealInExplorer;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyPopupMenuListener;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.hl7inspector.shared.desktop.MessageEditorDescriptor;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.ui.facades.GetCharsetFacade;
import de.elomagic.hl7inspector.shared.ui.facades.RefreshUIFacade;
import de.elomagic.hl7inspector.shared.ui.facades.SelectPathFacade;
import de.elomagic.hl7inspector.shared.ui.facades.SetCompactTreeViewFacade;
import de.elomagic.hl7inspector.shared.ui.facades.SetProfileFacade;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Comparator;

/**
 * Usually used for displaying and editing of HL7 messages in the DesktopPanel class.
 */
@Component
@Scope("prototype")
public class MessageEditorDocument extends JPanel
        implements RefreshUIFacade, SelectPathFacade, SetProfileFacade, GetCharsetFacade, SetCompactTreeViewFacade {

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private final CardLayout cardLayout = new CardLayout();

    private final transient BeanFactory beanFactory;
    private final transient ListableBeanFactory listableBeanFactory;

    private final MessageEditorPanel editor;
    private final HL7Tree tree;

    private transient Message message;

    public @Autowired MessageEditorDocument(BeanFactory beanFactory, ListableBeanFactory listableBeanFactory, @Qualifier("MessageEditorPanel") MessageEditorPanel editor, HL7Tree tree) {
        this.beanFactory = beanFactory;
        this.listableBeanFactory = listableBeanFactory;

        this.editor = editor;
        this.tree = tree;
    }

    @PostConstruct
    private void initUI() {
        setLayout(cardLayout);
        add(editor, Configuration.ViewMode.EDITOR.name());
        add(tree, Configuration.ViewMode.TREE.name());

        editor.setComponentPopupMenu(createPopupMenu());

        tree.setComponentPopupMenu(new JPopupMenu());
        tree.getComponentPopupMenu().addPopupMenuListener((SimplifyPopupMenuListener) e -> rebuildTreePopupMenu(tree.getComponentPopupMenu()));

        setViewMode(configuration.getAppEditorViewMode());
    }

    private JPopupMenu createPopupMenu() {
        JPopupMenu popupMenu = new JPopupMenu();

        rebuildPopupMenu(popupMenu);

        return popupMenu;
    }

    private void rebuildPopupMenu(@NotNull JPopupMenu popupMenu) {
        popupMenu.removeAll();

        popupMenu.add(new GenericAction(e -> setViewMode(Configuration.ViewMode.TREE)).setName("treeview"));
        popupMenu.addSeparator();

        editor.insertUndoRedo(popupMenu);
        popupMenu.addSeparator();
        editor.insertCopyPasteToPopupMenu(popupMenu);
        popupMenu.addSeparator();
        editor.insertFindMenu(popupMenu);

        rebuildCommonPopupMenu(popupMenu);
    }

    private void rebuildTreePopupMenu(@NotNull JPopupMenu popupMenu) {
        SwingUtilities.invokeLater(() -> {
            popupMenu.removeAll();

            JCheckBoxMenuItem miCompactView = new JCheckBoxMenuItem(beanFactory.getBean(ViewCompressedAction.class));
            miCompactView.setSelected(tree.getModel().isCompactView());

            popupMenu.add(new GenericAction(e -> setViewMode(Configuration.ViewMode.EDITOR)).setName("editor_view"));
            popupMenu.add(miCompactView);
            popupMenu.addSeparator();
            editor.insertFindMenu(popupMenu);

            rebuildCommonPopupMenu(popupMenu);

            popupMenu.revalidate();
        });
    }

    private void rebuildCommonPopupMenu(@NotNull JPopupMenu popupMenu) {
        popupMenu.addSeparator();
        popupMenu.add(beanFactory.getBean(SaveMessageFileAsAction.class));
        popupMenu.add(new RevealInExplorer(this::handleShowInFolder));
        popupMenu.add(beanFactory.getBean(OpenFileExternalAction.class));

        popupMenu.addSeparator();

        listableBeanFactory.getBeansOfType(MessageEditorDescriptor.class).values().stream()
                .sorted(Comparator.comparingDouble(MessageEditorDescriptor::getPriority))
                .filter(d -> d.getMessageEditorContextMenuItems() != null)
                .flatMap(d -> d.getMessageEditorContextMenuItems().stream())
                .forEach(popupMenu::add);

        popupMenu.add(beanFactory.getBean(ShowParserWindowAction.class));
    }

    private void handleShowInFolder(ActionEvent e) {
        Path folder = message.getMeta().getFile().getParent();
        if (folder == null) {
            SimpleDialog.info(LocaleTool.get("message_file_doesnt_exists"));
        } else {
            PlatformTool.showFolder(folder);
        }
    }

    public void openFileExternal() {
        if(configuration.getExternalFileViewer() == null) {
            SimpleDialog.info(LocaleTool.get("no_external_file_viewer_editor_set_please_check_your_configuration"));
        } else {
            Path file = message.getMeta().getFile();

            PlatformTool.openFileWith(configuration.getExternalFileViewer(), file);
        }
    }

    private void setViewMode(@NotNull Configuration.ViewMode mode) {
        cardLayout.show(this, mode.name());
    }

    @Override
    public void refresh(Hl7Object<?> o) {
        editor.refresh(o);
        tree.refresh(o);
    }

    @Override
    public void selectPath(@Nullable HL7Path path) {
        if (tree.isVisible()) {
            tree.selectNode(path);
        } else if (editor.isVisible()) {
            editor.selectPath(path);
        }
    }

    @Override
    public void setProfile(Profile profile) {
        tree.setProfile(profile);
    }

    @Override
    public @NotNull Charset getCharset() {
        return message.getMeta().getEncoding();
    }

    public void bindMessage(@Nullable Message message) {
        this.message = message;
        editor.bindMessage(message);
        tree.bindMessage(message);
    }

    public Message getMessage() {
        return message;
    }

    @NotNull
    public HL7Tree getTree() {
        return tree;
    }

    @NotNull
    public HL7TreeModel getModel() {
        return tree.getModel();
    }

    @Override
    public void setCompactView(Boolean compact) {
        tree.setCompactView(compact);
    }

}
