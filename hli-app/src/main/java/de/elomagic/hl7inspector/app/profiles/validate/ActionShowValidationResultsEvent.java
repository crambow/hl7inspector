/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.validate;

import de.elomagic.hl7inspector.app.profiles.validate.ui.ValidationResultsPanel;
import de.elomagic.hl7inspector.platform.events.OpenToolTabDocumentEvent;
import de.elomagic.hl7inspector.shared.profiles.validate.ValidateResult;

import java.util.Collections;
import java.util.List;

public class ActionShowValidationResultsEvent extends OpenToolTabDocumentEvent {

    private final transient List<ValidateResult> validateResults;

    public ActionShowValidationResultsEvent(Object source, List<ValidateResult> validateResults) {
        super(source, ValidationResultsPanel.class);

        this.validateResults = validateResults;
    }

    public List<ValidateResult> getList() {
        return Collections.unmodifiableList(validateResults);
    }

}
