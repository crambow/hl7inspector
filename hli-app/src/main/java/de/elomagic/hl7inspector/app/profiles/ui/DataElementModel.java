/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.shared.profiles.model.DataElement;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataElementModel extends ProfileModel<DataElement> {

    private static final long serialVersionUID = -7858155541327476573L;

    private static final Logger LOGGER = LogManager.getLogger(DataElementModel.class);

    public final void setModel(List<DataElement> list) {
        clear();
        addAll(list);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        DataElement de = getRow(rowIndex);

        switch(columnIndex) {
            case 0:
                return de.getItem();
            case 1:
                return de.getSegment();
            case 2:
                return de.getSequence();
            case 3:
                return de.getDataType();
            case 4:
                return de.getLen();
            case 5:
                return de.getRepeatable();
            case 6:
                return de.getTable();
            case 7:
                return de.getName();
            case 8:
                return de.getChapter();
            default:
                return "";
        }
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int col) {
        switch(col) {
            case 0:
                return LocaleTool.get("id");
            case 1:
                return LocaleTool.get("seg");
            case 2:
                return LocaleTool.get("seq");
            case 3:
                return LocaleTool.get("data_type");
            case 4:
                return LocaleTool.get("length");
            case 5:
                return LocaleTool.get("rpt");
            case 6:
                return LocaleTool.get("table");
            case 7:
                return LocaleTool.get("description");
            case 8:
                return LocaleTool.get("chapter");
            default:
                return "";
        }
    }

    @Override
    public Class<DataElement> getDefaultRowClass() {
        return DataElement.class;
    }

    /**
     * This empty implementation is provided so users don't have to implement this method if their data model is not editable.
     *
     * @param aValue value to assign to cell
     * @param rowIndex row of cell
     * @param columnIndex column of cell
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
            DataElement de = getRow(rowIndex);

            switch(columnIndex) {
                case 0:
                    de.setItem(aValue.toString());
                    break;
                case 1:
                    de.setSegment(aValue.toString());
                    break;
                case 2:
                    de.setSequence(Integer.parseInt(aValue.toString()));
                    break;
                case 3:
                    de.setDataType(aValue.toString());
                    break;
                case 4:
                    de.setLen(Integer.parseInt(aValue.toString()));
                    break;
                case 5:
                    de.setRepeatable(aValue.toString());
                    break;
                case 6:
                    de.setTable(aValue.toString());
                    break;
                case 7:
                    de.setName(aValue.toString());
                    break;
                case 8:
                    de.setChapter(aValue.toString());
                    break;
                default:
                    LOGGER.warn("Column index {} is out of range.", columnIndex);
            }

            fireTableCellUpdated(rowIndex, columnIndex);
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
}
