/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.editor.ui;

import com.alee.laf.tree.TreeNodeParameters;
import com.alee.laf.tree.WebTreeCellRenderer;

import de.elomagic.hl7inspector.app.profiles.MessageDescriptor;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.Html;
import de.elomagic.hl7inspector.platform.utils.StringEscapeUtils;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.hl7.model.EncodingObject;
import de.elomagic.hl7inspector.shared.hl7.model.HL7ObjectType;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.profiles.model.DataElement;
import de.elomagic.hl7inspector.shared.profiles.model.DataTypeItem;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.profiles.model.SegmentItem;
import de.elomagic.hl7inspector.shared.ui.HL7TreeNode;
import de.elomagic.hl7inspector.shared.ui.facades.SetProfileFacade;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;
import javax.swing.JTree;
import java.awt.Color;
import java.awt.Component;

public class HL7ObjectCellRenderer extends WebTreeCellRenderer<HL7TreeNode<?>, JTree, TreeNodeParameters<HL7TreeNode<?>, JTree>> implements SetProfileFacade {

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private String highlightedPhrase;
    private boolean highlightedCaseSensitive;
    private transient Profile profile;

    public void setHighlighted(@Nullable String highlightedPhrase, boolean highlightedCaseSensitive) {
        this.highlightedPhrase = highlightedPhrase;
        this.highlightedCaseSensitive = highlightedCaseSensitive;
    }

    public void setProfile(@Nullable Profile profile) {
        this.profile = profile;
    }

    @Override
    public Component getTreeCellRendererComponent(
            JTree tree,
            Object value,
            boolean isSelected,
            boolean expanded,
            boolean leaf,
            int row,
            boolean hasFocus) {

        Hl7Object<?> obj = (Hl7Object<?>)value;

        if (value == null || obj.toPath() == null) {
            return this;
        }

        StringBuilder sb = new StringBuilder();

        sb.append(createChildPosition(obj));

        String nodeText = obj.toHtmlEscapedString();

        boolean truncate = (configuration.getTreeNodeLength() != 0) && (configuration.getTreeNodeLength() < nodeText.length());

        if(truncate) {
            if(nodeText.length() > configuration.getTreeNodeLength()) {
                nodeText = nodeText.substring(0, configuration.getTreeNodeLength());
            }
            sb.append(nodeText);

            sb.append(Html.createFontElement(Color.magenta));

            sb.append(Html.surroundWithElement("###", "b"));

            sb.append("</font>");
        } else {
            sb.append(nodeText);
        }

        if(StringUtils.isNotBlank(highlightedPhrase)) {
            sb = new StringBuilder(createHighlightedPhrase(sb.toString()));
        }

        // Get node description
        sb.append(createDescription(obj));

        String fontName = configuration.getAppEditorTreeFont().getName();

        if(isSelected) {
            sb.insert(0, "<html><font face=\"" + fontName + "\" color=\"#" + Integer.toHexString(getForeground().getRGB() & 0xffffff) + "\">");
        } else {
            sb.insert(0, "<html><font face=\"" + fontName + "\">");
        }

        sb.append("</font>");
        sb.append("</html>");

        setFont(configuration.getAppEditorTreeFont());
        setText(sb.toString());
        setIcon(createValidationStatusIcon(obj));

        return this;
    }

    private String createChildPosition(Hl7Object<?> obj) {
        StringBuilder sb = new StringBuilder();

        if(!HL7ObjectType.is(obj, HL7ObjectType.SEGMENT) && !HL7ObjectType.is(obj, HL7ObjectType.MESSAGE)) {
            int index;
            if(HL7ObjectType.is(obj, HL7ObjectType.FIELD) && (obj.getHL7Parent().size() < 2)) {
                index = obj.getHL7Parent().getIndex();
            } else if(HL7ObjectType.is(obj, HL7ObjectType.REPETITION_FIELD)) {
                index = obj.getIndex();
            } else {
                index = obj.getIndex() + 1;
            }

            sb.append(Html.createFontElement(Color.orange));

            sb.append("&lt;");
            sb.append(index);
            sb.append("&gt;&nbsp;");

            sb.append("</font>");
        }

        return sb.toString();
    }

    private String createDescription(Hl7Object<?> obj) {
        StringBuilder sb = new StringBuilder();

        HL7Path path = obj.toPath();
        String desc = obj.getDescription();
        if(desc == null && path != null) {
            String segType = path.getSegmentName();

            desc = "";

            int index = obj.getIndex();
            int fieldIndex = index;
            if(HL7ObjectType.is(obj.getHL7Parent(), HL7ObjectType.REPETITION_FIELD)) {
                //    && (o.getHl7Parent().size() > 1))
                fieldIndex = obj.getHL7Parent().getIndex();
            }

            if (profile != null) {
                MessageDescriptor md = new MessageDescriptor(profile);

                if(HL7ObjectType.is(obj, HL7ObjectType.SEGMENT)) {
                    SegmentItem segDef = profile.getSegment(segType);
                    if(segDef != null) {
                        desc = segDef.getDescription();
                    }
                } else if(HL7ObjectType.is(obj, HL7ObjectType.REPETITION_FIELD) || HL7ObjectType.is(obj, HL7ObjectType.FIELD)) {
                    DataElement de = md.getDataElement(path);
                    if(de != null) {
                        desc = "[".concat(de.getDataType()).concat("] ").concat(de.getName());
                    }
                } else if(HL7ObjectType.is(obj, HL7ObjectType.COMPONENT)) {
                    DataElement de = profile.getDataElement(segType, fieldIndex);
                    if(de != null) {
                        DataTypeItem dt = profile.getDataType(de.getDataType(), index);
                        if(dt != null) {
                            desc = "[".concat(dt.getDataType()).concat("] ").concat(dt.getDescription());
                        }
                    }
                } else if(HL7ObjectType.is(obj, HL7ObjectType.SUB_COMPONENT)) {
                    DataElement de = profile.getDataElement(segType, fieldIndex);
                    if(de != null) {
                        int compIndex = obj.getHL7Parent().getIndex() + 1;
                        DataTypeItem dt = profile.getDataType(de.getDataType(), compIndex);
                        if(dt != null) {
                            dt = profile.getDataType(dt.getDataType(), index);
                            if(dt != null) {
                                desc = "[".concat(dt.getDataType()).concat("] ").concat(dt.getDescription());
                            }
                        }
                    }
                }
            }

            desc = StringEscapeUtils.escapeHtml(desc);

            obj.setDescription(desc);
        }

        if(StringUtils.isNotBlank(desc)) {
            sb.append(Html.createFontElement(Color.orange));
            sb.append(" (").append(desc).append(")");
            sb.append("</font>");
        }

        return sb.toString();
    }

    private String createHighlightedPhrase(@NotNull String value) {
        String result = value;

        String phrase = highlightedPhrase;
        String ov = value;
        if(!highlightedCaseSensitive) {
            value = value.toUpperCase();
            phrase = phrase.toUpperCase();
        }

        int i = value.indexOf(phrase);

        if(i != -1) {
            StringBuilder nv = new StringBuilder();
            int oi = 0;

            while(i != -1) {
                String op = ov.substring(i, i + phrase.length());

                nv.append(ov.substring(oi, i));

                nv.append(Html.createFontElement(Color.red));
                nv.append(Html.surroundWithElement(op, "b"));
                nv.append("</font>");
                i = i + phrase.length();
                oi = i;
                i = value.indexOf(phrase, oi);
            }

            result = nv.append(ov.substring(oi)).toString();
        }

        return result;
    }

    private Icon createValidationStatusIcon(@NotNull Hl7Object<?> obj) {
        Icon result;

        if(obj.getValidateStatus() == null) {
            if(HL7ObjectType.is(obj, HL7ObjectType.REPETITION_FIELD) && !obj.isEmpty() && !(obj instanceof EncodingObject)) {
                result = IconThemeManager.getImageIcon("repeatfield.png");
            } else {
                result = IconThemeManager.getImageIcon("hole_white.gif");
            }
        } else {
            result = obj.getValidateStatus().getStatus().getIcon();
        }

        return result;
    }

}
