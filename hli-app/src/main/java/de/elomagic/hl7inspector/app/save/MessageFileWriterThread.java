/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.save;

import de.elomagic.hl7inspector.app.MessageWriterBean;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.io.MLLPMessageOutputStream;
import de.elomagic.hl7inspector.shared.io.MessageFileIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Thread which can write a list of {@link Message} in one or more files.
 */
public class MessageFileWriterThread extends Thread {

    private static final Logger LOGGER = LogManager.getLogger(MessageFileWriterThread.class);

    private final AtomicBoolean terminating = new AtomicBoolean(false);
    private final List<MessageFileWriterListener> listener = new ArrayList<>();
    private final MessageWriterBean bean;
    private final List<Message> messages;

    private int messageIndex = 0;

    public MessageFileWriterThread(@NotNull List<Message> messageList, @NotNull MessageWriterBean mwb) {
        setPriority(MIN_PRIORITY);

        messages = messageList;
        bean = mwb;
    }

    public void addListener(MessageFileWriterListener value) {
        listener.add(value);
    }

    public void removeListener(MessageFileWriterListener value) {
        listener.remove(value);
    }

    @Override
    public void run() {
        try {
            if (bean.isManyFiles()) {
                writeMessageFiles();
            } else {
                writeMessagesFile();
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        } finally {
            fireWriterDoneEvent(messageIndex);
        }
    }

    /**
     * Writes ALL messages with {@link Frame} in ONE file .
     *
     * @throws IOException Thrown when unable to create or write file.
     */
    private void writeMessagesFile() throws IOException {
        try (MLLPMessageOutputStream out = new MLLPMessageOutputStream(Files.newOutputStream(bean.getSingleFileName()), bean.getFrame(), bean.getEncoding())) {
            while (!terminating.get() && messageIndex < messages.size()) {
                Message message = messages.get(messageIndex);
                out.writeMessage(message);

                fireMessageSavedEvent(bean.getSingleFileName(), messageIndex);
                messageIndex++;
            }
        }
    }

    /**
     * Writes one message in one file.
     *
     * @throws IOException Thrown when unable to create or write files.
     */
    private void writeMessageFiles() throws IOException {
        while(!terminating.get() && messageIndex < messages.size()) {
            Message message = messages.get(messageIndex);
            Path file = createDataFile(messageIndex);

            MessageFileIO.writeMessage(message, file);

            if(bean.isGenerateSemaphore()) {
                createSemaphoreFile(messageIndex);
            }

            fireMessageSavedEvent(file, messageIndex);
            messageIndex++;
        }
    }

    public void requestTermination() {
        terminating.set(true);
    }

    protected void fireMessageSavedEvent(Path file, int count) {
        listener.forEach(l -> l.messageSaved(this, file, count));
    }

    protected void fireWriterDoneEvent(int messagesWritten) {
        listener.forEach(l -> l.writerDone(this, messagesWritten));
    }

    private Path createDataFile(int indexName) throws IOException {
        return createIndexFile(indexName, bean.getDataFileExtension());
    }

    private void createSemaphoreFile(int indexName) throws IOException {
        createIndexFile(indexName, bean.getSemaphoreExtension());
    }

    private Path createIndexFile(int indexName, String extension) throws IOException {
        Path file = MessageFileIO.createFilename(indexName, MessageWriterBean.INDEX_DIGITS, bean.getDestinationFolder(), bean.getDataFilePrefix(), "." + extension);

        if(Files.notExists(file)) {
            Files.createFile(file);
        }

        return file;
    }

}
