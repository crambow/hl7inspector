/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop;

import de.elomagic.hl7inspector.app.profiles.ui.DetailPanel;
import de.elomagic.hl7inspector.shared.desktop.ValueEditorDescriptor;

import com.alee.extended.dock.SidebarButtonVisibility;
import com.alee.extended.dock.WebDockablePane;
import com.alee.managers.settings.Configuration;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;

/**
 * Contains at least two expandable panels (Details & Presenter) of the right side of the editor area.
 */
@Component
public class ViewDetailSplitPanel extends WebDockablePane {

    private final DocumentsPanel documentsPanel;
    private final DetailPanel detailPanel;

    private final transient ListableBeanFactory listableBeanFactory;

    public @Autowired ViewDetailSplitPanel(ListableBeanFactory listableBeanFactory, DocumentsPanel documentsPanel, DetailPanel detailPanel) {
        this.listableBeanFactory = listableBeanFactory;

        this.documentsPanel = documentsPanel;
        this.detailPanel = detailPanel;
    }

    @PostConstruct
    private void initUI() {
        setBorder(BorderFactory.createEmptyBorder());
        setSidebarButtonVisibility(SidebarButtonVisibility.always);

        addFrame(detailPanel);

        listableBeanFactory
                .getBeansOfType(ValueEditorDescriptor.class)
                .values()
                .stream()
                .filter(d -> d.getDockableFrame() != null)
                .map(ValueEditorDescriptor::getDockableFrame)
                .forEach(this::addFrame);

        setContent(documentsPanel);

        registerSettings(new Configuration<>("ViewDetailSplitPanel","state"));
    }

}
