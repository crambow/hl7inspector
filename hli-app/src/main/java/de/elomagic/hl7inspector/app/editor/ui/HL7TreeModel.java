/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.editor.ui;

import de.elomagic.hl7inspector.shared.hl7.HL7Tool;
import de.elomagic.hl7inspector.shared.hl7.model.HL7ObjectType;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.ui.HL7TreeNode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

public class HL7TreeModel implements TreeModel {

    private static final Logger LOGGER = LogManager.getLogger(HL7TreeModel.class);

    private Message message;
    private final List<TreeModelListener> listenerList = new ArrayList<>();
    private boolean compactView = true;

    public void bindMessage(@Nullable Message message) {
        this.message = message;
        fireTreeStructureChanged(message);
    }

    public Message getMessage() {
        return getRoot();
    }

    public final void setCompactView(boolean compactView) {
        if(this.compactView != compactView) {
            this.compactView = compactView;
            System.setProperty(HL7TreeNode.COMPRESSED_KEY, compactView ? "t" : "f");

            fireTreeStructureChanged(message);
        }
    }

    public boolean isCompactView() {
        return compactView;
    }

    @Override
    public boolean isLeaf(Object node) {
        return ((Hl7Object<?>)node).isSinglePath();
    }

    @Override
    public int getChildCount(Object parent) {
        return ((HL7TreeNode<?>)parent).getChildCount();
    }

    @Override
    public Object getChild(Object parent, int index) {
        return ((HL7TreeNode<?>)parent).getChildAt(index);
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if (parent == null) {
            return -1;
        }

        Hl7Object<?> po = (Hl7Object<?>)parent;
        Hl7Object<?> co = (Hl7Object<?>)child;
        int result = po.indexOf(co, compactView);

        if(HL7ObjectType.is(parent, HL7ObjectType.SEGMENT)) {
            result--; // Filter segment from fields
        }

        return result;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        // noop
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        listenerList.remove(l);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        listenerList.add(l);
    }

    @Override
    public Message getRoot() {
        return message;
    }

    public void fireTreeNodesInsert(TreePath parentPath, Object[] newNodes) {
        int[] index = new int[newNodes.length];

        for(int i = 0; i < newNodes.length; i++) {
            index[i] = getIndexOfChild(parentPath.getLastPathComponent(), newNodes[i]);
        }

        TreeModelEvent e = new TreeModelEvent(this, parentPath, index, newNodes);

        listenerList.forEach(l -> l.treeNodesInserted(e));
    }

    public void fireTreeStructureChanged(@Nullable TreePath path) {
        if (path == null) {
            return;
        }

        TreeModelEvent e = new TreeModelEvent(this, path);

        LOGGER.trace("fireTreeStructureChanged: {}", path);

        listenerList.forEach(l -> l.treeStructureChanged(e));
    }

    public void fireTreeStructureChanged(@Nullable Hl7Object<?> tn) {
        TreePath path = HL7Tool.toTreePath(tn);

        fireTreeStructureChanged(path);
    }

    public void removeHL7Object(Hl7Object<?> o) {
        o.clear();
        if(HL7ObjectType.is(o.getHL7Parent(), HL7ObjectType.REPETITION_FIELD) && o.getHL7Parent().getChildCount() == 1) {
            o.getHL7Parent().clear();
        }

        if(isCompactView()) {
            TreePath path = HL7Tool.createTreePath(o);
            fireTreeStructureChanged(path.getParentPath());
        }
    }

    public void setHL7ObjectValue(Hl7Object<?> o, String value) {
        o.parse(value);
        fireTreeStructureChanged(o);
    }

}
