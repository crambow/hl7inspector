/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.messagelist;

import de.elomagic.hl7inspector.app.desktop.messagelist.events.MessageRemovedEvent;
import de.elomagic.hl7inspector.app.desktop.messagelist.events.MessagesAddedEvent;
import de.elomagic.hl7inspector.shared.events.MessageEditedEvent;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.ListModel;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MessageListModel extends ListModel<Message> {

    private final transient EventMessengerProcessor eventMessengerProcessor;

    public @Autowired MessageListModel(EventMessengerProcessor eventMessengerProcessor) {
        super(true);

        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @Override
    public boolean remove(Object o) {
        Message message = (Message)o;

        if (message != null) {
            eventMessengerProcessor.publishEvent(new MessageRemovedEvent(this, message));
        }

        return super.remove(o);
    }

    @Override
    public boolean addAll(int index, @NotNull Collection<? extends Message> c) {
        boolean result = super.addAll(index, c);

        eventMessengerProcessor.publishEvent(new MessagesAddedEvent(this, c));

        return result;
    }

    @EventListener
    public void handleMessageEdited(MessageEditedEvent e) {
        int index = indexOf(e.getMessage());
        if (index != -1) {
            fireContentsChanged(e.getSource(), index, index);
        }
    }

}
