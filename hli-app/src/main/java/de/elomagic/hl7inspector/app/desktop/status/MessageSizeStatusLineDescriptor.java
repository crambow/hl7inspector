/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.status;

import de.elomagic.hl7inspector.shared.desktop.StatusLineToolDescriptor;
import de.elomagic.hl7inspector.shared.events.MessageEditedEvent;
import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.ui.UIFactory;

import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.swing.JComponent;
import javax.swing.JLabel;

@Component
public class MessageSizeStatusLineDescriptor implements StatusLineToolDescriptor {

    private JLabel label;

    @Override
    public float getPriority() {
        return 500;
    }

    @Override
    public @NotNull JComponent getComponent() {
        label = UIFactory.createLabel("");
        return label;
    }

    @EventListener
    public void handleMessageNodeSelectionChangedEvent(MessageNodeSelectionChangedEvent e) {
        refreshLabel(e.getMessage());
    }

    @EventListener
    public void handleCursorChanged(MessageEditedEvent e) {
        refreshLabel(e.getMessage());
    }

    private void refreshLabel(@Nullable Message message) {
        int size = message == null ? -1 : message.asText().length();

        label.setText(size == -1 ? "" : FileUtils.byteCountToDisplaySize(size));
    }

}
