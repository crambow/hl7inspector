/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.shared.desktop.StatusLineToolDescriptor;
import de.elomagic.hl7inspector.app.profiles.ProfileChangedEvent;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.swing.JButton;
import javax.swing.JComponent;

@Component
public class ProfileStatusLineDescriptor implements StatusLineToolDescriptor {

    private final BeanFactory beanFactory;

    private JButton profileButton;

    public @Autowired ProfileStatusLineDescriptor(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public float getPriority() {
        return 1000;
    }

    @Override
    @NotNull
    public JComponent getComponent() {
        profileButton = UIFactory.createFlatIconButton(JButton.class, LocaleTool.get("profile_no_profile_selected"));
        profileButton.addActionListener(e -> beanFactory.getBean(ProfileManagerDialog.class).ask());

        return profileButton;
    }

    @EventListener
    public void handleProfileChanged(ProfileChangedEvent e) {
        profileButton.setText(e.isError() ? e.getErrorMessage() : LocaleTool.get("profile_one_var", e.getCurrentProfileFile().getName()).trim());
        profileButton.setToolTipText(e.isError() ? null : e.getCurrentProfileFile().getDescription());
    }

}
