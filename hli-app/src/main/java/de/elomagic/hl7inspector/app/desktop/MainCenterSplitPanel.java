/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop;

import de.elomagic.hl7inspector.platform.ui.components.ToolTabPanel;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyComponentVisibilityListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JSplitPane;
import java.awt.event.ComponentEvent;

@Component
public class MainCenterSplitPanel extends JSplitPane implements SimplifyComponentVisibilityListener {

    private final MessageBorderPanel messageBorderPanel;
    private final ToolTabPanel toolPanel;

    @Autowired
    public MainCenterSplitPanel(MessageBorderPanel messageBorderPanel, ToolTabPanel toolPanel) {
        this.messageBorderPanel = messageBorderPanel;
        this.toolPanel = toolPanel;
    }

    @PostConstruct
    private void initUI() {
        setOrientation(JSplitPane.VERTICAL_SPLIT);
        setResizeWeight(1);
        setDividerSize(2);

        setTopComponent(messageBorderPanel);
        setBottomComponent(toolPanel);

        toolPanel.addComponentListener(this);
    }

    @Override
    public void componentVisibilityChanged(ComponentEvent e) {
        setDividerLocation(0.75);
    }
}
