/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.load.ui.actions;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.TextDroppedEvent;
import de.elomagic.hl7inspector.platform.ui.actions.AbstractThenDoAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;

@Component
public class ImportFromClipboardAction extends AbstractThenDoAction {

    private static final Logger LOGGER = LogManager.getLogger(ImportFromClipboardAction.class);

    public @Autowired ImportFromClipboardAction(EventMessengerProcessor eventMessengerProcessor) {

        setName("import_from_clipboard_dots");
        setShortDescription("import_hl7_messages_from_clipboard");

        thenDo(e -> {
            try {
                String value = (String)Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
                eventMessengerProcessor.publishEvent(new TextDroppedEvent(this, value));
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                SimpleDialog.error(ex.getMessage(), ex);
            }
        });

    }

}
