/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.messagelist.actions;

import de.elomagic.hl7inspector.app.desktop.messagelist.FindMessagesWindow;
import de.elomagic.hl7inspector.platform.ui.actions.AbstractThenDoAction;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

@Component
public class ShowFindMessagesWindowAction extends AbstractThenDoAction {

    private final transient BeanFactory beanFactory;

    public @Autowired ShowFindMessagesWindowAction(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;

        setName("find_in_messages");
        setSmallIcon("find.png");
        setAcceleratorKey(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.SHIFT_DOWN_MASK + (PlatformTool.isMacOS() ? InputEvent.META_DOWN_MASK : InputEvent.CTRL_DOWN_MASK)));

        thenDo(e -> handleShowFindMessageWindowEvent());
    }

    private void handleShowFindMessageWindowEvent() {
        FindMessagesWindow window = beanFactory.getBean(FindMessagesWindow.class);
        window.setVisible(true);

        //window.setLocationRelativeTo ( DemoApplication.getInstance () );
    }

}
