/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles;

import de.elomagic.hl7inspector.shared.profiles.ProfileFile;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;

import org.springframework.context.ApplicationEvent;

public class ProfileChangedEvent extends ApplicationEvent {

    private final transient Profile currentProfile;
    private final ProfileFile currentProfileFile;
    private boolean error;
    private String errorMessage;

    public ProfileChangedEvent(Object source, Profile currentProfile, ProfileFile currentProfileFile) {
        super(source);

        this.currentProfile = currentProfile;
        this.currentProfileFile = currentProfileFile;
    }

    public ProfileChangedEvent(Object source, ProfileFile currentProfileFile, String errorMessage) {
        super(source);

        this.currentProfile = null;
        this.currentProfileFile = currentProfileFile;
        this.error = true;
        this.errorMessage = errorMessage;
    }

    public Profile getCurrentProfile() {
        return currentProfile;
    }

    public ProfileFile getCurrentProfileFile() {
        return currentProfileFile;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
