/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.load.ui.actions;

import de.elomagic.hl7inspector.app.desktop.messagelist.MessageListPanel;
import de.elomagic.hl7inspector.platform.ui.actions.AbstractThenDoAction;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

@Component
public class OpenMessageFileAction extends AbstractThenDoAction {

    public @Autowired
    OpenMessageFileAction(MessageListPanel messageListPanel) {
        setName("file_open_dots");
        setShortDescription("load_hl7_messages_from_file");
        setSmallIcon("document-open.png");
        setAcceleratorKey(KeyStroke.getKeyStroke(KeyEvent.VK_O, PlatformTool.isMacOS() ? InputEvent.META_DOWN_MASK : InputEvent.CTRL_DOWN_MASK));

        thenDo(e -> messageListPanel.openFile());
    }

}
