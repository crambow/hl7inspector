/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.editor.ui;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import de.elomagic.hl7inspector.shared.hl7.HL7Tool;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;

import org.jetbrains.annotations.NotNull;

public class TreeNodeSearchEngine {

    private TreeNodeSearchEngine() {
    }

    public static TreePath findNextNode(@NotNull String phrase, boolean caseSensitive, @NotNull Hl7Object<?> sNode) {
        TreePath result = findNode(phrase, caseSensitive, sNode, 0);

        if(result == null) {
            result = findNode2(phrase, caseSensitive, sNode);
        }

        return result;
    }

    private static TreePath findNode(@NotNull String phrase, boolean caseSensitive, @NotNull Hl7Object<?> sNode, int index) {

        TreePath result = null;

        String t = sNode.asText();

        if(!caseSensitive) {
            t = t.toUpperCase();
            phrase = phrase.toUpperCase();
        }

        if(t.contains(phrase)) {
            for(int i = index; i < sNode.getChildCount() && result == null; i++) {
                Hl7Object<?> node = sNode.getChildAt(i);
                String tt = node.asText();

                if(!caseSensitive) {
                    tt = tt.toUpperCase();
                }

                if(tt.contains(phrase)) {
                    if(node.isLeaf()) {
                        result = findNode(phrase, caseSensitive, node, 0);
                    } else {
                        result = HL7Tool.toTreePath(node);
                    }

                    if(result == null) {
                        result = HL7Tool.toTreePath(node);
                    }
                }
            }
        }
        return result;
    }

    private static TreePath findNode2(@NotNull String phrase, boolean caseSensitive, @NotNull Hl7Object<?> sNode) {

        TreePath result = null;
        TreeNode node = sNode;
        Hl7Object<?> parent = sNode.getHL7Parent();

        while(result == null && parent != null) {
            int idx = parent.getIndex(node);
            String t = parent.asText();

            if(!caseSensitive) {
                t = t.toUpperCase();
                phrase = phrase.toUpperCase();
            }

            if(t.contains(phrase)) {
                result = findNode(phrase, caseSensitive, parent, idx + 1);
            }

            if(result == null) {
                node = parent;
                parent = parent.getHL7Parent();
            }
        }
        return result;
    }
}
