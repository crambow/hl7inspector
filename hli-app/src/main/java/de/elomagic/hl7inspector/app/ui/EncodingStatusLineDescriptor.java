/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.ui;

import com.alee.laf.combobox.WebComboBox;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.app.desktop.DocumentsPanel;
import de.elomagic.hl7inspector.app.desktop.messagelist.MessageListPanel;
import de.elomagic.hl7inspector.platform.events.DocumentOpenedEvent;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.desktop.StatusLineToolDescriptor;
import de.elomagic.hl7inspector.shared.ui.facades.GetCharsetFacade;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.swing.JComponent;
import java.nio.charset.Charset;

@Component
public class EncodingStatusLineDescriptor implements StatusLineToolDescriptor {

    private final BeanFactory beanFactory;

    private WebComboBox cbEncoding;

    public @Autowired EncodingStatusLineDescriptor(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public float getPriority() {
        return 900;
    }

    @Override
    public @NotNull JComponent getComponent() {
        cbEncoding = UIFactory.createCharsetComboBox();
        cbEncoding.setStyleId(StyleId.comboboxUndecorated);
        cbEncoding.addActionListener(e -> changeEncoding((Charset)cbEncoding.getSelectedItem()));

        return cbEncoding;
    }

    private void changeEncoding(Charset charset) {
        DocumentsPanel documentsPanel = beanFactory.getBean(DocumentsPanel.class);
        documentsPanel.doWithSelectedMessage(m -> {
            if (charset != m.getMeta().getEncoding()) {
                m.getMeta().setEncoding(charset);
                if (SimpleDialog.confirmYesNo(LocaleTool.get("reload_message_file")) == SimpleDialog.YES_OPTION) {
                    beanFactory.getBean(MessageListPanel.class).reloadMessage(m);
                }
            }
        });
    }

    @EventListener(DocumentOpenedEvent.class)
    public void handleDocumentOpenedEvent() {
        DocumentsPanel documentsPanel = beanFactory.getBean(DocumentsPanel.class);
        documentsPanel.doWithSelectedComponent(GetCharsetFacade.class, c -> cbEncoding.setSelectedItem(c.getCharset()));
        cbEncoding.setVisible(documentsPanel.isSelectedComponentClass(GetCharsetFacade.class));
    }
}
