/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles;

import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.events.BeforeWriteSettingsEvent;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.io.gson.GsonTool;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.configuration.AppConfigurationKey;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.shared.profiles.ProfileFile;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.services.ProfileService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.swing.DefaultListModel;
import javax.xml.bind.JAXB;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Application scoped service bean.
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ProfileServiceImpl implements ProfileService, Serializable {

    private enum Format {
        XML,
        JSON
    }

    private static final Logger LOGGER = LogManager.getLogger(ProfileServiceImpl.class);

    private final transient Configuration configuration = Configuration.getInstance();
    private final transient EventMessengerProcessor eventMessengerProcessor;

    private DefaultListModel<ProfileFile> profileModel;

    private transient Profile currentProfile;

    public @Autowired
    ProfileServiceImpl(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @Nullable
    public Profile getDefault() {
        return currentProfile;
    }

    @Nullable
    private ProfileFile getCurrentProfile() {
        try {
            Path file = configuration.getDefaultProfile();

            return file == null ? null : new ProfileFile(file);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        LOGGER.info("Initialize profile service.");
        init();
    }

    /**
     * Loads startup defaults.
     */
    public void init() {
        setCurrentProfile(getCurrentProfile());
    }

    public void setCurrentProfile(@Nullable ProfileFile file) {
        if(file == null || !file.exists()) {
            ProfileChangedEvent event = new ProfileChangedEvent(
                    this,
                    file,
                    LocaleTool.get("profile_not_found"));
            eventMessengerProcessor.publishEvent(event);
            return;
        }

        try {
            currentProfile = getProfile(file.getPath());

            // Recreate class including name and description
            file = new ProfileFile(file.getPath(), currentProfile.getName(), currentProfile.getDescription());

            ProfileChangedEvent event = new ProfileChangedEvent(this, currentProfile, file);
            configuration.setDefaultProfile(file.getPath());
            configuration.save();

            eventMessengerProcessor.publishEvent(event);
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);

            ProfileChangedEvent event = new ProfileChangedEvent(
                    this,
                    file,
                    LocaleTool.get("unable_to_load_default_profile"));
            eventMessengerProcessor.publishEvent(event);
        }
    }

    /**
     * Loads or returns current profile.
     *
     * @param file File of the profile
     * @return Returns profile but never null
     * @throws IOException Thrown when loading of profile failed
     */
    @NotNull
    public Profile getProfile(@NotNull Path file) throws IOException {
        // TODO When file same as already loaded then return these one
        LOGGER.debug("Reading profile '{}'...", file);
        try (InputStream in = Files.newInputStream(file, StandardOpenOption.READ)) {
            return getProfile(in);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    @NotNull
    private Profile getProfile(@NotNull InputStream in) throws IOException {
        try {
            InputStream fin = new BufferedInputStream(in);
            byte[] prefixBuffer = new byte[2];
            fin.mark(2);
            fin.read(prefixBuffer);
            fin.reset();

            String prefix = new String(prefixBuffer);

            boolean utf = false;
            Format format = Format.XML;

            if (prefix.equals("PK")) {
                ZipInputStream zin = new ZipInputStream(fin);
                ZipEntry entry = zin.getNextEntry();
                format = entry.getName().toLowerCase().endsWith(".json") ? Format.JSON : Format.XML;
                utf = entry.getName().toLowerCase().endsWith(".xml") || format == Format.JSON;
                fin = zin;
            }

            LOGGER.debug("Detected profile format {}.", format);

            try (InputStreamReader reader = utf ? new InputStreamReader(fin, StandardCharsets.UTF_8) : new InputStreamReader(fin)) {
                if (format == Format.JSON) {
                    return GsonTool.read(reader, Profile.class);
                } else {
                    return JAXB.unmarshal(reader, Profile.class);
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    public void save(Path file, Profile profile) throws IOException {
        LOGGER.debug("Writing profile '{}'...", file);
        try (OutputStream out = Files.newOutputStream(file, StandardOpenOption.CREATE)) {
            save(out, profile);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }

        if(getCurrentProfile() != null && file.equals(getCurrentProfile().getPath())) {
            setCurrentProfile(getCurrentProfile());
        }
    }

    private void save(OutputStream out, Profile profile) throws IOException {
        BufferedOutputStream bout = new BufferedOutputStream(out);
        ZipOutputStream zout = new ZipOutputStream(bout);
        zout.putNextEntry(new ZipEntry("profile-data.json"));
        zout.setComment("Generated by HL7 Inspector Version " + ApplicationProperties.getVersion());

        try {
            GsonTool.write(profile, zout);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }

        profile.reindex();
    }

    public DefaultListModel<ProfileFile> getProfileModel() {

        if (profileModel == null) {
            try {
                profileModel = new DefaultListModel<>();

                int i = 0;
                while (i < 999) {
                    String filename = configuration.getValueOfKeyWithPrefix(AppConfigurationKey.APP_PROFILES_AVAILABLE_FILES, i);

                    if (filename != null) {
                        ProfileFile file = new ProfileFile(Paths.get(filename));
                        file.setName(configuration.getValueOfKeyWithPrefix(AppConfigurationKey.APP_PROFILES_AVAILABLE_NAMES, i));
                        file.setDescription(configuration.getValueOfKeyWithPrefix(AppConfigurationKey.APP_PROFILES_AVAILABLE_DESCRIPTIONS, i));

                        profileModel.addElement(file);
                    } else {
                        break;
                    }

                    i++;
                }
            } catch(Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        return profileModel;
    }

    @EventListener(BeforeWriteSettingsEvent.class)
    public void handleBeforeWriteSettingsEvent() {
        configuration.removeKeysWithPrefix(AppConfigurationKey.APP_PROFILES_AVAILABLE_FILES);
        configuration.removeKeysWithPrefix(AppConfigurationKey.APP_PROFILES_AVAILABLE_NAMES);
        configuration.removeKeysWithPrefix(AppConfigurationKey.APP_PROFILES_AVAILABLE_DESCRIPTIONS);

        for(int i = 0; i < profileModel.size(); i++) {
            ProfileFile profile = profileModel.get(i);

            configuration.setProperty(AppConfigurationKey.APP_PROFILES_AVAILABLE_FILES, i, profile.getAbsolutePath());
            configuration.setProperty(AppConfigurationKey.APP_PROFILES_AVAILABLE_NAMES, i, profile.getName());
            configuration.setProperty(AppConfigurationKey.APP_PROFILES_AVAILABLE_DESCRIPTIONS, i, profile.getDescription());
        }
    }
}
