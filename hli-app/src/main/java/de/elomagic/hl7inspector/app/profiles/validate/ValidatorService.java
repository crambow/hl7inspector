/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.validate;

import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.app.profiles.PrimitiveTypes;
import de.elomagic.hl7inspector.app.profiles.ProfileServiceImpl;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.hl7.model.Field;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.RepetitionField;
import de.elomagic.hl7inspector.shared.hl7.model.Segment;
import de.elomagic.hl7inspector.shared.hl7.model.Subcomponent;
import de.elomagic.hl7inspector.shared.profiles.model.DataElement;
import de.elomagic.hl7inspector.shared.profiles.model.DataTypeItem;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.profiles.model.SegmentItem;
import de.elomagic.hl7inspector.shared.profiles.model.ValidateStatus;
import de.elomagic.hl7inspector.shared.profiles.validate.ValidateResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ValidatorService {

    private static final Logger LOGGER = LogManager.getLogger(ValidatorService.class);

    private static final String REGEX_TIME = "^((2[0-3]|[0-1][0-9]){1}(([0-5][0-9]){0,2})(((\\.[0-9])[0-9]{0,3}))?((\\+|\\-)(2[0-3]|[0-1][0-9])([0-5][0-9]))?)?$";

    private final EventMessengerProcessor eventMessengerProcessor;
    private final ProfileServiceImpl profileService;

    public @Autowired ValidatorService(EventMessengerProcessor eventMessengerProcessor, ProfileServiceImpl profileService) {
        this.eventMessengerProcessor = eventMessengerProcessor;
        this.profileService = profileService;
    }

    public void validate(@NotNull Message...messages) {
        Profile profile = profileService.getDefault();
        if (profile == null) {
            LOGGER.warn("No profile has been set. Unable to validate message(s).");
            return;
        }

        for (Message message : messages) {
            validateMessage(profile, message);
        }
    }

    private void validateMessage(@NotNull Profile profile, @NotNull Message message) {
        List<ValidateResult> statusList = new ArrayList<>();

        message.forEach(s -> statusList.addAll(validateSegment(profile, s)));

        message.setValidateStatus(aggregateStatus(statusList));
        message.setValidationText(buildStatusText(statusList));

        eventMessengerProcessor.publishEvent(new MessageValidatedEvent(this, message, statusList));
        eventMessengerProcessor.publishEvent(new ActionShowValidationResultsEvent(this, statusList));
    }

    @NotNull
    private List<ValidateResult> validateSegment(@NotNull Profile profile, @NotNull Segment segment) {
        List<ValidateResult> statusList = new ArrayList<>();

        String segmentName = segment.get(0).asText();
        SegmentItem si = profile.getSegment(segmentName);
        if(si == null) {
            statusList.add(new ValidateResult(profile.getValidateMapper().getMapDefNotFound(), segment.toPath(), LocaleTool.get("unknown_segment")));
        }

        for(int i = 1; i < segment.size(); i++) {
            statusList.addAll(validateRepetitionField(profile, segment.get(i)));
        }

        segment.setValidateStatus(aggregateStatus(statusList));
        segment.setValidationText(buildStatusText(statusList));

        return statusList;
    }

    @NotNull
    private List<ValidateResult> validateRepetitionField(@NotNull Profile profile, @NotNull RepetitionField repetitionField) {
        List<ValidateResult> statusList = new ArrayList<>();

        DataElement dataElement = getDataElement(profile, repetitionField);

        if(dataElement == null) {
            statusList.add(new ValidateResult(profile.getValidateMapper().getMapDefNotFound(), repetitionField.toPath(), LocaleTool.get("unknown_field")));
        } else {
            if(dataElement.getRepeatableCount() < repetitionField.size()) {
                statusList.add(new ValidateResult(profile.getValidateMapper().getMapRepetition(), repetitionField.toPath(), LocaleTool.get("repetition_count_too_large")));
            }

            repetitionField.forEach(field -> statusList.addAll(validateField(profile, field, dataElement)));
        }

        repetitionField.setValidateStatus(aggregateStatus(statusList));
        repetitionField.setValidationText(buildStatusText(statusList));

        return statusList;
    }

    @NotNull
    private List<ValidateResult> validateField(@NotNull Profile profile, @NotNull Field field, @NotNull DataElement dataElement) {
        List<ValidateResult> validateResults = new ArrayList<>();

        validateResults.addAll(validateLength(profile, field, dataElement.getLen()));
        validateResults.addAll(validateDataType(profile, field, dataElement));

        field.setValidateStatus(aggregateStatus(validateResults));
        field.setValidationText(buildStatusText(validateResults));

        return validateResults;
    }

    @NotNull
    private List<ValidateResult> validateDataType(@NotNull Profile profile, @NotNull Hl7Object<?> obj, DataElement dataElement) {
        List<ValidateResult> validateResults = new ArrayList<>();

        if (PrimitiveTypes.isPrimitive(dataElement.getDataType())) {
            if (obj.size() > 1) {
                // Violation because a primitive datatype must not have any child's
                validateResults.add(new ValidateResult(profile.getValidateMapper().getMapLength(), obj.toPath(), LocaleTool.get("invalid_primitive_data_type_1arg", dataElement.getDataType())));
            } else {
                // Validate Primitive datatype
                validateResults.addAll(validatePrimitive(profile, dataElement, obj));
            }
        } else {
            for(int i = 0; i < obj.size(); i++) {
                try {
                    Hl7Object<?> child = obj.get(i);

                    DataTypeItem dt = profile.getDataType(dataElement.getDataType(),i + 1);
                    if (dt == null) {
                        validateResults.add(new ValidateResult(profile.getValidateMapper().getMapDefNotFound(), child.toPath(), LocaleTool.get("invalid_data_type_object")));
                    } else {
                        validateResults.addAll(validateDataType(profile, child, dataElement));
                    }
                } catch (Exception ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }

        obj.setValidateStatus(aggregateStatus(validateResults));

        return validateResults;
    }

    @Nullable
    private DataElement getDataElement(@NotNull Profile profile, @NotNull RepetitionField repetitionField) {
        String segName = repetitionField.getHL7Parent().get(0).asText();

        return profile.getDataElement(segName, repetitionField.getIndex());
    }

    @NotNull
    private ValidateResult aggregateStatus(@NotNull List<ValidateResult> statusList) {
        ValidateResult result = new ValidateResult(ValidateStatus.OK);
        for (ValidateResult status : statusList) {
            result = result.getStatus().compareTo(status.getStatus()) > 0 ? result : status;
        }
        return result;
    }

    @Nullable
    private String buildStatusText(@NotNull List<ValidateResult> statusList) {
        String text = "";
        for (ValidateResult status : statusList) {
            HL7Path path = status.getPath();
            String p = path == null ? "" : (path.asText() + ": ");

            text = text.concat("\n".concat(p).concat(status.getText()));
        }

        return text.trim().isEmpty() ? null : text.trim();
    }

    @NotNull
    private ValidateResult createInvalidFormatResult(@NotNull Profile profile, @NotNull Hl7Object<?> obj, @NotNull String dataType) {
        return new ValidateResult(profile.getValidateMapper().getInvalidFormat(), obj.toPath(), LocaleTool.get("invalid_format_for_datatype_1arg", dataType));
    }

    private List<ValidateResult> validatePrimitive(@NotNull Profile profile, @NotNull DataElement dataElement, @NotNull Hl7Object<?> obj) {
        switch (dataElement.getDataType()) {
            case "DT":
                return validateDate(profile, obj);
            case "DTM":
                return validateDateTime(profile, obj);
            case "FT":
                return validateFormattedTextData(profile, obj);
            case "GTS":
                return validateGeneralTimingSpecification(profile, obj);
            case "ID":
            case "IS":
                return validateCodedValue(profile, dataElement, obj);
            case "NM":
                return validateNumeric(profile, obj);
            case "SI":
                return validateSequenceId(profile, obj);
            case "ST":
                return validateStringData(profile, obj);
            case "TM":
                return validateTime(profile, obj);
            case "TX":
                return validateTextData(profile, obj);

            default:
                return Collections.emptyList();
        }
    }

    /**
     * 2.A.21 DT - date
     */
    @NotNull
    List<ValidateResult> validateDate(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        List<ValidateResult> results = new ArrayList<>();

        String value = obj.asText();
        String pattern = "";

        switch (value.length()) {
            case 0:
                break;
            case 4:
                pattern = "yyyy";
                break;
            case 6:
                pattern = "yyyyMM";
                break;
            case 8:
                pattern = "yyyyMMdd";
                break;
            default:
                results.add(createInvalidFormatResult(profile, obj,"DT"));
        }

        if (!pattern.isEmpty() && !GenericValidator.isDate(value, pattern,true)) {
            results.add(createInvalidFormatResult(profile, obj,"DT"));
        }

        return results;
    }

    /**
     * 2.A.22 DTM - date/time.
     */
    @NotNull
    List<ValidateResult> validateDateTime(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        String value = obj.asText();

        // Check date part
        String datePattern = StringUtils.substring("yyyyMMdd",0, value.length());
        String date = StringUtils.substring(value, 0, datePattern.length());
        if (!datePattern.isEmpty() && !GenericValidator.isDate(date, datePattern,true)) {
            return Collections.singletonList(createInvalidFormatResult(profile, obj,"DTM"));
        }

        // Check time part
        Subcomponent subcomponent = new Subcomponent(StringUtils.substring(value,8));
        return validateRegEx(profile, subcomponent, REGEX_TIME, "DTM");
    }

    /**
     * 2.A.31 FT - formatted text data.
     */
    @NotNull
    List<ValidateResult> validateFormattedTextData(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        // Like ST plus formatting instructions.
        return new ArrayList<>(validateLength(profile, obj,65536));
    }

    /**
     * 2.A.32 GTS – general timing specification.
     */
    @NotNull
    List<ValidateResult> validateGeneralTimingSpecification(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        // Like ST
        return new ArrayList<>(validateLength(profile, obj, 199));
    }

    /**
     * 2.A.35 ID - coded value for HL7 defined tables.
     * 2.A.36 IS - coded value for user-defined tables.
     */
    @NotNull
    List<ValidateResult> validateCodedValue(@NotNull Profile profile, @NotNull DataElement dataElement, @NotNull Hl7Object<?> obj) {
        String tableId = dataElement.getTable();
        // Like ST
        // Must an item of an HL7 table
        if (profile.getTableData(tableId, obj.asText()) == null) {
            return Collections.singletonList(new ValidateResult(profile.getValidateMapper().getMapItemMiss(), obj.toPath(), LocaleTool.get("item_in_table_not_found_1arg", tableId)));
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * 2.A.47 NM - numeric.
     */
    @NotNull
    List<ValidateResult> validateNumeric(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        List<ValidateResult> results = new ArrayList<>();

        results.addAll(validateLength(profile, obj, 16));
        results.addAll(validateRegEx(profile, obj, "^[-+]?[0-9]*\\.?[0-9]+$", "NM"));

        return results;
    }

    /**
     * 2.A.69 SI - sequence ID.
     */
    @NotNull
    List<ValidateResult> validateSequenceId(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        return validateRegEx(profile, obj, "^[0-9]{0,4}$", "ID");
    }

    /**
     * 2.A.74 ST - string data.
     */
    @NotNull
    List<ValidateResult> validateStringData(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        List<ValidateResult> results = new ArrayList<>();
        // TODO String data is left justified with trailing blanks optional. Any displayable (printable) ACSII characters
        results.addAll(validateLength(profile, obj, 199));
        return results;
    }

    /**
     * 2.A.75 TM – time.
     */
    @NotNull
    List<ValidateResult> validateTime(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        return validateRegEx(profile, obj, REGEX_TIME, "TM");
    }

    /**
     * 2.A.78 TX - text data.
     */
    @NotNull
    List<ValidateResult> validateTextData(@NotNull Profile profile, @NotNull Hl7Object<?> obj) {
        List<ValidateResult> results = new ArrayList<>();
        // TODO Rule: Trailing spaces should be removed.
        results.addAll(validateLength(profile, obj, 65536));
        return results;
    }

    @NotNull
    private List<ValidateResult> validateLength(@NotNull Profile profile, @NotNull Hl7Object<?> obj, int maxLength) {
        if (obj.asText().length() > maxLength) {
            return Collections.singletonList(new ValidateResult(profile.getValidateMapper().getMapLength(), obj.toPath(), LocaleTool.get("field_length_too_large")));
        } else {
            return Collections.emptyList();
        }
    }

    @NotNull
    private List<ValidateResult> validateRegEx(@NotNull Profile profile, @NotNull Hl7Object<?> obj, @NotNull String regex, @NotNull String dataType) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(obj.asText());
        if (!matcher.find()) {
            return Collections.singletonList(createInvalidFormatResult(profile, obj,dataType));
        } else {
            return Collections.emptyList();
        }
    }

}
