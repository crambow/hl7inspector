/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.status;

import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.shared.desktop.StatusLineToolDescriptor;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

@Component
public class TextStatusLineDescriptor implements StatusLineToolDescriptor {

    private JLabel statusLabel;

    @Override
    public float getPriority() {
        return 400;
    }

    @Override
    public @NotNull JComponent getComponent() {
        statusLabel = UIFactory.createLabel("");
        statusLabel.setHorizontalAlignment(SwingConstants.LEFT);

        return statusLabel;
    }

}
