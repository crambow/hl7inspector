/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.validate.ui;

import com.alee.extended.link.AbstractLinkAction;
import com.alee.extended.link.WebLink;

import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.profiles.model.ValidateStatus;
import de.elomagic.hl7inspector.app.profiles.validate.ActionShowValidationResultsEvent;
import de.elomagic.hl7inspector.shared.profiles.validate.ValidateResult;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.components.AbstractToolTabDocumentPanel;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;

@Component("ValidationResultsPanel")
public class ValidationResultsPanel extends AbstractToolTabDocumentPanel {

    private final JScrollPane scrollPanel = new JScrollPane();

    private final transient EventMessengerProcessor eventMessengerProcessor;

    public @Autowired ValidationResultsPanel(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @PostConstruct
    private void initCharacterMonitorUI() {
        setLayout(new BorderLayout());
        add(scrollPanel, BorderLayout.CENTER);
    }

    @Override
    public @NotNull String getId() {
        return "ValidationResultsPanel";
    }

    @NotNull
    @Override
    public String getTitle() {
        return LocaleTool.get("validation_results");
    }

    @Override
    public ImageIcon getIcon() {
        return IconThemeManager.getImageIcon("spellcheck.png");
    }

    @EventListener
    public void handleActionShowValidationResultsEvent(ActionShowValidationResultsEvent e) {
        FormBuilder formBuilder = FormBuilder.createBuilder()
                .noBorder();

        e.getList().forEach(vr -> {
                    formBuilder.add(new JLabel(vr.getStatus().toString(), vr.getStatus().getIcon(), SwingConstants.LEFT), 1, 0);
                    formBuilder.add(createPathLink(vr.getPath()), 1, 0);
                    formBuilder.addRow(new JLabel(vr.getText()), 1, 1);
                });

        String footer = LocaleTool.get(
                "validation_result_summary",
                aggregate(ValidateStatus.ERROR, e.getList()),
                aggregate(ValidateStatus.WARN, e.getList()),
                aggregate(ValidateStatus.INFO, e.getList()),
                aggregate(ValidateStatus.OK, e.getList()));

        formBuilder
                .addRowSpacer(5)
                .addRow(new JLabel(footer), 3, 1);

        scrollPanel.setViewportView(formBuilder.build());
   }

   @NotNull
   private JComponent createPathLink(@Nullable HL7Path path) {
        if (path == null) {
            return new JLabel("?");
        }

       return new WebLink(path.asText(), new AbstractLinkAction() {
           @Override
           public void linkExecuted(ActionEvent event) {
               eventMessengerProcessor.publishEvent(new MessageNodeSelectionChangedEvent(this, null, path, null));
           }
       });
   }

   private long aggregate(ValidateStatus vs, List<ValidateResult> list) {
        return list.stream()
                .filter(i -> i.getStatus() == vs)
                .count();
   }

}
