/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.manager.panels;

import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.app.profiles.ui.FileImportDialog;
import de.elomagic.hl7inspector.app.profiles.ui.ProfileDefinitionDialog;
import de.elomagic.hl7inspector.app.profiles.ui.ProfileModel;
import de.elomagic.hl7inspector.app.profiles.ui.actions.ImportProfileAction;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.actions.AddItemAction;
import de.elomagic.hl7inspector.platform.ui.actions.DeleteAllItemsAction;
import de.elomagic.hl7inspector.platform.ui.actions.RemoveItemAction;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.components.SortedTableModel;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.List;

@Component
public abstract class ProfileTablePanel<M extends ProfileModel<?>> extends AbstractBannerPanel<Profile> implements TableModelListener {

    private static final Logger LOGGER = LogManager.getLogger(ProfileTablePanel.class);

    private ProfileDefinitionDialog dialog;
    private JTable table;
    private transient M model;

    @Autowired
    private FileImportDialog fileImportDialog;

    @PostConstruct
    private void initUI() {

        this.model = createProfileModel();

        setSize(300, 500);

        SortedTableModel<M> sortedModel = new SortedTableModel<>(model);
        table = new JTable();
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        getTable().setModel(sortedModel);
        sortedModel.setTableHeader(getTable().getTableHeader());

        getModel().addTableModelListener(this);
        JScrollPane scroll = new JScrollPane(table);

        JPanel main = new JPanel(new BorderLayout());
        main.setBorder(new EmptyBorder(6, 0, 0, 0));
        main.add(scroll, BorderLayout.CENTER);
        main.add(createButtonPanel(), BorderLayout.EAST);

        add(main, BorderLayout.CENTER);
    }

    public void setDialog(@NotNull ProfileDefinitionDialog dialog) {
        this.dialog = dialog;
    }

    protected abstract M createProfileModel();

    public JTable getTable() {
        return table;
    }

    public M getModel() {
        return model;
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        try {
            dialog.getPanel(CommonPanel.class).resetValidateStatus();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private JPanel createButtonPanel() {
        return FormBuilder.createBuilder()
                .noBorder()
                .addRow(new JButton(new AddItemAction(this::addItem)),0, 1)
                .addRow(new JButton(new RemoveItemAction(this::deleteItem)),0,1)
                .addRow(new JButton(new DeleteAllItemsAction(this::deleteAllItems)),0,1)
                .addRow(new JButton(new ImportProfileAction(this::importProfile)),0,1)
                .addRowSpacer()
                .build();
    }

    private void addItem(ActionEvent e) {
        try {
            if(table.getModel() instanceof SortedTableModel) {
                SortedTableModel sm = (SortedTableModel)table.getModel();
                if(sm.getTableModel() instanceof ProfileModel) {
                    int i = ((ProfileModel)sm.getTableModel()).addRowObject();

                    Rectangle r = new Rectangle(0, i, 0, i);
                    table.scrollRectToVisible(r);
                }
            }
        } catch(ReflectiveOperationException ee) {
            LOGGER.error(ee.getMessage(), ee);
            SimpleDialog.error(ee.getMessage(), ee);
        }
    }

    private void deleteItem(ActionEvent e) {
        try {
            if(table.getSelectedRow() != -1) {
                if(SimpleDialog.confirmYesNo(LocaleTool.get("are_you_sure")) == 0) {
                    int relativeRow = table.getSelectedRow();
                    int absoluteRow = ((SortedTableModel)table.getModel()).modelIndex(relativeRow);
                    ((ProfileModel)((SortedTableModel)table.getModel()).getTableModel()).deleteRow(absoluteRow);
                }
            } else {
                SimpleDialog.error(LocaleTool.get("no_profile_selected"));
            }
        } catch(Exception ee) {
            LOGGER.error(ee.getMessage(), ee);
            SimpleDialog.error(ee.getMessage(), ee);
        }
    }

    private void deleteAllItems(ActionEvent e) {
        try {
            if(SimpleDialog.confirmYesNo(LocaleTool.get("are_you_sure")) == 0) {
                model.clear();
            }
        } catch(Exception ee) {
            LOGGER.error(ee.getMessage(), ee);
            SimpleDialog.error(ee.getMessage(), ee);
        }
    }

    private void importProfile(ActionEvent e) {
        try {
            fileImportDialog.setModel(model);
            if(fileImportDialog.ask()) {
                List<String> mapList = fileImportDialog.getMappingList();

                model.lock();
                try {
                    model.clear();

                    try (FileReader rin = new FileReader(fileImportDialog.getFile());
                         LineNumberReader lin = new LineNumberReader(rin)) {
                        int l = 0;
                        String line = lin.readLine();

                        while ((line != null)) { // && (l< 10)) {
                            l++;
                            try {
                                if (l >= fileImportDialog.getBeginFromLine()) {
                                    List<String> lineStack = Arrays.asList(StringUtils.split(line, fileImportDialog.getSeparatorChar()));
                                    while (lineStack.size() < mapList.size()) {
                                        lineStack.add(""); // Add dummies
                                    }
                                    int r = model.addRowObject();

                                    for (int q = 0; q < mapList.size(); q++) {
                                        if (!mapList.get(q).equals("-")) {
                                            int c = model.findColumn(mapList.get(q));

                                            model.setValueAt(lineStack.get(q), r, c);
                                        }
                                    }
                                }

                                line = lin.readLine();
                            } catch (ReflectiveOperationException | IOException ee) {
                                String s = LocaleTool.get("error_occur_during_import_line_arg1", l);
                                LOGGER.warn(s, ee);
                                if (SimpleDialog.confirmYesNo(s + " " + LocaleTool.get("continue_question")) == SimpleDialog.NO_OPTION) {
                                    throw ee;
                                }
                            }
                        }
                    }
                } finally {
                    model.unlock();
                }
            }
        } catch(IOException | ReflectiveOperationException ee) {
            LOGGER.error(ee.getMessage(), ee);
            SimpleDialog.error(ee);
        }
    }

    @Override
    public void commit() {
        // Already done by table model. Bad design :-(
    }

}
