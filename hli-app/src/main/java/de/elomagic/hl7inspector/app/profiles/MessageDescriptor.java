/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles;

import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.profiles.model.DataElement;
import de.elomagic.hl7inspector.shared.profiles.model.DataTypeItem;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.profiles.model.SegmentItem;
import de.elomagic.hl7inspector.platform.utils.Html;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.MessageFormat;

public class MessageDescriptor {

    private static final String FONT_PATTERN = "<font face=\"Arial\">{0}</font>";

    private final Profile profile;

    public MessageDescriptor(@NotNull Profile profile) {
        this.profile = profile;
    }

    private SegmentItem getSegmentItem(@NotNull HL7Path path) {
        return profile.getSegment(path.getSegmentName());
    }

    @Nullable
    public DataElement getDataElement(HL7Path path) {
        if (path.getField() == null) {
            return null;
        }

        SegmentItem seg = getSegmentItem(path);

        return seg == null ? null : profile.getDataElement(seg.getId(), path.getField());
    }

    @Nullable
    private DataTypeItem getFieldDataType(@NotNull HL7Path path) {
        if(path.getComponent() == null) {
            return null;
        }

        DataElement de = getDataElement(path);

        return de == null ? null : profile.getDataType(de.getDataType(), path.getComponent());
    }

    @Nullable
    private DataTypeItem getComponentDataType(@NotNull HL7Path path) {
        return path.getSubcomponent() == null ? null : getFieldDataType(path);
    }

    @Nullable
    private DataTypeItem getSubcomponentDataType(@NotNull HL7Path path) {
        DataTypeItem pdt = getComponentDataType(path);

        return pdt == null ? null : profile.getDataType(pdt.getDataType(), path.getSubcomponent());
    }

    @NotNull
    public String getDescription(@Nullable HL7Path path) {
        if (path == null) {
            return "";
        }

        String result;
        if (path.getField() == null) {
            // Only segment seams to be known
            result = createSegmentDescription(path);
        } else {
            result = createFieldDescription(path);
            result += createComponentDescription(path);
            result += createSubcomponentDescription(path);
        }

        return result;
    }

    @NotNull
    private String createSegmentDescription(@NotNull HL7Path path) {
        String result = LocaleTool.get("no_description_in_profile_found");

        SegmentItem seg = getSegmentItem(path);
        if(seg != null) {
            result = Html.surroundWithElement(seg.getId() + " " + seg.getDescription(), "b") + Html.BR;
            result += LocaleTool.get("chapter_one_args", seg.getChapter());
        }

        return MessageFormat.format(FONT_PATTERN, result);
    }

    @NotNull
    private String createFieldDescription(@NotNull HL7Path path) {
        String result = "";

        DataElement de = getDataElement(path);
        if(de != null) {
            result = Html.surroundWithElement(de.getSegment() + "." + de.getSequence() + " " + de.getName(), "b") + Html.BR;
            result += LocaleTool.get("chapter_one_args", de.getChapter().concat(Html.BR));
            result += LocaleTool.get("data_type_arg1", de.getDataType().concat(Html.BR));
            result += LocaleTool.get("data_element_id_1arg", de.getItem().concat(Html.BR));
            result += LocaleTool.get("length_1arg", Integer.toString(de.getLen()).concat(Html.BR));
            result += LocaleTool.get("table_1arg", de.getTable().concat(Html.BR));
            result += LocaleTool.get("repetition_1arg", de.getRepeatable().concat(Html.BR));
        }
        return MessageFormat.format(FONT_PATTERN, result);
    }

    @NotNull
    private String createDataTypeDescription(@Nullable DataTypeItem dataType) {
        if (dataType == null) {
            return "";
        }

        String result = Html.surroundWithElement(dataType.getParentDataType() + "." + dataType.getIndex() + " " + dataType.getDescription(), "b") + Html.BR;
        result += LocaleTool.get("chapter_one_args", dataType.getChapter().concat(Html.BR));
        result += LocaleTool.get("data_type_arg1", dataType.getDataType().concat(Html.BR));
        result += LocaleTool.get("length_1arg", Integer.toString(dataType.getLength()).concat(Html.BR));
        result += LocaleTool.get("table_1arg", dataType.getTable().concat(Html.BR));
        result += LocaleTool.get("optionality_1arg", dataType.getOptionality().concat(Html.BR));

        return Html.HR + MessageFormat.format(FONT_PATTERN, result);
    }

    @NotNull
    private String createComponentDescription(@NotNull HL7Path path) {
        DataTypeItem dt = getComponentDataType(path);
        return createDataTypeDescription(dt);
    }

    @NotNull
    private String createSubcomponentDescription(@NotNull HL7Path path) {
        DataTypeItem dt = getSubcomponentDataType(path);
        return createDataTypeDescription(dt);
    }

}
