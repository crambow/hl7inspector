/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.shared.profiles.model.DataTypeItem;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataTypeModel extends ProfileModel<DataTypeItem> {

    private static final long serialVersionUID = -9155940816412596164L;

    private static final Logger LOGGER = LogManager.getLogger(DataTypeModel.class);

    public final void setModel(List<DataTypeItem> list) {
        clear();
        addAll(list);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        DataTypeItem de = getRow(rowIndex);

        switch(columnIndex) {
            case 0:
                return de.getParentDataType();
            case 1:
                return de.getIndex();
            case 2:
                return de.getDataType();
            case 3:
                return de.getLength();
            case 4:
                return de.getDescription();
            case 5:
                return de.getOptionality();
            case 6:
                return de.getChapter();
            case 7:
                return de.getParentDataTypeName();
            case 8:
                return de.getTable();
            default:
                return "";
        }
    }

    /**
     * This empty implementation is provided so users don't have to implement this method if their data model is not editable.
     *
     *
     * @param aValue value to assign to cell
     * @param rowIndex row of cell
     * @param columnIndex column of cell
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
            DataTypeItem de = getRow(rowIndex);

            switch(columnIndex) {
                case 0:
                    de.setParentDataType(aValue.toString());
                    break;
                case 1:
                    de.setIndex(aValue.toString());
                    break;
                case 2:
                    de.setDataType(aValue.toString());
                    break;
                case 3:
                    de.setLength(aValue.toString());
                    break;
                case 4:
                    de.setDescription(aValue.toString());
                    break;
                case 5:
                    de.setOptionality(aValue.toString());
                    break;
                case 6:
                    de.setChapter(aValue.toString());
                    break;
                case 7:
                    de.setParentDataTypeName(aValue.toString());
                    break;
                case 8:
                    de.setTable(aValue.toString());
                    break;
                default:
                    LOGGER.warn("Column index {} is out of range.", columnIndex);
            }

            fireTableCellUpdated(rowIndex, columnIndex);
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int col) {
        switch(col) {
            case 0:
                return LocaleTool.get("parent_data_type");
            case 1:
                return LocaleTool.get("index");
            case 2:
                return LocaleTool.get("data_type");
            case 3:
                return LocaleTool.get("length");
            case 4:
                return LocaleTool.get("description");
            case 5:
                return LocaleTool.get("opt");
            case 6:
                return LocaleTool.get("chapter");
            case 7:
                return LocaleTool.get("parent_data_type_name");
            case 8:
                return LocaleTool.get("table");
            default:
                return "";
        }
    }

    @Override
    public Class<DataTypeItem> getDefaultRowClass() {
        return DataTypeItem.class;
    }
}
