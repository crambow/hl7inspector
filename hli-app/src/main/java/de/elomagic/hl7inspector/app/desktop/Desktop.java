/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop;

import com.alee.laf.window.WebFrame;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.Hl7Inspector;
import de.elomagic.hl7inspector.app.desktop.mainbar.MainToolBar;
import de.elomagic.hl7inspector.app.desktop.mainmenu.MainMenuBar;
import de.elomagic.hl7inspector.app.desktop.status.StatusPanel;
import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.events.BeforeApplicationExitEvent;
import de.elomagic.hl7inspector.platform.events.BeforeWriteSettingsEvent;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.events.RequestApplicationQuitEvent;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 *
 * Areas (Classes) of the desktop:
 * <p>
 * <pre>
 * ----------------------------------------------------------------------------
 * |                              Main Menu Bar                               |
 * |--------------------------------------------------------------------------|
 * |                              Main Tool Bar                               |
 * |--------------------------------------------------------------------------|
 * |          |                                        |          |           |
 * |          |                                        |          |           |
 * | Message  |                                        |  Detail  |   Value   |
 * |   List   |             Document Panel             |  Panel   | Presenter |
 * |  Panel   |                                        |          |   Frame   |
 * |          |                                        |          |           |
 * |          |                                        |          |           |
 * |------------------------------ Split Panel -------------------------------|
 * |                                                                          |
 * |                             Tool Tab Panel                               |
 * |                                                                          |
 * |--------------------------------------------------------------------------|
 * |                      Find Bar (BorderLayout.NORTH)                       |
 * |--------------------------------------------------------------------------|
 * |                    Status Panel (BorderLayout.SOUTH)                     |
 * ----------------------------------------------------------------------------
 * </pre>
 * @see de.elomagic.hl7inspector.app.desktop
 */
@Component
public class Desktop extends WebFrame<Desktop> {

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private final transient EventMessengerProcessor eventMessengerProcessor;

    private final MainMenuBar mainMenuBar;
    private final MainToolBar mainToolBar;
    private final MainCenterSplitPanel mainCenterSplitPanel;
    private final StatusPanel statusPanel;

    public @Autowired Desktop(EventMessengerProcessor eventMessengerProcessor, MainCenterSplitPanel mainCenterSplitPanel, StatusPanel statusPanel, MainMenuBar mainMenuBar, MainToolBar mainToolBar) {
        super(StyleId.frameDecorated);

        this.eventMessengerProcessor = eventMessengerProcessor;

        this.mainMenuBar = mainMenuBar;
        this.mainToolBar = mainToolBar;
        this.mainCenterSplitPanel = mainCenterSplitPanel;
        this.statusPanel = statusPanel;
    }

    @PostConstruct
    private void init() {
        String s = LocaleTool.get("app_title",
                                        Hl7Inspector.APPLICATION_NAME,
                                        ApplicationProperties.getVersion(),
                                        ApplicationProperties.getChannel(),
                                        ApplicationProperties.getBuildDate() == null ? null : Date.from(ApplicationProperties.getBuildDate().toInstant()));

        setTitle(s);
        setIconImages(ApplicationProperties.getIcons());
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                handleRequestApplicationQuitEvent();
            }
        });

        setJMenuBar(mainMenuBar);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(mainToolBar, configuration.getToolbarPosition().getPosition());
        getContentPane().add(mainCenterSplitPanel, BorderLayout.CENTER);
        getContentPane().add(statusPanel, BorderLayout.SOUTH);

        restoreLastSessionBoundaries();
    }

    private void restoreLastSessionBoundaries() {
        Rectangle rectangle = configuration.getDesktopBoundaries();

        Rectangle bounds = isBoundsVisible(rectangle) ? rectangle : new Rectangle(0, 0, rectangle.width, rectangle.height);

        setBounds(bounds);
    }

    private boolean isBoundsVisible(@NotNull Rectangle boundaries) {
        GraphicsDevice[] graphicsDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();

        return Arrays.stream(graphicsDevices)
                .map(gp -> gp.getDefaultConfiguration().getBounds())
                .anyMatch(r -> SwingUtilities.isRectangleContainingRectangle(r, boundaries));
    }

    @EventListener(BeforeWriteSettingsEvent.class)
    public void handleBeforeWriteSettingsEvent() {
        BorderLayout layout = (BorderLayout)getContentPane().getLayout();
        String o = Objects.toString(layout.getConstraints(mainToolBar), null);

        configuration.setToolbarPosition(Configuration.ToolbarPosition.findByPosition(o));
        configuration.setDesktopBoundaries(getBounds());
    }

    @EventListener(RequestApplicationQuitEvent.class)
    public void handleRequestApplicationQuitEvent() {
        if (configuration.isQuitWithoutAsking()) {
            shutdown();
        }

        SimpleDialog.choose(LocaleTool.get("really_exit_hl7inspector"), SimpleDialog.YES_NO_OPTION, true, e1 -> {
            if (e1.getIndex() == SimpleDialog.YES_OPTION) {
                configuration.setQuitWithoutAsking(e1.isRemember());
                shutdown();
            }
        });
    }

    @EventListener(BeforeApplicationExitEvent.class)
    public void handleBeforeApplicationExitEvent() {
        // TODO Backup loaded file list to reload on next startup
    }

    private void shutdown() {
        eventMessengerProcessor.publishEvent(new BeforeWriteSettingsEvent(this));
        Configuration.getInstance().save();
        eventMessengerProcessor.publishEvent(new BeforeApplicationExitEvent(this));

        System.exit(0);
    }

}
