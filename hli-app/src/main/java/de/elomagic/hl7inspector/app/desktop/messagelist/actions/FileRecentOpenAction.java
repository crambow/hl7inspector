/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.messagelist.actions;

import de.elomagic.hl7inspector.platform.ui.actions.GenericAction;
import de.elomagic.hl7inspector.platform.ui.listener.GenericListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.swing.filechooser.FileSystemView;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileRecentOpenAction extends GenericAction {

    private static final Logger LOGGER = LogManager.getLogger(FileRecentOpenAction.class);

    public FileRecentOpenAction(@NotNull GenericListener listener, Path file) {
        super(listener);

        setName("arg", file.toAbsolutePath().toString());
        putValue(SMALL_ICON, FileSystemView.getFileSystemView().getSystemIcon(file.toFile()));

        try {
            setShortDescription("size_bytes_1arg", Files.size(file));
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

}
