/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.save;

import de.elomagic.hl7inspector.app.MessageWriterBean;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.Segment;
import de.elomagic.hl7inspector.app.ui.FramingSetupDialog;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.bindings.FormDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.BorderLayout;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class SaveDialog extends FormDialog<MessageWriterBean> {

    private static final Logger LOGGER = LogManager.getLogger(SaveDialog.class);

    private final transient Configuration configuration = Configuration.getInstance();

    private final FramingSetupDialog framingSetupDialog = new FramingSetupDialog();

    private Frame frame;
    private transient MessageWriterBean options = new MessageWriterBean();

    private JTextField editDestFolder;
    private JCheckBox btSelected;
    private JRadioButton btManyFiles;
    private JRadioButton btOneFile;
    private JComboBox<Charset> cbCharset;
    private JTextField editPrefix;
    private JComboBox<String> editDataExt;
    private JComboBox<String> editSemaExt;
    private JCheckBox btGenSema;
    private JTextField editFilename;
    private JEditorPane editFrame;

    @Autowired
    public SaveDialog() {
        super(LocaleTool.get("save_messages"), true);
    }

    @PostConstruct
    private void init() {
        getBanner().setVisible(false);

        editDestFolder = new JTextField();
        editDestFolder.setEditable(false);
        editDestFolder.setText(configuration.getAppFilesLastUsedFolder().toString());
        JButton btChooseFolder = new JButton("...");
        btChooseFolder.addActionListener(e -> selectFolder());
        btSelected = new JCheckBox(LocaleTool.get("write_only_selected_messages"));
        btManyFiles = new JRadioButton(LocaleTool.get("many_files"));
        btOneFile = new JRadioButton(LocaleTool.get("one_file"));
        cbCharset = new JComboBox<>(getSupportedCharsets());
        editPrefix = new JTextField();
        editDataExt = new JComboBox<>(new String[] {"hl7", "txt", "dat"});
        editDataExt.setEditable(true);
        editSemaExt = new JComboBox<>(new String[] {"sem", "frg"});
        editSemaExt.setEditable(true);
        btGenSema = new JCheckBox();
        btGenSema.setSelected(true);
        editFilename = new JTextField();
        editFilename.setEditable(false);
        JButton btFilename = new JButton("...");
        btFilename.addActionListener(e -> selectFilename());
        editFrame = new JEditorPane();
        editFrame.setContentType("text/html");
        editFrame.setEditable(false);
        editFrame.setBorder(LineBorder.createGrayLineBorder());
        JButton btFrame = new JButton("...");
        btFrame.addActionListener(e -> {
            framingSetupDialog.ask();
            updateFramePreview();
        });

        ButtonGroup btnGrp = new ButtonGroup();
        btnGrp.add(btManyFiles);
        btnGrp.add(btOneFile);

        JPanel panel = FormBuilder.createBuilder()
                .noBorder()
                .setColumnSize(0, 20)
                .setColumnSize(2, 60)
                .addSection("save_options", 7)
                .add("char_encoding", 2)
                .addRow(cbCharset, 2, 0)
                .add("only_selected", 2)
                .addRow(btSelected, 4, 0)
                .addRow(btManyFiles, 5, 0)
                .setColumnOffset(1)

                .add("folder", editDestFolder, 4, 1)
                .addRow(btChooseFolder, 1, 0)

                .add("prefix", editPrefix, 1, 1)
                .addRow("extension", editDataExt, 1, 0)

                .add("semaphore_file", btGenSema, 1, 0)
                .addRow("extension", editSemaExt, 1, 0)

                .setColumnOffset(0)
                .addRow(btOneFile, 5, 0)
                .setColumnOffset(1)
                .add("filename", editFilename, 4, 1)
                .addRow(btFilename, 1, 0)

                .add("frame", editFrame, 4, 1)
                .addRow(btFrame, 1, 0)
                .addRowSpacer()
                .build();

        add(panel, BorderLayout.CENTER);

        pack();

        setSize(getPreferredSize().width, getPreferredSize().height);

        center(getOwner());
    }

    public void setMessages(@NotNull List<Message> messages) {
        btManyFiles.setSelected(messages.size() != 1);
        btOneFile.setSelected(messages.size() == 1);
        editFilename.setText(createFilePath(messages).toString());
    }

    @Override
    public void bindBean(@NotNull MessageWriterBean bean) {
        this.options = bean;

        // Must be a clone
        frame = new Frame(bean.getFrame().getStartByte(), bean.getFrame().getStopBytes());
        framingSetupDialog.bindBean(frame);

        // TODO Set UI with bean data

        updateFramePreview();
    }

    @Override
    public boolean validateForm() {
        String message = validateOptions();
        if (message != null) {
            SimpleDialog.error(message);
        }

        return message == null;
    }

    @Override
    public void commit() {
        options.setDataFileExtension(editDataExt.getSelectedItem() == null ? "" : editDataExt.getSelectedItem().toString());
        options.setDataFilePrefix(editPrefix.getText());
        options.setDestinationFolder(Paths.get(editDestFolder.getText()));
        options.setFrame(frame);
        options.setGenerateSemaphore(btGenSema.isSelected());
        options.setManyFiles(btManyFiles.isSelected());
        options.setOnlySelectedFiles(btSelected.isSelected());
        options.setSemaphoreExtension(editSemaExt.getSelectedItem() == null ? "" : editSemaExt.getSelectedItem().toString());
        options.setSingleFileName(editFilename.getText().length() == 0 ? null : Paths.get(editFilename.getText()));
        options.setEncoding((Charset)cbCharset.getSelectedItem());

        Path path = options.isManyFiles() ? options.getDestinationFolder() : options.getSingleFileName().getParent();
        configuration.setAppFilesLastUsedFolder(path);
    }

    @Nullable
    private String validateOptions() {
        if(btManyFiles.isSelected()) {
            if(StringUtils.isBlank(editDestFolder.getText())) {
                return LocaleTool.get("destination_folder_not_set");
            }

            Path path = Paths.get(editDestFolder.getText());
            if(Files.notExists(path)) {
                return LocaleTool.get("destination_folder_not_exist");
            }

            String d = editDataExt.getSelectedItem() == null ? "" : editDataExt.getSelectedItem().toString();
            if(StringUtils.isBlank(d)) {
                return LocaleTool.get("invalid_data_file_extension");
            }

            String s = editSemaExt.getSelectedItem() == null ? "" : editSemaExt.getSelectedItem().toString();
            if(btGenSema.isSelected() && StringUtils.isBlank(s)) {
                return LocaleTool.get("invalid_semaphore_file_extension");
            }
        } else {
            Path file = editFilename.getText().length() == 0 ? null : Paths.get(editFilename.getText());
            if(file == null || Files.isDirectory(file)) {
                return LocaleTool.get("invalid_filename");
            }
        }

        return null;
    }

    private Path createFilePath(@NotNull List<Message> messages) {
        Path file = configuration.getAppFilesLastUsedFolder();

        if (messages.size() != 1) {
            file = file.resolve("HL7_message_bundle.hl7");
        } else if (messages.get(0).getMeta().isInMemory()) {
            Segment msh = messages.get(0).getFirstSegment("MSH");

            String messageType = msh == null || msh.size() < 9 ? "unknown" : msh.get(9).asText().replace("^", "_");
            String filename = "HL7_" + messageType + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".hl7";
            file = file.resolve(filename);

            if ("unknown".equals(messageType)) {
                LOGGER.info("Unable to create valid filename because message type not set in the message");
            }
        } else {
            file = messages.get(0).getMeta().getFile();
        }

        return file;
    }

    private Charset[] getSupportedCharsets() {
        return new Charset[] {
                StandardCharsets.ISO_8859_1,
                StandardCharsets.US_ASCII,
                StandardCharsets.UTF_8,
                StandardCharsets.UTF_16BE,
                StandardCharsets.UTF_16LE,
                StandardCharsets.UTF_16};
    }

    private void selectFolder() {
        SimpleDialog.chooseDirectory(
                Paths.get(editDestFolder.getText()),
                e -> editDestFolder.setText(e.getPath().toString())
        );
    }

    private void selectFilename() {
        SimpleDialog.saveFile(
                Paths.get(editFilename.getText()),
                null,
                e -> editFilename.setText(e.getPath().toString()),
                GenericFileFilter.HL7_FILTER
        );
    }

    private void updateFramePreview() {
        String s = "<body><html><font face=\"Arial, Tahoma\" size=\"3\">";

        s = s.concat("<font color=\"fuchsia\">&lt;0x" + ((frame.getStartByte() < 16) ? "0" : "") + Integer.toHexString(frame.getStartByte())).concat("&gt;</font>");

        s = s.concat(LocaleTool.get("hl7_message"));

        s = s.concat("<font color=\"fuchsia\">");
        for(int i = 0; i < frame.getStopBytesLength(); i++) {
            s = s.concat("&lt;0x".concat(((frame.getStopBytes()[i] < 16) ? "0" : "") + Integer.toHexString(frame.getStopBytes()[i]))).concat("&gt;");
        }
        s = s.concat("</font>");

        s = s.concat("</font></body></html>");

        editFrame.setText(s);
    }

}
