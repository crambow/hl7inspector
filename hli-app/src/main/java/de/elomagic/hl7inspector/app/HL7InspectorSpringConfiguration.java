/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app;

import de.elomagic.hl7inspector.platform.utils.PlatformTool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pf4j.AbstractPluginManager;
import org.pf4j.spring.SpringPluginManager;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = "de.elomagic.hl7inspector")
@ImportResource("classpath:beans.xml")
public class HL7InspectorSpringConfiguration {

    private static final Logger LOGGER = LogManager.getLogger(HL7InspectorSpringConfiguration.class);

    @Bean
    public SpringPluginManager pluginManager() {
        try {
            String path = System.getProperty(AbstractPluginManager.PLUGINS_DIR_PROPERTY_NAME);
            Path pluginsFolder = path == null ? PlatformTool.getUserPluginsFolder() : Paths.get(path);
            return new SpringPluginManager(pluginsFolder);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

}
