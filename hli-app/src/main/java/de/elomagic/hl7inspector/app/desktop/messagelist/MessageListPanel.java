/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.messagelist;

import com.alee.extended.dock.WebDockableFrame;

import de.elomagic.hl7inspector.app.MessageWriterBean;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.NewMessageAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.OpenFileExternalAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.RemoveMessagesAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.events.MessageRemovedEvent;
import de.elomagic.hl7inspector.app.desktop.messagelist.events.MessagesAddedEvent;
import de.elomagic.hl7inspector.app.desktop.messagelist.events.NewMessageEvent;
import de.elomagic.hl7inspector.app.desktop.messagelist.events.RemoveMessagesEvent;
import de.elomagic.hl7inspector.app.editor.ui.MessageEditorDocument;
import de.elomagic.hl7inspector.app.load.ui.ReaderProgressDialog;
import de.elomagic.hl7inspector.app.save.SaveDialog;
import de.elomagic.hl7inspector.app.save.SaveProgressDialog;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.events.ApplicationStartedEvent;
import de.elomagic.hl7inspector.platform.events.BeforeApplicationExitEvent;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.events.OpenDocumentEvent;
import de.elomagic.hl7inspector.platform.io.gson.GsonTool;
import de.elomagic.hl7inspector.platform.ui.FilesDroppedEvent;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.TextDroppedEvent;
import de.elomagic.hl7inspector.platform.ui.actions.RevealInExplorer;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyPopupMenuListener;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.hl7inspector.shared.desktop.MessageListToolDescriptor;
import de.elomagic.hl7inspector.shared.events.MessageEditedEvent;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.MessageMETA;
import de.elomagic.hl7inspector.shared.io.MessageFileIO;
import de.elomagic.hl7inspector.shared.ui.ImportOptionsDialog;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * UI component of the left side of the main frame.
 */
@Component
public class MessageListPanel extends WebDockableFrame {

    private static final String SESSION_FILES = "sessionFiles.json";

    private static final Logger LOGGER = LogManager.getLogger(MessageListPanel.class);

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private final transient BeanFactory beanFactory;
    private final transient ListableBeanFactory listableBeanFactory;

    private final transient EventMessengerProcessor eventMessengerProcessor;

    private JLabel header;

    private @Autowired MessageSelectionModel selectionModel;
    private @Autowired MessageListModel messageListModel;

    public @Autowired MessageListPanel(BeanFactory beanFactory, ListableBeanFactory listableBeanFactory, EventMessengerProcessor eventMessengerProcessor) {
        super ("message-list", LocaleTool.get("messages"));

        this.beanFactory = beanFactory;
        this.listableBeanFactory = listableBeanFactory;
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @PostConstruct
    private void initUI() {
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder());
        setPreferredSize(new Dimension(200, 600));

        JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.addPopupMenuListener((SimplifyPopupMenuListener) e -> {
            popupMenu.removeAll();

            popupMenu.add(beanFactory.getBean(NewMessageAction.class));
            popupMenu.add(beanFactory.getBean(RemoveMessagesAction.class));
            popupMenu.addSeparator();

            listableBeanFactory
                    .getBeansOfType(MessageListToolDescriptor.class)
                    .values()
                    .stream()
                    .sorted(Comparator.comparingDouble(MessageListToolDescriptor::getPriority))
                    .filter(d -> d.getContextMenuItems() != null)
                    .flatMap(d -> d.getContextMenuItems().stream())
                    .forEach(popupMenu::add);

            popupMenu.addSeparator();
            popupMenu.add(new RevealInExplorer(this::handleShowInFolder));
            popupMenu.add(beanFactory.getBean(OpenFileExternalAction.class));
        });

        JList<Message> listUI = new JList<>();
        listUI.setModel(messageListModel);
        listUI.setSelectionModel(selectionModel);
        listUI.setCellRenderer(new MessageSummaryCellRenderer());
        listUI.setComponentPopupMenu(popupMenu);
//        listUI.addMouseListener((SimplifyClickMouseListener) e -> {
//            JList<Message> list = (JList)e.getSource();
//            int index = list.locationToIndex(e.getPoint());
//            if (e.getClickCount() == 1) {
//                List<Message> messages = selectionModel.getSelectedMessages();
//                // When only one message was selected in the list then load this message...otherwise reset loaded message
//                documentsPanel.showMessage(messages.size() == 1 ? messages.get(0) : null);
//            } else if(e.getClickCount() == 2) {
//                // Double-click detected
//                documentsPanel.showMessage(messageModel.get(index));
//                documentsPanel.setViewMode(StartupProperties.ViewMode.EDITOR);
//            }
//        });

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(listUI);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        header = new JLabel(LocaleTool.get("messages"));
        header.setBorder(new EmptyBorder(4, 6, 0, 4));

        add(header, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);

        messageListModel.addListDataListener(new ListDataListener() {
            @Override
            public void intervalAdded(ListDataEvent e) {
                updateMessageCount();
            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                updateMessageCount();
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                // noop
            }
        });

        selectionModel.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                List<Message> messages = selectionModel.getSelectedMessages();

                Message message = messages.size() == 1 ? messages.get(0) : null;

                if (message != null) {
                    // When only one message was selected in the list then load this message...otherwise reset loaded message
                    MessageEditorDocument med = beanFactory.getBean(MessageEditorDocument.class);
                    med.bindMessage(message);
                    eventMessengerProcessor.publishEvent(new OpenDocumentEvent(this, message.getId(), med, message.get(0).get(9).asText()));
                }
            }
        });
    }

    public MessageSelectionModel getSelectionModel() {
        return selectionModel;
    }

    public void init() {
        restoreLastSession();
    }

    private void restoreLastSession() {
        try {
            messageListModel.clear();

            Path file = PlatformTool.getTempAppFolder().resolve(SESSION_FILES);

            MessageMETA[] metas = Files.notExists(file) ? null : GsonTool.read(file, MessageMETA[].class);

            if (metas == null || metas.length == 0) {
                newMessage();
                getSelectionModel().setSelectionInterval(0, 0);
            } else {
                for (MessageMETA meta : metas) {
                    if (Files.notExists(meta.getFile())) {
                        continue;
                    }

                    loadFile(meta.getFile(), meta.getEncoding());
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(LocaleTool.get("error_during_reading_file"), ex);
        }
    }

    private void saveCurrentSession() {
        try {
            MessageMETA[] metas = messageListModel.stream()
                    .map(Message::getMeta)
                    .toArray(MessageMETA[]::new);

            GsonTool.write(metas, PlatformTool.getTempAppFolder().resolve(SESSION_FILES));
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(ex.getMessage(), ex);
        }
    }

    private void updateMessageCount() {
        SwingUtilities.invokeLater(() -> header.setText(LocaleTool.get("messages_1arg", messageListModel.size())));
    }

    public void openFile() {
        Path path = Configuration.getInstance().getAppFilesLastUsedFolder();
        if(Files.notExists(path)) {
            path = Paths.get(System.getProperty("user.dir"));
        }

        SimpleDialog.chooseFile(
                path,
                null,
                e -> loadFile(e.getPath()),
                null, GenericFileFilter.TEXT_FILTER, GenericFileFilter.HL7_FILTER);
    }

    public void openFileExternal() {
        if(configuration.getExternalFileViewer() == null) {
            SimpleDialog.info(LocaleTool.get("no_external_file_viewer_editor_set_please_check_your_configuration"));
        } else {
            List<Message> messageList = selectionModel.getSelectedMessages();

            if(messageList.isEmpty()) {
                // TODO Check if action was fired by hovering message in panel
                SimpleDialog.error(LocaleTool.get("one_message_which_imported_from_a_file_must_be_selected"));
            } else if(messageList.size() > 1) {
                SimpleDialog.error(LocaleTool.get("select_only_one_message"));
            } else {
                Message message = messageList.get(0);

                Path file = message.getMeta().getSourceFile();

                if(file == null) {
                    SimpleDialog.error(LocaleTool.get("only_messages_from_a_file_can_be_open"));
                } else {
                    PlatformTool.openFileWith(configuration.getExternalFileViewer(), file);
                }
            }
        }
    }

    private ImportOptions createDefaultImportOptionBean(Path file) throws IOException {
        ImportOptions options = configuration.getImportOptions();
        options.setSourceDescription(LocaleTool.get("file_1arg", file.getFileName().toString()));
        options.setSourceFile(file);
        options.setFileSize(Files.size(file));
        return options;
    }

    public void importPastedText(@NotNull String text) {
        ImportOptions options = configuration.getImportOptions();
        options.setSourceDescription(LocaleTool.get("copied_from_the_clipboard"));
        options.setFileSize(text.length());

        ImportOptionsDialog importOptionsDialog = beanFactory.getBean(ImportOptionsDialog.class);
        importOptionsDialog.bindBean(options);
        if(importOptionsDialog.ask()) {
            ByteArrayInputStream in = new ByteArrayInputStream(text.getBytes(options.getEncoding()));
            beanFactory.getBean(ReaderProgressDialog.class).startImport(in, options);
        }
    }

    /**
     * Imports unspecified files.
     *
     * Additionally a import options dialog will popup before importing files
     *
     * @param files Bunch of files
     */
    public void importFiles(@NotNull Path ...files) {
        ImportOptions options = configuration.getImportOptions();
        if (files.length == 1) {
            options.setSourceDescription(LocaleTool.get("file_1arg", files[0].getFileName().toString()));
            options.setSourceFile(files[0]);
        } else {
            options.setSourceDescription(LocaleTool.get("bunch_of_files"));
        }

        ImportOptionsDialog importOptionsDialog = beanFactory.getBean(ImportOptionsDialog.class);
        importOptionsDialog.bindBean(options);
        if(importOptionsDialog.ask()) {
            try {
                for (Path file : files) {
                    Configuration.getInstance().getRecentOpenedFileModel().add(file);
                    loadFile(file, options);
                }
            } catch(Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                SimpleDialog.error(LocaleTool.get("error_during_reading_file"), ex);
            }
        }
    }

    @EventListener(NewMessageEvent.class)
    public void handleNewMessageEvent() {
        newMessage();
    }

    private void newMessage() {
        Message message = new Message();
        message.getMeta().setEncoding(StandardCharsets.UTF_8);
        message.getMeta().setInMemory(true);
        message.getMeta().setSourceType(MessageMETA.SourceType.IN_MEMORY);
        message.parse(LocaleTool.get("message_template", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())));

        messageListModel.add(message);
    }

    /**
     * Load HL7 file with user interaction.
     *
     * @param file File to load. Text or ZIP file
     */
    public void loadFile(@NotNull Path file) {
        Configuration.getInstance().getRecentOpenedFileModel().add(file);

        try {
            ImportOptions options = createDefaultImportOptionBean(file);
            options.setFileSize(Files.size(file));

            ImportOptionsDialog importOptionsDialog = beanFactory.getBean(ImportOptionsDialog.class);
            importOptionsDialog.bindBean(options);
            if(importOptionsDialog.ask()) {
                loadFile(file, options);
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(LocaleTool.get("error_during_reading_file"), ex);
        }
    }

    /**
     * Load simply a HL7 message file.
     *
     *
     * @param file HL7 Text file.
     */
    private void loadFile(@NotNull Path file, @NotNull Charset charset) throws IOException {
        Message message = MessageFileIO.readMessage(file, charset);
        messageListModel.add(message);
    }

    private void loadFile(@NotNull Path file, @NotNull ImportOptions options) throws IOException {
        InputStream fin;
        try {
            // Check if compressed file
            ZipFile zipFile = new ZipFile(file.toFile());

            Enumeration<? extends ZipEntry> enu = zipFile.entries();

            if(!enu.hasMoreElements()) {
                throw new IOException("No file found inside compressed file.");
            }

            ZipEntry entry = enu.nextElement();
            options.setFileSize(entry.getSize());
            options.setSourceDescription(LocaleTool.get("zip_archive"));
            //options.setSource(entry.getName());

            // FEATURE "ChooseDialog" must popup when using archive with more then one file.
            if(enu.hasMoreElements()) {
                SimpleDialog.info(LocaleTool.get("the_file_includes_more_then_one_compressed_file", entry.getName()));
            }

            fin = zipFile.getInputStream(entry);
        } catch(Exception ex) {
            // No zip archive
            fin = Files.newInputStream(file);
        }

        if (options.isClearBuffer()) {
            messageListModel.clear();
        }

        beanFactory.getBean(ReaderProgressDialog.class).startImport(fin, options);
        Configuration.getInstance().setAppFilesLastUsedFolder(file.toAbsolutePath().getParent());
    }

    /**
     * Reloads the message from the file.
     *
     * Usually called for example after user has changed encoding.
     *
     * @param message Message to reload
     */
    public void reloadMessage(@NotNull Message message) {
        try {
            MessageFileIO.rereadMessage(message);
            eventMessengerProcessor.publishEvent(new MessageEditedEvent(this, message, message));
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(LocaleTool.get("error_during_reading_file"), ex);
        }
    }

    /**
     * Confirm and save messages.
     */
    public void saveMessages() {
        List<Message> messages = getSelectionModel().getSelectedMessages();

        SaveDialog saveDialog = beanFactory.getBean(SaveDialog.class);
        saveDialog.setMessages(messages);

        MessageWriterBean options = new MessageWriterBean();

        saveDialog.bindBean(options);
        if(saveDialog.ask()) {
            messages = options.isOnlySelectedFiles() ? getSelectionModel().getSelectedMessages() : messageListModel;

            beanFactory.getBean(SaveProgressDialog.class).start(messages, options);
        }
    }

    @EventListener(RemoveMessagesEvent.class)
    public void handleRemoveMessagesEvent() {
        removeMessages();
    }

    /**
     * Confirm and remove selected messages.
     */
    public void removeMessages() {
        List<Message> selectedMessages = getSelectionModel().getSelectedMessages();

        if(selectedMessages.isEmpty()) {
            SimpleDialog.info(LocaleTool.get("no_message_selected"));
        } else if(SimpleDialog.confirmYesNo(LocaleTool.get("remove_selected_message_ask")) == SimpleDialog.YES_OPTION) {
            messageListModel.removeAll(selectedMessages);
        }
    }

    private void handleShowInFolder(ActionEvent e) {
        List<Message> selectedMessages = getSelectionModel().getSelectedMessages();
        if(selectedMessages.isEmpty()) {
            SimpleDialog.info(LocaleTool.get("no_message_selected"));
        } else {
            selectedMessages.forEach(m -> {
                Path folder = m.getMeta().getFile().getParent();
                if (folder == null) {
                    SimpleDialog.info(LocaleTool.get("message_file_doesnt_exists"));
                } else {
                    PlatformTool.showFolder(folder);
                }
            });
        }
    }

    @EventListener
    public void handleTextDroppedEvent(@NotNull TextDroppedEvent e) {
        importPastedText(e.getText());
    }

    @EventListener
    public void handleFilesDroppedEvent(@NotNull FilesDroppedEvent e) {
        importFiles(e.getFiles().toArray(new Path[0]));
    }

    @EventListener
    public void handleMessagesAddedEvent(@NotNull MessagesAddedEvent e) {
        try {
            // Save only in memory messages which not already saved
            List<Message> messages = e.getMessages().stream()
                    .filter(m -> m.getMeta().isInMemory())
                    .filter(m -> !m.getMeta().isFileExists())
                    .collect(Collectors.toList());

            for (Message m : messages) {
                Path file = PlatformTool.getInMemoryFolder().resolve(UUID.randomUUID().toString() + ".hl7");
                m.getMeta().setFile(file);
                MessageFileIO.writeMessage(m, file);
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @EventListener
    public void handleMessageRemovedEvent(@NotNull MessageRemovedEvent e) {
        if (e.getMessage().getMeta().isInMemory()) {
            // TODO Ask user to save file
            FileUtils.deleteQuietly(e.getMessage().getMeta().getFile().toFile());
        }
    }

    @EventListener
    public void handleApplicationStartedEvent(@NotNull ApplicationStartedEvent e) {
        if (!e.isKeepMessages()) {
            init();
        }

        List<Path> files = Arrays.stream(e.getArgs())
                .map(Paths::get)
                .filter(Files::exists)
                .collect(Collectors.toList());

        if (!files.isEmpty()) {
            importFiles(files.toArray(new Path[0]));
        }
    }

    @EventListener(BeforeApplicationExitEvent.class)
    public void handleBeforeApplicationExitEvent() {
        saveCurrentSession();
    }

}
