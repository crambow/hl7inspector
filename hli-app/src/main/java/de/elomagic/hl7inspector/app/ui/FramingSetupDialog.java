/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.ui;

import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.platform.ui.bindings.FormDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.shared.ui.MessageFramePanel;

import org.jetbrains.annotations.NotNull;

import java.awt.BorderLayout;

public class FramingSetupDialog extends FormDialog<Frame> {

    private final MessageFramePanel framePanel;

    public FramingSetupDialog() {
        super(LocaleTool.get("message_framing_setup_dialog"), true);

        framePanel = new MessageFramePanel();

        setBannerTitle(LocaleTool.get("setup_framing_of_messages"));

        getContentPane().add(framePanel, BorderLayout.CENTER);

        pack();

        setSize(getPreferredSize());
        setLocationRelativeTo(getOwner());
    }

    @Override
    public void bindBean(@NotNull Frame bean) {
        framePanel.bindBean(bean);
    }

    @Override
    public void commit() {
        framePanel.commit();
    }
}
