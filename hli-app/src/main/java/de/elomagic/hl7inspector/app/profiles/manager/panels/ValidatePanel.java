/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.manager.panels;

import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.profiles.model.ValidateStatus;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import java.awt.BorderLayout;

@Component
public class ValidatePanel extends AbstractBannerPanel<Profile> {

    private transient Profile profile;

    private final JComboBox<ValidateStatus> cbLength;
    private final JComboBox<ValidateStatus> cbDeprecated;
    private final JComboBox<ValidateStatus> cbConditional;
    private final JComboBox<ValidateStatus> cbRequired;
    private final JComboBox<ValidateStatus> cbItemInTable;
    private final JComboBox<ValidateStatus> cbDefNotFound;
    private final JComboBox<ValidateStatus> cbRepetition;
    private final JComboBox<ValidateStatus> cbInvalidFormat;

    public ValidatePanel() {
        super();

        ValidateStatus[] a = ValidateStatus.values();

        cbLength = new JComboBox<>(a);
        cbDeprecated = new JComboBox<>(a);
        cbConditional = new JComboBox<>(a);
        cbRequired = new JComboBox<>(a);
        cbItemInTable = new JComboBox<>(a);
        cbDefNotFound = new JComboBox<>(a);
        cbRepetition = new JComboBox<>(a);
        cbInvalidFormat = new JComboBox<>(a);

        JPanel panel = FormBuilder.createBuilder()
                .noBorder()
                .addSection("validate_mapping")
                .addRow("field_component_subcomponent_length_too_large", cbLength)
                .addRow("item_in_table_not_found", cbItemInTable)
                .addRow("deprecated_field_component_subcomponent", cbDeprecated)
                .addRow("conditional_field_component_subcomponent", cbConditional)
                .addRow("required_field_component_subcomponent", cbRequired)
                .addRow("definition_not_found", cbDefNotFound)
                .addRow("repetition_count_overflow", cbRepetition)
                .addRow("invalid_value_format", cbInvalidFormat)
                .addRowSpacer()
                .build();

        add(panel, BorderLayout.CENTER);
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("validator");
    }

    @Override
    public javax.swing.Icon getIcon() {
        return IconThemeManager.getImageIcon("spellcheck-32.png");
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public void bindBean(@NotNull Profile profile) {
        this.profile = profile;

        cbLength.setSelectedItem(profile.getValidateMapper().getMapLength());
        cbDeprecated.setSelectedItem(profile.getValidateMapper().getMapDeprecated());
        cbConditional.setSelectedItem(profile.getValidateMapper().getMapConditional());
        cbRequired.setSelectedItem(profile.getValidateMapper().getMapRequired());
        cbItemInTable.setSelectedItem(profile.getValidateMapper().getMapItemMiss());
        cbDefNotFound.setSelectedItem(profile.getValidateMapper().getMapDefNotFound());
        cbRepetition.setSelectedItem(profile.getValidateMapper().getMapRepetition());
        cbInvalidFormat.setSelectedItem(profile.getValidateMapper().getInvalidFormat());
    }

    @Override
    public void commit() {
        profile.getValidateMapper().setMapLength((ValidateStatus)cbLength.getSelectedItem());
        profile.getValidateMapper().setMapDeprecated((ValidateStatus)cbDeprecated.getSelectedItem());
        profile.getValidateMapper().setMapConditional((ValidateStatus)cbConditional.getSelectedItem());
        profile.getValidateMapper().setMapRequired((ValidateStatus)cbRequired.getSelectedItem());
        profile.getValidateMapper().setMapItemMiss((ValidateStatus)cbItemInTable.getSelectedItem());
        profile.getValidateMapper().setMapRepetition((ValidateStatus)cbRepetition.getSelectedItem());
        profile.getValidateMapper().setInvalidFormat((ValidateStatus)cbInvalidFormat.getSelectedItem());
    }

}
