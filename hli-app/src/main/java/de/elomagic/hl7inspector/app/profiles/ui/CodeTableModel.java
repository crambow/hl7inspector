/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.platform.ui.ArrayListModel;
import de.elomagic.hl7inspector.shared.profiles.model.TableItem;

import java.util.List;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class CodeTableModel extends ProfileModel<TableItem> {

    private static final long serialVersionUID = 3848893502635683042L;

    private static final Logger LOGGER = LogManager.getLogger(CodeTableModel.class);

    /**
     * Set a copy of list as model of this class.
     * @see ArrayListModel#setList(List)
     *
     * @param list
     */
    public final void setModel(@NotNull List<TableItem> list) {
        clear();
        addAll(list);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TableItem item = getRow(rowIndex);

        switch (columnIndex) {
            case 0:
                return item.getId();
            case 1:
                return item.getType();
            case 2:
                return item.getValue();
            case 3:
                return item.getDescription();
            case 4:
                return item.getTableDescription();
            default:
                return "";
        }
    }

    /**
     *  This empty implementation is provided so users don't have to implement
     *  this method if their data model is not editable.
     *
     *
     * @param aValue   value to assign to cell
     * @param rowIndex   row of cell
     * @param columnIndex  column of cell
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
            TableItem ti = getRow(rowIndex);

            switch (columnIndex) {
                case 0:
                    ti.setId(aValue.toString());
                    break;
                case 1:
                    ti.setType(aValue.toString());
                    break;
                case 2:
                    ti.setValue(aValue.toString());
                    break;
                case 3:
                    ti.setDescription(aValue.toString());
                    break;
                case 4:
                    ti.setTableDescription(aValue.toString());
                    break;
                default:
                    LOGGER.warn("Column index {} is out of range.", columnIndex);
            }

            fireTableCellUpdated(rowIndex, columnIndex);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int col) {
        switch (col) {
            case 0:
                return LocaleTool.get("table_id");
            case 1:
                return LocaleTool.get("type");
            case 2:
                return LocaleTool.get("value");
            case 3:
                return LocaleTool.get("description");
            case 4:
                return LocaleTool.get("table_description");
            default:
                return "";
        }
    }

    @Override
    public Class<TableItem> getDefaultRowClass() {
        return TableItem.class;
    }

}
