/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import javax.swing.JLabel;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class MapFieldModel extends AbstractTableModel {

    private final List<String> mapList = new ArrayList<>();

    public MapFieldModel(ImportFileModel fileModel) {
        for(int i = 0; i < fileModel.getColumnCount(); i++) {
            mapList.add("-");
        }
    }

    /**
     * Returns the value for the cell at
     * <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param	rowIndex	the row whose value is to be queried
     * @param	columnIndex the column whose value is to be queried
     * @return	the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return LocaleTool.get("line");
        }

        return mapList.get(columnIndex - 1);
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return mapList.size();
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it should display. This method should be quick, as it is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return 1;
    }

    /**
     * Returns false. This is the default implementation for all cells.
     *
     * @param rowIndex the row being queried
     * @param columnIndex the column being queried
     * @return false
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex > 0) && (rowIndex == 0);
    }

    /**
     * Returns
     * <code>Object.class</code> regardless of
     * <code>columnIndex</code>.
     *
     *
     * @param columnIndex the column being queried
     * @return the Object.class
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return (columnIndex == 0) ? JLabel.class : String.class;
    }

    /**
     * This empty implementation is provided so users don't have to implement this method if their data model is not editable.
     *
     * @param aValue value to assign to cell
     * @param rowIndex row of cell
     * @param columnIndex column of cell
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if((columnIndex > 0) && (rowIndex == 0)) {
            mapList.set(columnIndex - 1, aValue.toString());

            fireTableCellUpdated(rowIndex, columnIndex);
        }
    }
}
