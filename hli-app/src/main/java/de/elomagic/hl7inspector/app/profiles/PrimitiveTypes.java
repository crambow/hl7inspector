/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles;

import org.jetbrains.annotations.Nullable;

public class PrimitiveTypes {

    public static boolean isPrimitive(@Nullable String datatype) {
        if (datatype == null) {
            return false;
        }

        return "DT".equals(datatype) // 2.A.21 DT - date
                || "DTM".equals(datatype) // 2.A.22 DTM - date/time
                || "FT".equals(datatype) // 2.A.31 FT - formatted text data
                || "GTS".equals(datatype) // 2.A.32 GTS – general timing specification
                || "ID".equals(datatype) // 2.A.35 ID - coded value for HL7 defined tables
                || "IS".equals(datatype) // 2.A.36 IS - coded value for user-defined tables
                || "NM".equals(datatype) // 2.A.47 NM - numeric
                || "SI".equals(datatype) // 2.A.69 SI - sequence ID
                || "ST".equals(datatype) // 2.A.74 ST - string data
                || "TM".equals(datatype) // 2.A.75 TM – time
                || "TX".equals(datatype); // 2.A.78 TX - text data
    }

}
