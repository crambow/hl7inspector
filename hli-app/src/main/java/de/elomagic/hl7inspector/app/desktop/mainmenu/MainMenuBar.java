/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.mainmenu;

import de.elomagic.hl7inspector.app.desktop.DocumentsPanel;
import de.elomagic.hl7inspector.app.desktop.mainmenu.actions.ExitAction;
import de.elomagic.hl7inspector.app.desktop.mainmenu.actions.OpenBugReportPageAction;
import de.elomagic.hl7inspector.app.desktop.mainmenu.actions.OpenLoggingPathAction;
import de.elomagic.hl7inspector.app.desktop.mainmenu.actions.OpenNeoPageAction;
import de.elomagic.hl7inspector.app.desktop.mainmenu.actions.OpenProjectPageAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.MessageListPanel;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.ClearWorkspaceAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.FileRecentOpenAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.NewMessageAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.OpenFileExternalAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.RemoveMessagesAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.ShowFindMessagesWindowAction;
import de.elomagic.hl7inspector.app.editor.events.TreeCompactViewModeChangedEvent;
import de.elomagic.hl7inspector.app.editor.ui.MessageEditorDocument;
import de.elomagic.hl7inspector.app.editor.ui.actions.ViewCompressedAction;
import de.elomagic.hl7inspector.app.load.ui.actions.ImportFromClipboardAction;
import de.elomagic.hl7inspector.app.load.ui.actions.OpenMessageFileAction;
import de.elomagic.hl7inspector.app.profiles.ui.ProfileManagerDialog;
import de.elomagic.hl7inspector.app.profiles.ui.actions.ShowProfileManagerAction;
import de.elomagic.hl7inspector.app.profiles.validate.ui.actions.ValidateMessageAction;
import de.elomagic.hl7inspector.app.save.ui.actions.SaveMessageFileAsAction;
import de.elomagic.hl7inspector.app.ui.actions.ShowParserWindowAction;
import de.elomagic.hl7inspector.configuration.ui.OptionsDialog;
import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.events.BeforeApplicationExitEvent;
import de.elomagic.hl7inspector.platform.events.BeforeWriteSettingsEvent;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.AboutDialog;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.actions.AboutAction;
import de.elomagic.hl7inspector.platform.ui.actions.EditItemAction;
import de.elomagic.hl7inspector.platform.ui.actions.OpenUrlAction;
import de.elomagic.hl7inspector.platform.ui.actions.RemoveItemAction;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.updatecheck.CheckUpdateAction;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.PlatformTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.hl7inspector.shared.desktop.MainMenuDescriptor;
import de.elomagic.hl7inspector.shared.events.OpenSettingsEvent;
import de.elomagic.hl7inspector.shared.hl7.model.EncodingObject;
import de.elomagic.hl7inspector.shared.hl7.model.HL7ObjectType;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.ui.actions.OpenSettingsAction;
import de.elomagic.hl7inspector.shared.ui.actions.ShowDocumentFindBarAction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.Desktop;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

@Component
public class MainMenuBar extends JMenuBar {

    private static final Logger LOGGER = LogManager.getLogger(MainMenuBar.class);

    private final transient Configuration configuration = Configuration.getInstance();

    private final transient BeanFactory beanFactory;
    private final transient ListableBeanFactory listableBeanFactory;
    private final transient EventMessengerProcessor eventMessengerProcessor;

    private DocumentsPanel documentsPanel;
    private JCheckBoxMenuItem miCompressedView;

    public @Autowired MainMenuBar(BeanFactory beanFactory, ListableBeanFactory listableBeanFactory, EventMessengerProcessor eventMessengerProcessor) {
        this.beanFactory = beanFactory;
        this.listableBeanFactory = listableBeanFactory;
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @PostConstruct
    private void initUI() {
        LOGGER.debug("Initialize main menu UI");

        initPlatform(this);

        add(createFileMenu());
        add(createEditMenu());
        add(createViewMenu());
        add(createToolsMenu());
        add(createHelpMenu());
    }

    public void initPlatform(JMenuBar menuBar) {
        if (Desktop.getDesktop().isSupported(Desktop.Action.APP_MENU_BAR)) {
            Desktop.getDesktop().setDefaultMenuBar(menuBar);
        }
        if (Desktop.getDesktop().isSupported(Desktop.Action.APP_ABOUT)) {
            Desktop.getDesktop().setAboutHandler(e -> beanFactory.getBean(AboutDialog.class).setVisible(true));
        }
        if (Desktop.getDesktop().isSupported(Desktop.Action.APP_PREFERENCES)) {
            Desktop.getDesktop().setPreferencesHandler(e -> handleOpenSettingsEvent());
        }
        if(Desktop.getDesktop().isSupported(Desktop.Action.APP_QUIT_HANDLER)) {
            Desktop.getDesktop().setQuitHandler((e, response) -> {
                if (SharedConfiguration.getInstance().isQuitWithoutAsking() || SimpleDialog.confirmYesNo(LocaleTool.get("really_exit_hl7inspector")) == 0) {
                    eventMessengerProcessor.publishEvent(new BeforeWriteSettingsEvent(this));
                    Configuration.getInstance().save();
                    eventMessengerProcessor.publishEvent(new BeforeApplicationExitEvent(this));

                    // Will call System.exit(0); See also {@link java.awt.desktop.QuitStrategy}.
                    response.performQuit();
                } else {
                    response.cancelQuit();
                }
            });
        }
    }

    private JMenu createFileMenu() {
        JMenu miOpenRecentFiles = new JMenu(LocaleTool.get("open_recent_files"));
        miOpenRecentFiles.setIcon(IconThemeManager.getImageIcon("clear.png"));

        JMenu miFile = new JMenu(LocaleTool.get("file"));
        miFile.add(beanFactory.getBean(NewMessageAction.class));
        miFile.add(beanFactory.getBean(OpenMessageFileAction.class));
        miFile.add(beanFactory.getBean(ImportFromClipboardAction.class));
        miFile.add(miOpenRecentFiles);
        miFile.addSeparator();
        miFile.add(beanFactory.getBean(ClearWorkspaceAction.class));
        miFile.add(beanFactory.getBean(RemoveMessagesAction.class));
        miFile.addSeparator();
        miFile.add(beanFactory.getBean(SaveMessageFileAsAction.class).setAcceleratorKey(KeyStroke.getKeyStroke(KeyEvent.VK_S, PlatformTool.isMacOS() ? InputEvent.META_DOWN_MASK : InputEvent.CTRL_DOWN_MASK)));
        miFile.addSeparator();
        miFile.add(beanFactory.getBean(ExitAction.class));
        miFile.addChangeListener(e -> {
            if(((JMenuItem)e.getSource()).isSelected()) {
                miOpenRecentFiles.removeAll();

                configuration
                        .getRecentOpenedFileModel()
                        .stream()
                        .map(file -> new FileRecentOpenAction(l -> beanFactory.getBean(MessageListPanel.class).importFiles(file), file))
                        .map(JMenuItem::new)
                        .forEach(miOpenRecentFiles::add);

                miOpenRecentFiles.setEnabled(miOpenRecentFiles.getItemCount() != 0);
            }
        });

        return miFile;
    }

    private JMenu createEditMenu() {
        JMenuItem miEditItem = new JMenuItem(new EditItemAction(e -> doIfSelectedDocument(d -> d.getTree().editNode())));
        JMenuItem miEditRemoveItem = new JMenuItem(new RemoveItemAction(e -> doIfSelectedDocument(d -> d.getTree().removeNode())));

        JMenu menu = new JMenu(LocaleTool.get("edit"));
        menu.addChangeListener(e -> {
            if(((JMenuItem)e.getSource()).isSelected()) {

                miEditItem.setEnabled(false);
                miEditRemoveItem.setEnabled(false);

                doIfSelectedDocument(d -> {
                    List<Hl7Object<?>> selectedObjects = d.getTree().getSelectedObjects();

                    if (!selectedObjects.isEmpty()) {
                        Hl7Object<?> hl7o = selectedObjects.get(0);

                        if (!(hl7o instanceof EncodingObject)) {
                            miEditItem.setEnabled(hl7o.getChildClass() != null);
                            miEditRemoveItem.setEnabled(!HL7ObjectType.is(hl7o, HL7ObjectType.MESSAGE));
                        }
                    }
                });
            }
        });

        menu.add(miEditItem);
        menu.add(miEditRemoveItem);

        menu.addSeparator();
        menu.add(beanFactory.getBean(ShowDocumentFindBarAction.class));
        menu.add(beanFactory.getBean(ShowFindMessagesWindowAction.class));

        return menu;
    }

    private JMenu createViewMenu() {
        miCompressedView = new JCheckBoxMenuItem(beanFactory.getBean(ViewCompressedAction.class));
        miCompressedView.setSelected(true);

        JMenu menu = new JMenu(LocaleTool.get("view"));
        menu.add(miCompressedView);
        menu.addSeparator();
        menu.add(beanFactory.getBean(ValidateMessageAction.class));

        return menu;
    }

    private JMenu createToolsMenu() {
        JMenu menu = new JMenu(LocaleTool.get("tools"));
        menu.addChangeListener(e -> {
            if (((JMenuItem) e.getSource()).isSelected()) {
                menu.removeAll();

                menu.add(beanFactory.getBean(OpenFileExternalAction.class));
                menu.addSeparator();

                listableBeanFactory.getBeansOfType(MainMenuDescriptor.class).values().stream()
                        .sorted(Comparator.comparingDouble(MainMenuDescriptor::getPriority))
                        .filter(d -> d.getMenuItems(MainMenuDescriptor.Category.TOOLS) != null)
                        .flatMap(d -> d.getMenuItems(MainMenuDescriptor.Category.TOOLS).stream())
                        .forEach(menu::add);

                menu.add(beanFactory.getBean(ShowParserWindowAction.class));
                menu.addSeparator();
                menu.add(new ShowProfileManagerAction(e1 -> beanFactory.getBean(ProfileManagerDialog.class).ask()));

                if (!PlatformTool.isMacOS()) {
                    menu.addSeparator();
                    menu.add(beanFactory.getBean(OpenSettingsAction.class));
                }
            }
        });

        return menu;
    }

    private JMenu createHelpMenu() {
        JMenu menu = new JMenu(LocaleTool.get("help"));
        menu.add(beanFactory.getBean(CheckUpdateAction.class));
        menu.addSeparator();
        menu.add(new OpenUrlAction(ApplicationProperties.getReleaseNotesUrl(), "open_release_notes"));
        menu.add(beanFactory.getBean(OpenProjectPageAction.class));
        menu.add(beanFactory.getBean(OpenNeoPageAction.class));
        menu.addSeparator();
        menu.add(beanFactory.getBean(OpenBugReportPageAction.class));
        menu.add(beanFactory.getBean(OpenLoggingPathAction.class));

        if(!PlatformTool.isMacOS()) {
            menu.addSeparator();
            menu.add(new AboutAction(e -> beanFactory.getBean(AboutDialog.class).setVisible(true)));
        }

        return menu;
    }

    private void doIfSelectedDocument(@NotNull Consumer<MessageEditorDocument> consumer) {
        if (documentsPanel == null) {
            documentsPanel = beanFactory.getBean(DocumentsPanel.class);
        }

        documentsPanel.doWithSelectedComponent(MessageEditorDocument.class, consumer);
    }

    @EventListener
    public void handleCompactViewModeChanged(TreeCompactViewModeChangedEvent e) {
        miCompressedView.setSelected(e.isCompact());
    }

    @EventListener(OpenSettingsEvent.class)
    public void handleOpenSettingsEvent() {
        beanFactory.getBean(OptionsDialog.class).ask();
    }

}
