/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.shared.profiles.ProfileFile;
import de.elomagic.hl7inspector.app.profiles.ProfileServiceImpl;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.app.profiles.ui.actions.AddProfileAction;
import de.elomagic.hl7inspector.app.profiles.ui.actions.NewProfileAction;
import de.elomagic.hl7inspector.app.profiles.ui.actions.SetProfileAsDefaultAction;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.actions.DefaultCloseWindowAction;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.actions.EditItemAction;
import de.elomagic.hl7inspector.platform.ui.actions.RemoveItemAction;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyClickMouseListener;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.IOException;

@Component
public class ProfileManagerDialog extends MyBaseDialog {

    private static final Logger LOGGER = LogManager.getLogger(ProfileManagerDialog.class);

    private final transient Configuration configuration = Configuration.getInstance();

    private final transient BeanFactory beanFactory;
    private final ProfileServiceImpl profileService;

    private final JList<ProfileFile> lstProfiles = new JList<>();
    private final JButton btEdit = new JButton(new EditItemAction(this::editProfile));
    private final JButton btClose = new JButton(new DefaultCloseWindowAction(e -> setVisible(false)));

    @Autowired
    public ProfileManagerDialog(BeanFactory beanFactory, ProfileServiceImpl profileService) {
        super(LocaleTool.get("profile_manager"), true);

        this.beanFactory = beanFactory;
        this.profileService = profileService;
    }

    @PostConstruct
    private void init() {
        getBanner().setVisible(false);
        getButtonPane().setVisible(false);

        JScrollPane scroll = new JScrollPane(lstProfiles);

        JPanel main = new JPanel(new BorderLayout());
        main.add(scroll, BorderLayout.CENTER);
        main.add(createButtonPanel(), BorderLayout.EAST);

        getContentPane().add(main);

        lstProfiles.setModel(profileService.getProfileModel());
        lstProfiles.setCellRenderer(new ProfileCellRenderer());
        lstProfiles.addMouseListener((SimplifyClickMouseListener) e -> {
            if(e.getClickCount() == 2) {
                btEdit.doClick();
            }
        });

        setSize(600, 480);
    }

    private JPanel createButtonPanel() {
        JButton btAdd = new JButton(new AddProfileAction(this::addProfile));
        JButton btNew = new JButton(new NewProfileAction(this::newProfile));
        JButton btRemove = new JButton(new RemoveItemAction(this::removeProfile));
        JButton btDefault = new JButton(new SetProfileAsDefaultAction(this::setAsDefault));

        return FormBuilder.createBuilder()
                .noBorder()
                .addRow(btAdd, 0, 1)
                .addRow(btNew, 0, 1)
                .addRow(btEdit, 0, 1)
                .addRow(btRemove, 0, 1)
                .addRow(btDefault, 0, 1)
                .addRowSpacer()
                .addRow(btClose, 0, 1)
                .build();
    }

    private void newProfile(ActionEvent e) {
            SimpleDialog.saveFile(
                    configuration.getAppFilesLastUsedFolder(),
                    LocaleTool.get("create_new_profile"),
                    d -> {
                        try {
                        ProfileFile profileFile = new ProfileFile(d.getPath());

                        if(profileFile.exists() && SimpleDialog.confirmYesNo(LocaleTool.get("file_already_exists_overwrite")) == 0) {
                            profileFile.delete();
                        }

                        profileService.save(profileFile.getPath(), new Profile());

                        if(!profileService.getProfileModel().contains(profileFile)) {
                            profileService.getProfileModel().addElement(profileFile);
                        }

                        beanFactory.getBean(ProfileDefinitionDialog.class).ask(profileFile);
                        saveProfile();
                        } catch(Exception ex) {
                            LOGGER.error(ex.getMessage(), ex);
                            SimpleDialog.error(ex);
                        }
                    },
                    GenericFileFilter.PROFILE_FILTER
            );
    }

    private void addProfile(ActionEvent e) {
        SimpleDialog.chooseFile(
                null,
                "choose_hl7_profile",
                d -> {
                    ProfileFile profileFile = new ProfileFile(d.getPath());

                    try {
                        Profile p = profileService.getProfile(profileFile.getPath());
                        profileFile.setName(p.getName());
                        profileFile.setDescription(p.getDescription());

                        if(!profileService.getProfileModel().contains(profileFile)) {
                            profileService.getProfileModel().addElement(profileFile);
                        }
                    } catch(Exception ex) {
                        LOGGER.error(ex.getMessage(), ex);
                        SimpleDialog.error(LocaleTool.get("invalid_file_format"));
                    }

                    configuration.save();
                },
                GenericFileFilter.PROFILE_FILTER
        );
    }

    private void editProfile(ActionEvent e) {
        try {
            int i = lstProfiles.getSelectionModel().getMinSelectionIndex();
            if(i == -1) {
                SimpleDialog.error(LocaleTool.get("no_profile_selected"));
            } else {
                ProfileFile profileFile = profileService.getProfileModel().getElementAt(i);

                if(profileFile.exists()) {
                    if(beanFactory.getBean(ProfileDefinitionDialog.class).ask(profileFile)) {
                        saveProfile();
                    }
                } else {
                    SimpleDialog.error(LocaleTool.get("profile_not_found"));
                }
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(ex);
        }
    }

    private void saveProfile() {
        try {
            ProfileDefinitionDialog profileDefinitionDialog = beanFactory.getBean(ProfileDefinitionDialog.class);
            profileService.save(profileDefinitionDialog.getProfileFile().getPath(), profileDefinitionDialog.getProfile());

            // Fire content changed event
            int index = lstProfiles.getSelectionModel().getMinSelectionIndex();
            profileService.getProfileModel().set(index, profileService.getProfileModel().get(index));
        } catch(IOException ee) {
            LOGGER.error(ee.getMessage(), ee);
            SimpleDialog.error(ee);
        }
    }

    private void setAsDefault(ActionEvent e) {
        try {
            int i = lstProfiles.getSelectionModel().getMinSelectionIndex();
            if(i == -1) {
                SimpleDialog.error(LocaleTool.get("no_profile_selected"));
            } else {
                ProfileFile file = profileService.getProfileModel().getElementAt(i);

                profileService.setCurrentProfile(file);

                profileService.getProfileModel().set(i, file);
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(LocaleTool.get("unable_to_load_default_profile"), ex);
        }
    }

    private void removeProfile(ActionEvent e) {
        int i = lstProfiles.getSelectionModel().getMinSelectionIndex();
        if(i == -1) {
            SimpleDialog.error(LocaleTool.get("no_profile_selected"));
        } else if(SimpleDialog.confirmYesNo(LocaleTool.get("are_you_sure")) == 0) {
            profileService.getProfileModel().remove(i);
        }
    }

}
