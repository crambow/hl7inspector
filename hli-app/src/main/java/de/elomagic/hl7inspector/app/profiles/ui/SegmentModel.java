/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.shared.profiles.model.SegmentItem;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SegmentModel extends ProfileModel<SegmentItem> {

    private static final long serialVersionUID = -6889471829107478846L;

    private static final Logger LOGGER = LogManager.getLogger(SegmentModel.class);

    public final void setModel(List<SegmentItem> list) {
        clear();
        addAll(list);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SegmentItem seg = getRow(rowIndex);

        switch(columnIndex) {
            case 0:
                return seg.getId();
            case 1:
                return seg.getDescription();
            case 2:
                return seg.getChapter();
            default:
                return "";
        }
    }

    /**
     * This empty implementation is provided so users don't have to implement this method if their data model is not editable.
     *
     * @param aValue value to assign to cell
     * @param rowIndex row of cell
     * @param columnIndex column of cell
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
            SegmentItem seg = getRow(rowIndex);

            switch(columnIndex) {
                case 0:
                    seg.setId(aValue.toString());
                    break;
                case 1:
                    seg.setDescription(aValue.toString());
                    break;
                case 2:
                    seg.setChapter(aValue.toString());
                    break;
                default:
                    LOGGER.warn("Column index {} is out of range.", columnIndex);
            }

            fireTableCellUpdated(rowIndex, columnIndex);
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int col) {
        switch(col) {
            case 0:
                return LocaleTool.get("id");
            case 1:
                return LocaleTool.get("description");
            case 2:
                return LocaleTool.get("chapter");
            default:
                return "";
        }
    }

    @Override
    public Class<SegmentItem> getDefaultRowClass() {
        return SegmentItem.class;
    }
}
