/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import java.awt.Component;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;

import com.alee.laf.list.ListCellParameters;
import com.alee.laf.list.WebListCellRenderer;

import de.elomagic.hl7inspector.shared.profiles.ProfileFile;
import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.commons.lang3.StringUtils;

public class ProfileCellRenderer extends WebListCellRenderer<ProfileFile, JList<ProfileFile>, ListCellParameters<ProfileFile, JList<ProfileFile>>> {

    private static final long serialVersionUID = 3772768483753533378L;

    private final transient Configuration configuration = Configuration.getInstance();

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        ProfileFile file = (ProfileFile)value;

        setToolTipText(LocaleTool.get("file_1arg", file));
        if(!file.exists()) {
            setIcon(IconThemeManager.getImageIcon("warning.png"));
            setToolTipText(LocaleTool.get("profile_not_found_1arg", file));
        } else if(file.equals(configuration.getDefaultProfile())) {
            // TODO Line above is buggy
            setIcon(IconThemeManager.getImageIcon("ok.png"));
            setToolTipText("");
        } else {
            setIcon(IconThemeManager.getImageIcon("clear.png"));
            setToolTipText("");
        }

        List<String> lines = new ArrayList<>();
        if (StringUtils.isNotBlank(file.getName())) {
            lines.add(file.getName());
        }

        if (StringUtils.isNotBlank(file.getDescription())) {
            lines.add(file.getDescription());
        }

        if (StringUtils.isNotBlank(file.getAbsolutePath())) {
            lines.add(file.getAbsolutePath());
        }

        String pattern;

        switch (lines.size()) {
            case 1:
                pattern = "<html><b>{0}</b></<html>";
                break;
            case 2:
                pattern = "<html><b>{0}</b><br><small>{1}</small></<html>";
                break;
            default:
                pattern = "<html><b>{0}</b><br><small>{1}</small><br><small>{2}</small></<html>";
                break;
        }

        String[] items = lines.toArray(new String[0]);

        String text = MessageFormat.format(pattern, (Object) items);

        setText(text);

        return this;
    }
}
