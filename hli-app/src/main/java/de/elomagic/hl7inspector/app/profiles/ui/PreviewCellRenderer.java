/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.table.TableCellRenderer;

public class PreviewCellRenderer extends JLabel implements TableCellRenderer {

    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable jTable, Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
        setText(obj.toString());
        setOpaque(true);
        setBackground(column == 0 ? SystemColor.control : SystemColor.window);
        setForeground(column == 0 ? SystemColor.controlText : SystemColor.windowText);

        return this;
    }
}
