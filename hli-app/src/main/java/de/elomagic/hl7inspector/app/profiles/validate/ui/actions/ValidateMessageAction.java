/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.validate.ui.actions;

import de.elomagic.hl7inspector.app.desktop.DocumentsPanel;
import de.elomagic.hl7inspector.app.editor.ui.MessageEditorDocument;
import de.elomagic.hl7inspector.app.profiles.validate.ValidatorService;
import de.elomagic.hl7inspector.platform.ui.actions.AbstractThenDoAction;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
public class ValidateMessageAction extends AbstractThenDoAction {

    private final transient BeanFactory beanFactory;
    private final DocumentsPanel documentsPanel;

    public @Autowired ValidateMessageAction(BeanFactory beanFactory, DocumentsPanel documentsPanel) {
        this.beanFactory = beanFactory;
        this.documentsPanel = documentsPanel;

        setName("validate_messages_dots");
        setShortDescription("validate_selected_messages");
        setSmallIcon("spellcheck.png");

        thenDo(e -> validateMessage());
    }

    private void validateMessage() {
        // TODO Get message from list could be a problem.
        //beanFactory.getBean(ValidatorService.class).validate(selectionModel.getSelectedMessages().toArray(new Message[0]));

        doIfSelectedDocument(d -> beanFactory.getBean(ValidatorService.class).validate(d.getMessage()));
    }

    private void doIfSelectedDocument(@NotNull Consumer<MessageEditorDocument> consumer) {
        documentsPanel.doWithSelectedComponent(MessageEditorDocument.class, consumer);
    }

}
