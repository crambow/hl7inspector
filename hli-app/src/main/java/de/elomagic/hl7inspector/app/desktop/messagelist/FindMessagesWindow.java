/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.messagelist;

import com.alee.extended.image.WebImage;
import com.alee.laf.button.WebToggleButton;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.text.WebTextField;
import com.alee.laf.window.WebFrame;
import com.alee.managers.style.StyleId;
import com.alee.utils.SwingUtils;

import de.elomagic.hl7inspector.app.editor.ui.MessageEditorDocument;
import de.elomagic.hl7inspector.platform.ui.find.ui.actions.FindCaseSensitiveAction;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.events.OpenDocumentEvent;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.ListModel;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyClickMouseListener;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyKeyPressListener;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import java.awt.BorderLayout;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.stream.Collectors;

@Component
@Scope("singleton")
public class FindMessagesWindow extends WebFrame<FindMessagesWindow> {

    private final transient BeanFactory beanFactory;
    private final transient EventMessengerProcessor eventMessengerProcessor;
    private final ListModel<Message> resultModel = new ListModel<>();
    private final MessageListModel model;

    private JToggleButton cbCaseSensitive;

    public FindMessagesWindow(BeanFactory beanFactory, EventMessengerProcessor eventMessengerProcessor, MessageListModel model) {
        super(StyleId.frameDecorated);

        this.beanFactory = beanFactory;
        this.eventMessengerProcessor = eventMessengerProcessor;
        this.model = model;

        initUI();
    }

    private void initUI() {
        setTitle(LocaleTool.get("find_in_messages"));
        setDisplayMaximizeButton(false);
        setDisplayMinimizeButton(false);
        setSize(360, 480);
        setCloseOnFocusLoss(true);

        JPanel findBar = createFindBar();

        JList<Message> resultList = new JList<>();
        resultList.setModel(resultModel);
        resultList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        resultList.setCellRenderer(new MessageSummaryCellRenderer());
        resultList.addMouseListener((SimplifyClickMouseListener) e -> {
            if (SwingUtils.isDoubleClick(e)) {
                openMessage(resultList.getSelectedValue());
            }
        });
        resultList.addKeyListener((SimplifyKeyPressListener) e -> {
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                setVisible(false);
            }
        });

        add(findBar, BorderLayout.NORTH);
        add(resultList, BorderLayout.CENTER);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);

        if (b) {
            Point p = MouseInfo.getPointerInfo().getLocation();
            p.setLocation(p.getX() - (getWidth() / 2), p.getY() - 40);
            setLocation(p);
        }
    }

    public void findPhrase(@NotNull String phrase, boolean caseSensitive) {
        resultModel.clear();

        resultModel.addAll(model.stream()
                .filter(m -> contains(m, phrase, caseSensitive))
                .collect(Collectors.toList()));

        setTitle(LocaleTool.get("find_in_messages_arg", resultModel.size()));
    }

    private boolean contains(@NotNull Message message, @NotNull String phrase, boolean caseSensitive) {
        String text = message.asText();
        text = caseSensitive ? text : text.toUpperCase();
        phrase = caseSensitive ? phrase : phrase.toUpperCase();

        return text.contains(phrase);
    }

    @NotNull
    private JPanel createFindBar() {
        WebTextField editPhrase = new WebTextField();
        editPhrase.setLeadingComponent(new WebImage(IconThemeManager.getImage("find.png")));
        editPhrase.addKeyListener((SimplifyKeyPressListener) e -> {
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                setVisible(false);
            } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                findPhrase(editPhrase.getText(), cbCaseSensitive.isSelected());
            }
        });

        cbCaseSensitive = new WebToggleButton(StyleId.buttonIconHover, new FindCaseSensitiveAction(e -> {
            findPhrase(editPhrase.getText(), cbCaseSensitive.isSelected());
            editPhrase.requestFocus();
        }));

        return FormBuilder.createBuilder(new WebPanel())
                .noBorder()
                .noInsets()
                .add(editPhrase, 1, 1)
                .add(cbCaseSensitive, 1, 0)
                .build();
    }

    private void openMessage(@NotNull Message message) {
        MessageEditorDocument med = beanFactory.getBean(MessageEditorDocument.class);
        med.bindMessage(message);
        eventMessengerProcessor.publishEvent(new OpenDocumentEvent(this, message.getId(), med, message.get(0).get(9).asText()));
    }

}
