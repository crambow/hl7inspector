/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.messagelist;

import com.alee.extended.label.WebStyledLabel;
import com.alee.laf.list.ListCellParameters;
import com.alee.laf.list.WebListCellRenderer;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JList;
import javax.swing.border.EmptyBorder;
import java.awt.Component;
import java.text.MessageFormat;

public class MessageSummaryCellRenderer extends WebListCellRenderer<Message, JList<Message>, ListCellParameters<Message, JList<Message>>> {

    private static final Logger LOGGER = LogManager.getLogger(MessageSummaryCellRenderer.class);

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        try {
            Message message = (Message) value;

            String timestamp;
            String title;

            if (message == null || message.size() == 0 || message.get(0).size() < 10) {
                timestamp = "";
                title = LocaleTool.get("unable_to_get_message_type");
            } else {
                timestamp = message.get(0).get(7).toHtmlEscapedString();
                title = message.get(0).get(9).toHtmlEscapedString();
            }

            WebStyledLabel label = (WebStyledLabel) super.getListCellRendererComponent(list, title, index, isSelected, cellHasFocus);

            String subline = LocaleTool.get("file_1arg", message.getMeta().isInMemory() ? LocaleTool.get("untitled") : message.getMeta().getFile().getFileName().toString());
            String file = message.getMeta().isInMemory() ? LocaleTool.get("not_saved") : LocaleTool.get("file_1arg", message.getMeta().getFile().toString());
            String tooltipPattern = "<html>{0}<br>{1}</html>";
            String tooltip = MessageFormat.format(tooltipPattern, title, file);
            label.setToolTipText(tooltip);

            String pattern = "<html><b>{0}</b> {2}<br><small>{1}</small></<html>";
            String text = MessageFormat.format(pattern, title, subline, timestamp);

            label.setBorder(new EmptyBorder(4, 4, 4, 4));
            label.setText(text);

            return label;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

}
