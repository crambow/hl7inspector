/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.load;

import de.elomagic.hl7inspector.shared.hl7.model.Delimiters;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.io.IOBytesReadListener;
import de.elomagic.hl7inspector.shared.io.MLLPMessageInputStream;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions.StreamFormat;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * HL7 message stream reader.
 */
public class MessageFileInputStream {

    private long bytesReads = 0;
    private final List<IOBytesReadListener> listener = new ArrayList<>();
    private final InputStream in;
    private final Charset encoding;
    private StreamFormat format;
    private final Frame frame;
    private String bufferedLine = "";
    private LineNumberReader lineReader = null;
    private MLLPMessageInputStream messageInputStream;

    /**
     *
     * @param in Input stream to read messages from
     * @param format When FRAMED then it must the exact
     * @param frame Must be set when format is {@link StreamFormat#FRAMED} or {@link StreamFormat#AUTO_DETECT}
     */
    public MessageFileInputStream(@NotNull InputStream in, @NotNull Charset encoding, @NotNull StreamFormat format, @NotNull Frame frame) {
        this.in = in;
        this.encoding = encoding;
        this.format = format;
        this.frame = frame;
    }

    @Nullable
    public Message readMessage() throws IOException {
        return readNextMessage();
    }

    public long getBytesRead() {
        return bytesReads;
    }

    public void addListener(@NotNull IOBytesReadListener value) {
        listener.add(value);
    }

    public void removeListener(@NotNull IOBytesReadListener value) {
        listener.remove(value);
    }

    @Nullable
    private Message readNextMessage() throws IOException {
        Message result;

        switch(Objects.requireNonNull(determineFormat())) {
            case FRAMED:
                result = readFramed();
                break;

            case TEXT_LINE:
                result = readTextLines();
                break;

            default:
                throw new IOException("Unknown format detection setup. Check your configuration.");
        }

        return result;
    }

    @Nullable
    private Message readFramed() throws IOException {
        if(messageInputStream == null) {
            messageInputStream = new MLLPMessageInputStream(in, frame, encoding);
            // TODO messageInputStream.addListener();
        }

        return messageInputStream.readMessage();
    }

    @Nullable
    private Message readTextLines() throws IOException {
        Message result = null;

        if(lineReader == null) {
            lineReader = new LineNumberReader(new InputStreamReader(in, encoding));
        }

        String line = bufferedLine.length() == 0 ? lineReader.readLine() : bufferedLine;
        bufferedLine = "";

        boolean done = false;

        while((line != null) && !done) {
            int xOffset = line.indexOf("MSH");
            if(xOffset != -1) { // New message begins
                Delimiters del = new Delimiters(line.substring(xOffset + 3));

                List<String> msgText = new ArrayList<>();

                while((line != null) && !done) {
                    bytesReads += line.length() + 1;
                    if(!line.isEmpty()) {
                        String seg = (line.length() > xOffset + 3) ? line.substring(xOffset) : "";

                        if(!msgText.isEmpty() && seg.startsWith("MSH")) {
                            bufferedLine = line;
                            done = true;
                        } else {
                            if(seg.indexOf(del.fieldDelimiter) == 3) {
                                msgText.add(line.substring(xOffset));
                            }
                        }
                    }
                    if(!done) {
                        line = lineReader.readLine();
                    }

                }

                StringBuilder m = new StringBuilder();
                for(String s : msgText) {
                    m.append(s).append((char)0xd);
                }

                result = new Message();
                result.parse(m.toString(), del);
            } else {
                bytesReads += line.length() + 1;
                line = lineReader.readLine();
            }
        }

        return result;
    }

    private StreamFormat determineFormat() throws IOException {
        if (format != StreamFormat.AUTO_DETECT) {
            return format;
        }

        in.mark(1);
        int b = in.read();
        in.reset();
        if(b == -1) {
            return null;
        }

        format = b == frame.getStartByte() ? StreamFormat.FRAMED : StreamFormat.TEXT_LINE;

        fireBytesReceived(LocaleTool.get("using_parser_format", format == StreamFormat.FRAMED ? "FRAMED_FORMAT" : "LINE_FORMAT"));

        return format;
    }

    private void fireBytesReceived(@NotNull String s) {
        for(IOBytesReadListener l : listener) {
            l.bytesRead(this, s.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        }
    }

}
