/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app;

import de.elomagic.hl7inspector.platform.configuration.Configuration;
import de.elomagic.hl7inspector.shared.configuration.Frame;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

public class MessageWriterBean {

    private static final Configuration CONFIGURATION = Configuration.getInstance();

    public static final int INDEX_DIGITS = 5;

    private Frame frame = new Frame();
    private Path destinationFolder = CONFIGURATION.getAppFilesLastUsedFolder();
    private boolean onlySelectedFiles = false;
    private boolean manyFiles = true;
    private String dataFilePrefix = "";
    private String dataFileExtension = "hl7";
    private String semaphoreExtension = "sem";
    private boolean generateSemaphore = true;
    private Path singleFileName = null;
    private Charset encoding = StandardCharsets.ISO_8859_1;

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public Path getDestinationFolder() {
        return destinationFolder;
    }

    public void setDestinationFolder(Path destinationFolder) {
        this.destinationFolder = destinationFolder;
    }

    public boolean isOnlySelectedFiles() {
        return onlySelectedFiles;
    }

    public void setOnlySelectedFiles(boolean onlySelectedFiles) {
        this.onlySelectedFiles = onlySelectedFiles;
    }

    public boolean isManyFiles() {
        return manyFiles;
    }

    public void setManyFiles(boolean manyFiles) {
        this.manyFiles = manyFiles;
    }

    public String getDataFilePrefix() {
        return dataFilePrefix;
    }

    public void setDataFilePrefix(String dataFilePrefix) {
        this.dataFilePrefix = dataFilePrefix;
    }

    public String getDataFileExtension() {
        return dataFileExtension;
    }

    public void setDataFileExtension(String dataFileExtension) {
        this.dataFileExtension = dataFileExtension;
    }

    public String getSemaphoreExtension() {
        return semaphoreExtension;
    }

    public void setSemaphoreExtension(String semaphoreExtension) {
        this.semaphoreExtension = semaphoreExtension;
    }

    public boolean isGenerateSemaphore() {
        return generateSemaphore;
    }

    public void setGenerateSemaphore(boolean generateSemaphore) {
        this.generateSemaphore = generateSemaphore;
    }

    public Path getSingleFileName() {
        return singleFileName;
    }

    public void setSingleFileName(Path singleFileName) {
        this.singleFileName = singleFileName;
    }

    public Charset getEncoding() {
        return encoding;
    }

    public void setEncoding(Charset encoding) {
        this.encoding = encoding;
    }
}
