/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import com.alee.api.data.CompassDirection;
import com.alee.extended.dock.WebDockableFrame;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.app.profiles.MessageDescriptor;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.app.profiles.ProfileServiceImpl;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.html.HTMLDocument;

/**
 * The panel on the right side of the main frame.
 */
@Component
public class DetailPanel extends WebDockableFrame {

    private final ProfileServiceImpl profileService;
    private JEditorPane editorPane;

    @Autowired
    public DetailPanel(ProfileServiceImpl profileService) {
        super("details", LocaleTool.get("details"));

        this.profileService = profileService;
    }

    @PostConstruct
    private void initUI() {
        setPosition(CompassDirection.east);

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder());
        setPreferredSize(new Dimension(200, 600));

        editorPane = new JEditorPane();
        editorPane.putClientProperty(StyleId.STYLE_PROPERTY, StyleId.editorpane);
        editorPane.setEditable(false);
        editorPane.setContentType("text/html");

        if(Font.getFont("Arial") != null) {
            editorPane.setFont(Font.getFont("Arial"));
        }

        Color c = editorPane.getForeground();
        String rgb = String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
        ((HTMLDocument)editorPane.getDocument()).getStyleSheet().addRule("body { color: " + rgb + "; }");

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(editorPane);

        JLabel captionPane = new JLabel(LocaleTool.get("details"));
        captionPane.setBorder(new EmptyBorder(4, 6, 0, 4));

        add(captionPane, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
    }

    @EventListener
    public void handleSelectionChanged(MessageNodeSelectionChangedEvent e) {
        Profile profile = profileService.getDefault();

        if(isVisible() && profile != null) {
            MessageDescriptor md = new MessageDescriptor(profile);
            editorPane.setText(md.getDescription(e.getPath()));
        } else {
            editorPane.setText("");
        }
    }

}
