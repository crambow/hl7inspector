/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.editor.ui;

import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.shared.events.EditValuePresenterEvent;
import de.elomagic.hl7inspector.shared.events.MessageNodeSelectionChangedEvent;
import de.elomagic.hl7inspector.shared.events.ShowDocumentFindBarEvent;
import de.elomagic.hl7inspector.app.editor.events.TreeCompactViewModeChangedEvent;
import de.elomagic.hl7inspector.platform.ui.find.DocumentFindMethods;
import de.elomagic.hl7inspector.platform.ui.find.ui.FindBar;
import de.elomagic.hl7inspector.shared.hl7.HL7Path;
import de.elomagic.hl7inspector.shared.hl7.HL7Tool;
import de.elomagic.hl7inspector.shared.hl7.model.HL7ObjectType;
import de.elomagic.hl7inspector.shared.hl7.model.Hl7Object;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.ui.facades.RefreshUIFacade;
import de.elomagic.hl7inspector.shared.ui.facades.SetCompactTreeViewFacade;
import de.elomagic.hl7inspector.shared.ui.facades.SetProfileFacade;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.ui.DragAndDropService;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.components.ExtendedTooltip;
import de.elomagic.hl7inspector.platform.ui.listener.SimplifyClickMouseListener;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;
import de.elomagic.hl7inspector.platform.utils.StringEscapeUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolTip;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
@Scope("prototype")
public class HL7Tree extends JScrollPane implements RefreshUIFacade, SetProfileFacade, SetCompactTreeViewFacade, DocumentFindMethods {

    private static final Logger LOGGER = LogManager.getLogger(HL7Tree.class);

    private final transient EventMessengerProcessor eventMessengerProcessor;
    private final transient DragAndDropService dragAndDropService;
    private final transient HL7TreeModel model;
    private final HL7ObjectCellRenderer nodeRenderer = new HL7ObjectCellRenderer();

    private FindBar findBar;
    private final JTree tree;

    public @Autowired HL7Tree(EventMessengerProcessor eventMessengerProcessor, DragAndDropService dragAndDropService) {
        this.eventMessengerProcessor = eventMessengerProcessor;
        this.dragAndDropService = dragAndDropService;
        this.model = new HL7TreeModel();

        this.tree = new JTree(model);
    }

    @PostConstruct
    private void initUI() {
        setBorder(BorderFactory.createEmptyBorder());

        tree.putClientProperty(StyleId.STYLE_PROPERTY, StyleId.tree);
        tree.setModel(model);
        tree.setRootVisible(false);
        tree.setExpandsSelectedPaths(true);
        tree.setToolTipText("");
        tree.setCellRenderer(nodeRenderer);

        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.getSelectionModel().addTreeSelectionListener(this::handleTreeSelectionChanged);
        tree.addMouseListener((SimplifyClickMouseListener)this::handleMouseClick);

        dragAndDropService.addMessageDropSupport(tree);

        findBar = new FindBar();
        findBar.setVisible(false);
        findBar.setPresenter(this);
        findBar.addPropertyChangeListener("visible", e -> {
            boolean visible = (boolean)e.getNewValue();
            setColumnHeaderView(visible ? findBar : null);
            if (!visible) {
                tree.requestFocus();
            }
        });

        setViewportView(tree);
    }

    @Override
    public JPopupMenu getComponentPopupMenu() {
        return tree.getComponentPopupMenu();
    }

    @Override
    public void setComponentPopupMenu(@Nullable JPopupMenu popup) {
        tree.setComponentPopupMenu(popup);
    }

    private void handleMouseClick(MouseEvent e) {
        if(e.getClickCount() == 2) {
            TreePath path = tree.getPathForLocation(e.getX(), e.getY());
            if(path != null) {
                Hl7Object<?> o = (Hl7Object<?>)path.getLastPathComponent();
                if((HL7ObjectType.get(o) != HL7ObjectType.MESSAGE) && (o.getChildCount() == 0)) {
                    editNode();
                }
            }
        }
    }

    private void handleTreeSelectionChanged(TreeSelectionEvent e) {
        TreePath treePath = e.getNewLeadSelectionPath();
        Hl7Object<?> o = treePath == null ? null : (Hl7Object<?>)treePath.getLastPathComponent();
        Message message = o == null ? null : o.getRoot();
        HL7Path path = o == null ? null : o.toPath();
        eventMessengerProcessor.publishEvent(new MessageNodeSelectionChangedEvent(this, message, path, null));
    }

    @Override
    public JToolTip createToolTip() {
        ExtendedTooltip tip = new ExtendedTooltip();
        tip.setComponent(tree);
        return tip;
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        String tt = "";
        TreePath path = tree.getPathForLocation(event.getX(), event.getY());

        if((path != null) && (path.getLastPathComponent() instanceof Hl7Object)) {
            Hl7Object<?> obj = (Hl7Object<?>)path.getLastPathComponent();

            tt = "";

            if(obj.getValidationText() != null) {
                tt = tt.concat(obj.getValidationText());
            }
        }

        return "".equals(tt) ? null : tt;
    }

    public HL7TreeModel getModel() {
        return (HL7TreeModel)tree.getModel();
    }

    public void bindMessage(Message message) {
        ((HL7TreeModel)tree.getModel()).bindMessage(message);
    }

    public void selectNode(@Nullable HL7Path path) {
        TreePath tp = HL7Tool.toTreePath(getModel().getRoot(), path);
        tree.expandPath(tp);
        tree.getSelectionModel().setSelectionPath(tp);
    }

    public List<Hl7Object<?>> getSelectedObjects() {
        List<Hl7Object<?>> result = new ArrayList<>();

        if (tree.getSelectionPaths() == null) {
            return result;
        }

        for(final TreePath path : tree.getSelectionPaths()) {
            result.add((Hl7Object<?>)path.getLastPathComponent());
        }

        return result;
    }

    /**
     * Edit selected node.
     */
    public void editNode() {
        eventMessengerProcessor.publishEvent(new EditValuePresenterEvent(this, true));
    }

    /**
     * Confirm and remove selected node.
     */
    public void removeNode() {
        List<Hl7Object<?>> selectedObjects = getSelectedObjects();

        if(selectedObjects.isEmpty()) {
            SimpleDialog.info(LocaleTool.get("no_node_selected"));
        } else if(selectedObjects.size() > 1) {
            SimpleDialog.error(LocaleTool.get("only_one_selected_node_can_clear"));
        } else if(SimpleDialog.confirmYesNo(LocaleTool.get("clear_selected_nodes_question")) == SimpleDialog.YES_OPTION) {
            model.removeHL7Object(selectedObjects.get(0));
        }
    }

    public void selectPath(TreePath path) {
        tree.expandPath(path.getParentPath());

        int row = tree.getRowForPath(path);

        tree.scrollRowToVisible(row);
        tree.setSelectionRow(row);
    }

    @Override
    public void setProfile(Profile profile) {
        nodeRenderer.setProfile(profile);
        tree.updateUI();
    }

    @Override
    public void refresh(Hl7Object<?> o) {
        model.fireTreeStructureChanged(o);
    }

    @Override
    public void setCompactView(@Nullable Boolean compact) {
        model.setCompactView(Objects.requireNonNullElseGet(compact, () -> !model.isCompactView()));

        eventMessengerProcessor.publishEvent(new TreeCompactViewModeChangedEvent(this, model.isCompactView()));
    }

    @EventListener(ShowDocumentFindBarEvent.class)
    public void handleShowDocumentFindBarEvent() {
        if (isShowing()) {
            findBar.setVisible(true);
        }
    }

    @Override
    public void findNextPhrase(@NotNull String phrase, boolean caseSensitive) {
        try {
            String escapedPhrase = StringEscapeUtils.escapeHtml(phrase);

            List<Hl7Object<?>> selectedObjects = getSelectedObjects();

            findNextPhrase(escapedPhrase, selectedObjects.isEmpty() ? null : selectedObjects.get(0), caseSensitive);
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(ex.getMessage());
        }
    }

    private void findNextPhrase(@NotNull String escapedPhrase, @Nullable Hl7Object<?> startFrom, boolean caseSensitive) {
        if(!escapedPhrase.isEmpty() && getModel().getChildCount(getModel().getRoot()) != 0) {
            Hl7Object<?> startingNode = startFrom == null ? model.getMessage() : startFrom;
            TreePath path = TreeNodeSearchEngine.findNextNode(escapedPhrase, caseSensitive, startingNode);

            if(path == null) {
                SimpleDialog.info(LocaleTool.get("end_of_message_tree_reached"));
            } else {
                selectPath(path.getParentPath());
                // Note, selection editor works only when edit has focus. So, no need to implement it here.
            }
        }
    }

    @Override
    public void highlightPhrase(@Nullable String phrase, boolean caseSensitive) {
        nodeRenderer.setHighlighted(phrase, caseSensitive);
    }

    @Override
    public boolean supportHighlightPhrase() {
        return true;
    }
}
