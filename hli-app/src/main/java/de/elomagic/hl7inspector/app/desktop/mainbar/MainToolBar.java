/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.mainbar;

import de.elomagic.hl7inspector.app.desktop.mainmenu.actions.ExitAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.NewMessageAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.OpenFileExternalAction;
import de.elomagic.hl7inspector.app.desktop.messagelist.actions.RemoveMessagesAction;
import de.elomagic.hl7inspector.app.editor.events.TreeCompactViewModeChangedEvent;
import de.elomagic.hl7inspector.app.editor.ui.actions.ViewCompressedAction;
import de.elomagic.hl7inspector.app.load.ui.actions.OpenMessageFileAction;
import de.elomagic.hl7inspector.app.profiles.validate.ui.actions.ValidateMessageAction;
import de.elomagic.hl7inspector.app.save.ui.actions.SaveMessageFileAsAction;
import de.elomagic.hl7inspector.app.ui.actions.ShowParserWindowAction;
import de.elomagic.hl7inspector.platform.ui.UIFactory;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;
import de.elomagic.hl7inspector.shared.desktop.MainToolBarDescriptor;
import de.elomagic.hl7inspector.shared.ui.actions.ShowDocumentFindBarAction;

import com.alee.managers.style.StyleId;
import com.alee.managers.style.StyleManager;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.util.Comparator;

@org.springframework.stereotype.Component
public class MainToolBar extends JToolBar {

    private final transient SharedConfiguration configuration = SharedConfiguration.getInstance();

    private JToggleButton btCompactView;

    private final transient BeanFactory beanFactory;
    private final transient ListableBeanFactory listableBeanFactory;

    @Autowired
    public MainToolBar(BeanFactory beanFactory, ListableBeanFactory listableBeanFactory) {
        this.beanFactory = beanFactory;
        this.listableBeanFactory = listableBeanFactory;
    }

    @PostConstruct
    private void initUI() {
        setRollover(true);
        setFloatable(true);
        setOrientation(configuration.getToolbarPosition().getOrientation());

        StyleManager.setStyleId(this, StyleId.toolbarUndecorated);

        rebuildButtons();
    }

    public void rebuildButtons() {
        removeAll();

        // TODO Buggy because it will create a new instance of HL7TreeModel :-(
        btCompactView = UIFactory.createFlatIconButton(JToggleButton.class, beanFactory.getBean(ViewCompressedAction.class));
        btCompactView.setSelected(true);

        // FEATURE Button for rereading file.

        add(beanFactory.getBean(RemoveMessagesAction.class));
        add(beanFactory.getBean(NewMessageAction.class));
        add(beanFactory.getBean(OpenMessageFileAction.class));
        add(beanFactory.getBean(SaveMessageFileAsAction.class));
        addSeparator();
        add(beanFactory.getBean(ShowDocumentFindBarAction.class));
        addSeparator();
        add(btCompactView);
        add(beanFactory.getBean(ValidateMessageAction.class));
        addSeparator();
        add(beanFactory.getBean(OpenFileExternalAction.class));
        addSeparator();

        listableBeanFactory
                .getBeansOfType(MainToolBarDescriptor.class)
                .values()
                .stream()
                .sorted(Comparator.comparingDouble(MainToolBarDescriptor::getPriority))
                .filter(d -> d.getMenuBarItems() != null)
                .flatMap(d -> d.getMenuBarItems().stream())
                .forEach(this::add);

        add(beanFactory.getBean(ShowParserWindowAction.class));

        addSeparator();
        add(beanFactory.getBean(ExitAction.class));
    }

    @Override
    public Component add(Component comp) {
        if (AbstractButton.class.isAssignableFrom(comp.getClass())) {
            AbstractButton button = (AbstractButton) comp;
            button.setText(null);
            button.putClientProperty( StyleId.STYLE_PROPERTY, StyleId.buttonIconHover);
        }

        return super.add(comp);
    }

    @EventListener
    public void handleCompactViewModeChanged(TreeCompactViewModeChangedEvent e) {
        btCompactView.setSelected(e.isCompact());
    }

}
