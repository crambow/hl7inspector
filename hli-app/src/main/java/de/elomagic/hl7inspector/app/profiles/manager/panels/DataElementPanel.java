/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.manager.panels;

import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.app.profiles.ui.DataElementModel;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.swing.Icon;

@Component
public class DataElementPanel extends ProfileTablePanel<DataElementModel> {

    @Override
    protected DataElementModel createProfileModel() {
        return new DataElementModel();
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("data_elements");
    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("table-32.png");
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public void bindBean(@NotNull Profile profile) {
        getModel().setList(profile.getDataElementList());
    }

}
