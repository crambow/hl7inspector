/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.app.profiles.manager.panels.CommonPanel;
import de.elomagic.hl7inspector.app.profiles.manager.panels.DataElementPanel;
import de.elomagic.hl7inspector.app.profiles.manager.panels.DataTypePanel;
import de.elomagic.hl7inspector.app.profiles.manager.panels.SegmentPanel;
import de.elomagic.hl7inspector.app.profiles.manager.panels.TablePanel;
import de.elomagic.hl7inspector.app.profiles.manager.panels.ValidatePanel;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.profiles.ProfileFile;
import de.elomagic.hl7inspector.app.profiles.ProfileServiceImpl;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.components.AbstractPanelDialog;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class ProfileDefinitionDialog extends AbstractPanelDialog<Profile> {

    private static final Logger LOGGER = LogManager.getLogger(ProfileDefinitionDialog.class);

    private final ProfileServiceImpl profileService;

    private ProfileFile profileFile;
    private transient Profile profile;

    @Autowired
    public ProfileDefinitionDialog(BeanFactory beanFactory, ProfileServiceImpl profileService) {
        super(beanFactory, LocaleTool.get("profile_definition"), true);

        this.profileService = profileService;
    }

    public boolean ask(ProfileFile profileFile) {
        boolean result = false;
        this.profileFile = profileFile;

        profile = new Profile();

        try {
            profile = profileService.getProfile(profileFile.getPath());

            bindBean(profile);

            result = super.ask();
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            SimpleDialog.error(LocaleTool.get("unable_to_read_profile"), ex);
        }

        return result;
    }

    @Override
    public void bindBean(@NotNull Profile profile) {
        super.bindBean(profile);

        try {
            getPanel(CommonPanel.class).resetValidateStatus();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @PostConstruct
    private void initUI() {
        try {
            getPanel(CommonPanel.class).setDialog(this);
            getPanel(SegmentPanel.class).setDialog(this);
            getPanel(DataElementPanel.class).setDialog(this);
            getPanel(DataTypePanel.class).setDialog(this);
            getPanel(TablePanel.class).setDialog(this);

            setSize(750, 500);
            center(getOwner());
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
            SimpleDialog.error(e.getMessage(), e);
        }
    }

    @Override
    protected List<Class<? extends AbstractBannerPanel<Profile>>> getPanelClasses() {
        return List.of(
                CommonPanel.class,
                ValidatePanel.class,
                SegmentPanel.class,
                DataElementPanel.class,
                DataTypePanel.class,
                TablePanel.class);
    }

    public ProfileFile getProfileFile() {
        profileFile.setName(profile.getName());
        profileFile.setDescription(profile.getDescription());

        return profileFile;
    }

    public Profile getProfile() {
        return profile;
    }
}
