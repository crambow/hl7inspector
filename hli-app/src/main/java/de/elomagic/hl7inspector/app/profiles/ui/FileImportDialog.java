/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.ui;

import de.elomagic.hl7inspector.platform.ui.GenericFileFilter;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.BorderLayout;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class FileImportDialog extends MyBaseDialog {

    private static final Logger LOGGER = LogManager.getLogger(FileImportDialog.class);
    
    private File selectedFile = null;
    private transient TableModel model;
    private JTextField editFile;
    private JSpinner btBeginFrom;
    private JComboBox<String> cbSepChar;
    private JScrollPane scroller;
    private JTable tblMap;
    private JTable tblPreview;

    public FileImportDialog() {
        super(LocaleTool.get("file_import"), true);
    }

    @PostConstruct
    private void init() {
        getBanner().setVisible(false);
        setAlwaysOnTop(true);

        editFile = new JTextField();
        editFile.setEditable(false);

        JButton btChooseFile = new JButton("...");
        btChooseFile.addActionListener(e -> selectFilename());

        btBeginFrom = new JSpinner(new SpinnerNumberModel(1, 1, 99, 1));
        btBeginFrom.addChangeListener(e -> {
            if(selectedFile != null) {
                loadFile(selectedFile);
            }
        });

        cbSepChar = new JComboBox<>(new String[] {",", ";", "TAB", "SPACE"});
        cbSepChar.addItemListener(e -> {
            if(selectedFile != null) {
                loadFile(selectedFile);
            }
        });

        initTables();

        cbSepChar.setSelectedIndex(0);

        JPanel panel = FormBuilder.createBuilder()
                .noBorder()
                .addSection("filename", 4)
                .add("filename", editFile, 2, 1)
                .addRow(btChooseFile, 1, 0)
                .addSection("options", 4)
                .addRow("begin_line", btBeginFrom)
                .addRow("separate_char", cbSepChar)
                .addSection("columns_mapping", 4)
                .addRow(scroller, 4, 1, 1, 1)
                .build();

        add(panel, BorderLayout.CENTER);
        pack();
        setSize(Math.max(getPreferredSize().width, 640), Math.max(getPreferredSize().height, 480));
        center(getOwner());
    }

    public void setModel(TableModel model) {
        this.model = model;
    }

    public List<String> getMappingList() {
        List<String> v = new ArrayList<>();

        TableModel m = tblMap.getModel();

        for(int i = 1; i < m.getColumnCount(); i++) {
            v.add(m.getValueAt(0, i).toString());
        }

        return v;
    }

    public File getFile() {
        return selectedFile;
    }

    private void initTables() {
        tblMap = new JTable();
        tblPreview = new JTable();

        JPanel panel = FormBuilder.createBuilder()
                .noBorder()
                .addRow(tblMap, 1, 1, 1, 1)
                .addRow(tblPreview, 1, 1, 1, 1)
                .build();

        scroller = new JScrollPane(panel);
    }

    private void selectFilename() {
        Path path = Paths.get(editFile.getText().isEmpty() ? System.getProperty("user.dir") : editFile.getText());

        SimpleDialog.chooseFile(
                path,
                LocaleTool.get("choose_csv_import_file"),
                e -> {
                    editFile.setText(e.getPath().toString());
                    selectedFile = e.getPath().toFile();
                    loadFile(e.getPath().toFile());
                },
                GenericFileFilter.TEXT_FILTER, GenericFileFilter.CSV_FILTER
        );
    }

    public char getSeparatorChar() {
        char sep;

        switch(cbSepChar.getSelectedIndex()) {
            case 1:
                sep = ';';
                break;
            case 2:
                sep = (char)7;
                break;
            case 3:
                sep = ' ';
                break;
            default:
                sep = ',';
                break;
        }

        return sep;
    }

    public int getBeginFromLine() {
        return Integer.parseInt(btBeginFrom.getValue().toString());
    }

    private void loadFile(File file) {
        List<List<?>> lines = new ArrayList<>();

        char sep = getSeparatorChar();

        try {
            try (FileReader rin = new FileReader(file); LineNumberReader lin = new LineNumberReader(rin)) {
                int l = 0;
                String line = lin.readLine();

                int begin = Integer.parseInt(btBeginFrom.getValue().toString());

                while((line != null) && (l < (10 + begin))) {
                    l++;

                    if(l >= begin) {
                        lines.add(Arrays.asList(StringUtils.split(line, sep)));
                    }

                    line = lin.readLine();
                }
            }
        } catch(IOException | NumberFormatException e) {
            LOGGER.error(e.getMessage(), e);
            SimpleDialog.error(e);
        }

        updatePreview(lines);
    }

    private void updatePreview(List<List<?>> lines) {
        ArrayList<String> mapItems = new ArrayList<>();
        mapItems.add("-");

        for(int i = 0; i < model.getColumnCount(); i++) {
            mapItems.add(model.getColumnName(i));
        }

        ImportFileModel importModel = new ImportFileModel(lines);

        JComboBox<String> cbMapper = new JComboBox<>(mapItems.toArray(new String[0]));

        tblMap.setModel(new MapFieldModel(importModel));
        tblMap.getColumnModel().getColumn(0).setMaxWidth(40);
        tblMap.getModel().addTableModelListener(new MappingChangeAction());
        tblMap.setDefaultEditor(String.class, new DefaultCellEditor(cbMapper));
        tblMap.setDefaultRenderer(String.class, new MapFieldCellRenderer<String>());
        tblMap.setDefaultRenderer(JLabel.class, new PreviewCellRenderer());

        tblPreview.setModel(importModel);
        tblPreview.getColumnModel().getColumn(0).setMaxWidth(40);
        tblPreview.setDefaultRenderer(Object.class, new PreviewCellRenderer());

        pack();
    }

    class MappingChangeAction implements TableModelListener {
        @Override
        public void tableChanged(javax.swing.event.TableModelEvent e) {
            if(e.getType() == TableModelEvent.UPDATE) {
                int col = e.getColumn();
                Object aValue = tblMap.getModel().getValueAt(0, col);

                for(int i = 0; i < tblMap.getModel().getColumnCount(); i++) {
                    if((i != col) && (tblMap.getModel().getValueAt(0, i).equals(aValue)) && !("-".equals(aValue))) {
                        tblMap.getModel().setValueAt("-", 0, i);
                    }
                }
            }
        }
    }
}
