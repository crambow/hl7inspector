/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.save;

import com.alee.laf.window.WebDialog;

import de.elomagic.hl7inspector.app.MessageWriterBean;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.swing.*;

@Component
public class SaveProgressDialog extends WebDialog<SaveProgressDialog> implements MessageFileWriterListener, ActionListener {

    private final JLabel lblDest = new JLabel();
    private final JLabel lblMessageIndex = new JLabel("0");
    private final JLabel lblCount = new JLabel("0");
    private final JProgressBar bar = new JProgressBar(SwingConstants.HORIZONTAL);
    private transient MessageFileWriterThread thread = null;
    private boolean userAbort;
    private int messageIndex = 0;
    private transient List<Message> messages;

    @Autowired
    public SaveProgressDialog() {
        super(MyBaseDialog.getFocusedWindow());
    }

    public void start(List<Message> messageList, MessageWriterBean bean) {
        messages = messageList;

        if (thread != null) {
            thread.requestTermination();
        }

        thread = new MessageFileWriterThread(messageList, bean);
        thread.addListener(this);
        thread.start();

        setVisible(true);
    }

    @PostConstruct
    private void init() {
        setModal(true);
        setTitle(LocaleTool.get("save_messages_progress"));
        setIconImage(IconThemeManager.getImage("hl7inspector-32.png"));
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        setLayout(new BorderLayout());

        lblDest.getFont().deriveFont(Font.BOLD);
        lblDest.setText(LocaleTool.get("unknown"));

        bar.setIndeterminate(false);

        JButton btAbort = new JButton(LocaleTool.get("abort"));
        btAbort.addActionListener(this);
        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(btAbort);


        JPanel panel = FormBuilder.createBuilder()
                .addSection("destination", 10)
                .addRow("file", lblDest, 9, 1)
                .addRowSpacer(10)
                .addSection("progress", 10)
                .add("message", lblMessageIndex, 1, 0)
                .addRow("total", lblCount)
                .addRow(bar, 10, 1)
                .addRow(buttonPanel, 10, 1)
                .addRowSpacer()
                .build();

        getContentPane().add(panel, BorderLayout.CENTER);

        pack();

        setSize(400, getPreferredSize() == null ? 230 : getPreferredSize().height);

        setLocationRelativeTo(getOwner());
    }

    @Override
    public void actionPerformed(ActionEvent ee) {
        if(thread != null) {
            thread.requestTermination();
            userAbort = true;
        }
    }

    // Interface MessageWriterListener
    @Override
    public void messageSaved(MessageFileWriterThread source, Path file, int count) {
        messageIndex++;

        SwingUtilities.invokeLater(() -> {
            if(file != null) {
                lblDest.setText(file.toAbsolutePath().toString());
            }

            lblMessageIndex.setText(Integer.toString(messageIndex));
            lblCount.setText(Integer.toString(messages.size()));

            bar.setMaximum(messages.size());
            bar.setValue(messageIndex);
        });
    }

    @Override
    public void writerDone(MessageFileWriterThread source, int count) {
        setVisible(false);
        SimpleDialog.info(LocaleTool.get(userAbort ? "saving_message_abort_by_user" : "saving_message_successful_done"));
    }

}
