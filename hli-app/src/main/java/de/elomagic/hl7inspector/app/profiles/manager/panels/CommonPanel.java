/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.profiles.manager.panels;

import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.platform.ui.components.AbstractBannerPanel;
import de.elomagic.hl7inspector.platform.ui.FormBuilder;
import de.elomagic.hl7inspector.platform.ui.components.AbstractPanelDialog;
import de.elomagic.hl7inspector.platform.ui.SimpleDialog;
import de.elomagic.hl7inspector.platform.ui.themes.IconThemeManager;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.util.List;

@Component
public class CommonPanel extends AbstractBannerPanel<Profile> {

    private AbstractPanelDialog<Profile> dialog;

    private transient Profile profile;

    private final JTextField editName;
    private final JTextField edtDesc;
    private final JLabel lbValidateStatus;

    public CommonPanel() {
        super();

        editName = new JTextField();
        edtDesc = new JTextField();
        JButton btValidate = new JButton(new ValidateProfileAction());
        lbValidateStatus = new JLabel();
        lbValidateStatus.setBorder(new EmptyBorder(3, 3, 3, 3));
        lbValidateStatus.setOpaque(true);
        lbValidateStatus.setBackground(SystemColor.info);
        lbValidateStatus.setForeground(SystemColor.infoText);

        JPanel panel = FormBuilder.createBuilder()
                .noBorder()
                .addRow("name", editName, 1, 1)
                .addRow("description", edtDesc, 1, 1)
                .addRowSpacer(10)
                .addRow(btValidate, 2, 0)
                .addRow(lbValidateStatus, 3, 1)
                .addRowSpacer()
                .build();

        add(panel, BorderLayout.CENTER);
    }

    public void setDialog(@NotNull AbstractPanelDialog<Profile> dialog) {
        this.dialog = dialog;
    }

    public void resetValidateStatus() {
        lbValidateStatus.setText(LocaleTool.get("validate_status_unknown_press_validate_button"));
        lbValidateStatus.setIcon(IconThemeManager.getImageIcon("warning.png"));
    }

    public void setValidateStatus(boolean valid) {
        lbValidateStatus.setText(valid ? LocaleTool.get("profile_is_valid") : LocaleTool.get("profile_includes_errors_press_validate_to_show_errors"));
        lbValidateStatus.setIcon(IconThemeManager.getImageIcon(valid ? "check.png" : "warning.png"));
    }

    @Override
    public String getTitle() {
        return LocaleTool.get("common");
    }

    @Override
    public Icon getIcon() {
        return IconThemeManager.getImageIcon("info-32.png");
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public void bindBean(@NotNull Profile profile) {
        this.profile = profile;

        profile.setDescription(edtDesc.getText());
        profile.setName(editName.getText());
    }

    @Override
    public void commit() {
        profile.setDescription(edtDesc.getText());
        profile.setName(editName.getText());
    }

    class ValidateProfileAction extends AbstractAction {
        public ValidateProfileAction() {
            super(LocaleTool.get("validate_profile"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            dialog.commit();

            List<String> val = profile.validate();

            setValidateStatus(val.isEmpty());

            if(!val.isEmpty()) {
                SimpleDialog.warn(LocaleTool.get("list_of_invalid_profile_entries"), String.join("\n", val));
            }
        }
    }

}
