/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.app.desktop.messagelist;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.services.MessageManagerService;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageManagerServiceImpl implements MessageManagerService {

    private final MessageListModel listModel;
    private final MessageSelectionModel selectionModel;

    public @Autowired MessageManagerServiceImpl(MessageListModel listModel, MessageSelectionModel selectionModel) {
        this.listModel = listModel;
        this.selectionModel = selectionModel;
    }

    @Override
    public @NotNull List<Message> getSelectedMessages() {
        return selectionModel.getSelectedMessages();
    }

    @Override
    public boolean add(@NotNull Message message) {
        return listModel.add(message);
    }

}
