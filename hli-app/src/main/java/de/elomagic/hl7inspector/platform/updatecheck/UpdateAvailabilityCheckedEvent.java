/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.updatecheck;

import org.springframework.context.ApplicationEvent;

/**
 * Event published after checked for application updates.
 */
public class UpdateAvailabilityCheckedEvent extends ApplicationEvent {

    private final transient ManifestDTO manifest;
    private final Exception exception;
    private final boolean updateAvailable;

    public UpdateAvailabilityCheckedEvent(Object source, ManifestDTO manifest, boolean updateAvailable, Exception exception) {
        super(source);

        this.manifest = manifest;
        this.updateAvailable = updateAvailable;
        this.exception = exception;
    }

    public boolean isUpdateAvailable() {
        return updateAvailable;
    }

    public ManifestDTO getManifest() {
        return manifest;
    }

    public Exception getException() {
        return exception;
    }

}
