/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import com.alee.extended.link.AbstractLinkAction;
import com.alee.extended.link.WebLink;

import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.annotation.PostConstruct;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.net.URI;
import java.text.MessageFormat;
import java.util.Set;
import java.util.TreeSet;

@Component
public class AboutDialog extends MyBaseDialog {

    private static final Logger LOGGER = LogManager.getLogger(AboutDialog.class);

    static class ArtifactLicense {
        String name;
        String license;
        String link;
    }

    @PostConstruct
    private void init() {
        getBanner().setVisible(false);

        setDialogMode(MyBaseDialog.Mode.CLOSE);
        setTitle(LocaleTool.get("about"));
        setResizable(true);

        setLayout(new BorderLayout());

        JTabbedPane tabbedPane = new JTabbedPane();
        getContentPane().add(tabbedPane, BorderLayout.CENTER);

        tabbedPane.add(LocaleTool.get("about"), new SplashscreenPanel());
        tabbedPane.add(LocaleTool.get("licences"), createLicensesPanel());

        setSize(750, 600);

        center(getOwner());
    }

    private JPanel createLicensesPanel() {
        JLabel header = UIFactory.createLabel(LocaleTool.get("powered_by_oss"));
        header.setBorder(new EmptyBorder(2, 4, 2, 4));

        FormBuilder fb = FormBuilder.createBuilder()
                .addRowSpacer()
                .add(createBoldLabel(LocaleTool.get("software")), 1, 0)
                .addRow(createBoldLabel(LocaleTool.get("licence")), 1, 0);

        loadLicenses().forEach(i -> {
            fb.add(UIFactory.createLabel(i.name), 1, 0);
            fb.addRow(createUriLabel(i.license, i.link), 1, 0);
        });

        JPanel licensePanel = fb
                .addRowSpacer()
                .build();

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(licensePanel);
        scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));

        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.add(header, BorderLayout.NORTH);
        p.add(scrollPane, BorderLayout.CENTER);

        return p;
    }

    private JLabel createBoldLabel(@Nls String text) {
        JLabel label = UIFactory.createLabel(text);
        label.setFont(label.getFont().deriveFont(Font.BOLD));

        return label;
    }

    private Set<ArtifactLicense> loadLicenses() {
        Set<ArtifactLicense> result = new TreeSet<>((o1, o2) -> StringUtils.compare(o1.name, o2.name));

        try {
            //an instance of factory that gives a document builder
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            //an instance of builder to parse the specified xml file
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(getClass().getResourceAsStream("/licenses.xml"));

            //    <dependency>
            //      <groupId>com.google.code.gson</groupId>
            //      <artifactId>gson</artifactId>
            //      <version>2.8.6</version>
            //      <licenses>
            //        <license>
            //          <name>Apache 2.0</name>
            //          <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            //          <file>apache 2.0 - license-2.0.txt</file>
            //        </license>
            //      </licenses>
            //    </dependency>

            NodeList nList = doc.getElementsByTagName("dependency");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Element element = (Element)nList.item(temp);

                String groupId = getElementValue(element,"groupId");
                String artifactId = getElementValue(element,"artifactId");
                String version = getElementValue(element,"version");

                String license = getElementValue(element,"name");
                String url = getElementValue(element,"url");

                ArtifactLicense af = new ArtifactLicense();
                af.name = MessageFormat.format("{0} : {1} : {2}", groupId, artifactId, version);
                af.license = license;
                af.link = url;

                result.add(af);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return result;
    }

    private String getElementValue(Element element, String name) {
        NodeList list = element.getElementsByTagName(name);

        return list.getLength() == 0 ? "": list.item(0).getTextContent();
    }

    @NotNull
    private WebLink createUriLabel(@NonNls @NotNull String text, @NotNull String uri) {
        return new WebLink(text, new AbstractLinkAction() {
            @Override
            public void linkExecuted(ActionEvent event) {
                try {
                    URI u = new URI(uri);
                    if (uri.indexOf('@') == -1) {
                        java.awt.Desktop.getDesktop().browse(u);
                    } else {
                        Desktop.getDesktop().mail(u);
                    }
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                    SimpleDialog.error(ex);
                }
            }
        });
    }

}
