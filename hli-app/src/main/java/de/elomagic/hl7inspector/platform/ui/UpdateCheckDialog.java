/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui;

import com.alee.extended.link.UrlLinkAction;
import com.alee.extended.link.WebLink;
import com.alee.managers.style.StyleId;

import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.ui.components.MyBaseDialog;
import de.elomagic.hl7inspector.platform.updatecheck.CheckUpdateAvailabilityEvent;
import de.elomagic.hl7inspector.platform.updatecheck.UpdateAvailabilityCheckedEvent;
import de.elomagic.hl7inspector.platform.utils.LocaleTool;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.Font;

@Component
public class UpdateCheckDialog extends MyBaseDialog {

    private final JLabel lblText = new JLabel(LocaleTool.get("looking_for_updates_on_channel_1arg", ApplicationProperties.getChannel()));
    private final JProgressBar pbCheck = new JProgressBar();
    private final JLabel lbResult = new JLabel();
    private final WebLink lbLink = new WebLink(StyleId.link);
    private final JButton btClose = new JButton(LocaleTool.get("close"));

    @PostConstruct
    private void initUI() {
        getBanner().setVisible(false);
        getButtonPane().setVisible(false);

        setTitle(LocaleTool.get("update_check"));
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        setLayout(new BorderLayout(4, 4));

        lblText.setHorizontalAlignment(SwingConstants.CENTER);

        pbCheck.setIndeterminate(true);

        lbResult.setHorizontalAlignment(SwingConstants.CENTER);
        lbResult.setFont(lbResult.getFont().deriveFont(Font.BOLD));
        lbResult.setVisible(false);

        lbLink.setHorizontalAlignment(SwingConstants.CENTER);
        lbLink.setVisible(false);

        btClose.addActionListener(e -> setVisible(false));

        JPanel panel = FormBuilder.createBuilder()
                .addRow(lblText, 1, 1)
                .addRow(pbCheck, 1, 1)
                .addRow(lbResult, 1, 1)
                .addRow(lbLink, 1, 1)
                .addRow(btClose, 1, 1)
                .build();

        getContentPane().add(panel, BorderLayout.CENTER);

        setSize(500, 300);
        center(getOwner());
    }

    public void reset() {
        lbLink.setVisible(false);
        lbResult.setVisible(false);
    }

    public void setLink(String uri) {
        while (!lbLink.getActions().isEmpty()) {
            lbLink.removeAction(lbLink.getActions().get(0));
        }

        lbLink.setVisible(true);
        lbLink.addAction(new UrlLinkAction(uri));
    }

    public void setProgressText(String text) {
        pbCheck.setVisible(false);
        lbResult.setText(text);
        lbResult.setVisible(true);
    }

    @EventListener
    public void handleCheckUpdateAvailabilityEvent(@NotNull CheckUpdateAvailabilityEvent e) {
        SwingUtilities.invokeLater(() -> {
            reset();
            setVisible(true);
        });
    }

    @EventListener
    public void handleUpdateAvailabilityCheckedEvent(@NotNull UpdateAvailabilityCheckedEvent e) {
        SwingUtilities.invokeLater(() -> {
            String text;
            if (e.isUpdateAvailable()) {
                text = LocaleTool.get("a_new_version_available");
                setLink(ApplicationProperties.getHomepageUrl());
            } else {
                text = LocaleTool.get("no_update_available_in_channel_1arg", ApplicationProperties.getChannel());
            }

            setProgressText(text);
            //setVisible(true);
        });
    }

}
