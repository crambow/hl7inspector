/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.events;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationEvent;

/**
 * Fired after when application was successful initialized by the main class.
 *
 * @see org.springframework.boot.context.event.ApplicationStartedEvent
 */
public class ApplicationStartedEvent extends ApplicationEvent {

    private final String[] args;
    private final boolean keepMessages;

    public ApplicationStartedEvent(@NotNull Object source, @NotNull String[] args) {
        super(source);
        this.args = args;
        this.keepMessages = false;
    }

    public ApplicationStartedEvent(@NotNull Object source, @NotNull String[] args, boolean keepMessages) {
        super(source);
        this.args = args;
        this.keepMessages = keepMessages;
    }

    public final String[] getArgs() {
        return this.args;
    }

    public boolean isKeepMessages() {
        return keepMessages;
    }
}
