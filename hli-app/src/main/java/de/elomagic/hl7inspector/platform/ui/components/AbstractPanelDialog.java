/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.ui.components;

import de.elomagic.hl7inspector.shared.HL7Exception;
import de.elomagic.hl7inspector.platform.ui.bindings.FormBinder;
import de.elomagic.hl7inspector.platform.ui.bindings.FormDialog;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.BeanFactory;

import javax.annotation.PostConstruct;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractPanelDialog<T> extends FormDialog<T> {

    private final List<AbstractBannerPanel<T>> panelList = new ArrayList<>();
    private final transient BeanFactory beanFactory;

    protected AbstractBannerPanel<T> selectedPanel = null;

    protected AbstractPanelDialog(BeanFactory beanFactory, String title, boolean modal) {
        super(title, modal);
        this.beanFactory = beanFactory;
    }

    public void setSelected(AbstractBannerPanel<T> optionPanel) {
        if(!optionPanel.equals(selectedPanel)) {
            for(int i = 0; i < getContentPane().getComponentCount(); i++) {
                if(getContentPane().getComponent(i) instanceof AbstractBannerPanel) {
                    getContentPane().remove(i);
                }
            }

            selectedPanel = optionPanel;

            getContentPane().add(selectedPanel, BorderLayout.CENTER);

            selectedPanel.revalidate();
            selectedPanel.repaint();
        }
    }

    @PostConstruct
    private void init() {
        getBanner().setVisible(false);

        ButtonBar buttonBar = new ButtonBar();

        for(Class<? extends AbstractBannerPanel<T>> clazz : getPanelClasses()) {
            AbstractBannerPanel<T> p = beanFactory.getBean(clazz);
            panelList.add(p);
            buttonBar.addItem(p.getTitle(), p.getDescription(), p.getIcon(), e -> setSelected(p));
        }

        buttonBar.setPreferredSize(new Dimension(80, 10));

        getContentPane().add(buttonBar, BorderLayout.WEST);

        if(!panelList.isEmpty()) {
            setSelected(panelList.get(0));
        }
    }

    protected abstract List<Class<? extends AbstractBannerPanel<T>>> getPanelClasses();

    /**
     *
     * @param clazz CLass to lookup for
     * @param <P>
     * @return Returns the instance of given class
     * @throws HL7Exception Thrown when given class not found
     */
    @NotNull
    public <P extends AbstractBannerPanel<T>> P getPanel(@NotNull Class<? extends P> clazz) throws HL7Exception {
        return (P)panelList.stream()
                .filter(p -> p.getClass().isAssignableFrom(clazz))
                .findFirst()
                .orElseThrow(() -> new HL7Exception("A instance of class " + clazz.getName() + " not found in the list." ));
    }

    public void bindBean(@NotNull T bean) {
        panelList.forEach(p -> p.bindBean(bean));
    }

    public void commit() {
        panelList.forEach(FormBinder::commit);
    }

}
