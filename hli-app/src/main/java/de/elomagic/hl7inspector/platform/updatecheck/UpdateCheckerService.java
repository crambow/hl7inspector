/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.updatecheck;

import de.elomagic.hl7inspector.platform.ApplicationProperties;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import de.elomagic.hl7inspector.platform.io.gson.GsonTool;
import de.elomagic.hl7inspector.shared.configuration.SharedConfiguration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class UpdateCheckerService {

    private static final Logger LOGGER = LogManager.getLogger(UpdateCheckerService.class);

    private final SharedConfiguration configuration = SharedConfiguration.getInstance();

    private final EventMessengerProcessor eventMessengerProcessor;

    public @Autowired UpdateCheckerService(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    /**
     * Check update period.
     *
     * @return Returns true when next check is reached
     * @throws ParseException Thrown when unable to parse the result
     */
    public boolean nextCheckReached() throws ParseException {
        try {
            Calendar lc = configuration.getLastUpdateCheck();

            boolean doCheck = configuration.getAutoUpdatePeriod() >= 0;

            if (doCheck) {
                lc.add(Calendar.DAY_OF_MONTH, configuration.getAutoUpdatePeriod());

                Calendar n = Calendar.getInstance();
                n.setTime(new Date());
                n.set(Calendar.HOUR_OF_DAY, lc.get(Calendar.HOUR_OF_DAY));
                n.set(Calendar.MINUTE, lc.get(Calendar.MINUTE));
                n.set(Calendar.SECOND, lc.get(Calendar.SECOND));
                n.set(Calendar.MILLISECOND, lc.get(Calendar.MILLISECOND));

                return lc.before(n) || lc.equals(n);
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    /**
     *
     * @param manifest Manifest to check against running application
     * @return Returns true when given manifest is fresher then the this running instance
     */
    private boolean checkUpdates(@NotNull ManifestDTO manifest) {
        String currentVersion = ApplicationProperties.getVersion();

        if (ApplicationProperties.isMasterRelease()) {
            return currentVersion.compareTo(manifest.getVersion()) < 0;
        } else {
            // When SNAPSHOT then we have to compare build date because both version can be the same SNAPSHOT version.
            return manifest.getDate().after(Date.from(ApplicationProperties.getBuildDate().toInstant()));
        }
    }

    @Async
    public Future<UpdateAvailabilityCheckedEvent> checkAsync() {
        return new AsyncResult<>(process());
    }

    private UpdateAvailabilityCheckedEvent process() {
        eventMessengerProcessor.publishEvent(new CheckUpdateAvailabilityEvent(this));

        UpdateAvailabilityCheckedEvent e;
        ManifestDTO manifestDTO = null;
        Exception ex = null;
        boolean updateAvailable = false;
        try {
            manifestDTO = downloadManifest();
            updateAvailable = checkUpdates(manifestDTO);
            configuration.setLastUpdateCheck(Calendar.getInstance());
        } catch (Exception exx) {
            ex = exx;
            LOGGER.error(ex.getMessage(), ex);
        } finally {
            e = new UpdateAvailabilityCheckedEvent(this, manifestDTO, updateAvailable, ex);
        }

        eventMessengerProcessor.publishEvent(e);

        return e;
    }

    private ManifestDTO downloadManifest() throws IOException, URISyntaxException {
        try {
            String surl = ApplicationProperties.isMasterRelease() ?  ApplicationProperties.getHomepageManifestUrl() : ApplicationProperties.getHomepageManifestSnapshotUrl();

            URL url = new URL(surl);
            LOGGER.debug("Downloading manifest on {}", url);

            HttpURLConnection uc;
            HttpURLConnection.setFollowRedirects(true);

            switch (configuration.getProxyMode()) {
                case 1:
                    ProxySelector ps = ProxySelector.getDefault();
                    List<Proxy> l = ps.select(url.toURI());

                    if (l.isEmpty()) {
                        uc = (HttpURLConnection) url.openConnection();
                    } else {
                        Proxy proxy = l.get(0);
                        uc = (HttpURLConnection) url.openConnection(proxy);
                    }

                    break;

                case 2:
                    InetSocketAddress isa = new InetSocketAddress(configuration.getProxyHost(), configuration.getProxyPort());
                    uc = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, isa));
                    break;

                default:
                    uc = (HttpURLConnection) url.openConnection();
                    break;
            }

            uc.setConnectTimeout(10000);
            uc.setReadTimeout(10000);

            LOGGER.trace("HTTP response code: {}", uc.getResponseCode());

            if(uc.getResponseCode() > 299) {
                throw new IOException("Unexpected HTTP return code " + uc.getResponseCode() + " with message '" + uc.getResponseMessage() + "' on " + url);
            }

            try (InputStream in = uc.getInputStream()) {
                return GsonTool.read(in, ManifestDTO.class);
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }

}
