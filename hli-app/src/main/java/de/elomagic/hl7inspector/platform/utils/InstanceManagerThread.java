/*
 * HL7 Inspector
 *
 * Copyright © 2000-present Carsten Rambow (hl7inspector.dev@elomagic.de)
 *
 * This file is part of HL7 Inspector.
 *
 * HL7 Inspector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HL7 Inspector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HL7 Inspector. If not, see <https://www.gnu.org/licenses/>.
 */
package de.elomagic.hl7inspector.platform.utils;

import de.elomagic.hl7inspector.platform.events.ApplicationStartedEvent;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Scope("prototype")
public class InstanceManagerThread extends Thread {

    private static final Logger LOGGER = LogManager.getLogger(InstanceManagerThread.class);
    private final AtomicBoolean terminating = new AtomicBoolean(false);
    private static final String APP_GUID = "1ddf4bc0-4977-11da-8cd6-0800200c9a66";
    private static final int APP_INSTANCE_PORT = 49153;

    private final EventMessengerProcessor eventMessengerProcessor;

    private Window window;


    public @Autowired InstanceManagerThread(EventMessengerProcessor eventMessengerProcessor) {
        this.eventMessengerProcessor = eventMessengerProcessor;
    }

    @Override
    public void run() {
        try {
            ServerSocket ss = new ServerSocket(APP_INSTANCE_PORT);

            while(!terminating.get()) {
                try {
                    try (Socket s = ss.accept()) {
                        s.setSoTimeout(1000);
                        try (OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
                                InputStreamReader in = new InputStreamReader(s.getInputStream());
                                BufferedReader bin = new BufferedReader(in)) {
                            String guid = bin.readLine();

                            if(APP_GUID.equals(guid)) {
                                out.write(APP_GUID.concat("\n"));
                                out.flush();

                                //Toolkit.getDefaultToolkit().
                                window.toFront();
                                /* int paramtCount = */
                                bin.read();

                                String fn = bin.readLine();
                                eventMessengerProcessor.publishEvent(new ApplicationStartedEvent(this, new String[] {fn}, true));
                            }
                        }
                    }
                } catch(Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void requestTermination() {
        terminating.set(true);
    }

    public static boolean lookupInstance(String[] args) {
        boolean result = false;
        try {
            try {
                try (Socket s = new Socket("localhost", APP_INSTANCE_PORT)) {
                    s.setSoTimeout(1000);
                    try (OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
                         InputStreamReader in = new InputStreamReader(s.getInputStream());
                         BufferedReader bin = new BufferedReader(in)) {
                        out.write(APP_GUID.concat("\n"));
                        out.flush();

                        String guid = bin.readLine();

                        if(APP_GUID.equals(guid)) {
                            out.write(args.length != 0 ? 1 : 0);

                            if(args.length != 0) {
                                out.write(args[0].concat("\n"));
                            }

                            out.flush();

                            System.exit(0);
                        }
                    }
                }
            } catch(ConnectException e) {
                //
            }
        } catch(Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public void setMainWindow(Window window) {
        this.window = window;
    }
}
