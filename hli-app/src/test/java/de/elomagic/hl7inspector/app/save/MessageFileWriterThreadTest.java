package de.elomagic.hl7inspector.app.save;

import de.elomagic.hl7inspector.TestTool;
import de.elomagic.hl7inspector.app.MessageWriterBean;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.io.MessageFileIO;
import de.elomagic.hl7inspector.shared.configuration.Frame;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;

class MessageFileWriterThreadTest {

    @Test
    void testRunManyFiles() throws Exception {

        List<Message> messages = List.of(
                TestTool.readMessage("/A01-UTF8-CR.hl7"),
                TestTool.readMessage("/A02-UTF8-CR.hl7"),
                TestTool.readMessage("/A08-UTF8-CR.hl7")
        );

        MessageWriterBean mwb = new MessageWriterBean();
        mwb.setDestinationFolder(TestTool.getTempTestPath());
        mwb.setGenerateSemaphore(false);

        MessageFileWriterThread thread = new MessageFileWriterThread(messages, mwb);

        thread.run();

        Message message1 = MessageFileIO.readMessage(getFile(0), StandardCharsets.UTF_8);
        Message message2 = MessageFileIO.readMessage(getFile(1), StandardCharsets.UTF_8);
        Message message3 = MessageFileIO.readMessage(getFile(2), StandardCharsets.UTF_8);

        Assertions.assertEquals("A01", message1.getFirstSegment("EVN").get(1).asText());
        Assertions.assertEquals("A02", message2.getFirstSegment("EVN").get(1).asText());
        Assertions.assertEquals("A08", message3.getFirstSegment("EVN").get(1).asText());
    }

    @Test
    void testRunSingleFile() throws Exception {

        Path file = TestTool.getTempTestPath().resolve("messages.txt");
        file.toFile().deleteOnExit();

        List<Message> messages = List.of(
                TestTool.readMessage("/A01-UTF8-CR.hl7"),
                TestTool.readMessage("/A02-UTF8-CR.hl7"),
                TestTool.readMessage("/A08-UTF8-CR.hl7")
        );

        MessageWriterBean mwb = new MessageWriterBean();
        mwb.setSingleFileName(file);
        mwb.setManyFiles(false);

        MessageFileWriterThread thread = new MessageFileWriterThread(messages, mwb);

        thread.run();

        byte[] data = FileUtils.readFileToByteArray(file.toFile());

        Assertions.assertEquals(950, data.length);
        Assertions.assertEquals(Frame.DEFAULT_START, data[0]);
        Assertions.assertEquals(Frame.DEFAULT_STOP1, data[data.length-2]);
        Assertions.assertEquals(Frame.DEFAULT_STOP2, data[data.length-1]);
    }

    private Path getFile(int index) throws IOException {
        String filename = Integer.toString(index);

        filename = StringUtils.leftPad(filename, 1, "0") + ".hl7";

        Path file = TestTool.getTempTestPath().resolve(filename);
        file.toFile().deleteOnExit();

        return TestTool.getTempTestPath().resolve(filename);
    }

}