/*
 * Copyright 2011-present Carsten Rambow
 *
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/gpl.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector.app.profiles;

import de.elomagic.hl7inspector.shared.profiles.model.DataElement;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;
import de.elomagic.hl7inspector.shared.profiles.model.SegmentItem;

import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = { ProfileServiceImpl.class, EventMessengerProcessor.class})
class ProfileServiceImplTest {

    @Autowired
    private ProfileServiceImpl profileService;

    public File createTempFile() throws IOException  {
        File file = File.createTempFile("HL7I_TEST_", ".hip");
        file.deleteOnExit();

        Files.copy(getProfileStream(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);

        return file;
    }

    private InputStream getProfileStream() {
        return getClass().getResourceAsStream("/Sample Profile.hip");
    }

    @Test
    void testLoad() throws Exception {
        System.out.println("load");
        Profile result = profileService.getProfile(createTempFile().toPath());
        assertEquals("Sample Profile", result.getName());
        assertEquals("Sample profile based on HL7 Version 2.5", result.getDescription());

        assertEquals(25, result.getDataElementList().size());
        assertEquals(3, result.getSegmentList().size());
        assertEquals(36, result.getDataTypeList().size());
        assertEquals(129, result.getTableDataList().size());
    }

    @Test
    void testSave() throws Exception {
        System.out.println("save");
        
        DataElement de = new DataElement("1", "CE", "DESC", 1, "1234");
        de.setSegment("PID");                
        de.setSequence(3);

        // Prepate test
        Profile profile = new Profile();
        profile.setName("NAME");
        profile.setDescription("DESC");
        profile.getDataElementList().add(de);
        profile.getSegmentList().add(new SegmentItem("MSH", "DESC-SEG", "1"));

        File file = File.createTempFile("HL7I_TEST_", ".hip");
        file.deleteOnExit();

        // Do test
        profileService.save(file.toPath(), profile);

        // Check results
        Profile p = profileService.getProfile(file.toPath());

        assertEquals("NAME", p.getName());
        assertEquals("DESC", p.getDescription());
        assertEquals("DESC-SEG", p.getSegment("MSH").getDescription());
        assertEquals("DESC", p.getDataElement("PID", 3).getName());
    }

}
