package de.elomagic.hl7inspector.app.profiles.validate;

import de.elomagic.hl7inspector.shared.hl7.model.Subcomponent;
import de.elomagic.hl7inspector.shared.profiles.model.Profile;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ValidatorServiceTest {

    private final ValidatorService service = new ValidatorService(null,null);

    Subcomponent createValue(String value) {
        return new Subcomponent(value);
    }

    @Test
    void testValidateDate() {
        Assertions.assertFalse(service.validateDate(new Profile(), createValue("0.1234")).isEmpty());
        Assertions.assertFalse(service.validateDate(new Profile(), createValue("20200231")).isEmpty());
        Assertions.assertFalse(service.validateDate(new Profile(), createValue("2019021212")).isEmpty());
        Assertions.assertFalse(service.validateDate(new Profile(), createValue("194013")).isEmpty());
        Assertions.assertTrue(service.validateDate(new Profile(), createValue("19400203")).isEmpty());
        Assertions.assertTrue(service.validateDate(new Profile(), createValue("194002")).isEmpty());
        Assertions.assertTrue(service.validateDate(new Profile(), createValue("1940")).isEmpty());
    }

    @Test
    void testValidateDateTime() {
        Assertions.assertFalse(service.validateDateTime(new Profile(), createValue("0.1234")).isEmpty());
        Assertions.assertFalse(service.validateDateTime(new Profile(), createValue("20200231")).isEmpty());
        Assertions.assertFalse(service.validateDateTime(new Profile(), createValue("194013")).isEmpty());
        Assertions.assertFalse(service.validateDateTime(new Profile(), createValue("1940131")).isEmpty());
        Assertions.assertTrue(service.validateDateTime(new Profile(), createValue("2019021212")).isEmpty());
        Assertions.assertTrue(service.validateDateTime(new Profile(), createValue("19400203")).isEmpty());
        Assertions.assertTrue(service.validateDateTime(new Profile(), createValue("194002")).isEmpty());
        Assertions.assertTrue(service.validateDateTime(new Profile(), createValue("1940")).isEmpty());
    }

    @Test
    void validateFormattedTextData() {
    }

    @Test
    void validateGeneralTimingSpecification() {
    }

    @Test
    void validateCodedValueForHL7() {
    }

    @Test
    void validateCodedValueForUser() {
    }

    @Test
    void testValidateNumeric() {
        Assertions.assertTrue(service.validateNumeric(new Profile(), createValue("0.1234")).isEmpty());
    }

    @Test
    void validateSequenceId() {
    }

    @Test
    void validateStringData() {
    }

    @Test
    void validateTime() {
        Assertions.assertFalse(service.validateTime(new Profile(), createValue("0.1234")).isEmpty());
        Assertions.assertFalse(service.validateTime(new Profile(), createValue("1940131")).isEmpty());
        Assertions.assertFalse(service.validateTime(new Profile(), createValue("2359590")).isEmpty());
        Assertions.assertFalse(service.validateTime(new Profile(), createValue("0506070200-2400")).isEmpty());
        Assertions.assertFalse(service.validateTime(new Profile(), createValue("0506070200")).isEmpty());
        Assertions.assertTrue(service.validateTime(new Profile(), createValue("000000")).isEmpty());
        Assertions.assertTrue(service.validateTime(new Profile(), createValue("00")).isEmpty());
        Assertions.assertTrue(service.validateTime(new Profile(), createValue("050607.012")).isEmpty());
        Assertions.assertTrue(service.validateTime(new Profile(), createValue("050607.012-0200")).isEmpty());
        Assertions.assertTrue(service.validateTime(new Profile(), createValue("050607.012+0100")).isEmpty());
    }

    @Test
    void validateTextData() {
    }


}