/*
 * Copyright 2011-present Carsten Rambow
 * 
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.gnu.org/licenses/gpl.txt
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector.app.load;

import de.elomagic.hl7inspector.TestTool;
import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.configuration.Frame;
import de.elomagic.hl7inspector.shared.configuration.ImportOptions.StreamFormat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

class MessageParserStreamReaderTest {

    @Test
    void testReadMessage() throws Exception {
        Message result = createMessageReader("/NonDefaultDelimiters.HL7", StandardCharsets.ISO_8859_1).readMessage();
        Assertions.assertEquals(5, result.getChildCount());

        result = createMessageReader("/A08-UTF8-CR.hl7", StandardCharsets.UTF_8).readMessage();
        Assertions.assertEquals(7, result.getChildCount());

        MessageFileInputStream reader = createMessageReader("/Logging Sample 1.txt", StandardCharsets.UTF_8);
        Assertions.assertEquals(4, reader.readMessage().size());
        Assertions.assertEquals(4, reader.readMessage().size());
        Assertions.assertEquals(4, reader.readMessage().size());
        Assertions.assertNull(reader.readMessage());
    }

    @Test
    void testGetBytesRead() throws IOException {
        System.out.println("getBytesRead");

        MessageFileInputStream reader = createMessageReader("/NonDefaultDelimiters.HL7", StandardCharsets.UTF_8);

        reader.readMessage();
        Assertions.assertTrue(420 < reader.getBytesRead());
    }

    private MessageFileInputStream createMessageReader(String resource, Charset charset) throws IOException {
        String msg = TestTool.readResource(resource, charset);
        ByteArrayInputStream in = new ByteArrayInputStream(msg.getBytes(charset));

        return new MessageFileInputStream(in, charset, StreamFormat.AUTO_DETECT, new Frame());
    }

}
