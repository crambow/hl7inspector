/*
 * Copyright 2020-present Carsten Rambow
 *
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/gpl.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector;

import de.elomagic.hl7inspector.platform.ApplicationProperties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;

class ApplicationPropertiesTest {

    @Test
    void testGetVersion() {
        String url = ApplicationProperties.getVersion();
        assertTrue(url.startsWith("${pom.version}"), "URL \"" + url + "\" doesn't start with \"${pom.version}\"");
    }

    @Test
    void testGetBuildDate() {
        ZonedDateTime time = ApplicationProperties.getBuildDate();
        assertEquals(2020, time.getYear());
        assertEquals(04, time.getHour());
    }

    @Test
    void testGetHomepageManifestUrl() {
         String url = ApplicationProperties.getHomepageManifestUrl();
         assertEquals("http://localhost:9870/manifest.json", url);
    }

    @Test
    void testGetHomepageManifestSnapshotUrl() {
        String url = ApplicationProperties.getHomepageManifestSnapshotUrl();
        assertEquals("http://localhost:9870/manifest-snapshot.json", url);
    }


}