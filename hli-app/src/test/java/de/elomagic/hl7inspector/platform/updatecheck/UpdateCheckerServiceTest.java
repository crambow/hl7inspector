/*
 * Copyright 2020-present Carsten Rambow
 *
 * Licensed under the GNU Public License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/gpl.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.hl7inspector.platform.updatecheck;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import de.elomagic.hl7inspector.HttpsWiremockConfigFactory;
import de.elomagic.hl7inspector.TestTool;
import de.elomagic.hl7inspector.platform.events.EventMessengerProcessor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {UpdateCheckerService.class, EventMessengerProcessor.class})
class UpdateCheckerServiceTest {

    private WireMockServer server;

    @Autowired
    private BeanFactory beanFactory;

    @BeforeEach
    public void before() throws Exception {
        server = new WireMockServer(HttpsWiremockConfigFactory.create()); //No-args constructor will start on port 8080, no HTTPS
        server.stubFor(WireMock.get(WireMock.urlPathEqualTo("/manifest.json"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withBody(TestTool.readResource("/manifest.json"))));

        server.start();
    }

    @AfterEach
    public void after() {
        server.stop();
    }

    @Test
    void testThread() throws Exception {

        UpdateCheckerService service = beanFactory.getBean(UpdateCheckerService.class);
        Future<UpdateAvailabilityCheckedEvent> future =  service.checkAsync();

        UpdateAvailabilityCheckedEvent e = future.get();

        ManifestDTO manifest = e.getManifest();
        assertEquals("2.3.0.56-SNAPSHOT", manifest.getVersion());
        assertEquals("https://anywhere.com", manifest.getUrl());
        assertEquals("2020-05-27", new SimpleDateFormat("yyyy-MM-dd").format(manifest.getDate()));
    }

}
