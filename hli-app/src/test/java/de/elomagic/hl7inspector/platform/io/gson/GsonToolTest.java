package de.elomagic.hl7inspector.platform.io.gson;

import com.google.gson.Gson;

import de.elomagic.hl7inspector.shared.hl7.model.Message;
import de.elomagic.hl7inspector.shared.hl7.model.MessageMETA;
import de.elomagic.hl7inspector.shared.profiles.model.ValidateMapper;
import de.elomagic.hl7inspector.shared.profiles.model.ValidateStatus;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

class GsonToolTest {

    @Test
    void readWriteArray() throws Exception {
        Path file = Files.createTempFile("temp_", ".json");
        file.toFile().deleteOnExit();

        Message m = new Message();
        MessageMETA meta = m.getMeta();
        meta.setFile(file);
        meta.setEncoding(StandardCharsets.UTF_8);
        meta.setSourceType(MessageMETA.SourceType.IN_MEMORY);
        meta.setModifiedTime(Files.getLastModifiedTime(file));

        MessageMETA[] metas = new MessageMETA[] {meta};

        Gson gson = GsonTool.createGsonInstance();

        String json = gson.toJson(metas);

        System.out.println(json);

        StringReader reader = new StringReader(json);
        MessageMETA[] metaRead = gson.fromJson(reader, MessageMETA[].class);

        Assertions.assertArrayEquals(metas, metaRead);


        ValidateMapper mapper = new ValidateMapper();
        Assertions.assertEquals(ValidateStatus.ERROR, mapper.getInvalidFormat());
        Assertions.assertEquals(ValidateStatus.INFO, mapper.getMapConditional());

        mapper.setInvalidFormat(ValidateStatus.OK);
        mapper.setMapConditional(ValidateStatus.WARN);

        json = gson.toJson(mapper);

        System.out.println(json);

        reader = new StringReader(json);
        mapper = gson.fromJson(reader, ValidateMapper.class);

        Assertions.assertEquals(ValidateStatus.OK, mapper.getInvalidFormat());
        Assertions.assertEquals(ValidateStatus.WARN, mapper.getMapConditional());
    }

}