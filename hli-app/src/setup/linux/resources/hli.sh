#!/bin/bash

APP_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Create desktop file
if [ ! -f ~/.local/share/applications/hl7inspector.desktop ]; then
    echo "Create desktop file"
    sed 's|#APP_FOLDER#|'$APP_HOME'|g' hl7inspector-template.desktop > ~/.local/share/applications/hl7inspector.desktop
fi

JAVACMD=$APP_HOME/java/bin/java
CMD_LINE_ARGS=%*

$JAVACMD \
    -Dlog4j.configurationFile="$APP_HOME/conf/log4j2.xml" \
    -jar "$APP_HOME/libs/${project.build.finalName}.jar" \
    $CMD_LINE_ARGS

