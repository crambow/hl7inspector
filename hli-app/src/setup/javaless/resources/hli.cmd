@echo off

@setlocal ENABLEEXTENSIONS
@cd %~dp0

set "JAVACMD=%JAVA_HOME%\bin\javaw.exe

if exist "%JAVACMD%" goto chkHome

echo The JAVA_HOME environment variable is not defined correctly >&2
echo This environment variable is needed to run this program >&2
echo NB: JAVA_HOME should point to a Java 11 >&2
goto error

:chkHome
set "APP_HOME=%~dp0"
if not "%APP_HOME%"=="" goto valHome
goto error

:valHome

:init
set CMD_LINE_ARGS=%*

:endInit

set CLASS_LAUNCHER=de.elomagic.hl7inspector.Hl7Inspector

start /b "" ^
    "%JAVACMD%" ^
    -Dlog4j.configurationFile="%APP_HOME%conf\log4j2.xml" ^
    -jar "%APP_HOME%libs\${project.build.finalName}.jar" ^
    %CLASS_LAUNCHER% %CMD_LINE_ARGS%

goto end

:error
set ERROR_CODE=1

:end
@endlocal & set ERROR_CODE=%ERROR_CODE%