#!/bin/bash

if [ -f $JAVA_HOME/bin/java ]
then
    export JAVACMD=$JAVA_HOME/bin/java
    APP_HOME=.
    CMD_LINE_ARGS=%*

    CLASS_LAUNCHER=de.elomagic.hl7inspector.Hl7Inspector

    $JAVACMD \
        -Dlog4j.configurationFile="$APP_HOME/conf/log4j2.xml" \
        -jar "$APP_HOME/libs/${project.build.finalName}.jar" \
        $CLASS_LAUNCHER $CMD_LINE_ARGS
else
    echo No Java 11 found.
    echo Please install Java 11 at first and then try again.
fi

