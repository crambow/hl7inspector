# HL7 Inspector Setup Script
# (c)2010-present Carsten Rambow, Karlsruhe, Germany
# Requires NSIS V3.05 or higher

#!addincludedir $%NSIS_INCLUDE%
#!addplugindir $%NSIS_PLUGINS%

# --------------------------------
# General
#!define APP_VERSION "1.0.0.9" < Must be set external
!define PROJECT_FOLDER "..\..\..\.."

!define APP_NAME "HL7 Inspector"
!define APP_MANUFACTOR "elomagic"
!define APP_REGKEY "SOFTWARE\${APP_NAME}"
!define APP_REGKEY_UNINSTALL "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}"
!define APP_UNINSTALL "Uninstall ${APP_NAME} (64-bit).exe"
!define APP_START_MENU "$SMPROGRAMS\${APP_NAME}"

!include x64.nsh
!include "MUI2.nsh"

# Name and file
Name "${APP_NAME} ${APP_VERSION}"
OutFile "${PROJECT_FOLDER}\target\${APP_NAME} ${APP_VERSION}-x64 Setup.exe"

# Default installation folder
InstallDir "$PROGRAMFILES64\${APP_NAME}"

# Doesn't work with 64bit app. Use ".init" instead # Get installation folder from registry if available
#InstallDirRegKey HKLM64 "${APP_REGKEY}" ""

# Request application privileges for Microsoft Windows
RequestExecutionLevel admin

# --------------------------------
# Interface Settings

!define MUI_ABORTWARNING
!define MUI_ICON "${PROJECT_FOLDER}\src\setup\win\resources\application.ico"
!define MUI_UNICON "${PROJECT_FOLDER}\src\setup\win\resources\application.ico"
!define MUI_FINISHPAGE_RUN "$INSTDIR\hl7inspector.exe"

# --------------------------------
# Pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "${PROJECT_FOLDER}\src\setup\src\licenses\license-gpl.txt"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES


# --------------------------------
# Languages
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "German"

!macro RemoveAll32Bit
    ReadRegStr $0 HKLM32 "SOFTWARE\elomagic\HL7 Inspector" ""

    ${IfNot} $0 == ""
        DetailPrint "Remove 32bit installation"

        # Remove Start Menu links, if any
        Delete "${APP_START_MENU}\*.*"
        RMDir "${APP_START_MENU}"

        # Remove desktop shortcuts
        Delete "$DESKTOP\HL7 Inspector.lnk"

        # Remove Unistaller
        Delete "$0\Uninstall HL7 Inspector (32-bit).exe"

        # Remove directory
        RMDir /r "$0"

        DeleteRegKey /ifempty HKLM32 "SOFTWARE\elomagic\HL7 Inspector"
        DeleteRegKey /ifempty HKLM32 "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\elomagicHL7 Inspector"
    ${EndIf}
!macroend

!macro RemoveAll
    # Remove Start Menu links, if any
    Delete "${APP_START_MENU}\*.*"
    RMDir "${APP_START_MENU}"

    # Remove desktop shortcuts
    Delete "$DESKTOP\${APP_NAME}.lnk"

    # Remove directories used
    Delete "$INSTDIR\${APP_UNINSTALL}"

    # Remove directory
    RMDir /r "$INSTDIR"

    # Remove log files
    RMDir /r "$APPDATA\hl7inspector"

    DeleteRegKey /ifempty HKLM64 "${APP_REGKEY}"
    DeleteRegKey /ifempty HKLM64 "${APP_REGKEY_UNINSTALL}"
!macroend

Icon "${PROJECT_FOLDER}\src\setup\win\resources\application.ico"
ShowInstDetails show

#${StrContains} $0 "-" "${APP_VERSION}"
#StrCmp $0 "" noversion
VIProductVersion ${APP_VERSION_PATCHED}
VIAddVersionKey ProductName "${APP_NAME}"
VIAddVersionKey ProductVersion "${APP_VERSION}"
#VIAddVersionKey CompanyName "${COMPANY}"
VIAddVersionKey CompanyWebsite "https://bitbucket.org/crambow/hl7inspector"
VIAddVersionKey FileVersion "${APP_VERSION}"
VIAddVersionKey FileFlag "${APP_VERSION}"
#VIAddVersionKey FileDescription ""
VIAddVersionKey LegalCopyright "Copyright 2020-present Carsten Rambow, Karlsruhe, Germany"
#noversion:

;--------------------------------
;Installer Sections

Section "${APP_NAME} ${APP_VERSION}" SecDummy
    SectionIn RO
    SetOutPath "$INSTDIR"

    # !insertmacro RemoveAll32Bit
    !insertmacro RemoveAll

    # ADD YOUR OWN FILES HERE...
    File /r "${PROJECT_FOLDER}\target\distribution\win-setup\*.*"

    # Create program data folder
    CreateDirectory "$APPDATA\hl7inspector\logs"

    # Create shortcuts
    CreateDirectory "$SMPROGRAMS\${APP_NAME}"

    CreateShortCut "${APP_START_MENU}\Uninstall ${APP_NAME} (64-bit).lnk" "$INSTDIR\${APP_UNINSTALL}" "" "$INSTDIR\${APP_UNINSTALL}" 0
    CreateShortCut "${APP_START_MENU}\${APP_NAME} (64-bit).lnk" "$INSTDIR\hl7inspector.exe" "" "$INSTDIR\application.ico" 0
    CreateShortCut "${APP_START_MENU}\Samples.lnk" "$INSTDIR\samples" "" "$INSTDIR\samples"

    CreateShortCut "$DESKTOP\${APP_NAME}.lnk" "$INSTDIR\hl7inspector.exe" "" "$INSTDIR\application.ico" 0

    # Create uninstaller
    WriteUninstaller "$INSTDIR\${APP_UNINSTALL}"

    # Store installation folder
    WriteRegStr HKLM64 "${APP_REGKEY}" "" $INSTDIR

    WriteRegStr HKLM64 "${APP_REGKEY_UNINSTALL}" "DisplayName" "${APP_NAME} (64-bit)"
    WriteRegStr HKLM64 "${APP_REGKEY_UNINSTALL}" "DisplayVersion" "${APP_VERSION}"
    WriteRegStr HKLM64 "${APP_REGKEY_UNINSTALL}" "Version" "${APP_VERSION}"
    WriteRegStr HKLM64 "${APP_REGKEY_UNINSTALL}" "UninstallString" '"$INSTDIR\${APP_UNINSTALL}"'
    WriteRegDWORD HKLM64 "${APP_REGKEY_UNINSTALL}" "NoModify" 1
    WriteRegDWORD HKLM64 "${APP_REGKEY_UNINSTALL}" "NoRepair" 1
    WriteRegStr HKLM64 "${APP_REGKEY_UNINSTALL}" "InstallLocation" "$INSTDIR"
    WriteRegStr HKLM64 "${APP_REGKEY_UNINSTALL}" "Readme" "https://bitbucket.org/crambow/hl7inspector/src/master/README.md"
    WriteRegStr HKLM64 "${APP_REGKEY_UNINSTALL}" "URLInfoAbout " "https://bitbucket.org/crambow/hl7inspector/wiki/Home"
    WriteRegStr HKLM64 "${APP_REGKEY_UNINSTALL}" "URLUpdateInfo" "https://bitbucket.org/crambow/hl7inspector/downloads/"
SectionEnd

Section "$(NAME_SectionSample)" SectionSamples
    ReadRegStr $0 HKCU \
                 "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" \
                 Personal

    CreateDirectory "$0\HL7 Inspector"
    CopyFiles $INSTDIR\samples\*.* "$0\HL7 Inspector"
    RMDir /r "$INSTDIR\samples\*.*"
SectionEnd

# --------------------------------
# Descriptions

# Language strings
LangString DESC_SecDummy ${LANG_ENGLISH} "${APP_NAME} application files"
LangString DESC_SecDummy ${LANG_GERMAN} "${APP_NAME} Programm Dateien"
LangString NAME_SectionSample ${LANG_ENGLISH} "Samples"
LangString NAME_SectionSample ${LANG_GERMAN} "Beispiele"
LangString DESC_SectionSample ${LANG_ENGLISH} "Installs HL7 messages and profile examples in the user document directory"
LangString DESC_SectionSample ${LANG_GERMAN} "Installiert HL7-Nachrichten und Profilbeispiele im Benutzer-Dokumentenverzeichnis"

# Assign language strings to sections
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
    !insertmacro MUI_DESCRIPTION_TEXT ${SectionSamples} $(DESC_SectionSample)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"
    !insertmacro RemoveAll
SectionEnd

Function .onInit
    # Variable "APP_REGKEY" doesn't work here because it isn't initialize yet
    ReadRegStr $INSTDIR HKLM64 "SOFTWARE\${APP_NAME}" ""

    ${If} $INSTDIR == ""
         StrCpy $INSTDIR "$PROGRAMFILES64\${APP_NAME}"
    ${EndIf}
FunctionEnd

