#!/bin/bash

JAVACMD=./java/bin/java
APP_HOME=.
CMD_LINE_ARGS=%*

$JAVACMD \
    -Dlog4j.configurationFile="$APP_HOME/conf/log4j2.xml" \
    -jar "$APP_HOME/libs/${project.build.finalName}.jar" \
    $CMD_LINE_ARGS

